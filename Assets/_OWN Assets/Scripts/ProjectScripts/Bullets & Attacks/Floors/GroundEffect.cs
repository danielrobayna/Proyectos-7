﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectEnum
{
    Fire,
    Electric,
    Acid,
    Spike
}

public class GroundEffect : DamageObject
{
    public ParticleSystem m_particle;
    [SerializeField] int m_damageByTime;
    [SerializeField] float m_timeInFire;
    public EffectEnum effect;
    public float timeToDetroy;
    public float activateEachSeconds;
    float m_timer;
    GameObject m_instaPar;

    [Header("Partícula de anticipación (Suelo eléctrico)")]
    [SerializeField] private ParticleSystem anticipationParticle; //Para indicar que se va a activar el suelo eléctrico

    [Header("Collider del Objeto")]
    private Collider groundCollider;

    [Header("Luz del Suelo Ácido")]
    private Light acidGroundLight;

    [Header("Animator del Objeto")]
    private Animator groundAnimator;

    [Header("Empezar desactivado?")]
    [SerializeField] private bool startDeactivated;

    AudioSource soundEffectSource;

    void Start()
    {
        groundCollider = GetComponent<Collider>();
        groundAnimator = GetComponent<Animator>();
        if (effect == EffectEnum.Acid)
        {
            acidGroundLight = GetComponentInChildren<Light>();
        }

        if (!m_particle.isPlaying)
        {
            m_particle.Play();
        }
        if (soundEffectSource == null)
        {
            PlayFXSound();
        }
        if (startDeactivated)
        {
            DeactivateGround();
        }

    }
    private void OnEnable()
    {
        if (!m_particle.isPlaying)
        {
            m_particle.Play();
        }
    }

    private void Update()
    {
        if (timeToDetroy != 0)
        {
            timeToDetroy -= Time.deltaTime;
            if (timeToDetroy < 0)
            {
                if (groundAnimator == null)
                {
                    Destroy(gameObject);
                }
                else
                {
                    DestroyWithAnimation();
                }
            }
        }
        if (activateEachSeconds != 0 && m_particle != null)
        {
            m_timer += Time.deltaTime;

            if (groundCollider.enabled == false && anticipationParticle != null && m_timer >= activateEachSeconds * 0.6)
            {
                if (!anticipationParticle.isPlaying)
                {
                    anticipationParticle.Play();
                }
            }
            if (m_timer >= activateEachSeconds)
            {
                if (groundCollider.enabled == false)
                {
                    ActivateGround();
                }
                else
                {
                    DeactivateGround();
                }
                m_timer = 0;
            }
        }
    }

    private void DestroyWithAnimation() //En principio sólo para el suelo ácido
    {
        groundAnimator.SetTrigger("DestroyGround");
        groundCollider.enabled = false;
        if (m_particle.isPlaying)
        {
            m_particle.Stop();
        }

        ParticleSystem.MainModule main = m_particle.main;
        float timeLeft = main.startLifetimeMultiplier + main.duration;
        Destroy(gameObject, timeLeft);
    }

    private void OnTriggerStay(Collider collision)
    {
        if(effect == EffectEnum.Fire || effect == EffectEnum.Acid || effect == EffectEnum.Electric)
        {
            if(collision.GetComponent<PlayerHealthController>() != null)
            {
                CollisionWithPlayer(collision.gameObject.GetComponent<PlayerHealthController>());
            }
            if (collision.GetComponent<EnemyHealth>() != null)
            {
                CollisionWithEnemy(collision);
            }
        }
    }
    private void HideMesh()
    {
        if(effect == EffectEnum.Fire)
        {
            GetComponentInChildren<MeshRenderer>().enabled = false;
        }
    }
  
    public void ActivateGround()
    {
        groundCollider.enabled = true;
        if (!m_particle.isEmitting)
        {
            m_particle.Play();
        }
        if(soundEffectSource == null)
        {
            PlayFXSound();
        }
    }

    public void DeactivateGround()
    {
        groundCollider.enabled = false;
        if(m_particle.isEmitting)
        m_particle.Stop();
        if (soundEffectSource != null)
        {
            soundEffectSource.Stop();
        }
    }

    public void DestroyByTime(float destroyTime)
    {
        timeToDetroy = destroyTime;
    }
    
    public void ActivationByTime(float activatedEverySeconds)
    {
        activatedEverySeconds = activateEachSeconds;
    }
    override protected void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
        base.CollisionWithPlayer(playerHealth);
        if (effect == EffectEnum.Fire || effect == EffectEnum.Acid)
        {
            playerHealth.StartFireState(m_damageByTime, m_timeInFire);
        }
        if (effect == EffectEnum.Electric)
        {
            playerHealth.gameObject.GetComponent<PossessAbility>().StartElectricStun();
        }
    }
    protected override void CollisionWithEnemy(Collider enemy)
    {
        EnemyHealth enemyHealth = enemy.GetComponent<EnemyHealth>();
        if ( effect == EffectEnum.Acid)
        {
            if (enemyHealth != null && !enemyHealth.GetIsOnFire())
            {
                enemyHealth.StartFireState(m_damageByTime, m_timeInFire, false);
            }
        }
        if(effect == EffectEnum.Fire)
        {
            enemyHealth.StartFireState(m_damageByTime, m_timeInFire);
        }

        if (effect == EffectEnum.Electric && !enemy.GetComponent<ShockerSetControl>())
        {
            EnemySetControl myEnemySetControl = enemy.GetComponent<EnemySetControl>();
            if (myEnemySetControl.IsEnemyPossessed())
            {
                myEnemySetControl.UnpossessEnemy(Vector3.zero, EnemySetControl.UnposessTypes.electric);     //Desposeer al jugador por causa de electricidad
            }
        }
    }

    void PlayFXSound()
    {
        switch (effect)
        {
            case EffectEnum.Electric:
                soundEffectSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.ELECTRIC_FLOOR, 1f, 10f, this.transform);
                break;
            case EffectEnum.Fire:
                soundEffectSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.FIRE_FLOOR, this.transform);
                break;
            default:
                break;
              
        }
    }
}