﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExplosiveGrenade : Grenade
{
    [Header("Control de tiempo de explosión")]
    [SerializeField] TextMeshProUGUI timeRemainingText;
    [SerializeField] private float timeToExplode;
    [SerializeField] private float maxBounceReach;
    private bool startExplosion;

    private ContactPoint contact;
    private bool wasLastBounceVertical = false;
    private int maxNumberOfBounce = 1;

    private void Update()
    {
        timeRemainingText.text = timeToExplode.ToString("F3");

        if (timeToExplode > 0)
        {
            timeToExplode -= 1 * Time.deltaTime;
        }
        else if (timeToExplode <= 0 && !startExplosion)
        {
            timeToExplode = 0;
            StartCoroutine(ExplodeCoroutine());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 9)
        {
            maxBounceReach = 0.2f;

            wasLastBounceVertical = false;

            contact = collision.contacts[0];
            SetBounceTarget(contact.normal);

            collisioned = true;
        }

        if (!collisioned && collision.gameObject.layer != 16)
        {
            if (maxNumberOfBounce == 0)
            {
                collisioned = true;
            }

            StopCoroutine(actualGrenadeMovementCoroutine);

            contact = collision.contacts[0];

            if (contact.normal == Vector3.up)
            {
                firingAngle = 80;
                wasLastBounceVertical = true;
            }
            else
            {
                firingAngle = initialFiringAngle;
                wasLastBounceVertical = false;
            }

            SetBounceTarget(contact.normal);
            maxNumberOfBounce -= 1;
            maxBounceReach -= maxBounceReach / 2;
        }

        else if (!collisioned && collision.gameObject.layer == 16)
        {
            wasLastBounceVertical = false;
            collisioned = true;
        }
    }

    private void SetBounceTarget(Vector3 bounceNormal)
    {
        target = transform.position + (Vector3.Reflect(transform.right, bounceNormal) * maxBounceReach);

        if (maxNumberOfBounce == 0 && wasLastBounceVertical == true)
        {
            StopCoroutine(actualGrenadeMovementCoroutine);
        }

        else
        {
            target = new Vector3(target.x, 0.1f, target.z);
            actualGrenadeMovementCoroutine = StartCoroutine(Bounce());
        }
    }

    protected IEnumerator ExplodeCoroutine() //Corutina de explosión retardada para granadas normales
    {
        startExplosion = true;

        yield return new WaitForSeconds(0.3f);

        timeRemainingText.enabled = false;

        yield return new WaitForSeconds(0.1f);

        timeRemainingText.enabled = true;

        yield return new WaitForSeconds(0.1f);

        timeRemainingText.enabled = false;

        yield return new WaitForSeconds(0.1f);

        timeRemainingText.enabled = true;

        yield return new WaitForSeconds(0.1f);

        Instantiate(grenadeExplosion, transform.position, grenadeExplosion.transform.rotation);
        Destroy(gameObject);
    }
    IEnumerator Bounce()  //Simulación de rebote tras impactar granadas normales
    {
        // Calcular distancia al target
        float target_Distance = Vector3.Distance(transform.position, target);

        // Calcular la velocidad necesaria para lanzar el objeto al target con el ángulo especificado
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extraer los componentes X e Y de la velocidad
        Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calcular el tiempo de vuelo
        float flightDuration = target_Distance / Vx;

        //Rotar el proyectil para que mire al target
        transform.rotation = Quaternion.LookRotation(target - transform.position) * Quaternion.Euler(0, -90, 0);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            transform.Translate(Vx * Time.deltaTime, ((Vy - (gravity * elapse_time)) / 4) * Time.deltaTime, 0);

            elapse_time += Time.deltaTime;

            yield return null;
        }

        if(collisioned)
        {
            transform.position = new Vector3(transform.position.x, 0.1f, transform.position.z);
        }
    }
}
