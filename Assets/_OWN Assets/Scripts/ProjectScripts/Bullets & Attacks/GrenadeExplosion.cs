﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : Explosion
{
    protected override void PlayExplosionSound()
    {
        //Sonido
        int random = Random.Range(0, 2);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.GRANADE_EXP_1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.GRANADE_EXP_2, this.transform);
                break;
            default:
                break;
        }
    }
}
