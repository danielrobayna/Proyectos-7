﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ch.sycoforge.Decal;
using BossBattle;

public class BulletBase : DamageObject
{
    [Header("Variables de las Balas")]
    [SerializeField] protected float m_velocity;
    private Rigidbody bulletRb;
   
    [Header("Partículas (PlaceHolders)")]
    public GameObject hitWallPart;
    public GameObject hitCharacterPart;

    [Header("Lista de Decals al impactar con enemigos")]
    [SerializeField] protected List<GameObject> bloodDecals;
    protected LayerMask bloodDecalRaycastLayerMask;

    [Header("Destroy de las balas")]
    [SerializeField] float timeToDestroy;
    [SerializeField] float distanceToDestroy;
    private bool doOnce = false;
    float timerToDestroy;
    float distanceTravelled;

    private ParticleSystemRenderer bulletRenderer;
    private Collider bulletCollider;

    [Header("Si hay, Partícula que debe parar antes de destruir bala, diferente a la del padre")]
    [SerializeField] private ParticleSystem bulletLifeParticle;

    [Header("Si se rellena el anterior y hay, Luz de la bala a desactivar")]
    [SerializeField] private Light bulletLight;

    [Header("Si hay, Partículas que deben desaparecer según impacta la bala, diferentes a la del padre")]
    [SerializeField] private List<ParticleSystem> particlesToDestroyOnImpact = new List<ParticleSystem>();

    private void Start()
    {
        bloodDecalRaycastLayerMask = (1 << 0) | (1 << 16) | (1 << 18) | (1 << 20); //Añadidas layers Default, PlaneFromCamera, Doors y Blockades a la LayerMask del Raycast para instanciar decals de sangre.

        bulletRenderer = GetComponent<ParticleSystemRenderer>();
        bulletCollider = GetComponent<Collider>();

        bulletRb = GetComponent<Rigidbody>();
        bulletRb.velocity = transform.right * m_velocity;
        ZoneManager.Instance.AddBulletInActiveRoom(this);
                                                                            //MULTIPLIER UPGRADE
        bulletDamageToEnemy *= GameManager.Instance.playerDamageMultiplier; //Se aplica el multiplicador de Daño en GameManager.
    }
    protected virtual void Update()
    {
        if (timerToDestroy != 0)
        {
            timerToDestroy += Time.deltaTime;
            if (timeToDestroy < timerToDestroy && !doOnce)
            {
                doOnce = true;
                Instantiate(hitWallPart, transform.position, transform.rotation);
                DestroyBullet();
            }
        }
        if (distanceToDestroy != 0)
        {
            distanceTravelled += m_velocity * Time.deltaTime;
            if (distanceToDestroy < distanceTravelled && !doOnce)
            {
                doOnce = true;
                Instantiate(hitWallPart, transform.position, transform.rotation);
                DestroyBullet();
            }
        } 
        if(gameObject.layer == 12) //Destruir balas disparadas por el jugador fuera de pantalla
        {
            if(!bulletRenderer.isVisible)
            {
                DestroyBullet();
            }
        }
    }
    protected override void CollisionWithEnemy(Collider enemy)
    {
        EnemyControl_MovementController enemyController = enemy.GetComponent<EnemyControl_MovementController>();

        if (enemyController != null && !enemyController.IsDashing())
        {
            CollisionWithEnemyEffects(collisionIsEnemy); //ES IMPORTANTE QUE VAYA ANTES QUE EL RESTO DE LA LOGICA PARA DETECTAR CORRECTAMENTE SI ES INVULNERABLE EN EL MOMENTO DE LA COLISIÓN Y SE GENERE UN CAMERA IMPULSE

            collisionIsEnemy.ReceiveDamage(bulletDamageToEnemy);
            EnemySetControl myEnemySetControl = enemy.GetComponent<EnemySetControl>();
            if (myEnemySetControl != null)
            {
                myEnemySetControl.StopConsumingAction();
            }

            //////// DECALS //////////

            enemy.TryGetComponent(out EnemyAI_Standard thisEnemyAI);  //Comprobación para no sacar decals de sangre si no son humanos
            if(!(thisEnemyAI is EnemyAI_Bomber) && !(thisEnemyAI is EnemyAI_Turret))
            {
                if (Physics.Raycast(transform.position, -transform.up, out RaycastHit rayHitDown, 2, bloodDecalRaycastLayerMask))
                {
                    EasyDecal.Project(bloodDecals[Random.Range(0, bloodDecals.Count)], rayHitDown.point, rayHitDown.normal);
                }
                if (Physics.Raycast(transform.position, transform.right, out RaycastHit rayHitRight, 2, bloodDecalRaycastLayerMask))
                {
                    EasyDecal.Project(bloodDecals[Random.Range(0, bloodDecals.Count)], rayHitRight.point, rayHitRight.normal);
                }
            }
        }   
    }
    protected override void CollisionWithEnemyEffects(EnemyHealth enemyHealth)
    {
        DestroyBullet();
       
        //Partículas
        Instantiate(hitCharacterPart, transform.position, transform.rotation);  //PLACEHOLDER

        enemyHealth.this_enemySounds.PlayHitSound();
    }
    
    protected override void CollisionWithPlayerEffects(PlayerHealthController playerHealth)
    {
        //Partículas
        Instantiate(hitCharacterPart, transform.position, transform.rotation);  //PLACEHOLDER

        ///Sonido
        PlayPlayerHitSound();

        DestroyBullet();
    }
    protected void PlayPlayerHitSound()
    {
        int randomNum = Random.Range(0, 3);
        switch (randomNum)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_HIT_1);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_HIT_2);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_HIT_3);
                break;
            default:
                break;
        }
    }
    protected override void CollisionWithOther(Collider otherObject)
    {
        ShieldScript shield = otherObject.GetComponent<ShieldScript>();
        BossFinalReflectBullet bossSound = otherObject.gameObject.GetComponent<BossFinalReflectBullet>();

        if (bossSound != null)
        {
            bossSound.BulletImpact(this);
        }
        else if (shield == null)
        {
            base.CollisionWithOther(otherObject);
        }
    }
    protected override void CollisionWithOtherEffects()
    {
        DestroyBullet();
        GameObject impactParticle = Instantiate(hitWallPart, transform.position, transform.rotation);
        PlayOtherHitSound(impactParticle);
    }
    void PlayOtherHitSound(GameObject particle)
    {
        int randomNum = Random.Range(0, 3);
        switch (randomNum)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.BULLET_IMPACT_1, 1, 1, 10, particle.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.BULLET_IMPACT_2, 1, 1, 10, particle.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.BULLET_IMPACT_3, 1, 1, 10, particle.transform);
                break;
            default:
                break;
        }
    }
    protected void DestroyBullet()
    {
        if (bulletLifeParticle != null) //Si hay partícula que debe terminar antes de destruir la bala
        {
            ParticleSystem.MainModule main = bulletLifeParticle.main;
            float timeLeft = main.startLifetimeMultiplier + main.duration;
            if (bulletCollider != null)
            {
                bulletCollider.enabled = false;
            }
            if (bulletRb != null)
            {
                bulletRb.velocity = Vector3.zero;
            }

            if(bulletLight != null)
            {
                bulletLight.enabled = false;
            }

            for (int i = 0; i < particlesToDestroyOnImpact.Count; i++) //Deactivar partículas diferentes a la del padre que no deben seguir palyeando
            {
                if (particlesToDestroyOnImpact[i].isPlaying)
                {
                    particlesToDestroyOnImpact[i].Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
                }
            }

            Destroy(gameObject, timeLeft);
        }
        else // Si no hay una partícula que deba acabar antes de destruir la bala
        {
            Destroy(gameObject);
        }
        ZoneManager.Instance.RemoveBulletInActiveRoom(this);
    }

    public float BulletVelocity() //Sacar fuera de la clase la velocidad de la bala
    {
        return m_velocity;
    }
    public void ResetDestroyVariables()
    {
        distanceTravelled = 0;
        timerToDestroy = 0;
    }
}
