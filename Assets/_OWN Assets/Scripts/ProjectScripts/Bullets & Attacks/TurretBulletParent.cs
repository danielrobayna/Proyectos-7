﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBulletParent : MonoBehaviour
{
    [Header("Las balas hijas con el comportamiento Bullet")]
    [SerializeField] private List<GameObject> bulletChilds;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < bulletChilds.Count; i++)
        {
            bulletChilds[i].layer = gameObject.layer;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (bulletChilds.Count > 0)
        {
            for (int i = 0; i < bulletChilds.Count; i++)
            {
                if (bulletChilds[i] == null)
                {
                    bulletChilds.Remove(bulletChilds[i]);
                }
            }
        }

        else
        {
            Destroy(gameObject);
        }
    }
}
