﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Grenade : MonoBehaviour
{
    [Header ("Variables para controlar el lanzado de la granada")]
    [SerializeField] private float maxReach;

    [Header("Velocidades de la granada")]
    protected float Vx;
    protected float Vy;

    protected Vector3 target;
    [SerializeField] protected float firingAngle = 45.0f;
    protected float initialFiringAngle;
    [SerializeField] protected float gravity = 9.8f;

    [Header("Control de Colisiones")]
    protected bool collisioned;

    [Header("Resultado de la Explosión")]
    [SerializeField] protected GameObject grenadeExplosion;

    [Header("Corutina de Movimiento actual de la Granada")]
    protected Coroutine actualGrenadeMovementCoroutine;

    protected virtual void Start()
    {
        initialFiringAngle = firingAngle;

        if (gameObject.layer == 10) //Lanzado por un enemigo
        {
            if (GameManager.Instance.ActualPlayerController is EnemyControl_MovementController)   //Comprobar si el controlador actual es un enemigo controlado
            {
                SetGrenadeTarget(GameManager.Instance.ActualPlayerController.transform.position);    //Setear lanzamiento a los pies del enemigo controlado                
            }
            else                                                                                   //Comprobar si es el player real
            {
                SetGrenadeTarget(GameManager.Instance.ActualPlayerController.modelParent.transform.position);   //Setear el lanzamiento al MODELO del jugador
                Physics.IgnoreCollision(GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<Collider>(), true); //Ignorar colision con el collider grande del player
            }
        }
        else if (gameObject.layer == 12) //Lanzado por el jugador
        {
            if (InputManager.Instance.IsControllingWithController) //Si esta activado es control de mando
            {
                SetGrenadeTarget();
            }

            else //Si no es control de ratón
            {
                if (Vector3.Distance(transform.position, InputManager.Instance.aimTranform.position) <= maxReach) //Lanzar al punto de apuntado si es menor o igual que el alnzace máximo
                {
                    SetGrenadeTarget(InputManager.Instance.aimTranform.position + new Vector3(0, 0, 0.65f));
                }
                else    //Si el apuntado está más allá del maximo, se lanza al máximo
                {
                    SetGrenadeTarget();
                }
            }
        }
    }

    public void SetGrenadeTarget()  //Lanzar granada sin posición
    {
        target = transform.position + (transform.right * maxReach);
        target = new Vector3(target.x, 0, target.z);
        actualGrenadeMovementCoroutine = StartCoroutine(SimulateProjectile());
    }

    public void SetGrenadeTarget(Vector3 position) //Lanzar granada a posición
    {
        target = position;
        target = new Vector3(target.x, 0, target.z);
        actualGrenadeMovementCoroutine = StartCoroutine(SimulateProjectile());
    }

    IEnumerator SimulateProjectile() //Simulación de  vuelo en parábola
    {
        // Calcular distancia al target
        float target_Distance = Vector3.Distance(transform.position, target);

        // Calcular la velocidad necesaria para lanzar el objeto al target con el ángulo especificado
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extraer los componentes X e Y de la velocidad
        Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calcular el tiempo de vuelo
        float flightDuration = target_Distance / Vx;

        //Rotar el proyectil para que mire al target
        transform.rotation = Quaternion.LookRotation(target- transform.position) * Quaternion.Euler(0, -90, 0);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            transform.Translate(Vx * Time.deltaTime, ((Vy - (gravity * elapse_time)) / 4) * Time.deltaTime, 0);

            elapse_time += Time.deltaTime;

            yield return null;
        }

        transform.position = new Vector3(transform.position.x, 0.1f, transform.position.z);
    }
}
