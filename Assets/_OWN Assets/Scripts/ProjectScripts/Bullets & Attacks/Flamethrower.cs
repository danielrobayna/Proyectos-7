﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamethrower : MonoBehaviour
{
    [Header("Daño dentro de Lanzallamas al Enemigo")]
    [SerializeField] private float damageInFireToEnemy;
    [Header("Daño dentro de Lanzallamas en Fragmentos Perdidos al Jugador")]
    [SerializeField] private int damageInFireToPlayer;
    [Header("Cada cuanto tiempo se hace daño dentro del Lanzallamas")]
    [SerializeField] private float timeToDamage;

    [Header("Daño por tiempo tras salir del fuego a Enemigo")]
    [Space(20)]
    [SerializeField] private float damagePerSecondToEnemy;
    [Header("Daño por tiempo tras salir del fuego en fragmentos al Jugador")]
    [SerializeField] private int damagePerSecondToPlayer;
    [Header("Tiempo quemandose")]
    [SerializeField] private float timeInFire;

    [Header("Gestión del Trigger")]
    [SerializeField] private LayerMask layersToRaycast;
    private BoxCollider m_Collider;
    private float initialXSize;
    private float intialXPosition;
    private RaycastHit hit;

    [Header("Variables para Comprobar Colisión en TriggerEnter")]
    private EnemyHealth triggerIsEnemy;
    private PlayerHealthController triggerIsPlayer;
    private HeatBarScript triggerHasHeatBar;  //El enemigo tiene barra de calor

    private void Start()
    {
        m_Collider = GetComponent<BoxCollider>();
        initialXSize = m_Collider.size.x;
        intialXPosition = m_Collider.center.x;
                                                            //MULTIPLIER UPGRADE
        damageInFireToEnemy *= GameManager.Instance.playerDamageMultiplier; //Se aplica el multiplicador de Daño en GameManager.
    }

    private void FixedUpdate() //Modificación del trigger de laznallamas en función de elementos que choquen contra el raycast, para que el trigger no atraviese paredes
    {
        Physics.Raycast(transform.position, transform.right * m_Collider.size.x, out hit, initialXSize, layersToRaycast);

        if(hit.collider != null)
        {
            m_Collider.size = new Vector3(hit.distance, m_Collider.size.y, m_Collider.size.z);
            m_Collider.center = new Vector3(intialXPosition * (m_Collider.size.x/initialXSize), 0, 0);

            if (hit.collider.TryGetComponent(out DestructibleObject barrel)) //Destruir Barriles
            {
                barrel.Destroy();
            }
        }

        else if(hit.collider == null)
        {
            m_Collider.size = new Vector3(initialXSize, m_Collider.size.y, m_Collider.size.z);
            m_Collider.center = new Vector3(intialXPosition, 0, 0);
        }

        //Debug.DrawRay(transform.position, transform.right * m_Collider.size.x, Color.black);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<RoomManager>() == null)
        {
            FireBlockade isOtherAFireBlockade = other.GetComponent<FireBlockade>();

            if (isOtherAFireBlockade != null)
            {
                isOtherAFireBlockade.BreakBlockade();
            }
           
        }
    }

    private void OnTriggerStay(Collider other)
    {
        triggerIsEnemy = other.GetComponent<EnemyHealth>();
        triggerIsPlayer = other.GetComponent<PlayerHealthController>();
        triggerHasHeatBar = other.GetComponent<HeatBarScript>();

        if(triggerHasHeatBar)
        {
            triggerHasHeatBar.FillHeatBar();
        }

        else if (triggerIsEnemy != null && !triggerIsEnemy.GetIsOnFire())
        {
            triggerIsEnemy.StartFireState(damageInFireToEnemy, 10, timeToDamage);

            ///// LOGRO ///////
            SteamManager.Instance.Achievement_Burn50Enemies();
            /////

            triggerIsEnemy.TryGetComponent(out EnemySetControl enemySetControl);
            enemySetControl.StopConsumingAction();
        }

        else if(triggerIsPlayer != null && !triggerIsPlayer.GetIsOnFire())
        {
            triggerIsPlayer.StartFireState(damageInFireToPlayer, 10, timeToDamage);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        triggerIsEnemy = other.GetComponent<EnemyHealth>();
        triggerIsPlayer = other.GetComponent<PlayerHealthController>();
        triggerHasHeatBar = other.GetComponent<HeatBarScript>();

        if (triggerHasHeatBar)
        {
            triggerHasHeatBar.StopFillingHeatBar();
        }

        else if (triggerIsEnemy != null)
        {
            triggerIsEnemy.StartFireState(damagePerSecondToEnemy, timeInFire);
        }

        else if(triggerIsPlayer != null)
        {
             triggerIsPlayer.StartFireState(damagePerSecondToPlayer, timeInFire);
        }

        triggerIsPlayer = null;
        if (other.GetComponent<DestructibleObject>() != null)
        {
            other.GetComponent<DestructibleObject>().Destroy();
        }
    }

    public void FlamethrowerStopped() //Llamada desde el DisableThrower en ShootingScript para realizar funciones al dejar de disparar el jugador o la IA.
    {
        if (triggerHasHeatBar)
        {
            triggerHasHeatBar.StopFillingHeatBar();
        }

        else if (triggerIsEnemy != null)
        {
            triggerIsEnemy.StartFireState(damagePerSecondToEnemy, timeInFire);
        }

        else if (triggerIsPlayer != null)
        {
            triggerIsPlayer.StartFireState(damagePerSecondToPlayer, timeInFire);
        }

        triggerHasHeatBar = null;
        triggerIsEnemy = null;
        triggerIsPlayer = null;
    }

    private void OnDisable()    //Ejecutado al morir el enemigo con el lanzallamas.
    {
        if (triggerHasHeatBar)
        {
            triggerHasHeatBar.StopFillingHeatBar();
        }

        else if (triggerIsEnemy != null)
        {
            triggerIsEnemy.StartFireState(damagePerSecondToEnemy, timeInFire);
        }

        triggerHasHeatBar = null;
        triggerIsEnemy = null;
    }
}
