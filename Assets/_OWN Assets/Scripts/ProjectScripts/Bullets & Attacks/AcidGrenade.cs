﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidGrenade : Grenade
{
    public GameObject acidImpactParticle;
    [SerializeField] private LayerMask acidToGroundRayMask = 16;

    [Header("Elementos para no destruir granada hasta que se acaben sus partículas")]
    [SerializeField] private GameObject acidGrenadeObject;
    private ParticleSystem acidGrenadePS;

    protected override void Start()
    {
        base.Start();

        acidGrenadePS = GetComponentInChildren<ParticleSystem>();

        ParticleSystem.MainModule main = acidGrenadePS.main;
        float timeLeft = main.startLifetimeMultiplier + main.duration;
        Destroy(gameObject, timeLeft);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!collisioned)  //Romper la granada de ácido según colisiona
        {
            //Efectos
            Instantiate(acidImpactParticle, transform.position, acidImpactParticle.transform.rotation);
            MusicManager.Instance.PlaySound(AppSounds.ACID_GLASS, this.transform);

            Physics.Raycast(transform.position + new Vector3(0, 0.2f, 0), Vector3.down, out RaycastHit ray, 5, acidToGroundRayMask);  //Raycast hacia el suelo para instanciar suelo de ácido, elevado origen en Y por si al chocar se lanza el raycast por debajo del suelo

            if (ray.collider != null && ray.collider.gameObject.layer == 16)
            {
                Instantiate(grenadeExplosion, ray.point + new Vector3(0, 0.1f, 0), grenadeExplosion.transform.rotation); //Elevada posición en Y para evitar overlap del suelo ácido con el suelo
            }

            collisioned = true;
            acidGrenadeObject.SetActive(false);
            StopCoroutine(actualGrenadeMovementCoroutine);
        }
    }
}
