﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricBullet : BulletBase
{
    protected override void CollisionWithEnemy(Collider enemy)
    {
        base.CollisionWithEnemy(enemy);

        EnemySetControl myEnemySetControl = enemy.GetComponent<EnemySetControl>();
        if (myEnemySetControl.IsEnemyPossessed())
        {
            myEnemySetControl.UnpossessEnemy(Vector3.zero, EnemySetControl.UnposessTypes.electric);     //Desposeer al jugador por causa de electricidad
        }

        EnemyAI_Standard myEnemyAI = enemy.GetComponent<EnemyAI_Standard>();
        myEnemyAI.StartElectricalStun();

        ///// LOGRO ///////
        SteamManager.Instance.Achievement_Shock50Enemies();
        /////
    }

    protected override void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
        base.CollisionWithPlayer(playerHealth);

        PossessAbility playerPossessAbility = playerHealth.GetComponent<PossessAbility>(); 
        playerPossessAbility.StartElectricStun();                   //Stun eléctrico al jugador 
    }
    protected override void CollisionWithOther(Collider otherObject)  //La bala eléctrica chequea en colision with other si es un bloqueo eléctrico
    {
        base.CollisionWithOther(otherObject);

        ElectricBlockade isOtherAnElectricBlockade = otherObject.GetComponent<ElectricBlockade>();

        if (isOtherAnElectricBlockade != null)
        {
            isOtherAnElectricBlockade.BreakBlockade();
        }
    }
}
