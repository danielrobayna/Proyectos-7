﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ch.sycoforge.Decal;

public class SniperBullet : BulletBase
{
    [Header("SniperBullet layers a checkear")]
    private LayerMask sniperCheckLayer;
    private bool checkedColOnce = false;
    private bool changeLayerOnce = false;

    Transform thisTransform;
    Transform bulletTransform;

    [Header("Es la bala de Railgun?")]
    [SerializeField] private bool isRailgunBullet;

    [Header("Simulacion de Raycast y Rebotes")]
    private int nReflections = 1;
    private Ray ray;
    private RaycastHit hit;
    private Vector3 inDirection;
    private List<Ray> bouncedRays = new List<Ray>();
    private List<RaycastHit> rayHits = new List<RaycastHit>();
    private int bulletColisionIndex = 0;

    [Header("Objeto bala hijo")]
    [SerializeField] private GameObject bullet;
    float fraction = 0;

    [Header("Control del logro del Collateral")]
    private int enemiesHitAfterAnother = 0;

    private void Awake()
    {
        thisTransform = transform;
        bulletTransform = bullet.GetComponent<Transform>();
    }

    private void Start()
    {
        bloodDecalRaycastLayerMask = (1 << 0) | (1 << 16) | (1 << 18) | (1 << 20); //Añadidas layers Default, PlaneFromCamera, Doors y Blockades a la LayerMask del Raycast para instanciar decals de sangre.

        if (gameObject.layer == 10 || gameObject.layer == 12)
        {
           sniperCheckLayer = GetPhysicsLayerMask(gameObject.layer);    //Cogemos las colisiones de la layer BulletPlayer o BulletEnemy y se las aplicamos a la layermask del raycast
        }

        SimulateRayBullet();
    }

    private LayerMask GetPhysicsLayerMask(int currentLayer)     //Coger la layer mask de collision de la layer pasada por referencia
    {
        int finalMask = 0;
        for (int i = 0; i < 32; i++)
        {
            if (!Physics.GetIgnoreLayerCollision(currentLayer, i)) finalMask = finalMask | (1 << i);
        }
        return finalMask;
    }

    // Update is called once per frame
    protected override void Update()
    {
        if(bulletColisionIndex < rayHits.Count) //Mover la bala falsa si hay rebotes
        {
            if (fraction < 1)
            {
                fraction += Time.deltaTime * m_velocity;
                bulletTransform.position = Vector3.Lerp(bulletTransform.position, rayHits[bulletColisionIndex].point, fraction);
            }

            if(fraction >= 1)
            {
                CheckCollision(rayHits[bulletColisionIndex]);
                bulletColisionIndex++;
                fraction = 0;
            }
        }

        //No parece necesario y genera problemas
        /*else if(bouncedRays.Count == 0) //Mover la bala falsa si NO hay rebotes
        {
            if (fraction < 1)
            {
                fraction += Time.deltaTime * m_velocity;
                bulletTransform.position = Vector3.Lerp(bulletTransform.position, rayHits[bulletColisionIndex].point, fraction);
            }

            if (fraction >= 1)
            {
                CheckCollision(rayHits[bulletColisionIndex]);
                bulletColisionIndex++;
                fraction = 0;
            }
        }*/
    }

    void CheckCollision(RaycastHit hit) //Comprobar efectos de colisión al llegar la bala falsa a la posición de un RaycastHit determinado en Update
    {
        if (hit.collider != null)
        {
            if(hit.collider.gameObject.TryGetComponent(out ShieldScript shield))
            {
                enemiesHitAfterAnother = 0; //Se pierde el contador de collateral si pega contra escudo

                Instantiate(shield.shieldImpactParticle, rayHits[bulletColisionIndex].point, hit.collider.transform.rotation);
                MusicManager.Instance.PlaySound(AppSounds.SHIELD_BOUNCE, this.transform); //Fx rebote de la bala

                if (isRailgunBullet)
                {
                    shield.ShieldTakeDamage(100);
                }
                else
                {
                    shield.ShieldTakeDamage(2);
                }
            }

            else if (hit.collider.gameObject.TryGetComponent(out EnemyHealth enemyHealth))
            {
                enemiesHitAfterAnother += 1; //Se suma el contador de collateral si pega contra enemigos

                ///// LOGRO ///////
                if (enemiesHitAfterAnother > 1)
                {
                    SteamManager.Instance.UnlockAchievement("SNIPER_COLLATERAL");
                }
                /////

                collisionIsEnemy = enemyHealth;
                CollisionWithEnemy(hit.collider);
            }

            else if (!checkedColOnce)
            {
                AlertEnemies();

                if (hit.collider.gameObject.TryGetComponent(out PlayerHealthController playerHealth))
                {
                    checkedColOnce = true;
                    CollisionWithPlayer(playerHealth);
                }

                else if(hit.collider.gameObject.TryGetComponent(out FinalBossZona2Behaviour boss2Health)) //Para el Boss de la Zona 2
                {
                    checkedColOnce = true;
                    boss2Health.BossReceiveHit();
                    CollisionWithBoss2Effects();
                }

                else if (!hit.collider.gameObject.TryGetComponent(out RoomManager roomManager))
                {
                    checkedColOnce = true;
                    if (hit.collider.TryGetComponent(out DestructibleObject barrel)) //Destruir Barriles
                    {
                        barrel.Destroy();
                    }
                    CollisionWithOther(hit.collider);
                }
            }
        }
    }

    void SimulateRayBullet() //Simulación de raycast y rebotes
    {
        //Clampear el numero de rebotes entre 0 y la capacidad del tipo numérico int
        nReflections = Mathf.Clamp(nReflections, 1, nReflections);
        //Castear nuevo rayo hacia la derecha del transform
        ray = new Ray(transform.position, transform.right);
        //Representar el rayo
        //Debug.DrawRay(transform.position, transform.right, Color.magenta);

        for (int i = 0; i < nReflections; i++)
        {
            //Comprobar si el rayo ha colisionado
            if (Physics.Raycast(ray.origin, ray.direction, out hit, 100, sniperCheckLayer)) 
            {
                //Añadir información de hit a lista para comprobacion de colisiones de bala
                rayHits.Add(hit);

                if (hit.collider.gameObject.TryGetComponent(out ShieldScript shield)) //Si la colisión es un escudo, rebota
                {
                    ChangePlayerBulletLayerOnShieldBounce(); //Cambiar layer de la bala si es del jugador a default

                    //La dirección de rebote es la reflejada entre la dirección del rayo y la normal del punto de impacto proyectada en el plano XZ, para que no tenga valor en Y que pueda surgir de la normal
                    inDirection = Vector3.ProjectOnPlane(Vector3.Reflect(ray.direction, hit.normal), Vector3.up);
                    //Lanzar el rayo reflejado, usando el punto deimpacto anterior y la dirección reflejada
                    ray = new Ray(hit.point, inDirection);
                    //Añadir rayo a lista para comprobación de movimiento de falsa bala
                    bouncedRays.Add(ray);

                    //Dibujar la normal de impacto
                    //Debug.DrawRay(hit.point, hit.normal * 3, Color.blue);
                    //Dibujar rayo
                    //Debug.DrawRay(hit.point, inDirection, Color.magenta);
                    //Se aumenta la capacidad de rebote
                    nReflections += 1;
                }

                else if (hit.collider.gameObject.TryGetComponent(out EnemySetControl enemy))
                {
                    if(inDirection == Vector3.zero)
                    {
                        inDirection = transform.right;
                    }

                    //Colocar el origen del siguiente rayodentro del collider del enemigo para que no lo chequee
                    Vector3 newRayOrigin = hit.point + inDirection * 0.1f;

                    //Crear nuevo rayo
                    ray = new Ray(newRayOrigin, inDirection);

                    //Se toma como un rebote y se aumenta la capacidad de rebote
                    nReflections += 1;
                }
            }
        }
    }

    void ChangePlayerBulletLayerOnShieldBounce()
    {
        if (!changeLayerOnce)
        {
            sniperCheckLayer = GetPhysicsLayerMask(0); //Cambiar layermask de la bala a la matriz de colisiones de default (layer 0)
            changeLayerOnce = true;
        }
    }

    //////////////METODOS OVERWRITEADOS PARA INTANCIAR EN EL PUNTO DE IMPACTO DEL rayHitCAST //////////////

    protected override void AlertEnemies()
    {
        Collider[] hitColliders = Physics.OverlapSphere(rayHits[bulletColisionIndex].point, m_bulletHearingDistance, m_enemyLayer);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            EnemyAI_Standard enemyAI = hitColliders[i].GetComponent<EnemyAI_Standard>();
            if (enemyAI != null && enemyAI.currentAIState == EnemyAI_Standard.AIState.idle)
            {
                enemyAI.ExternalDetectPlayer();
            }
        }
    }

    private void CollisionWithBoss2Effects()
    {
        //Partículas
        Instantiate(hitCharacterPart, rayHits[bulletColisionIndex].point, transform.rotation);  //PLACEHOLDER

        DestroyBullet();
    }

    protected override void CollisionWithEnemy(Collider enemy)
    {
        if (enemy.GetComponent<EnemyHealth>().enemyHealth > 0)
        {
            EnemyControl_MovementController enemyController = enemy.GetComponent<EnemyControl_MovementController>();

            if (enemyController != null && !enemyController.IsDashing())
            {
                CollisionWithEnemyEffects(collisionIsEnemy); //ES IMPORTANTE QUE VAYA ANTES QUE EL RESTO DE LA LOGICA PARA DETECTAR CORRECTAMENTE SI ES INVULNERABLE EN EL MOMENTO DE LA COLISIÓN Y SE GENERE UN CAMERA IMPULSE

                collisionIsEnemy.ReceiveDamage(bulletDamageToEnemy);
                EnemySetControl myEnemySetControl = enemy.GetComponent<EnemySetControl>();
                if (myEnemySetControl != null)
                {
                    myEnemySetControl.StopConsumingAction();
                }

                //////// DECALS //////////
                enemy.TryGetComponent(out EnemyAI_Standard thisEnemyAI);  //Comprobación para no sacar decals de sangre si no son humanos
                if (!(thisEnemyAI is EnemyAI_Bomber) && !(thisEnemyAI is EnemyAI_Turret))
                    if (Physics.Raycast(rayHits[bulletColisionIndex].point, -transform.up, out RaycastHit rayHitDown, 2, bloodDecalRaycastLayerMask))
                {
                    {
                        EasyDecal.Project(bloodDecals[Random.Range(0, bloodDecals.Count)], rayHitDown.point, rayHitDown.normal);
                    }
                    if (Physics.Raycast(rayHits[bulletColisionIndex].point, transform.right, out RaycastHit rayHitRight, 2, bloodDecalRaycastLayerMask))
                    {
                        EasyDecal.Project(bloodDecals[Random.Range(0, bloodDecals.Count)], rayHitRight.point, rayHitRight.normal);
                    }
                }
            }
        }
    }

    protected override void CollisionWithEnemyEffects(EnemyHealth enemyHealth)
    {
        //Partículas
        Instantiate(hitCharacterPart, rayHits[bulletColisionIndex].point, transform.rotation);  //PLACEHOLDER

        //Sonido
        enemyHealth.this_enemySounds.PlayHitSound();
    }
    protected override void CollisionWithPlayerEffects(PlayerHealthController playerHealth)
    {
        //Partículas
        Instantiate(hitCharacterPart, rayHits[bulletColisionIndex].point, transform.rotation);  //PLACEHOLDER

        //Sonido
        PlayPlayerHitSound();

        DestroyBullet();
    }
    protected override void CollisionWithOther(Collider otherObject)
    {
        RailgunBlockade isRailgunBlockade = otherObject.GetComponent<RailgunBlockade>();

        if (isRailgunBlockade != null)
        {
            isRailgunBlockade.BreakBlockade();
        }

        CollisionWithOtherEffects(); //Modificar con efectos especiales del escudo si hay
    }
    protected override void CollisionWithOtherEffects()
    {
        DestroyBullet();
        Instantiate(hitWallPart, rayHits[bulletColisionIndex].point, transform.rotation);
    }
}