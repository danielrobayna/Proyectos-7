﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using ch.sycoforge.Decal;

public class Explosion : DamageObject
{
    [SerializeField] protected ParticleSystem particles;
    protected float timer=0;
    [SerializeField] protected float colliderExplosionDurationTime;
    protected float colliderTimer=0;

    [SerializeField] LayerMask collisionMask;

    [Header("Decals de Explosión, Si no hay no se usan")]
    [SerializeField] private List<GameObject> explosionDecals = new List<GameObject>();
    private LayerMask explosionDecalLayerMask;

    [Header("Decals de Sangre")]
    [SerializeField] private List<GameObject> bloodDecals = new List<GameObject>();

    private void Start()
    {
        PlayExplosionSound();

        /////CAMERA SHAKE///////
        CinemachineImpulseSource impulse = GetComponent<CinemachineImpulseSource>();
        impulse.GenerateImpulse();

        //////DECALS DE EXPLOSIÓN///////
        ///
        explosionDecalLayerMask |= (1 << 16); //Meter solo layer de suelo para chequear

        if (explosionDecals.Count != 0 && Physics.Raycast(transform.position + new Vector3(0, 0.2f, 0), -transform.up, out RaycastHit rayHitDown, 2, explosionDecalLayerMask))
        {
            EasyDecal.Project(explosionDecals[Random.Range(0, explosionDecals.Count)], rayHitDown.point, Quaternion.Euler(0, Random.Range(0.0f, 360.0f), 0));
        }
    }

    protected virtual void Update()
    { 
        timer += Time.deltaTime;
        colliderTimer += Time.deltaTime;
        if (colliderExplosionDurationTime < colliderTimer) //Timer para desactivar las colisiones de la explosión
        {
            gameObject.GetComponent<SphereCollider>().enabled = false;
        }
        if (particles.main.duration<timer) //Timer para desactivar el objeto
        {
            Destroy(gameObject);
        }
    }

    protected override void CollisionWithOther(Collider otherObject)  //La explosión chequea en colision with other si es un bloqueo de explosión
    {
        base.CollisionWithOther(otherObject);

        ExplosionBlockade isOtherAnExplosionBlockade = otherObject.GetComponent<ExplosionBlockade>();

        if (isOtherAnExplosionBlockade != null)
        {
            isOtherAnExplosionBlockade.BreakBlockade();
        }

        if (Physics.Linecast(transform.position, otherObject.transform.position, out RaycastHit info, collisionMask))
        {
            if(info.collider.TryGetComponent(out DestructibleObject barrel))
            {
                barrel.Destroy();
            }
        }
    }
    protected override void CollisionWithEnemy(Collider enemy)
    {
        if (!Physics.Linecast(transform.position, enemy.transform.position, collisionMask))
        {
            if (enemy.GetComponent<EnemySetControl>().IsEnemyPossessed()) //Detectar si está siendo poseído
            {
                gameObject.layer = 12; //De esta forma, si el enemigo que matas está siendo poseído, no hace daño al jugador
            }
            base.CollisionWithEnemy(enemy);

            //////// DECALS DE SANGRE //////////
            ///
            enemy.TryGetComponent(out EnemyAI_Standard thisEnemyAI);  //Comprobación para no sacar decals de sangre si no son humanos
            if (!(thisEnemyAI is EnemyAI_Bomber) && !(thisEnemyAI is EnemyAI_Turret))
            {
                EasyDecal.Project(bloodDecals[Random.Range(0, bloodDecals.Count)], enemy.transform.position, Quaternion.Euler(0, Random.Range(0.0f, 360.0f), 0));
            }
        }      
    }
    protected override void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
        if (!Physics.Linecast(transform.position, playerHealth.transform.position, collisionMask))
        {
            base.CollisionWithPlayer(playerHealth);
        }
    }

    protected virtual void PlayExplosionSound()
    {
        //Sonido
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_1, 20, 40, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_2, 20, 40, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_3, 20, 40, this.transform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_4, 20, 40, this.transform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_5, 20, 40, this.transform);
                break;
            default:
                break;
        }
    }
}
