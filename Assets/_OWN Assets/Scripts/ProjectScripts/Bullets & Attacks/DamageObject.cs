﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageObject : MonoBehaviour
{
    [Header("El Daño en Número de Fragmentos Perdidos al Jugador")]
    public int bulletDamageToPlayer;
    [Header("El Daño al Enemigo")]
    public float bulletDamageToEnemy;

    [Header("Variables para Comprobar Colisión")]
    protected EnemyHealth collisionIsEnemy;
    protected PlayerControl_MovementController collisionIsPlayer;

    [Header("Variables de alerta al enemigo")]
    [SerializeField] protected float m_bulletHearingDistance;
    [SerializeField] protected LayerMask m_enemyLayer = 9;

    private void OnTriggerEnter(Collider collision)
    {
        collisionIsEnemy = collision.GetComponent<EnemyHealth>();
        collisionIsPlayer = collision.GetComponent<PlayerControl_MovementController>();

        AlertEnemies();

        if (collisionIsEnemy != null)   ////Colisión con Enemigo
        {
            CollisionWithEnemy(collision);
        } 
        else if (collisionIsPlayer != null && collisionIsPlayer == GameManager.Instance.ActualPlayerController) ////Colisión con Player
        {
            PlayerHealthController playerHealth = collision.GetComponent<PlayerHealthController>();
            if (playerHealth != null)
            {
                CollisionWithPlayer(playerHealth);
            }          
        }
        else if (collision.GetComponent<RoomManager>() == null)  ////Colisión con cualquier cosa que no sea las anteriores
        {
            CollisionWithOther(collision);  //El método de colision con "Otros" recibe la información de colisión para que los que heredan puedan chequear para, por ejemplo, ver si es un bloqueo.
        }
    }
    protected virtual void AlertEnemies()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, m_bulletHearingDistance, m_enemyLayer);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            EnemyAI_Standard enemyAI = hitColliders[i].GetComponent<EnemyAI_Standard>();
            if (enemyAI != null && enemyAI.currentAIState == EnemyAI_Standard.AIState.idle)
            {
                enemyAI.ExternalDetectPlayer();
            }
        }
    }

    protected virtual void CollisionWithEnemy(Collider enemy)
    {
        CollisionWithEnemyEffects(collisionIsEnemy); //ES IMPORTANTE QUE VAYA ANTES QUE EL RESTO DE LA LOGICA PARA DETECTAR CORRECTAMENTE SI ES INVULNERABLE EN EL MOMENTO DE LA COLISIÓN Y SE GENERE UN CAMERA IMPULSE
        if(collisionIsEnemy != null)
        collisionIsEnemy.ReceiveDamage(bulletDamageToEnemy);
        EnemySetControl myEnemySetControl = enemy.GetComponent<EnemySetControl>();
        if (myEnemySetControl != null)
        {
            myEnemySetControl.StopConsumingAction();
        }
    }
    protected virtual void CollisionWithEnemyEffects(EnemyHealth enemyHealth)
    {

    }

    protected virtual void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
        CollisionWithPlayerEffects(playerHealth); //ES IMPORTANTE QUE VAYA ANTES QUE EL RESTO DE LA LOGICA PARA DETECTAR CORRECTAMENTE SI ES INVULNERABLE EN EL MOMENTO DE LA COLISIÓN Y SE GENERE UN CAMERA IMPULSE
        playerHealth.DamagePlayer(bulletDamageToPlayer);
    }

    protected virtual void CollisionWithPlayerEffects(PlayerHealthController playerHealth)
    {

    }

    protected virtual void CollisionWithOther(Collider otherObject)
    {
        CollisionWithOtherEffects();
    }
    protected virtual void CollisionWithOtherEffects()
    { 
    }
}
