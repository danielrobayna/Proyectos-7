﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour
{
    public enum whoIsShooting { enemy, player }; //Quien dispara

    [Header("Variables de detección de la IA")]
    [SerializeField] float m_weaponHearingDistance;
    [SerializeField] LayerMask m_enemyLayer = 9;

    [Header("Rellenar SOLO si es un Lanzallamas o lanzapartículas, si no rellenar el resto")]
    [Space(20)]
    public bool isParticle_Thrower;
    [SerializeField] private Light particle_ThrowerLight;
    public GameObject particleThrowerPrefab;
    private GameObject instantiatedParticleThrower;
    [HideInInspector] public Flamethrower thisEnemy_Flamethrower; //Se llama desde enemycontrolmovement para comprobar que hay un lanzallamas activo
    private bool disabledThrowerOnce = false;
    private bool enableThrowerOnce = false;
    private Collider particleCollider;

    [Header("Variables Generales de Disparo")]
    [Space(20)]
    [SerializeField] GameObject m_bullet;
    [SerializeField] Transform m_shootingPos;
    [SerializeField] bool m_killAll; //Para saber si mata tanto al jugador como a los enemigos.
    [SerializeField] int m_bulletsToInstantiateAtOnce = 1;
    [SerializeField] float m_dispersionAngle;
    [SerializeField] float m_shootingAngle;//Para cuando se disparan varias balas, el ángulo en el que se reparten equitativamente
    [SerializeField] int m_bulletsPerBurst = 1;
    [SerializeField] float m_burtFiringRate;
    [Header ("Boleano para indicar cuando puedes disparar después de una ráfaga o de un disparo cargado")]
    private bool isAbleToShoot = true;

    [Header("Rellenar SOLO si es un disparo que hay que cargar")]
    public bool isChargedShot;
    [SerializeField] private float timetoCharge;
    [SerializeField] private bool isEnemySniper;
    [SerializeField] private bool isEnemyGrenadier;
    private LineRenderer sniperLaser; //Para el sniper
    private int sniperLaserPoints; //Número de puntos del LineRenderer
    private int laserReflections = 1; //Rebotes del LineRenderer sobre escudo, solo 1 sobre escudo de player
    private Ray ray;
    private RaycastHit hit;
    private Vector3 inDirection;
    [SerializeField] private LayerMask sniperLaserMask;

    [Header("Variables del Disparo Enemigo")]
    [SerializeField] float m_firingRate;
    [SerializeField] float m_randomFiringRateDeviation;

    [Header("Variables de las animaciones")]
    SetAnimatorVariables m_animationVariables;

    float m_firingRateTimer = 0; //Para que comience disparando
    float m_onStartFiringRate;

    [Header("Variables del Disparo del Jugador")]//Para el control del jugador, afectado por más parámetros y/o mejoras.
    [SerializeField] float player_firingRate;
    float player_firingRateTimer;

    [Header("SetControl del enemigo, para ejecutar operaciones si es un Shocker por ejemplo")]
    private EnemySetControl thisEnemy_SetControl;

    [Header("Partículas")]
    public GameObject shootPart;

    EnemySounds this_enemySound;

    private void Start()
    {
        sniperLaser = GetComponent<LineRenderer>();
        m_onStartFiringRate = m_firingRate;
        thisEnemy_SetControl = GetComponent<EnemySetControl>();
        m_animationVariables = GetComponent<SetAnimatorVariables>();
        this_enemySound = GetComponent<EnemySounds>();
    }
    private void OnEnable()
    {
        player_firingRateTimer = 0;
    }

    private void OnDisable()
    {
        ResetShootingVariables();
    }

    private void Update()
    {
        OnUpdateSetAnimatorVariables();
    }

    private LayerMask GetPhysicsLayerMask(int currentLayer)     //Coger la layer mask de collision de la layer pasada por referencia
    {
        int finalMask = 0;
        for (int i = 0; i < 32; i++)
        {
            if (!Physics.GetIgnoreLayerCollision(currentLayer, i)) finalMask = finalMask | (1 << i);
        }
        return finalMask;
    }

    public void FireInShootingPos(whoIsShooting shooter)
    {
        if (shooter == whoIsShooting.enemy) ////Como dispara el enemigo
        {
            EnemyShoot();
        }

        else if(shooter == whoIsShooting.player) //Como dispara el player al controlar este enemigo.
        {
            PlayerShoot();
        }
    }

    void EnemyShoot()
    {
        if (!isParticle_Thrower) //Funcionalidad de disparar balas si no es lanza-algo (lanzallamas...)
        {
            if (isChargedShot || m_bulletsPerBurst > 1)     //Funcionalidad cuando el disparo es cargado o de ráfagas.
            {
                if (isAbleToShoot && m_firingRateTimer <= 0)
                {
                    m_firingRate = Random.Range(m_onStartFiringRate - m_randomFiringRateDeviation, m_onStartFiringRate + m_randomFiringRateDeviation);
                    m_firingRateTimer = m_firingRate;

                    if (isChargedShot)
                    {
                        StartCoroutine(ChargedShot(m_bullet, 10, timetoCharge));
                    }
                    else if (m_bulletsPerBurst > 1)
                    {
                        StartCoroutine(BurstShot(m_bullet, 10));
                    }
                }
                else if (isAbleToShoot && m_firingRateTimer >= 0)
                {
                    m_firingRateTimer -= 1 * Time.deltaTime;
                }
            }

            else                                        //Funcionalidad cuando el disparo es normal.
            {
                m_firingRateTimer -= Time.deltaTime;

                if (m_firingRateTimer <= 0)
                {
                    m_firingRate = Random.Range(m_onStartFiringRate - m_randomFiringRateDeviation, m_onStartFiringRate + m_randomFiringRateDeviation);
                    m_firingRateTimer = m_firingRate;

                    InstantiateBullet(m_bullet, 10);

                    if (thisEnemy_SetControl is ShockerSetControl) //Desactivar el aura del shocker al disparar
                    {
                        ShockerSetControl shockerSetControl = (ShockerSetControl)thisEnemy_SetControl;
                        shockerSetControl.DeactivateShockAura_ElectricBack();
                    }
                }
            }
        }

        else //Es lanzallamas o lanzapartículas
        {
            if (instantiatedParticleThrower == null)    //Se spawnea el Lanzallamas o lanzaParticulas con layer de ThrowerEnemy si no está en escena.
            {
                SpawnThrowerPrefab(22);
            }

            else if(!enableThrowerOnce)
            {
                EnableThrower(22);  //Si está en escena se activa
            }
        }
    }
   
    void PlayerShoot()
    {      
        if (!isParticle_Thrower) //Funcionalidad de disparar balas si no es lanza-algo (lanzallamas...)
        {
            if (isChargedShot || m_bulletsPerBurst > 1)     //Funcionalidad cuando el disparo es cargado o de ráfagas.
            {
                if (isAbleToShoot && player_firingRateTimer <= 0)
                {                                                                                                    //MULTIPLIER UPGRADE
                    player_firingRateTimer = player_firingRate - GameManager.Instance.playerShootRatioMultiplier;    //Se aplica el multiplicador de Ratio del GameManager

                    if (isChargedShot)
                    {
                        StartCoroutine(ChargedShot(m_bullet, 12, timetoCharge));
                    }
                    else if (m_bulletsPerBurst > 1)
                    {
                        StartCoroutine(BurstShot(m_bullet, 12));
                    }                                                                                                                     
                }
                else if (isAbleToShoot && player_firingRate >= 0)
                {
                    player_firingRateTimer -= 1 * Time.deltaTime;
                }
            }

            else                                              //Funcionalidad cuando el disparo es normal.
            {
                if (player_firingRateTimer <= 0) 
                {
                    InstantiateBullet(m_bullet, 12);                                                                 //MULTIPLIER UPGRADE
                    player_firingRateTimer = player_firingRate - GameManager.Instance.playerShootRatioMultiplier;    //Se aplica el multiplicador de Ratio del GameManager
                }
                else
                {
                    player_firingRateTimer -= 1 * Time.deltaTime;
                }
            }
        }

        else //Es lanzallamas o lanzapartículas
        {
            if (instantiatedParticleThrower == null)    //Se spawnea el Lanzallamas o lanzaParticulas con layer de ThrowerPlayer si no está en escena
            {
                SpawnThrowerPrefab(23);
            }

            else if(!enableThrowerOnce) //Si está en escena se activa
            { 
                EnableThrower(23);
            }
        }
    }

    IEnumerator BurstShot(GameObject bullet, int layer)
    {
        isAbleToShoot = false;

        for (int i = 0; i < m_bulletsPerBurst; i++)
        {
            InstantiateBullet(bullet,layer);
            yield return new WaitForSeconds(m_burtFiringRate);

            if(i == m_bulletsPerBurst - 1)
            {
                isAbleToShoot = true;
            }
        }
    }
    private Color laserGreen = new Color(0, 1, 0);
    private Color laserRed = new Color(1, 0, 0);
    IEnumerator ChargedShot(GameObject bullet, int layer, float chargeTime)
    {
        isAbleToShoot = false;
        bool grenadeThrowed = false;

        sniperLaserMask = GetPhysicsLayerMask(layer); //coger la Layermask de colisiones de la layer de la bala a instanciar para el laser de sniper

        for(float chargeTimeLeft = chargeTime; chargeTimeLeft > 0; chargeTimeLeft -= Time.deltaTime) //Sustituido WaitForSeconds por este bucle for
        {
            if(isEnemySniper && sniperLaser != null)    //Si es enemigo sniper, se ejecuta el laser de sniper mientras dura el wait(for), si no solo se espera
            {
                SimulateLineRenderereBounces();
                m_animationVariables.SetIsAimingBool(true);

                if((chargeTimeLeft <= (0.5 * chargeTime) && chargeTimeLeft > (0.4 * chargeTime)) || (chargeTimeLeft <= (0.3 * chargeTime) && chargeTimeLeft < (0.2 * chargeTime))) //Porcentajes de tiempo restante donde cambiar color del laser
                {
                    sniperLaser.startColor = laserRed;
                    sniperLaser.endColor = laserRed;
                }
                else
                {
                    sniperLaser.startColor = laserGreen;
                    sniperLaser.endColor = laserGreen;
                }
            }

            else if(isEnemyGrenadier && !grenadeThrowed)
            {
                m_animationVariables.SetShootTrigger();
                grenadeThrowed = true;
            }
            yield return null;
        }

        if(isEnemySniper && sniperLaser != null)    //Al acabar la espera de carga, si es sniper se desactiva el laser
        {
            laserReflections = 1;
            sniperLaser.positionCount = laserReflections;
            sniperLaser.SetPosition(0, m_shootingPos.position);
            m_animationVariables.SetIsAimingBool(false);
        }

        InstantiateBullet(bullet, layer);
        isAbleToShoot = true;
        m_animationVariables.SetIsAimingBool(false);

    }
    void InstantiateBullet(GameObject bullet,int layer)
    {
        //Angle calculations
        float angleDiference = m_shootingAngle / m_bulletsToInstantiateAtOnce;
        int subdivisions = Mathf.FloorToInt(m_bulletsToInstantiateAtOnce / 2);
        float initialShootingAngle = -angleDiference * subdivisions;
       
        for (int i = 0; i < m_bulletsToInstantiateAtOnce; i++)
        {
            float randomAngleDispersion = Random.Range(-m_dispersionAngle, m_dispersionAngle);
            GameObject obj = Instantiate(bullet, m_shootingPos.position, m_shootingPos.rotation * Quaternion.AngleAxis(randomAngleDispersion + initialShootingAngle + i * angleDiference, Vector3.up));

            if (!m_killAll)
            {
                obj.layer = layer; ////Se le pone a la bala la layer correspondiente
            }
        }

        if (!isEnemyGrenadier)
        {
            m_animationVariables.SetIsAimingBool(false);
            m_animationVariables.SetShootTrigger();
            
        }
        AlertEnemies();
        ShootingEffects();
    }

    /// 
    /// //////////////////////////// Simulación de rebotes de LineRendeer ////////////////////
    ///

    void SimulateLineRenderereBounces() //Simulación de raycast y rebotes del LineRenderer laser de los Sniper y Railgun
    {
        //Clampear el numero de rebotes entre 0 y la capacidad del tipo numérico int
        laserReflections = Mathf.Clamp(laserReflections, 1, laserReflections);
        //Castear nuevo rayo hacia la derecha del transform
        ray = new Ray(m_shootingPos.position, m_shootingPos.right);

        //Que el numero de puntos del LineRenderer sea el mismo que el numero de rebotes
        sniperLaserPoints = laserReflections;
        //Aplicar numero de puntos al LineRenderer
        sniperLaser.positionCount = sniperLaserPoints;
        //Colocar posición 0 del LineRenderer en el objeto origen
        sniperLaser.SetPosition(0, m_shootingPos.position);

        for (int i = 0; i < laserReflections; i++)
        {
            //Comprobar si el rayo ha colisionado
            if (Physics.Raycast(ray.origin, ray.direction, out hit, 100, sniperLaserMask))
            {
                if (hit.collider.gameObject.TryGetComponent(out ShieldScript shield)) //Si la colisión es un escudo, rebota
                {
                    //La dirección de rebote es la reflejada entre la dirección del rayo y la normal del punto de impacto proyectada en el plano XZ, para que no tenga valor en Y que pueda surgir de la normal
                    inDirection = Vector3.ProjectOnPlane(Vector3.Reflect(ray.direction, hit.normal), Vector3.up);
                    //Lanzar el rayo reflejado, usando el punto deimpacto anterior y la dirección reflejada
                    ray = new Ray(hit.point, inDirection);

                    //Se aumenta la capacidad de rebote
                    laserReflections += 1;
                }

                //Añadir un nuevo punto al LineRenderer 
                sniperLaser.positionCount = ++sniperLaserPoints;

                //Colocar la siguiente posición del lineRenderer en el hit.point
                sniperLaser.SetPosition(i + 1, hit.point);
            }          
        }

        laserReflections = 1;
    }

    /// 
    /// /////////////////////////////////////////////////////////////////
    ///

    void AlertEnemies()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, m_weaponHearingDistance, m_enemyLayer);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            EnemyAI_Standard enemyAI = hitColliders[i].GetComponent<EnemyAI_Standard>();
            if (enemyAI != null && enemyAI.currentAIState == EnemyAI_Standard.AIState.idle)
            {
                enemyAI.ExternalDetectPlayer();
            }
        }
    }
    public void ResetShootingVariables() //Para el Possess y Unpossess de SetControl y StartElectricState en la IA
    {
        player_firingRateTimer = 0;
        m_firingRateTimer = 0;
        isAbleToShoot = true;

        if(isEnemySniper && sniperLaser != null)
        {
            sniperLaser.startColor = laserGreen;
            sniperLaser.endColor = laserGreen;
            laserReflections = 1;
            sniperLaser.positionCount = laserReflections;
            sniperLaser.SetPosition(0, m_shootingPos.position);
            m_animationVariables.SetIsAimingBool(false);
        }
    }
    public void ResetPlayerFiringRateTimer() //Método que se llama desde EnemyController si no hay input de RightTrigger
    {
        if (player_firingRateTimer <= 0)
        {
            player_firingRateTimer = 0;
        }

        else if(isAbleToShoot) //Restar siempre que sea capaz de disparar, no afecta al disparo normal y no produce problemas a los cargados y las ráfagas
        {
            player_firingRateTimer -= 1 * Time.deltaTime;
        }

        if (isParticle_Thrower && instantiatedParticleThrower != null && !disabledThrowerOnce)
        {
            DisableThrower();   // Desactivar lanzapartículas en el Reset
        }
    }
    public void ResetEnemyFiringRateTimer()
    {
        m_firingRateTimer = 0;

        if(isParticle_Thrower && instantiatedParticleThrower != null && !disabledThrowerOnce)
        {
            DisableThrower();   // Desactivar lanzapartículas en el Reset
        }
    }
 
    void ShootingEffects()
    {
        if (shootPart != null)
        {
            Instantiate(shootPart, m_shootingPos.position, m_shootingPos.rotation);
        }

        this_enemySound.PlayFiringSound();                                                     ///Sonido
    }

    public bool IsEnemyAbleToShoot() //Devuelve el valor de isAbleToShoot para fuera del script
    {
        return isAbleToShoot;
    }
    public bool IsEnemyASniper()    //Devuelve el valor de isEnemySniper para fuera del script
    {
        return isEnemySniper;
    }

    /////////////// FUNCIONES DE LOS LANZA_PARTÍCULAS (LANZALLAMAS...) /////////////////////
    ///

    private void SpawnThrowerPrefab(int instanceLayer)
    {
        disabledThrowerOnce = false;
        instantiatedParticleThrower = Instantiate(particleThrowerPrefab, m_shootingPos.position, m_shootingPos.rotation);
        instantiatedParticleThrower.transform.SetParent(m_shootingPos);     //Se coloca el lanzapartículas como hijo de la posición de disparo
        instantiatedParticleThrower.layer = instanceLayer;

        thisEnemy_Flamethrower = instantiatedParticleThrower.GetComponent<Flamethrower>();
        particleCollider = instantiatedParticleThrower.GetComponent<Collider>();
    }

    private void EnableThrower(int changedLayer) //Activar Lanzapartículas
    {
        enableThrowerOnce = true;
        disabledThrowerOnce = false;

        particle_ThrowerLight.enabled = true;
        instantiatedParticleThrower.layer = changedLayer;  //Cambiar la layer al Poseer/Desposeer
        m_animationVariables.SetShootTrigger();

        instantiatedParticleThrower.GetComponent<ParticleSystem>().Play();

        particleCollider.enabled = true;

        //Sonidos
        FlamethrowerSounds myFlameSound = this_enemySound as FlamethrowerSounds;
        if (myFlameSound != null)
        {
            myFlameSound.PlayFiringSound();
        }
    }

    public void DisableThrower()  //Desactivar Lanzapartículas
    {
        enableThrowerOnce = false;
        disabledThrowerOnce = true;
        particle_ThrowerLight.enabled = false;
        thisEnemy_Flamethrower.FlamethrowerStopped(); //Se llama a esta función si se deja de disparar el lanzallamas
        instantiatedParticleThrower.GetComponent<ParticleSystem>().Stop();

        particleCollider.enabled = false;
        m_animationVariables.SetShootTrigger();

        //Sonidos
        FlamethrowerSounds myFlameSound = this_enemySound as FlamethrowerSounds;
        if (myFlameSound != null)
        {
            myFlameSound.StopFireAudio();
        }
    }

    /////////////SETEO DE VARIABLES DE ANIMACIONES////////////////
    ///
    [Header("Multiplicador para la animación de disparo")]
    [SerializeField] float shootMultiplier;
    void OnUpdateSetAnimatorVariables() //Setea el shootmultiplier del animator del objeto dependiente de la cadencia de disparo
    {
        m_animationVariables.SetShootMultiplier(shootMultiplier);
    }
}
