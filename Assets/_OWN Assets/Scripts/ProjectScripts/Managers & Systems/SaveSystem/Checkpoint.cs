﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.XInput;

public class Checkpoint : MonoBehaviour
{
    [SerializeField] Sprite xboxSavePrompt, playStationSavePromt, keyboardSavePrompt;
    private SpriteRenderer promptRenderer;

    private bool playerInTrigger = false;
    Collider collisionObject;

    [Header ("Variables para cooldown tras guardar, y no poder guardar seguidamente")]
    private static bool systemAbleToSave = true;
    private static bool gameSceneLoaded = true;
    private static float cooldownTimer = 0;

    [SerializeField] Transform respawnTransform;

    private void Start()
    {
        systemAbleToSave = true;
        gameSceneLoaded = true;
        cooldownTimer = 0;
        promptRenderer = GetComponentInChildren<SpriteRenderer>();
        promptRenderer.enabled = false;
    }

    void ShowSaveButtonPrompts()
    {
        promptRenderer.enabled = true;

        if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Keyboard & Mouse")
        {
            promptRenderer.sprite = keyboardSavePrompt;
        }

        else if(InputManager.Instance.m_myPlayerInput.currentControlScheme == "Gamepad")
        {
            if(Gamepad.current == DualShockGamepad.current && Gamepad.current != null)
            {
                promptRenderer.sprite = playStationSavePromt;
            }
            else if(Gamepad.current == XInputControllerWindows.current && Gamepad.current != null)
            {
                promptRenderer.sprite = xboxSavePrompt;
            }
        }
    }

    private void Update()
    {
        if(playerInTrigger)
        {
            if (InputManager.Instance.ActionButtonTriggered() && systemAbleToSave) //Botón de acción para guardar
            {
                ///// LOGRO ///////
                SteamManager.Instance.UnlockAchievement("SAVE_FIRST_TIME");
                /////
                
                respawnTransform = collisionObject.gameObject.transform;
                GameManager.Instance.SaveGame(respawnTransform.position); ////Guardar el juego al usar el Checkpoint
                HealthHeartsVisual.healthHeartsSystemStatic.Heal(100);

                StartCoroutine(LoadActualGameAsyncScene());
            }
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.GetComponent <PlayerControl_MovementController>() != null)
        {
            collisionObject = collision;

            if (systemAbleToSave)
            {
                ShowSaveButtonPrompts();
                playerInTrigger = true;
            }

            else
            {
                promptRenderer.enabled = false;
                playerInTrigger = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerControl_MovementController>() != null)
        {
            promptRenderer.enabled = false;
            playerInTrigger = false;
        }
    }

    public static bool IsGameSceneLoaded()
    {
        return gameSceneLoaded;
    }

    public static IEnumerator LoadActualGameAsyncScene()
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.
        systemAbleToSave = false;
        gameSceneLoaded = false;
        InputManager.Instance.PauseGameInputs();    //Detener los inputs de jugador mientras dure la carga
        yield return new WaitForSeconds(0.1f);

        RoomManager.ActivateClearQueuesEvent(); //Vaciar Queues de Decals y Dummies de las Rooms
        SceneManager.UnloadSceneAsync(ZoneManager.Instance.zoneScene);

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(ZoneManager.Instance.zoneScene, LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

       if (asyncLoad.isDone)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);

                if(!scene.name.Contains("Permanent") && !scene.name.Contains("Save"))
                {
                    SceneManager.SetActiveScene(scene);
                }
            }
        }
        InputManager.Instance.UnpauseGameInputs();  //Reanudar Inputs del jugador
        GameManager.Instance.OnLevelLoad(true);
        gameSceneLoaded = true;
        yield return new WaitUntil(() => SaveCoolDown());
    }

    static bool SaveCoolDown()
    {
        if (cooldownTimer < 1)
        {
            cooldownTimer += Time.deltaTime;
            return false;
        }

        else
        {
            systemAbleToSave = true;
            cooldownTimer = 0;
            return true;
        }
    }
}
