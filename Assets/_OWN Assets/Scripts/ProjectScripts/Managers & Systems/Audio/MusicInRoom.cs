﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MusicInRoom : MonoBehaviour
{
    RoomManager thisRoomManager;
    string musicInRoom;
    string combatMusic = AppSounds.ZONA1_C_MUSIC;
    bool isPlayerInCombat;

    // Start is called before the first frame update
    void Start()
    {
        thisRoomManager = GetComponent<RoomManager>();
        SetSceneCombatMusicOnStart();
        SetSceneMusicOnStart();
    }

    private void Update()
    {
        if (thisRoomManager.isRoomActive)
        {
            SetToCombatMusic();
        }
    }

    void SetSceneCombatMusicOnStart()
    {
        string activeSceneName = GameManager.Instance.GetActiveSceneName();

        if (activeSceneName.Contains("2"))
        {
            combatMusic = AppSounds.ZONA2_C_MUSIC;
        }
        else if (activeSceneName.Contains("3"))
        {
            combatMusic = AppSounds.ZONA3_C_MUSIC;
        }
        else
        {
            combatMusic = AppSounds.ZONA1_C_MUSIC;
        }
    }
    void SetSceneMusicOnStart()
    {
        string sceneName = thisRoomManager.gameObject.scene.name;
        if (sceneName.Contains("Save"))
        {
            if (sceneName.Contains("2"))
            {
                musicInRoom = AppSounds.ZONA2_B_MUSIC;
            }
            else if (sceneName.Contains("3"))
            {
                musicInRoom = AppSounds.ZONA3_B_MUSIC;
            }
            else
            {
                musicInRoom = AppSounds.ZONA1_B_MUSIC;
            }
        }
        else
        {
            if (sceneName.Contains("2"))
            {
                musicInRoom = AppSounds.ZONA2_N_MUSIC;
            }
            else if (sceneName.Contains("3"))
            {
                musicInRoom = AppSounds.ZONA3_N_MUSIC;
            }
            else
            {
                musicInRoom = AppSounds.ZONA1_N_MUSIC;
            }
        }
    }
    protected virtual void SetToCombatMusic()
    {
        bool activeAttackingEnemy = IsThereAnAttackingEnemy();
        if (!isPlayerInCombat && activeAttackingEnemy)
        {
            MusicManager.Instance.PlayBackgroundMusic(combatMusic);
            isPlayerInCombat = true;
        }
        if(isPlayerInCombat && !activeAttackingEnemy)
        {
            MusicManager.Instance.PlayBackgroundMusic(musicInRoom);
            isPlayerInCombat = false;
        }
    }

    public void StartMusicOnRoomChange()
    {
        MusicManager.Instance.PlayBackgroundMusic(musicInRoom);
    }
    bool IsThereAnAttackingEnemy()
    {
        if (thisRoomManager.currentEnemiesInRoom.Count != 0)
        {
            foreach (EnemyControl_MovementController enemy in thisRoomManager.currentEnemiesInRoom)
            {
                if (enemy != GameManager.Instance.ActualPlayerController && enemy.GetComponent<EnemyAI_Healer>() == null && enemy.GetComponent<EnemyAI_Standard>().currentAIState != EnemyAI_Standard.AIState.idle)
                {
                    return true;
                }
            }
        }
        return false;
    }
}