﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddAudioSourceToMainList : MonoBehaviour
{
    void Start()
    {
        AudioSource audioSourceToAdd = GetComponent<AudioSource>();
        if (audioSourceToAdd != null)
        {
            MusicManager.Instance.AddAudioSourceToPauseList(audioSourceToAdd);
        }
    }
}
