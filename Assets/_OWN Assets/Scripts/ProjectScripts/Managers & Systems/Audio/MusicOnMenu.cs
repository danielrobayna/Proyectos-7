﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicOnMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MusicManager.Instance.PlayBackgroundMusic(AppSounds.MAIN_TITLE_MUSIC);
    }
}
