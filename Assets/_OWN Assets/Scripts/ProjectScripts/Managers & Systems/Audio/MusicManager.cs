﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : PersistentSingleton<MusicManager>
{
    bool isOnpause;
    AudioMixer m_audioMixer;
    AudioMixerGroup m_effectsGroup;
    AudioMixerGroup m_musicGroup;

    private float m_musicMixerVolume;

    public float  MusicVolume
    {
        get
        {
            return m_musicMixerVolume;
        }

        set
        {
            value = Mathf.Clamp(value, 0, 1);
            m_musicMixerVolume = value;
            m_audioMixer.SetFloat("MusicVolume", Mathf.Log10(m_musicMixerVolume) * 20);
        }
    }
   
    public float  MusicVolumeSave
    {
        get
        {
            return m_musicMixerVolume;
        }

        set
        {
            value = Mathf.Clamp(value, 0, 1);
            PlayerPrefs.SetFloat(AppPlayerPrefKeys.MUSIC_VOLUME, value);
            m_musicMixerVolume = value;
            m_audioMixer.SetFloat("MusicVolume", Mathf.Log10(m_musicMixerVolume) * 20);
        }
    }

    private float m_sfxMixerVolume;

    public float  SfxVolume
    {
        get
        {
            return m_sfxMixerVolume;
        }
        set
        {
            value = Mathf.Clamp(value, 0, 1);
            m_sfxMixerVolume = value;
            m_audioMixer.SetFloat("EffectsVolume", Mathf.Log10(m_sfxMixerVolume) * 20);
        }
    }

    public float  SfxVolumeSave
    {
        get
        {
            return m_sfxMixerVolume;
        }

        set
        {
            value = Mathf.Clamp(value, 0, 1);
            PlayerPrefs.SetFloat(AppPlayerPrefKeys.SFX_VOLUME, value);
            m_sfxMixerVolume = value;
            m_audioMixer.SetFloat("EffectsVolume", Mathf.Log10(m_sfxMixerVolume) * 20);
        }
    }
 
    public  override void Awake                ()
	{
        base.Awake();

        m_audioMixer = Resources.Load<AudioMixer>(AppPaths.PATH_AUDIOMIXER);
        m_effectsGroup = m_audioMixer.FindMatchingGroups("Effects")[0];
        m_musicGroup = m_audioMixer.FindMatchingGroups("Music")[0];

        m_soundFXDictionary      = new Dictionary<string, AudioClip>();
        m_soundMusicDictionary   = new Dictionary<string, AudioClip>();

        m_backgroundMusic = CreateAudioSource("Music", true);
        m_backgroundMusic.outputAudioMixerGroup = m_musicGroup;
        AddLowPassFilter(m_backgroundMusic);

        m_musicFadeInAudioSource = CreateAudioSource("FadeInMusic", true);
        m_musicFadeInAudioSource.outputAudioMixerGroup = m_musicGroup;
        AddLowPassFilter(m_musicFadeInAudioSource);

        MusicVolume = PlayerPrefs.GetFloat(AppPlayerPrefKeys.MUSIC_VOLUME, 0.75f);
        SfxVolume          = PlayerPrefs.GetFloat(AppPlayerPrefKeys.SFX_VOLUME  , 0.75f);

        AudioClip[] audioSfxVector   = Resources.LoadAll<AudioClip>(AppPaths.PATH_RESOURCE_SFX);

        for (int i = 0; i < audioSfxVector.Length; i++)
        {
            m_soundFXDictionary.Add(audioSfxVector[i].name, audioSfxVector[i]);
        }

        audioSfxVector   = Resources.LoadAll<AudioClip>(AppPaths.PATH_RESOURCE_MUSIC);

        for (int i = 0; i < audioSfxVector.Length; i++)
        {
            m_soundMusicDictionary.Add(audioSfxVector[i].name, audioSfxVector[i]);
        }
	}
       
    public  void PlayBackgroundMusic (string audioName)
	{
        if(m_backgroundMusic != null && m_backgroundMusic.clip != null) //Comprobar que el clip no sea nulo
        {
            bool activeMusicIsEqual = audioName == m_backgroundMusic.clip.name;

            //Condiciones para cambiar de música
            if (!activeMusicIsEqual)
            {
                StopAllCoroutines();
                StartCoroutine(FadeInCoroutine(m_soundMusicDictionary[audioName]));
            }else if(m_musicFadeInAudioSource.clip != null)
            {
                bool fadeMusicIsEqual = audioName == m_musicFadeInAudioSource.clip.name;
                if (!fadeMusicIsEqual)
                {
                    StopAllCoroutines();
                    StartCoroutine(StopFadeInCoroutine());
                }
            }
        }else if (m_soundMusicDictionary.ContainsKey(audioName)) //Si el activo es nulo, no hay música sonando, reproducir directamente
        {
            m_backgroundMusic.clip = m_soundMusicDictionary[audioName];
            m_backgroundMusic.volume = 1;
            m_backgroundMusic.Play();
        }
	}
    float timeToFadeIn = 1;
    float timeToFadeInLeft;
    IEnumerator StopFadeInCoroutine()
    {
        float volumeDiference = m_musicFadeInAudioSource.volume / timeToFadeInLeft;

        float oldVolumeDiference = 1 / timeToFadeInLeft;

        while (m_backgroundMusic.volume < 1 || m_musicFadeInAudioSource.volume > 0)
        {
            m_musicFadeInAudioSource.volume -= volumeDiference * Time.deltaTime;
            m_musicFadeInAudioSource.volume = Mathf.Clamp(m_musicFadeInAudioSource.volume, 0, 1);

            m_backgroundMusic.volume += oldVolumeDiference * Time.deltaTime;
            m_backgroundMusic.volume = Mathf.Clamp(m_backgroundMusic.volume, 0, 1);

            yield return null;
        }

        m_musicFadeInAudioSource.Stop();
        m_musicFadeInAudioSource.clip = null;
    }
    IEnumerator FadeInCoroutine(AudioClip newAudio)
    {
        //Start fade
        m_musicFadeInAudioSource.clip = newAudio;
        m_musicFadeInAudioSource.volume = 0;
        m_musicFadeInAudioSource.Play();

        //Detectar cuánto debe variar el volumen
        float volumeDiference = 1 / timeToFadeIn; 
        float oldVolumeDiference = m_backgroundMusic.volume / timeToFadeIn;

        timeToFadeInLeft = timeToFadeIn;

        while (m_musicFadeInAudioSource.volume < 1 || m_backgroundMusic.volume > 0)
        {
            timeToFadeInLeft -= Time.deltaTime;
            timeToFadeInLeft = Mathf.Clamp(timeToFadeInLeft, 0, timeToFadeIn);

            m_musicFadeInAudioSource.volume += volumeDiference * Time.deltaTime;
            m_musicFadeInAudioSource.volume = Mathf.Clamp(m_musicFadeInAudioSource.volume, 0, 1);

            m_backgroundMusic.volume -= oldVolumeDiference * Time.deltaTime;
            m_backgroundMusic.volume = Mathf.Clamp(m_backgroundMusic.volume, 0, 1);

            yield return null;
        }

        //Seteo finalmente el fade in como actual
        m_backgroundMusic.clip = m_musicFadeInAudioSource.clip;
        m_backgroundMusic.volume = 1;
        m_backgroundMusic.Play();
        m_backgroundMusic.time = m_musicFadeInAudioSource.time;

        m_musicFadeInAudioSource.Stop();
        m_musicFadeInAudioSource.clip = null;

    }

    public AudioSource PlayLoopControlledSound(string audioName)
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, true);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
            return m_sfxSound;
        }
        else
        {
            return null;
        }
    }
    public void PlaySound (string audioName) //Sonido 2D
	{
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, false);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
        }
    }
    public void PlaySound (string audioName, float customVolume) //Sonido 2D
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, false);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = customVolume;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
        }
    }
    public void PlaySound(string audioName, float minDistance, float maxDistance, Transform soundTransform) //Sonido 3D
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, false, soundTransform);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.minDistance = minDistance;
            m_sfxSound.maxDistance = maxDistance;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);

        }
    }
    public void PlaySound(string audioName, Transform soundTransform) //Sonido 3D con distancias default (las puestas en create Audio Source)
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, false, soundTransform);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
        }
    }
    public void PlaySound(string audioName, float customVolume, float minDistance, float maxDistance, Transform soundTransform) //Sonido 3D con elección de volumen
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, false, soundTransform);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = customVolume;
            m_sfxSound.minDistance = minDistance;
            m_sfxSound.maxDistance = maxDistance;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
        }
    }
    public AudioSource PlayLoopControlledSound(string audioName, Transform soundTransform)
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, true, soundTransform);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
            return m_sfxSound;
        }
        else
        {
            return null;
        }
    }
    public AudioSource PlayLoopControlledSound(string audioName, float minDistance, float maxDistance, Transform soundTransform)
    {
        if (m_soundFXDictionary.ContainsKey(audioName))
        {
            AudioSource m_sfxSound = CreateAudioSource(audioName, true, soundTransform);
            m_sfxSound.outputAudioMixerGroup = m_effectsGroup;
            m_sfxSound.clip = m_soundFXDictionary[audioName];
            m_sfxSound.volume = 1;
            m_sfxSound.minDistance = minDistance;
            m_sfxSound.maxDistance = maxDistance;
            m_sfxSound.Play();
            m_sfxAudioSources.Add(m_sfxSound);
            return m_sfxSound;
        }
        else
        {
            return null;
        }
    }
    public  void         StopBackgroundMusic   ()
	{
        if (m_backgroundMusic != null)
        {
            m_backgroundMusic.Stop();
            m_backgroundMusic.clip = null;
        }
	}	                     
    public  void         PauseBackgroundMusic  ()
	{
		if (m_backgroundMusic != null)
			m_backgroundMusic.Pause();
	}

    private AudioSource  CreateAudioSource     (string name, bool isLoop)
    {
        GameObject temporaryAudioHost         = new GameObject(name);
        AudioSource audioSource               = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;  
		audioSource.playOnAwake               = false;
        audioSource.loop                      = isLoop;
        audioSource.spatialBlend              = 0.0f;
        temporaryAudioHost.transform.SetParent(this.transform);
        return audioSource;
    }
    private AudioSource CreateAudioSource(string name, bool isLoop, Transform soundTransform)
    {
        if (GameManager.Instance.ActualPlayerController.transform == soundTransform.transform)//Comprobación si es el player para crear sonido en 2D
        {
            return CreateAudioSource(name, isLoop);
        }
        else
        {
            GameObject temporaryAudioHost = new GameObject(name);
            AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource;
            audioSource.playOnAwake = false;
            audioSource.loop = isLoop;

            //Colocación en 3D
            temporaryAudioHost.transform.position = soundTransform.position;
            temporaryAudioHost.transform.SetParent(soundTransform);
            audioSource.spatialBlend = 1f;
            audioSource.spread = 180f; //Esto quita el paneo en 3D, no hace falta, ya que da problemas debido a que el audio listener está en el followplayer, y además al ser con vista desde arriba, lía un poco.
            audioSource.minDistance = 4f;
            audioSource.maxDistance = 15f;
            audioSource.rolloffMode = AudioRolloffMode.Linear;

            return audioSource;
        }
    }
    private void Update()
    {
        if (!isOnpause)
        {
            int audioSourcesInScene = m_sfxAudioSources.Count;
            List<AudioSource> audioSourcesToDestroy = new List<AudioSource>();
            for (int i = 0; i < audioSourcesInScene; i++)
            {
                AudioSource actualAudioSource = m_sfxAudioSources[i];
                if (actualAudioSource != null && !actualAudioSource.isPlaying)
                {
                    audioSourcesToDestroy.Add(actualAudioSource);
                }
            }
            foreach (AudioSource actualAudioSourceToDestroy in audioSourcesToDestroy)
            {
                m_sfxAudioSources.Remove(actualAudioSourceToDestroy);
                Destroy(actualAudioSourceToDestroy.gameObject);
            }
        }
    }

    public void PauseAllAudiosOnGamePause()
    {
        isOnpause = true;
        foreach (AudioSource localAudioSource in m_sfxAudioSources) //Esto se necesita ya que la lista sirve para bajar el volumen de los audiosources activos (en lugar de usar destroy)
        {
            if (localAudioSource != null)
            {
                localAudioSource.Pause();
            }
        }
        foreach(AudioSource worldAudioSource in m_sfxWorldAudioSourcesToPause)
        {
            if (worldAudioSource != null)
            {
                worldAudioSource.Pause();
            }
        }
    }
    public void ResumeAudioOnGameResume()
    {
        foreach (AudioSource localAudioSource in m_sfxAudioSources) //Esto se necesita ya que la lista sirve para bajar el volumen de los audiosources activos (en lugar de usar destroy)
        {
            if (localAudioSource != null)
            {
                localAudioSource.UnPause();
            }
        }
        foreach (AudioSource worldAudioSource in m_sfxWorldAudioSourcesToPause)
        {
            if (worldAudioSource != null)
            {
                worldAudioSource.UnPause();
            }
        }
        isOnpause = false;
    }

    void AddLowPassFilter(AudioSource audioSource)
    {
        AudioLowPassFilter filter = audioSource.gameObject.AddComponent<AudioLowPassFilter>();
        filter.cutoffFrequency = 1200f;
        filter.enabled = false;
    }

    float backgroundVolumeOnPause;
    float fadeInVolumeOnPause;
    public void SetLowPassFilter(bool isActive)
    {
        if (isActive)
        {
            m_backgroundMusic.GetComponent<AudioLowPassFilter>().enabled = true;
            m_musicFadeInAudioSource.GetComponent<AudioLowPassFilter>().enabled = true;

            backgroundVolumeOnPause = m_backgroundMusic.volume;
            m_backgroundMusic.volume /= 2;

            fadeInVolumeOnPause = m_musicFadeInAudioSource.volume;
            m_musicFadeInAudioSource.volume /= 2;
        }
        else
        {
            m_backgroundMusic.GetComponent<AudioLowPassFilter>().enabled = false;
            m_musicFadeInAudioSource.GetComponent<AudioLowPassFilter>().enabled = false;

            m_backgroundMusic.volume = backgroundVolumeOnPause;
            m_musicFadeInAudioSource.volume = fadeInVolumeOnPause;
        }
    }

    private Dictionary<string, AudioClip> m_soundFXDictionary    = null;
    private Dictionary<string, AudioClip> m_soundMusicDictionary = null;
    private AudioSource                   m_backgroundMusic      = null;
    private AudioSource m_musicFadeInAudioSource = null;
    List<AudioSource> m_sfxAudioSources = new List<AudioSource>();
    List<AudioSource> m_sfxWorldAudioSourcesToPause = new List<AudioSource>();
    public void AddAudioSourceToPauseList(AudioSource newAudioSource)
    {
        if (!m_sfxWorldAudioSourcesToPause.Contains(newAudioSource))
        {
            m_sfxWorldAudioSourcesToPause.Add(newAudioSource);
        }
    }
    public Dictionary<string, AudioClip> GetMusicDictionary()
    {
        return m_soundMusicDictionary;
    }
}