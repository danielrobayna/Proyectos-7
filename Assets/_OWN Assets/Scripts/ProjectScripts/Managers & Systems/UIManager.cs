﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Timeline;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Playables;

public class UIManager : TemporalSingleton<UIManager>
{
    [Header("Variables del mapa")]
    public MapBehaviour mapBehaviour;
    [SerializeField] GameObject mapObject;
    [SerializeField] GameObject moveScrollView;
    [SerializeField] RectTransform roomsMapRectTransform;
    [SerializeField] MoveScrollView myScrollView;
    [Header("Variables de pausa")]
    public GameObject pauseObject;
    [SerializeField] GameObject firstPauseSelectedButton;

    [Header("Animator de fade al cambiar de habitación")]
    [SerializeField] Animator m_fadeAnimation;

    [Header("Variables del minimapa")]
    [SerializeField] private Animator m_minimapArrow;
    public Animator minimapArrow {
        get {
            return m_minimapArrow;
        }
    }

    [Header("Barra de vida del enemigo")]
    [SerializeField] private GameObject enemyHealthUIObject;
    [SerializeField] Animator enemyHealthUIAnimator;
    int hastToBeInViewBoolID;

    [Header("Texto de Guardado al guardar")]
    [SerializeField] private Animator savingTextAnimator;
    int savingTextAnimTriggerID;

    OptionsMenuController m_optionsMenuController;
    public override void Awake()
    {
        base.Awake();
      
        hastToBeInViewBoolID = Animator.StringToHash("hasToBeInView"); //Convertir el bool de animator en int MUCHO MAS EFICIENTE
        savingTextAnimTriggerID = Animator.StringToHash("showSavingText");
        m_optionsMenuController = GetComponent<OptionsMenuController>();
    }

    void Update()
    {
        if (InputManager.Instance.MapButtonTriggered() && !pauseObject.activeSelf)
        {
            StartCoroutine(UseMapButton());
        }
        if (InputManager.Instance.PauseButtonTriggered())
        {
            UsePauseButton();
        }
        if (GameManager.Instance.ActualPlayerController.GetComponent<EnemySetControl>() != null && !enemyHealthUIAnimator.GetBool(hastToBeInViewBoolID))
        {
            SetActiveEnemyHealthUI();
        }
        SetLowLifeBlood();    
    }
    public IEnumerator UseMapButton()
    {
        if (!mapObject.activeSelf && CutsceneManager.Instance.director.state != PlayState.Playing) //Pausar el juego si el canvas del mapa está inactivo
        {
            GameManager.Instance.PauseGame();

            mapObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(moveScrollView);

            yield return new WaitForEndOfFrame();

            //Centrado del mapa
            float xDistanceFromCenter = mapBehaviour.activeRoomImage.rectTransform.position.x - (Screen.width / 2);
            float yDistanceFromCenter = mapBehaviour.activeRoomImage.rectTransform.position.y - (Screen.height / 2);

            Vector3 correction = new Vector3(xDistanceFromCenter, yDistanceFromCenter, 0);
            myScrollView.content.position -= correction;
        }
        else //Quitar la pausa
        {
            GameManager.Instance.UnPauseGame();
            mapObject.SetActive(false);
        }
    }
    public void UsePauseButton()
    {
        if (!pauseObject.activeSelf && CutsceneManager.Instance.director.state != PlayState.Playing) //Pausar el juego si el canvas de la pausa está inactivo
        {
            mapObject.SetActive(false);
            GameManager.Instance.PauseGame();
            pauseObject.SetActive(true);
            SelectedObjectHandler.Instance.DetectIfSelectObject(firstPauseSelectedButton);
        }
        else //Quitar la pausa
        {
            GameManager.Instance.UnPauseGame();
            pauseObject.SetActive(false);
            m_optionsMenuController.DeactivateOptionsPanel();
        }
    }

    public void Fade()
    {
        m_fadeAnimation.SetTrigger("FadeTrigger");
    }
    public bool IsScreenOnBlack()
    {
        if (m_fadeAnimation.GetComponent<Image>().color.a >= 0.95f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Unfade()
    {
        m_fadeAnimation.SetTrigger("UnfadeTrigger");
    }
    public bool IsScreenTransparentAgain()
    {
        if (m_fadeAnimation.GetComponent<Image>().color.a <= 0.05f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
   
    /////////////Funcionalidad del texto "Saving" al guardar ///////////////

    public void ActivateSavingText()    //Llamando desde la funcionalidad de Guardado en GameManager
    {
        savingTextAnimator.SetTrigger(savingTextAnimTriggerID);
    }

    ////////////// Funciones de la Barra de Vida de los Enemigos ////////////

    Stack<GameObject> lifeElementsUI = new Stack<GameObject>();
    Stack<GameObject> emptyLifeElementsUI = new Stack<GameObject>();
    [Header("Fragmentos de vida del enemigo")]
    [SerializeField] GameObject enemyLifeUIElement;
    [SerializeField] GameObject enemyEmptyLifeUIElement;

    public void SetEnemyHealthUI(float enemyHealth, float maxEnemyHealth)
    {
        if (enemyHealth != lifeElementsUI.Count)
        {
            float enemyEmptyHealthDifference = maxEnemyHealth-enemyHealth;
           
            if (enemyHealth > lifeElementsUI.Count)
            {
                AddEnemyLifeAtUi(enemyHealth-lifeElementsUI.Count, emptyLifeElementsUI.Count - enemyEmptyHealthDifference);
            }
            else
            {
                RemoveEnemyLifeAtUI(lifeElementsUI.Count - enemyHealth, enemyEmptyHealthDifference - emptyLifeElementsUI.Count);
            }

            if (emptyLifeElementsUI.Count == 0) //Si no hay elementos vacíos instanciados, pon tantos como sean necesarios
            {
                for (int i = 0; i < enemyEmptyHealthDifference; i++)
                {
                    GameObject newEnemyEmptyLifeElement = Instantiate(enemyEmptyLifeUIElement);

                    newEnemyEmptyLifeElement.transform.SetParent(enemyHealthUIObject.transform, false);
                    emptyLifeElementsUI.Push(newEnemyEmptyLifeElement);
                }
            }
        }
    }

    public void AddEnemyLifeAtUi(float enemyHealthToAdd, float enemyEmptyHealthToRemove)
    {
        //Elementos llenos
        for(int i = 0; i < enemyHealthToAdd; i++)
        {
            GameObject newEnemyLifeElement = Instantiate(enemyLifeUIElement);

            newEnemyLifeElement.transform.SetParent(enemyHealthUIObject.transform, false);

            lifeElementsUI.Push(newEnemyLifeElement);
        }

        //Elementos vacíos
        if (emptyLifeElementsUI.Count != 0)
        {
            for (int i = 0; i < -enemyEmptyHealthToRemove; i++)
            {
                Destroy(emptyLifeElementsUI.Pop());
            }
        }     
    }
    public void RemoveEnemyLifeAtUI(float enemyHealthToRemove, float enemyEmptyHealthToAdd)
    {
        //Elementos llenos
        for(int i=0; i< enemyHealthToRemove; i++)
        {
            Destroy(lifeElementsUI.Pop());
        }

        //Elementos vacíos
        for (int i = 0; i < enemyEmptyHealthToAdd; i++)
        {
            GameObject newEnemyEmptyLifeElement = Instantiate(enemyEmptyLifeUIElement);

            newEnemyEmptyLifeElement.transform.SetParent(enemyHealthUIObject.transform, false);
            emptyLifeElementsUI.Push(newEnemyEmptyLifeElement);
        }   
    }
    public void SetActiveEnemyHealthUI() //Activado en enemysetcontrol
    {
        enemyHealthUIAnimator.SetBool(hastToBeInViewBoolID, true);
    }
    public void SetUnactiveEnemyHealthUI() // Desactivado en enemySetControl
    {
        int elementsToDestroy = lifeElementsUI.Count;
        for (int i =0; i < elementsToDestroy; i++)
        {
            Destroy(lifeElementsUI.Pop());
        }
        lifeElementsUI.Clear();

        elementsToDestroy = emptyLifeElementsUI.Count;
        for (int i = 0; i < elementsToDestroy; i++)
        {
            Destroy(emptyLifeElementsUI.Pop());
        }
        emptyLifeElementsUI.Clear();

        enemyHealthUIAnimator.SetBool(hastToBeInViewBoolID, false);
    }

    //Funcionalidad de sangre con vida baja
    [Header("Variables de vida baja del jugador")]
    [SerializeField] float lifesToStartBlood;
    [SerializeField] Animator lowLifeAnimator;

    void SetLowLifeBlood()
    {
        int totalLife = 0;
        foreach (HealthHeartsSystem.Heart actualHeart in HealthHeartsVisual.healthHeartsSystemStatic.GetHeartList())
        {
            totalLife += actualHeart.GetFragmentAmount();
        }
        if (totalLife < lifesToStartBlood)
        {
            lowLifeAnimator.SetBool("hasLowLife", true);
        }
        else
        {
            lowLifeAnimator.SetBool("hasLowLife", false);
        }
    }
    public void SetPlayerDeathEffects()
    {
        lowLifeAnimator.SetBool("hasLowLife", false);
        lowLifeAnimator.SetBool("PlayerIsDeath", true);

    }
    public void StopPlayerDeathEffects()
    {
       lowLifeAnimator.SetBool("PlayerIsDeath", false);
    }
}