﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraScript : MonoBehaviour
{
    CinemachineVirtualCamera m_camera;
    [SerializeField] BoxCollider roomCollider;
    FollowPlayer followPlayer;

    float correctionAngle;

    //Variables de z
    float m_zPositiveDistance;
    float m_zNegativeDistance;
    float zMaxPosition;
    float zMinPosition;

    //Variables de x
    float m_xDistance;
    float xMaxPosition;
    float xMinPosition;

    //variables de límites
    bool onXLeftLimit;
    bool onXRightLimit;
    bool onZUpperLimit;
    bool onZDownLimit;

    public bool doesNotHaveConfinement = false;

    private void OnDisable()
    {
        onXLeftLimit = false;
        onXRightLimit = false;
        onZUpperLimit = false;
        onZDownLimit = false;
    }
    private void Awake()
    {
        ////¿EL ORDEN EN ESTA FUNCIÓN ES IMPORTANTE!
        m_camera = GetComponent<CinemachineVirtualCamera>();

        followPlayer = FollowPlayer.Instance;

        SetVariablesOnAwake();
        SetZVariablesOnAwake();
        SetXVariablesOnAwake();

        if (followPlayer != null)
        {
            m_camera.Follow = followPlayer.transform;
        }
    }
    private void Update()
    {

        if (!onZUpperLimit && !onZDownLimit)
        {
            followPlayer.SetZDesiredPosition(); //Importante calcular esto antes de colocar según los límites
        }
        if (!onXRightLimit && !onXLeftLimit)
        {
            followPlayer.SetXDesiredPosition();
        }

        CalculateZDistance();
        CalculateXDistance();

        if (GameManager.Instance.ActualPlayerController != null) //Pare evitar que se reproduzca en el cambio de escena
        {
            if (!doesNotHaveConfinement)
            {
                LimitZPosition();
                LimitXPosition();
            }

            followPlayer.SetPosition();
        }
    }
    //Funciones que se setean en awake
    void SetVariablesOnAwake()
    {
        // El ángulo al que está girada la cámara
        correctionAngle = 90 - transform.eulerAngles.x;

        //Cálculo del camera distance dependiendo del ángulo de giro de la cámara y la altura en y
        m_camera.GetCinemachineComponent<CinemachineFramingTransposer>().m_CameraDistance = (gameObject.transform.position.y) / Mathf.Cos(correctionAngle * Mathf.Deg2Rad);
    }
    void SetZVariablesOnAwake()
    {
        //Cálculo de posiciones máximas de las cámaras
        zMaxPosition = roomCollider.gameObject.transform.position.z + roomCollider.center.z + roomCollider.size.z / 2;
        zMinPosition = roomCollider.gameObject.transform.position.z + roomCollider.center.z - roomCollider.size.z / 2;
    }
    void SetXVariablesOnAwake()
    {
        //Cálculo de posiciones máximas de las cámaras
        xMaxPosition = roomCollider.gameObject.transform.position.x + roomCollider.center.x + roomCollider.size.x / 2;
        xMinPosition = roomCollider.gameObject.transform.position.x + roomCollider.center.x - roomCollider.size.x / 2;
    }

    //Funciones del límite de la cámara
    void CalculateZDistance()
    {
        //Cálculo de distancia que hay entre el centro de la cámara y el límite de esta.
        m_zPositiveDistance = Mathf.Tan(((m_camera.m_Lens.FieldOfView / 2) + correctionAngle) * Mathf.Deg2Rad) * transform.position.y;
        m_zNegativeDistance = Mathf.Tan(((m_camera.m_Lens.FieldOfView / 2) - correctionAngle) * Mathf.Deg2Rad) * transform.position.y;
    }
    void LimitZPosition()
    {
        //Cálculo de posiciones actuales de los límites de la cámara
        float zPostitivePosition = transform.position.z + m_zPositiveDistance;
        float zNegativePosition = transform.position.z - m_zNegativeDistance;
        //Posición central de la cámara
        float centralCameraZPosition = ((zPostitivePosition + zNegativePosition) / 2);

        //Actualización de la posición del Follow Player
        //Posición superior
        float zPositiveCentralCameraPositionOnLimit = zMaxPosition - (zPostitivePosition - centralCameraZPosition);
        if (zPostitivePosition > zMaxPosition && !onZUpperLimit)
        {
            if (followPlayer.desiredPosition.z > zPositiveCentralCameraPositionOnLimit)
            {
                followPlayer.SetDesiredPosition(new Vector3(followPlayer.desiredPosition.x, followPlayer.transform.position.y, zPositiveCentralCameraPositionOnLimit));
                onZUpperLimit = true;
            }
        }
        if (followPlayer.GetDesiredPosition().z < zPositiveCentralCameraPositionOnLimit)
        {
            onZUpperLimit = false;
        }

        //Posición inferior
        float zNegativeCentralCameraPositionOnLimit = zMinPosition + centralCameraZPosition - zNegativePosition;
        if (zNegativePosition < zMinPosition && !onZDownLimit)
        {
            if (followPlayer.desiredPosition.z < zNegativeCentralCameraPositionOnLimit)
            {
                followPlayer.SetDesiredPosition(new Vector3(followPlayer.desiredPosition.x, followPlayer.transform.position.y, zNegativeCentralCameraPositionOnLimit));
                onZDownLimit = true;
            }
        }
        if (followPlayer.GetDesiredPosition().z > zNegativeCentralCameraPositionOnLimit)
        {
            onZDownLimit = false;
        }
    }
    void CalculateXDistance()
    {
        //Cálculo de distancia que hay entre el centro de la cámara y el límite de esta
        m_xDistance = Mathf.Tan(((m_camera.m_Lens.FieldOfView / 2) * m_camera.m_Lens.Aspect) * Mathf.Deg2Rad) * transform.position.y;
    }
    void LimitXPosition()
    {
        //Cálculo de posiciones actuales de los límites de la cámara
        float xPostitivePosition = transform.position.x + m_xDistance;
        float xNegativePosition = transform.position.x - m_xDistance;

        //Posición central de la cámara
        float centralCameraXPosition = ((xPostitivePosition + xNegativePosition) / 2);

        //Actualización de la posición del Follow Player
        //Posición positiva
       
        float xPositiveCentralCameraPositionOnLimit = xMaxPosition - (xPostitivePosition - centralCameraXPosition);
        if (xPostitivePosition > xMaxPosition && !onXRightLimit)
        {
            if (followPlayer.desiredPosition.x > xPositiveCentralCameraPositionOnLimit)
            {
                followPlayer.SetDesiredPosition(new Vector3(xPositiveCentralCameraPositionOnLimit, followPlayer.transform.position.y, followPlayer.desiredPosition.z));
                onXRightLimit = true;
            }
        }
        if (followPlayer.GetDesiredPosition().x < xPositiveCentralCameraPositionOnLimit)
        {
            onXRightLimit = false;
        }

        //Posición negativa
        float xNegativeCentralCameraPositionOnLimit = xMinPosition + centralCameraXPosition - xNegativePosition;
        if (xNegativePosition < xMinPosition && !onXLeftLimit)
        {
            if (followPlayer.desiredPosition.x < xNegativeCentralCameraPositionOnLimit)
            {
                followPlayer.SetDesiredPosition(new Vector3(xNegativeCentralCameraPositionOnLimit, followPlayer.transform.position.y, followPlayer.desiredPosition.z));
                onXLeftLimit = true;
            }
        }
        if (followPlayer.GetDesiredPosition().x > centralCameraXPosition)
        {
            onXLeftLimit = false;
        }
    }
}