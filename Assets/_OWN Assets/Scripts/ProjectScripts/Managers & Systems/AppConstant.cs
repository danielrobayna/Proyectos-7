﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AppPaths
{
	public static readonly string  	PERSISTENT_DATA      = Application.persistentDataPath;
	public static readonly string	PATH_RESOURCE_SFX    = "Audio/FX/";
    public static readonly string	PATH_RESOURCE_MUSIC  = "Audio/Music/";
    public static readonly string   PATH_AUDIOMIXER      = "Audio/MasterMixer";
}

public class AppPlayerPrefKeys
{
	public static readonly string	MUSIC_VOLUME = "MusicVolume";
	public static readonly string	SFX_VOLUME   = "SfxVolume";
}

public class AppSounds
{
    public static readonly string   MAIN_TITLE_MUSIC = "Main_Mus";

    public static readonly string   ZONA1_B_MUSIC = "Zona1_Base";   //Implementado
    public static readonly string   ZONA1_N_MUSIC = "Zona1_Normal"; //Implementado
    public static readonly string   ZONA1_C_MUSIC = "Zona1_Combate";//Implementado
    public static readonly string   ZONA2_B_MUSIC = "Zona2_Base";   //Implementado
    public static readonly string   ZONA2_N_MUSIC = "Zona2_Normal"; //Implementado
    public static readonly string   ZONA2_C_MUSIC = "Zona2_Combate";//Implementado
    public static readonly string   ZONA3_B_MUSIC = "Zona3_Base";   //Implementado
    public static readonly string   ZONA3_N_MUSIC = "Zona3_Normal"; //Implementado
    public static readonly string   ZONA3_C_MUSIC = "Zona3_Combate";//Implementado
    public static readonly string   BOSS1_MUSIC   = "Boss1_Mus";//Implementado
    public static readonly string   BOSS2_MUSIC   = "Boss2_Mus";

    //Sonidos jugador y enemigos
    public static readonly string   PLAYER_HIT_1       = "Player_Hit1_Aud";//Implementado
    public static readonly string   PLAYER_HIT_2       = "Player_Hit2_Aud";//Implementado
    public static readonly string   PLAYER_HIT_3       = "Player_Hit3_Aud";//Implementado
    public static readonly string   PLAYER_DEATH       = "Player_D_Aud";//Implementado
    public static readonly string   PLAYER_CONSUME_1   = "Player_Cons1_Aud";//Implementado
    public static readonly string   PLAYER_CONSUME_2   = "Player_Cons2_Aud";//Implementado
    public static readonly string   PLAYER_POSSESION_1 = "Player_Pos1_Aud";//Implementado
    public static readonly string   PLAYER_POSSESION_2 = "Player_Pos2_Aud";//Implementado
    public static readonly string   PLAYER_POS_JUMP1   = "Player_Pos_Jump1_Aud";//Implementado
    public static readonly string   PLAYER_POS_JUMP2   = "Player_Pos_Jump2_Aud";//Implementado
    public static readonly string   PLAYER_STEP_1      = "Player_Step1_Aud";//Implementado
    public static readonly string   PLAYER_STEP_2      = "Player_Step2_Aud";//Implementado
    public static readonly string   PLAYER_STEP_3      = "Player_Step3_Aud";//Implementado
    public static readonly string   PLAYER_STEP_4      = "Player_Step4_Aud";//Implementado
    public static readonly string   PLAYER_STEP_5      = "Player_Step5_Aud";//Implementado
    public static readonly string   PLAYER_STEP_6      = "Player_Step6_Aud";//Implementado
    public static readonly string   PLAYER_STEP_7      = "Player_Step7_Aud";//Implementado
    public static readonly string   PLAYER_STEP_8      = "Player_Step8_Aud";//Implementado
    public static readonly string   PLAYER_STEP_9      = "Player_Step9_Aud";//Implementado
    public static readonly string   NPC_DEATH          = "NPC_D_Aud";//Implementado
    public static readonly string   NPC_SCREAM1        = "NPC_Scr_Aud_1";//Implementado
    public static readonly string   NPC_SCREAM2        = "NPC_Scr_Aud_2";//Implementado
    public static readonly string   NPC_SCREAM3        = "NPC_Scr_Aud_3";//Implementado
    public static readonly string   HUMAN_DEATH1       = "Hum_D_1_Aud";//Implementado
    public static readonly string   HUMAN_DEATH2       = "Hum_D_2_Aud";//Implementado
    public static readonly string   HUMAN_DEATH3       = "Hum_D_3_Aud";//Implementado
    public static readonly string   HUMAN_DEATH4       = "Hum_D_4_Aud";//Implementado
    public static readonly string   HUMAN_DEATH5       = "Hum_D_5_Aud";//Implementado
    public static readonly string   HUMAN_DEATH_CONS   = "Hum_D_Cons_Aud";//Implementado
    public static readonly string   HUMAN_STEP1        = "Hum_Step1_Aud";//Implementado
    public static readonly string   HUMAN_STEP2        = "Hum_Step2_Aud";//Implementado
    public static readonly string   HUMAN_STEP3        = "Hum_Step3_Aud";//Implementado
    public static readonly string   HUMAN_STEP4        = "Hum_Step4_Aud";//Implementado
    public static readonly string   HUMAN_STEP5        = "Hum_Step5_Aud";//Implementado
    public static readonly string   HUMAN_HIT1         = "Hum_Hit_1_Aud";//Implementado
    public static readonly string   HUMAN_HIT2         = "Hum_Hit_2_Aud";//Implementado
    public static readonly string   HUMAN_HIT3         = "Hum_Hit_3_Aud";//Implementado
    public static readonly string   HUMAN_HIT4         = "Hum_Hit_4_Aud";//Implementado
    public static readonly string   HUMAN_HIT5         = "Hum_Hit_5_Aud";//Implementado
    public static readonly string   PISTOL_FIRE_1      = "Pistol_1_Aud";//Implementado
    public static readonly string   PISTOL_FIRE_2      = "Pistol_2_Aud";//Implementado
    public static readonly string   PISTOL_FIRE_3      = "Pistol_3_Aud";//Implementado
    public static readonly string   SHOOTGUN_FIRE_1    = "Shootgun_1_Aud";//Implementado
    public static readonly string   SHOOTGUN_FIRE_2    = "Shootgun_2_Aud";//Implementado
    public static readonly string   SHOOTGUN_FIRE_3    = "Shootgun_3_Aud";//Implementado
    public static readonly string   MACHINEGUN_FIRE_1  = "Machinegun_1_Aud";//Implementado
    public static readonly string   MACHINEGUN_FIRE_2  = "Machinegun_2_Aud";//Implementado
    public static readonly string   MACHINEGUN_FIRE_3  = "Machinegun_3_Aud";//Implementado
    public static readonly string   FLAMER_FIRE        = "Flamer_Aud";//Implementado
    public static readonly string   SNIPER_FIRE        = "Sniper_Aud";//Implementado
    public static readonly string   MEATHEAD_FIRE      = "Meathead_Aud";//Implementado
    public static readonly string   TURRET_FIRE        = "Turret_Shot_Aud";//Implementado
    public static readonly string   TURRET_MOVE        = "Turret_Move_Aud";//Implementado
    public static readonly string   SHOCKER_FIRE       = "Shocker_Shot_Aud";//Implementado
    public static readonly string   SHOCKER_LOOP       = "Shocker_Loop_Aud";//Implementado
    public static readonly string   ACID_THROW         = "Acid_Throw_Aud";//Implementado
    public static readonly string   GRANADIER_THROW    = "Grana_Throw_Aud";//Implementado
    public static readonly string   ACID_GLASS         = "Acid_Glass_Aud";//Implementado
    public static readonly string   RAILGUN_FIRE       = "Railgun_Shot_Aud";//Implementado
    public static readonly string   RAILGUN_MOVE       = "Railgun_Move_Aud";//Implementado
    public static readonly string   SHIELD_FIRE        = "Shield_Shot_Aud";//Implementado
    public static readonly string   SHIELD_DEPLOY      = "Shield_Deploy_Aud";//Implementado
    public static readonly string   SHIELD_HIDE        = "Shield_Hide_Aud";//Implementado
    public static readonly string   SHIELD_BREAK       = "Shield_Break_Aud";//Implementado
    public static readonly string   SHIELD_BOUNCE      = "Shield_Bounce_Aud";//Implementado
    public static readonly string   EXPLOSION_1        = "Rob_Ex_1_Aud";//Implementado
    public static readonly string   EXPLOSION_2        = "Rob_Ex_2_Aud";//Implementado
    public static readonly string   EXPLOSION_3        = "Rob_Ex_3_Aud";//Implementado
    public static readonly string   EXPLOSION_4        = "Rob_Ex_4_Aud";//Implementado
    public static readonly string   EXPLOSION_5        = "Rob_Ex_5_Aud";//Implementado
    public static readonly string   GRANADE_EXP_1      = "Gra_Exp1_Aud";//Implementado
    public static readonly string   GRANADE_EXP_2      = "Gra_Exp2_Aud";//Implementado
    public static readonly string   ROBOT_MUERTE       = "Rob_D_Aud";//Implementado
    public static readonly string   ROBOT_HIT_1        = "Rob_Hit_1_Aud";//Implementado
    public static readonly string   ROBOT_HIT_2        = "Rob_Hit_2_Aud";//Implementado
    public static readonly string   ROBOT_HIT_3        = "Rob_Hit_3_Aud";//Implementado
    public static readonly string   ROBOT_HIT_4        = "Rob_Hit_4_Aud";//Implementado
    public static readonly string   HUMAN_DASH         = "Hum_Dash_Aud";//Implementado
    public static readonly string   HUMAN_THROW        = "Hum_Throw_Aud";//Implementado
    public static readonly string   PLAYER_LATIDO      = "Player_L_Aud";//Implementado en escena, está en una animación

    //Sonidos generales
    public static readonly string DOOR_SFX              = "Door_Close";//Implementado
    public static readonly string BLOCKADE_SHIELD_DESTROY = "Blockade_Shield_Aud";//Implementado
    public static readonly string UPGRADE_PICK_UP       = "Upg_PU_Aud";//Implementado 
    public static readonly string BULLET_IMPACT_1       = "Bullet_Impact1_Aud";//Implementado
    public static readonly string BULLET_IMPACT_2       = "Bullet_Impact2_Aud";//Implementado
    public static readonly string BULLET_IMPACT_3       = "Bullet_Impact3_Aud";//Implementado

    public static readonly string BLOCKADE_FIRE_DESTROY = "Blockade_Fire_Aud";//Implementado
    public static readonly string BLOCKADE_ELECTRIC_DESTROY = "Blockade_Electric_Aud";//Implementado
    public static readonly string FIRE_FLOOR            = "Fire_Floor_Aud";//Implementado
    public static readonly string ELECTRIC_FLOOR        = "Electric_Floor_Aud";//Implementado

    //Sonidos del boss final 1
    public static readonly string   BOSS1_ATTACK_START  = "Boss1_Attacks_Start_Aud";
    public static readonly string   BOSS1_ATTACK_LOOP   = "Boss1_Attacks_Loop_Aud";//Implementado en escena debido a que está en una animación
    public static readonly string   BOSS1_THROW_DRILL   = "Boss1_ThrowDrill_Aud";//Implementado
    public static readonly string   BOSS1_IMPACT_DRILL  = "Boss1_DrillImp_Aud";//Implementado
    public static readonly string   BOSS1_AIM           = "Bs1_Aim_Aud";//Implementado
    public static readonly string   BOSS1_DEATH         = "Bs1_D_Aud";//Implementado
    public static readonly string   BOSS1_EXHAUST       = "Bs1_Exh_Aud";//Implementado
    public static readonly string   BOSS1_HIT           = "Bs1_Hit_Aud";//Implementado
    public static readonly string   BOSS1_LASER         = "Bs1_Las_Aud";//Implementado
    public static readonly string   BOSS1_LIGHTS        = "Bs1_Light_Aud";//Implementado
    public static readonly string   BOSS1_LOOP          = "Bs1_Loop_Aud_3";//Implementado
    public static readonly string   BOSS1_SHOVEL        = "Bs1_Shov_Aud";//Implementado
    public static readonly string   BOSS1_START         = "Bs1_Start_Aud_3";//Implementado
    public static readonly string   BOSS1_DRILL_PREP = "Bs1_Drill_Prep_Aud";//Implementado

    //Sonidos boss final 2
    public static readonly string HEAVY_STEP_1 = "Bs2_Step1_Aud";//Implementado
    public static readonly string HEAVY_STEP_2 = "Bs2_Step2_Aud";//Implementado
    public static readonly string HEAVY_STEP_3 = "Bs2_Step3_Aud";//Implementado
    public static readonly string HEAVY_STEP_4 = "Bs2_Step4_Aud";//Implementado
    public static readonly string BOSS2_LAUGH_1 = "Bs2_Laugh_Aud_1";//Implementado
    public static readonly string BOSS2_LAUGH_2 = "Bs2_Laugh_Aud_2";//Implementado
    public static readonly string BOSS2_LAUGH_3 = "Bs2_Laugh_Aud_3";//Implementado
    public static readonly string BOSS2_LAUGH_4 = "Bs2_Laugh_Aud_4";//Implementado
    public static readonly string BOSS2_WAVE = "Bs2_Wave_Aud";//Implementado
    public static readonly string BOSS2_HIT = "Bs2_VOICE_MALE_Argh_1_mono";//Implementado
    public static readonly string BOSS2_THROW_GRENADE = "Grana_Throw_Aud";//Implementado
    public static readonly string BOSS2_CHANGE_PHASE = "Bs2_Change_Aud";//Implementado
    public static readonly string BOSS2_SHOOT_SMALL_CRYSTAL = "Bs2_Disp_1_Aud";//Implementado
    public static readonly string BOSS2_SHOOT_BIG_CRYSTAL = "Bs2_Lanz_Aud";//Implementado
    public static readonly string BOSS2_PREPARE_CHARGE = "Bs2_Charge_Aud";//Implementado
    public static readonly string BOSS2_STRUCK_WALL = "Bs2_HitWall_Aud";//Implementado
    public static readonly string BOSS2_DEATH = "Bs2_D_Aud";//Implementado
    public static readonly string BOSS2_DEATH_FALL_1 = "Bs2_DeathFall1";//Implementado
    public static readonly string BOSS2_DEATH_FALL_2 = "Bs2_DeathFall2";//Implementado

    //Sonidos UI
    public static readonly string   PAUSE               = "Pause_Aud";//Implementado
    public static readonly string   BUTTON              = "Button_Nav_Aud";//Implementado
    public static readonly string   RESUME              = "Resume_Aud";//Implementado
}