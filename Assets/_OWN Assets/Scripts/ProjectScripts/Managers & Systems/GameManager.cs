﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using Steamworks;

public class GameManager : TemporalSingleton<GameManager>
{
    private PlayerControl_MovementController m_actualPlayerController;
    [HideInInspector]public GameObject realPlayerGO;
    private SetAnimatorVariables realPlayerAnimationsControl;
    bool isGamePaused;
    FollowPlayer followPlayerObject;
    [HideInInspector] public Camera mainCamera;
    private bool respawningPlayer = false;
    string activeSceneName;

    public PlayerControl_MovementController ActualPlayerController
    {
        get {return m_actualPlayerController; }
        set { m_actualPlayerController = value; }
    }

    [Header("Multiplicadores estáticos de mejora de Cadencia y Daño")]
    [HideInInspector] public float playerShootRatioMultiplier = 0;
    [HideInInspector] public float playerDamageMultiplier = 1;

    public override void Awake()
    {
        base.Awake();

        SearchForPlayers();
        SearchForMainCamera();
        activeSceneName = SceneManager.GetActiveScene().name;
    }

    /// 
    /// ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓ ELIMINAR PARA BUILD ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    /// 

    private void Start()
    {
        //ELIMINAR PARA LA BUILD
        OnLevelLoadOnEditorStart();
    }
    private void Update()
    {
        if (isGamePaused)
        {
            SetActiveCursorOnPause();
        }
    }
    //ELIMINAR PARA BUILD
    void OnLevelLoadOnEditorStart()
    {
        ZoneManager.Instance.GetAllSaveableObjectsInScene();
        ZoneManager.Instance.SetPlayerInPositionOnLevelStart();
        followPlayerObject.ResetPosition();
    }

    /// 
    ///   ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑ ELIMINAR PARA BUILD  ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
    /// 

    public void SearchForPlayers()
    {
        realPlayerGO = GameObject.FindGameObjectWithTag("Player");
        if (realPlayerGO != null)
        {
            ActualPlayerController = realPlayerGO.GetComponent<PlayerControl_MovementController>();
            realPlayerAnimationsControl = realPlayerGO.GetComponent<SetAnimatorVariables>();
        }
        followPlayerObject = FindObjectOfType<FollowPlayer>();
    }
    public void SearchForMainCamera()
    {
        mainCamera = Camera.main;
    }

    public void OnLevelLoad(bool putPlayerOnCheckpoint)
    {
        //¡¡¡EL ORDEN EN ESTA FUNCIÓN ES IMPORTANTE!!!!
        ZoneManager.Instance.GetAllSaveableObjectsInScene();
        
        LoadGame(putPlayerOnCheckpoint);
        SaveGame(ActualPlayerController.transform.position);
        followPlayerObject.ResetPosition();

        UIManager.Instance.Unfade();
    }

    private void OnEnable()
    {
        SteamFriends.OnGameOverlayActivated += PauseGameOnSteamOverlay;
    }
    private void OnDisable()
    {
        SteamFriends.OnGameOverlayActivated -= PauseGameOnSteamOverlay;
    }
    public void PauseGameOnSteamOverlay(bool overlayActive)
    {
        if(overlayActive && CutsceneManager.Instance.director.state == PlayState.Playing)
        {
            Time.timeScale = 0;
        }
        else if(!overlayActive && CutsceneManager.Instance.director.state == PlayState.Playing)
        {
            Time.timeScale = 1; 
        }
        else if(overlayActive && !UIManager.Instance.pauseObject.activeSelf )
        {
            UIManager.Instance.UsePauseButton();
        }    
    }
    public bool IsGamePaused() //Sacar el valor de si el juego está pausado fuera de GameManager
    {
        return isGamePaused; 
    }
    public void PauseGame()
    {
        if (!isGamePaused) 
        {
            isGamePaused = true;
            InputManager.Instance.PauseGameInputs();
            Time.timeScale = 0;
            SetActiveCursorOnPause();
            MusicManager.Instance.SetLowPassFilter(true);
            MusicManager.Instance.PauseAllAudiosOnGamePause();
            MusicManager.Instance.PlaySound(AppSounds.PAUSE);
        }
    }
    public void UnPauseGame()
    {
         if(isGamePaused)
         {
            InputManager.Instance.UnpauseGameInputs();
            isGamePaused = false;
            Time.timeScale = 1;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            MusicManager.Instance.SetLowPassFilter(false);
            MusicManager.Instance.ResumeAudioOnGameResume();
            MusicManager.Instance.PlaySound(AppSounds.RESUME);
        }
    }

    void SetActiveCursorOnPause()
    {
        if (!InputManager.Instance.IsControllingWithController)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
    public string GetActiveSceneName()
    {
        return activeSceneName;
    }

    //RESPAWN DEL JUGADOR CUANDO MUERE EN LA PARTIDA
    public IEnumerator RespawnPlayer() ////Respawn de jugador cuando muere
    {
        respawningPlayer = true;

        //Matar al jugador
        UIManager.Instance.SetPlayerDeathEffects();
        InputManager.Instance.PauseGameInputs();
        realPlayerAnimationsControl.SetDeadTrigger();
        MusicManager.Instance.PlaySound(AppSounds.PLAYER_DEATH);
        MusicManager.Instance.StopBackgroundMusic();
       
        yield return new WaitForSeconds(1.5f);
        UIManager.Instance.Fade();

        yield return new WaitUntil(() => UIManager.Instance.IsScreenOnBlack());
        StartCoroutine(Checkpoint.LoadActualGameAsyncScene());
        UIManager.Instance.StopPlayerDeathEffects();

        //¡¡¡EL ORDEN EN ESTA FUNCIÓN ES IMPORTANTE!!!!
        yield return new WaitUntil(() => Checkpoint.IsGameSceneLoaded());//¡Esto debe hacerse antes de resetear la posición del follow player!
        UIManager.Instance.Unfade();
        ///////////////////////////////
        
        yield return null;

        //Curar al jugador
        HealthHeartsVisual.healthHeartsSystemStatic.Heal(100);       
        ResetPlayerStates();

        yield return null;

        respawningPlayer = false;
    }
    public bool IsPlayerRespawning()  //Sacar el valor de respawningPlayer fuera de GameManager
    {
        return respawningPlayer;
    }
    void ResetPlayerStates()
    {
        PlayerHealthController actualPlayer= GameManager.Instance.realPlayerGO.GetComponent<PlayerHealthController>();
        if (actualPlayer != null)
        {
            actualPlayer.ResetPlayerStates();
        }
    }

    ///                                                         ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    ///////////////// GUARDADO DE PARTIDA //////////////////////Se puede modificar para tener métodos que guarden o carguen cosas específicas y no todo de manera general, pero hay que cargar antes el archivo guardado para no hacer un NEW SaveObject y sobreescribir el contenido guardado/////////////////
    ///                                                         ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
    ///////// PRUEBA DE GUARDADO////////// 
    /*
    private void Update() 
    {
       if(Input.GetKeyDown(KeyCode.K))
       {
           SaveGame(realPlayerGO.transform.position);
       }

       if (Input.GetKeyDown(KeyCode.L))
       {
           LoadGame(true);
       }

       if (Input.GetKeyDown(KeyCode.J))
       {
           EraseSavings();
       }
   }*/
    ///////// PRUEBA ////////// 

    public void SaveGame(Vector3 savedPosition) ////Guardado de Datos, se pasa la posicion del checkpoint porque cada uno debe decir cual es su posición al guardar al entrar en su trigger
    {
        UIManager.Instance.ActivateSavingText();

        SaveObject savedObject = new SaveObject ////Aquí se igualan los datos del SaveObject a los datos que se quieren guardar
        {
            lastCheckPointPositionSaved = savedPosition,
            numberOfHeartsSaved = HealthHeartsVisual.healthHeartsSystemStatic.GetHeartList().Count,
            shootRatioMultiplierSaved = playerShootRatioMultiplier,
            damageMultiplierSaved = playerDamageMultiplier,
            blockadesStateSaved = new BlockadeState[ZoneManager.Instance.allBlockades.Length],
            upgradesStateSaved = new UpgradeState[ZoneManager.Instance.allUpgrades.Length],
            cutscenesTriggersStateSaved = new CutsceneTriggerState[ZoneManager.Instance.allCutsceneTriggers.Length],
            activeSceneNames = new string[SceneManager.sceneCount],
            activatedRooms = ZoneManager.Instance.mapManager.GetActiveRoomsOnSave()
    };

        for (int i = 0; i < ZoneManager.Instance.allBlockades.Length; i++)
        {
            savedObject.blockadesStateSaved[i] = new BlockadeState
            {
                blockadePos = ZoneManager.Instance.allBlockades[i].transform.position,
                blockadeStatus = ZoneManager.Instance.allBlockades[i].hasBeenDestroyed
            };
        }

        for (int i = 0; i < ZoneManager.Instance.allUpgrades.Length; i++)
        {
            savedObject.upgradesStateSaved[i] = new UpgradeState
            {
                upgradePos = ZoneManager.Instance.allUpgrades[i].transform.position,
                upgradeStatus = ZoneManager.Instance.allUpgrades[i].hasBeenTaken,
            };
        }

        for (int i = 0; i < ZoneManager.Instance.allCutsceneTriggers.Length; i++)
        {
            savedObject.cutscenesTriggersStateSaved[i] = new CutsceneTriggerState
            {
                cutsceneTriggerPos = ZoneManager.Instance.allCutsceneTriggers[i].transform.position,
                cutsceneTriggerStatus = ZoneManager.Instance.allCutsceneTriggers[i].hasBeenTriggered,
            };
        }
 
        for (int i =0; i < SceneManager.sceneCount; i++) //Guardado del nombre de las escenas activas
        {
            savedObject.activeSceneNames[i] = SceneManager.GetSceneAt(i).name;
        }

        SaveSystem.Serialize(savedObject); ////Llamada al guardado en SaveSystem

        Debug.Log("Game Saved!");
    }

    public void SaveGameObjectStatusOnly() //No incluimos la llamada a guardar posición de guardado. Guardar solo estado de objetos en la escena y efectos de mejoras...
    {
        UIManager.Instance.ActivateSavingText();

        SaveObject loadedObject = SaveSystem.Deserialize<SaveObject>();
        SaveObject overwriteObject = new SaveObject();
        if (loadedObject != null)
        {
            overwriteObject = loadedObject;
        }

        overwriteObject.numberOfHeartsSaved = HealthHeartsVisual.healthHeartsSystemStatic.GetHeartList().Count;
        overwriteObject.shootRatioMultiplierSaved = playerShootRatioMultiplier;
        overwriteObject.damageMultiplierSaved = playerDamageMultiplier;
        overwriteObject.blockadesStateSaved = new BlockadeState[ZoneManager.Instance.allBlockades.Length];
        overwriteObject.upgradesStateSaved = new UpgradeState[ZoneManager.Instance.allUpgrades.Length];
        overwriteObject.cutscenesTriggersStateSaved = new CutsceneTriggerState[ZoneManager.Instance.allCutsceneTriggers.Length];
        overwriteObject.activatedRooms = ZoneManager.Instance.mapManager.GetActiveRoomsOnSave();

        for (int i = 0; i < ZoneManager.Instance.allBlockades.Length; i++)
        {
            overwriteObject.blockadesStateSaved[i] = new BlockadeState
            {
                blockadePos = ZoneManager.Instance.allBlockades[i].transform.position,
                blockadeStatus = ZoneManager.Instance.allBlockades[i].hasBeenDestroyed
            };
        }

        for (int i = 0; i < ZoneManager.Instance.allUpgrades.Length; i++)
        {
            overwriteObject.upgradesStateSaved[i] = new UpgradeState
            {
                upgradePos = ZoneManager.Instance.allUpgrades[i].transform.position,
                upgradeStatus = ZoneManager.Instance.allUpgrades[i].hasBeenTaken,
            };
        }

        for (int i = 0; i < ZoneManager.Instance.allCutsceneTriggers.Length; i++)
        {
            overwriteObject.cutscenesTriggersStateSaved[i] = new CutsceneTriggerState
            {
                cutsceneTriggerPos = ZoneManager.Instance.allCutsceneTriggers[i].transform.position,
                cutsceneTriggerStatus = ZoneManager.Instance.allCutsceneTriggers[i].hasBeenTriggered,
            };
        }

        SaveSystem.Serialize(overwriteObject); ////Llamada al guardado en SaveSystem

        Debug.Log("Game Objects Status Saved!");
    }

    public void LoadGame(bool putPlayerOnCheckpoint) ////Carga de Datos
    {
        SaveObject loadedObject = SaveSystem.Deserialize<SaveObject>(); ////LLamada de carga al SaveSystem

        if (loadedObject != null) ////Si hay datos, hay que igualar las variables que queremos con ese valor a las guardadas en el SaveObject
        {
            if (putPlayerOnCheckpoint)
            {
                realPlayerGO.transform.position = loadedObject.lastCheckPointPositionSaved; //Posición Checkpoint
            }
            else
            {
                ZoneManager.Instance.SetPlayerInPositionOnLevelStart();
            }
            HealthHeartsVisual.healthHeartsSystemStatic.visualSystemReference.CreateNewSystem(loadedObject.numberOfHeartsSaved); // Vidas del player 
            playerShootRatioMultiplier = loadedObject.shootRatioMultiplierSaved; // Multiplicador de cadencia
            playerDamageMultiplier = loadedObject.damageMultiplierSaved; // Multiplicador de daño
            UIManager.Instance.mapBehaviour.SetActiveRoomsOnLoad(loadedObject.activatedRooms);//Seteo de las habitaciones descubiertas en el mapa

            for (int i = 0; i < loadedObject.blockadesStateSaved.Length; i++) //Estado de bloqueos
            {
                for(int j = 0; j < ZoneManager.Instance.allBlockades.Length; j++)
                {
                    if (loadedObject.blockadesStateSaved[i].blockadePos == ZoneManager.Instance.allBlockades[j].transform.position)
                    {
                        ZoneManager.Instance.allBlockades[j].hasBeenDestroyed = loadedObject.blockadesStateSaved[i].blockadeStatus;
                        if (ZoneManager.Instance.allBlockades[j].hasBeenDestroyed)
                        {
                            ZoneManager.Instance.allBlockades[j].BreakBlockadeOnLoad();
                        }
                        break;
                    }
                }
            }
            for (int i = 0; i < loadedObject.upgradesStateSaved.Length; i++) //Estado de upgrades
            {
                for (int j = 0; j < ZoneManager.Instance.allUpgrades.Length; j++)
                {
                    if (loadedObject.upgradesStateSaved[i].upgradePos == ZoneManager.Instance.allUpgrades[j].transform.position)
                    {
                        ZoneManager.Instance.allUpgrades[j].hasBeenTaken = loadedObject.upgradesStateSaved[i].upgradeStatus;
                        if (ZoneManager.Instance.allUpgrades[j].hasBeenTaken)
                        {
                            ZoneManager.Instance.allUpgrades[j].DeactivateUpgradeObjectOnLoad();
                        }
                        break;
                    }
                }
            }
            for (int i = 0; i < loadedObject.cutscenesTriggersStateSaved.Length; i++) //Estado de cutsceneTriggers
            {
                for (int j = 0; j < ZoneManager.Instance.allCutsceneTriggers.Length; j++)
                {
                    if (loadedObject.cutscenesTriggersStateSaved[i].cutsceneTriggerPos == ZoneManager.Instance.allCutsceneTriggers[j].transform.position)
                    {
                        ZoneManager.Instance.allCutsceneTriggers[j].hasBeenTriggered = loadedObject.cutscenesTriggersStateSaved[i].cutsceneTriggerStatus;
                        if (ZoneManager.Instance.allCutsceneTriggers[j].hasBeenTriggered)
                        {
                            ZoneManager.Instance.allCutsceneTriggers[j].DeactivateTriggerOnLoad();
                        }
                        break;
                    }
                }
            }

            Debug.Log("Game Loaded!");
        }

        else    ////Si no hay ARCHIVO.XML con datos guardados, se idica
        {
            ZoneManager.Instance.SetPlayerInPositionOnLevelStart();
            Debug.Log("No Save File!");
        }
    }
    
    public void EraseSavedMapData()
    {
        SaveObject loadedObject = SaveSystem.Deserialize<SaveObject>();
        SaveObject overwriteObject = new SaveObject();
        if (loadedObject != null)
        {
            overwriteObject = loadedObject;
        }

        overwriteObject.activatedRooms = new int[0];

        SaveSystem.Serialize(overwriteObject); ////Llamada al guardado en SaveSystem
        Debug.Log("Map data erased!");
    }

    public void EraseSavings()
    {
        SaveSystem.EraseSaveFile();
        Debug.Log("Savings Erased!");
    }
}

//CLASES DEL SISTEMA DE GUARADO
public class SaveObject //// Clase que contiene la información de Guardado, todo lo que se quiera guardar tiene que tener una variable AQUI dentro del mismo tipo para que se pueda guardar
{
    public Vector3 lastCheckPointPositionSaved;
    public int numberOfHeartsSaved;
    public float shootRatioMultiplierSaved;
    public float damageMultiplierSaved;
    public BlockadeState[] blockadesStateSaved;
    public UpgradeState[] upgradesStateSaved;
    public CutsceneTriggerState[] cutscenesTriggersStateSaved;
    public string[] activeSceneNames;
    public int[] activatedRooms;
}
public class BlockadeState
{
    public Vector3 blockadePos;
    public bool blockadeStatus;
}
public class UpgradeState
{
    public Vector3 upgradePos;
    public bool upgradeStatus;
}
public class CutsceneTriggerState
{
    public Vector3 cutsceneTriggerPos;
    public bool cutsceneTriggerStatus;
}