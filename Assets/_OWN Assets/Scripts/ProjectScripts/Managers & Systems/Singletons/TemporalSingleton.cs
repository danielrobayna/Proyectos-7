﻿using UnityEngine;
using System.Collections;


public class TemporalSingleton<T> : MonoBehaviour	where T : Component
{
	public static T Instance 
	{
		get 
		{
			return _instance;
		}
	}
	
	public virtual void Awake ()
	{
		if (_instance == null) 
		{
			_instance = this as T;
		} 
		else 
		{
			Destroy (_instance.gameObject);
            _instance = this as T;
        }
    }

    private static T _instance;
}
