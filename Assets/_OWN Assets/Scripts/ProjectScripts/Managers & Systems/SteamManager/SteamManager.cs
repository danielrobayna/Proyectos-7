﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using Steamworks.Data;
using System.Linq;

public class SteamManager : PersistentSingleton<SteamManager>
{

    // Start is called before the first frame update
    /*void Start()
    {
        Debug.Log("init");
        try
        {
            SteamClient.Init(1299820, true);
        }
        catch (System.Exception e)
        {
            Debug.Log("No se ha conectado con Steam");
            // Something went wrong! Steam is closed?
        }

        SteamUserStats.OnUserStatsReceived += DebugUserStatsReceived;
        SteamUserStats.OnUserStatsStored += DebugUserStatsSaved;

        SteamUserStats.OnAchievementProgress += DebugAchievementChanged;
    }*/

    /*private void DebugUnlockAlmostAll()    //Debug para desbloquear todos los logros excepto el logro de matar 1 enemigo y el logro de tenerlos todos, para debugear este último.
    {
        foreach (var a in SteamUserStats.Achievements)
        {
            if (a.Identifier == "KILL_FIRST_ENEMY" || a.Identifier == "GET_ALL_ACHIEVEMENTS")
            {
                Debug.Log(a.Identifier + " ESTE NO " + a.State);
            }

            else
            {
                Debug.Log(a.Identifier + " " + a.State);
                a.Trigger();
            }
        }
    }*/

    private void DebugUserStatsReceived(SteamId id, Result result) //Debugeo cuando carga stats desde Steam
    {
        Debug.Log($"{id} da resultado {result}");
    }

    private void DebugUserStatsSaved(Result result) //Debugeo cuando guarda stats en Steam
    {
        Debug.Log($"Guardar datos con resultado {result}");

        /*foreach (var a in SteamUserStats.Achievements)
        {
            Debug.Log(a.Name + " " + a.State);
        }*/
    }

    private void DebugAchievementChanged(Achievement ach, int currentProgress, int progress)
    {
        if (ach.State)
        {
            Debug.Log($"{ach.Name} WAS UNLOCKED!");
        }
    }

    public void Init()
    {
        //Llamada vacía para intanciar en escena
    }

    public void ExitSteamClient()
    {
        Achievement_GetAllAchievements(); //Comprobar logro de tenerlos todos al salir por si acaso.
        SteamClient.Shutdown();
    }

    private void OnDisable()
    {
        SteamUserStats.OnUserStatsReceived -= DebugUserStatsReceived;
        SteamUserStats.OnUserStatsStored -= DebugUserStatsSaved;
        SteamUserStats.OnAchievementProgress -= DebugAchievementChanged;
        ExitSteamClient();
    }

    ///
    /// ///////////// MÉTODOS DE DESBLOQUEO DE LOGROS //////////////
    /// 

    public void UnlockAchievement(string achievementToUnlockName)
    {
        if (SteamClient.IsValid)
        {
            Achievement ach = new Achievement(achievementToUnlockName);
            if (!ach.State) //Si el logro esta bloqueado, desbloquearlo
            {
                ach.Trigger();
                Achievement_GetAllAchievements(); //Comprobación de si tienes todos los logros siempre que desbloquees alguno para desbloquar este último si los tienes todos.
            }
        }
    }
    public void Achievement_GetAllUpgrades()
    {
        if (SteamClient.IsValid)
        {
            int progress = SteamUserStats.GetStatInt("UPGRADES_COUNT");
            progress += 1;
            if (progress < 4)
            {
                SteamUserStats.IndicateAchievementProgress("GET_ALL_UPGRADES", progress, 4);
                SteamUserStats.SetStat("UPGRADES_COUNT", progress);
            }

            else if (progress == 4) //Desbloqueo a mano por problemas con elconteo cuando llega al máximo
            {
                UnlockAchievement("GET_ALL_UPGRADES");
            }
        }
    }
    public void ResetUpgradesCountStat() // Para resetear cuenta de upgrades para el logro en el botón de Nuevo juego.
    {
        if (SteamClient.IsValid)
        {
            Achievement ach = new Achievement("GET_ALL_UPGRADES");
            if (!ach.State) //Si el logro esta bloqueado, resetear stats
            {
                int progress = SteamUserStats.GetStatInt("UPGRADES_COUNT");
                progress = 0;
                SteamUserStats.SetStat("UPGRADES_COUNT", progress);
            }
        }
    }
    public void Achievement_Burn50Enemies()
    {
        if (SteamClient.IsValid)
        {
            int progress = SteamUserStats.GetStatInt("BURNED_COUNT");
            progress += 1;
            if (progress < 50)
            {
                SteamUserStats.SetStat("BURNED_COUNT", progress);
            }

            else if (progress == 50) //Desbloqueo a mano por problemas con elconteo cuando llega al máximo
            {
                UnlockAchievement("BURN_50_ENEMIES");
            }

            if (progress == 10 || progress == 25 || progress == 40) //PopUps
            {
                SteamUserStats.IndicateAchievementProgress("BURN_50_ENEMIES", progress, 50);
            }
        }
    }
    public void Achievement_Shock50Enemies()
    {
        if (SteamClient.IsValid)
        {
            int progress = SteamUserStats.GetStatInt("SHOCKED_COUNT");
            progress += 1;
            if (progress < 50)
            {             
                SteamUserStats.SetStat("SHOCKED_COUNT", progress);
            }

            else if (progress == 50) //Desbloqueo a mano por problemas con elconteo cuando llega al máximo
            {
                UnlockAchievement("SHOCK_50_ENEMIES");
            }

            if (progress == 10 || progress == 25 || progress == 40) //PopUps
            {
                SteamUserStats.IndicateAchievementProgress("SHOCK_50_ENEMIES", progress, 50);
            }
        }
    }
    public void Achievement_KillEnemies100_150_200()
    {
        if (SteamClient.IsValid)
        {
            int progress = SteamUserStats.GetStatInt("KILL_ENEMIES_COUNT");
            progress += 1;
            if (progress < 200)
            {
                SteamUserStats.SetStat("KILL_ENEMIES_COUNT", progress);
            }

            if(progress < 100 && (progress == 25 || progress == 50 || progress == 75)) //PopUps
            {
                SteamUserStats.IndicateAchievementProgress("KILL_100_ENEMIES", progress, 100);
            }

            else if (progress < 150 && progress == 125) //PopUps
            {
                SteamUserStats.IndicateAchievementProgress("KILL_150_ENEMIES", progress, 150);
            }

            else if (progress < 200 && progress == 175) //PopUps
            {
                SteamUserStats.IndicateAchievementProgress("KILL_200_ENEMIES", progress, 200);
            }

            else if (progress == 200) //Desbloqueo a mano por problemas con elconteo cuando llega al máximo
            {
                UnlockAchievement("KILL_200_ENEMIES");
            }
        }
    }
    public void Achievement_GetAllAchievements()
    {
        if (SteamClient.IsValid)
        {
            int progress = 0;

            foreach (var ach in SteamUserStats.Achievements)
            {
                if(ach.State)
                {
                    progress += 1;
                }
            }

            if(progress == SteamUserStats.Achievements.Count() - 1)
            {
                UnlockAchievement("GET_ALL_ACHIEVEMENTS");
            }
        }
    }
}
