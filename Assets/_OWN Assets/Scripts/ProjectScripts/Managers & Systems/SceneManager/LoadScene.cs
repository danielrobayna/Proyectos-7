﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LoadScene : MonoBehaviour
{
    [Header("Las escenas a cargar")]
    public SceneReference gameSceneToLoad;
    public SceneReference permanentSceneToLoad;
    public SceneReference saveSceneToLoad;

    [Header("La escena del Menú Principal")]
    protected List<Scene> thisScenes = new List<Scene>();

    protected EventSystem oldEventSystem;
    public void LoadScenes()
    { 
        oldEventSystem = FindObjectOfType<EventSystem>();
        oldEventSystem.gameObject.SetActive(false);
        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnChangeInputDeactivateEvent();
        }

        //Introducir en la lista las escenas activas
        GetActiveScenesToUnload();

        StartCoroutine(LoadScenesCoroutine());
    }
    protected virtual IEnumerator LoadScenesCoroutine() //Lo cambia loadSceneDuringCutscene
    {
        //Carga de loading screen
        AsyncOperation loadingSceneAsync = SceneManager.LoadSceneAsync("LoadingScene", LoadSceneMode.Additive);

        yield return new WaitUntil(() => loadingSceneAsync.isDone);

        //Desactivado de permanent si existe
        Scene activeGameScene = GetActiveGameScene();
        if (activeGameScene.name != null)
        {
            AsyncOperation unloadGameSceneAsync = SceneManager.UnloadSceneAsync(activeGameScene);
        }

        //Desactivado del resto de escenas
        UnloadPreviousScenes();

        //Carga de las escenas nuevas
        StartLoadingScenesCoroutine();
    }
    protected virtual void StartLoadingScenesCoroutine() //Lo cambia loading on continue para poder cargar las escenas guardadas en el objeto de guardado
    {
        LoadingScene loadingSceneScript = FindObjectOfType<LoadingScene>();
        loadingSceneScript.StartCoroutine(loadingSceneScript.LoadDelay(gameSceneToLoad, permanentSceneToLoad, saveSceneToLoad,false));
    }
    protected void UnloadPreviousScenes()
    {
        foreach(Scene previousScene in thisScenes)
        {
            SceneManager.UnloadSceneAsync(previousScene);
        }
    }
    protected void GetActiveScenesToUnload()
    {
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            thisScenes.Add(SceneManager.GetSceneAt(i));
        }
    }
    Scene GetActiveGameScene()
    {
        int scenesCount = thisScenes.Count;
        for (int i = 0; i < scenesCount; i++)
        {
            if (!thisScenes[i].name.Contains("Permanent") && !thisScenes[i].name.Contains("Save") && thisScenes[i].name.Contains("Game"))
            {
                Scene gameScene = thisScenes[i];
                thisScenes.Remove(gameScene);
                return gameScene;
            }
        }
        return new Scene();
    }
}