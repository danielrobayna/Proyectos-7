﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

public class LoadSceneDuringCutscene : LoadScene
{
    public TimelineAsset playable;

    CutsceneManager cutManager;

    protected void Start()
    {
        cutManager = FindObjectOfType<CutsceneManager>();
    }

    protected override IEnumerator LoadScenesCoroutine()
    {
        cutManager.PlayCutscene(playable, true);

        while (true)
        {
            if (!cutManager.cutsceneIsPlaying)//Cargar escena cuando lo cinemática termina
            {
                StartCoroutine(base.LoadScenesCoroutine());
                break;
            }
            if (cutManager.CutsceneUI.gameObject.activeSelf) //Si el texto de la cinemática está activo(Es decir, un botón ha sido pulsado 1 vez)
            {
                if (Input.anyKeyDown)
                {
                    StartCoroutine(base.LoadScenesCoroutine());
                    break;
                }

            }
            else if (Input.anyKeyDown && !cutManager.CutsceneUI.gameObject.activeSelf) //Sacar texto en la cinemática al pulsar un botón
            {
                StartCoroutine(HideSkipText());
            }
            yield return null;
        }
    }

    private IEnumerator HideSkipText()
    {
        cutManager.CutsceneUI.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);

        cutManager.CutsceneUI.gameObject.SetActive(false);
    }
}