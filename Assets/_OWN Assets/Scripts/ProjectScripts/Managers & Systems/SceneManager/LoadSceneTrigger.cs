﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneTrigger : LoadScene
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
        {
            LoadScenes();
        }
    }
}
