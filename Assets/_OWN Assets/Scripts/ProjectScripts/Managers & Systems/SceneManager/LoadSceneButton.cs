﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneButton : LoadScene
{
    [Header("El Componente de Botón de este botón")]
    private Button thisButton;

    protected void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(LoadScenes);
    }
}
