﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class CreditsBackToMenu : MonoBehaviour
{
    VideoPlayer videoPlayer;
    private bool videoStarted = false;
    private bool videoSkipped = false;
    [SerializeField] GameObject skipText;
    [SerializeField] Animator fadeAnimator;

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.Prepare();
        MusicManager.Instance.StopBackgroundMusic();
    }

    // Update is called once per frame
    void Update()
    {
        if (videoPlayer.isPrepared && !videoStarted)
        {
            videoPlayer.Play();
            videoStarted = true;
        }
        else if(videoPlayer.isPlaying && videoStarted)
        {
            if(Input.anyKeyDown && !skipText.activeSelf)
            {
                StartCoroutine(HideSkipText());
            }

            else if (Input.anyKeyDown && skipText.activeSelf && !videoSkipped)
            {
                StartCoroutine(LoadMainMenu());
            }
        }
        else if (!videoPlayer.isPlaying && videoStarted)
        {
            GetComponent<SimpleSceneLoading>().LoadScene();
        }
    }

    private bool QuitVideoVolume()
    {
        float volume = videoPlayer.GetDirectAudioVolume(0);
        if(volume >= 0)
        {
            volume -= 1 * Time.deltaTime;
            videoPlayer.SetDirectAudioVolume(0, volume);
            return false;
        }
        else
        {
            return true;
        }
    }

    private IEnumerator LoadMainMenu()
    {
        videoSkipped = true;
        fadeAnimator.SetTrigger("FadeTrigger");

        yield return new WaitUntil(() => QuitVideoVolume());

        GetComponent<SimpleSceneLoading>().LoadScene();
    }

    private IEnumerator HideSkipText()
    {
        skipText.SetActive(true);

        yield return new WaitForSeconds(2);

        skipText.SetActive(false);
    }
}
