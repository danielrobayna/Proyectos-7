﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnContinue : LoadScene
{
    string gameSceneToLoadString;
    string permanentSceneToLoadString;
    string saveSceneToLoadString;

    protected override void StartLoadingScenesCoroutine()
    {
        SetScenesToLoad();
        LoadingScene loadingSceneScript = FindObjectOfType<LoadingScene>();
        loadingSceneScript.StartCoroutine(loadingSceneScript.LoadDelay(gameSceneToLoadString, permanentSceneToLoadString, saveSceneToLoadString, true));
    }
    void SetScenesToLoad()
    {
        SaveObject loadedObject = SaveSystem.Deserialize<SaveObject>(); ////LLamada de carga al SaveSystem

        if (loadedObject != null && loadedObject.activeSceneNames != null) ////Si hay datos, hay que igualar las variables que queremos con ese valor a las guardadas en el SaveObject
        {
            for(int i = 0; i < loadedObject.activeSceneNames.Length; i++)
            {
                string actualSceneName = loadedObject.activeSceneNames[i];

                if (actualSceneName.Contains("Permanent") )
                {
                    permanentSceneToLoadString = actualSceneName;
                }
                else if(actualSceneName.Contains("Save"))
                {
                    saveSceneToLoadString = actualSceneName;
                }
                else
                {
                    gameSceneToLoadString = actualSceneName;
                }
            }
        }
        else
        {
            Debug.Log("No hay datos que cargar");
        }
    }
}