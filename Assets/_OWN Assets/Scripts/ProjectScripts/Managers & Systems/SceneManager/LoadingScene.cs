﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    bool canLoad;

    AsyncOperation gameSceneAsyncOperation;
    AsyncOperation permanentSceneAsyncOperation;
    AsyncOperation saveSceneAsyncOperation;

    private void Update()
    {
        if (SceneManager.sceneCount == 1 && SceneManager.GetActiveScene().name.Equals("LoadingScene"))
        {
            canLoad = true;
        }
    }

    public IEnumerator LoadDelay(string gameScene, string permanentScene, string saveScene, bool hasToPutPlayerOnCheckpoint)
    {
        yield return new WaitUntil(()=>canLoad);

        SceneManager.sceneLoaded += FinishLoading;

        AsyncOperation permanentAsyncOperation = SceneManager.LoadSceneAsync(permanentScene, LoadSceneMode.Additive);
        yield return new WaitUntil(() => permanentAsyncOperation.isDone);

        AsyncOperation saveAsyncOperation = SceneManager.LoadSceneAsync(saveScene, LoadSceneMode.Additive);
        AsyncOperation gameAsyncOperation = SceneManager.LoadSceneAsync(gameScene, LoadSceneMode.Additive);

        yield return new WaitUntil(() => gameAsyncOperation.isDone && saveAsyncOperation.isDone);

        SceneManager.sceneLoaded -= FinishLoading;

        GameManager.Instance.OnLevelLoad(hasToPutPlayerOnCheckpoint);
        SceneManager.UnloadSceneAsync("LoadingScene");
    }

    void FinishLoading(Scene scene, LoadSceneMode mode)
    {
        if (!scene.name.Contains("Permanent") && !scene.name.Contains("Save"))
        {
            SceneManager.SetActiveScene(scene);
        }
    }
}