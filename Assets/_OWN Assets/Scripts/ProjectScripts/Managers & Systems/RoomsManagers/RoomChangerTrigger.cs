﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomChangerTrigger : MonoBehaviour
{
    [SerializeField] bool imRight;
    [SerializeField] RoomChangerObject roomChanger;

    private void Start()
    {
        Physics.IgnoreCollision(GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider); //Ignorar la colision del modelo del player para que no se llame 2 veces el trigger
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
        {
            roomChanger.DiscoverRoom(); //Llamar a descubrir habitación en el objeto Changer
            InputManager.Instance.PauseGameInputs();

            if (imRight)
            {
                roomChanger.MovePlayerFromRight(collision.gameObject);
            }
            else
            {
                roomChanger.MovePlayerFromLeft(collision.gameObject);
            }

            if (roomChanger.m_isSaveRoomChanger)
            {
                EnemySetControl enemy = collision.GetComponent<EnemySetControl>();
                if (enemy != null)
                {
                    enemy.StartCoroutine(enemy.ConsumeEnemy());
                    enemy.GetComponent<EnemyControl_MovementController>().stopUnpossessTimer = true;
                }
            }
        }
    }
}
