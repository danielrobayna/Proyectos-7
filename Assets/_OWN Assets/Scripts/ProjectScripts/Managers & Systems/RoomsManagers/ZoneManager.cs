﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ZoneManager : TemporalSingleton<ZoneManager>
{
    public MapBehaviour mapManager;
    [HideInInspector] public CinemachineVirtualCamera m_activeCamera;
    [HideInInspector] public RoomManager m_activeRoom;
    public BlockadeBase[] allBlockades;
    public BaseUpgrade[] allUpgrades;
    public CutsceneTrigger[] allCutsceneTriggers;

    [Header("Posición inicial del jugador al comenzar el nivel")]
    [SerializeField] Transform initialPlayerPosition;

    [Header("La Escena Contiene la Zona actual para cargar en Checkpoints")]
    public SceneReference zoneScene;

    public void GetAllSaveableObjectsInScene()
    {
        allBlockades = FindObjectsOfType<BlockadeBase>();
        allUpgrades = FindObjectsOfType<BaseUpgrade>();
        allCutsceneTriggers = FindObjectsOfType<CutsceneTrigger>();
    }

    public IEnumerator ChangeRoom(CinemachineVirtualCamera newCamera, RoomManager newRoom)
    {           
        if (m_activeRoom != null && m_activeRoom!=newRoom && !GameManager.Instance.IsPlayerRespawning()) //Efectos de fade y de cámara
        {
            UIManager.Instance.Fade();
            Time.timeScale = 0;

            //Inicialmente pause game input iba aquí, pero eso causaba que la cámara se moviera durante 1 frame, así que cambiado al room changer trigger

            yield return new WaitUntil(() => UIManager.Instance.IsScreenOnBlack());
            UIManager.Instance.Unfade();

            if (!GameManager.Instance.IsGamePaused())
            {
                InputManager.Instance.UnpauseGameInputs();
                Time.timeScale = 1;
            }

            FollowPlayer.Instance.ResetPositionToActualController();
        }

        SetNewActiveCamera(newCamera);
        SetNewActiveRoom(newRoom); //Cambio de habitación
    }

    public void SetNewActiveCamera(CinemachineVirtualCamera newCamera)
    {
        if (m_activeCamera != null && m_activeCamera != newCamera) //Desactivado de cámara activa
        {
            m_activeCamera.gameObject.SetActive(false);
        }

        //Activado de nueva cámara
        m_activeCamera = newCamera;
        newCamera.gameObject.SetActive(true);
    }
    public void SetNewActiveRoom(RoomManager newRoom)
    {
        if (m_activeRoom != newRoom)
        {
            if (m_activeRoom != null)
            {
                m_activeRoom.DeactivateRoom();
            }
            m_activeRoom = newRoom;
            m_activeRoom.ActivateRoom();
        }

    }
    public void DeleteEnemyFromCurrentRoom(EnemyControl_MovementController enemy)
    {
        m_activeRoom.RemoveEnemyAtRoom(enemy);
    } //Borrado del enemigo para cuando muere
    public void AddBulletInActiveRoom(BulletBase bullet)
    {
        m_activeRoom.AddBulletInRoom(bullet);
    }
    public void RemoveBulletInActiveRoom(BulletBase bullet)
    {
        m_activeRoom.RemoveBulletInRoom(bullet);
    }

    // Métodos creados para permitir setear la posición del jugador cuando se cambia de zona
    public void SetPlayerInPositionOnLevelStart(Transform newTransform)
    {
        GameManager.Instance.realPlayerGO.transform.position = newTransform.position;
    }
    public void SetPlayerInPositionOnLevelStart() 
    {
        GameManager.Instance.realPlayerGO.transform.position = initialPlayerPosition.position;
    }
}
