﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomChangerObject : MonoBehaviour
{
    [SerializeField] Transform m_rightTransform;
    [SerializeField] Transform m_leftTransform;

    public bool m_isSaveRoomChanger;

    [Header("Habitación a descubrir en mapa al usarse el change")]
    [SerializeField] private List<RoomManager> roomsToDiscoverInMap = new List<RoomManager>();
    [Header("Hay una habitación en otra escena para descurir? Meter el numero de habitación a mano")]
    [SerializeField] private bool oneRoomIsInAnotherScene;
    [SerializeField] private int otherSceneRoomNumberInMap;

    public void DiscoverRoom()
    {
        for(int i = 0; i<roomsToDiscoverInMap.Count; i++)
        {
            UIManager.Instance.mapBehaviour.DiscoverRoom(roomsToDiscoverInMap[i].ThisRoomNumberInMap());
        }

        if(oneRoomIsInAnotherScene)
        {
            UIManager.Instance.mapBehaviour.DiscoverRoom(otherSceneRoomNumberInMap);
        }
    }

    public void MovePlayerFromRight(GameObject player)
    {
        player.transform.position = m_leftTransform.position;
    }
    public void MovePlayerFromLeft(GameObject player)
    {
        player.transform.position = m_rightTransform.position;
    }
}
