﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using ch.sycoforge.Decal;

public class RoomManager : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera m_roomCamera;
    [SerializeField] Camera m_minimapCamera;

    public bool isRoomActive;
    Dictionary<Transform,Vector3> originalEnemiesAtRoomPosition = new Dictionary<Transform, Vector3>();

    [Header("Máximo de Dummies en la Room")]
    [SerializeField] float maxDummiesInRoom = 10;
    [SerializeField] int roomInMap;

    [Header("Máximo de Decals en la Room")]
    [SerializeField] int maxRoomDecals = 10;
    private Queue<GameObject> activeDecalsInRoom = new Queue<GameObject>();

    public static event Action ClearQueues;

    //List of active object in room
    [HideInInspector] public List<BulletBase> activeBulletsInRoom = new List<BulletBase>();
    public List<EnemyControl_MovementController> currentEnemiesInRoom = new List<EnemyControl_MovementController>();
    List<EnemyControl_MovementController> enemiesToDelete = new List<EnemyControl_MovementController>();
    List<Spawner> activeSpawnersInRoom = new List<Spawner>();
    private Queue<GameObject> activeDummiesInRoom = new Queue<GameObject>();
    List<Door> activeDoorsInRoom = new List<Door>();
    [HideInInspector] public List<DestructibleObject> barrelsInRoom = new List<DestructibleObject>();

    BoxCollider myCollider;
    [SerializeField] LayerMask m_enemyLayer;

    MusicInRoom thisRoomMusic;

    public CinemachineVirtualCamera roomCamera
    {
        get
        {          
            return m_roomCamera;
        }
        private set
        {          
            m_roomCamera = value;
        }
    }

    private void OnEnable()
    {
        ClearQueues += ClearDecaAndDummiesQueues;
    }

    public static void ActivateClearQueuesEvent() //Se llama en la recarga de nivel del Checkpoint cuando se usa o cuando se recarga escen en el respawn del player
    {
        ClearQueues?.Invoke();
    }

    private void OnDisable()
    {
        ClearQueues -= ClearDecaAndDummiesQueues;
    }

    void Awake()
    {
        //encuentro el script de música de la habitación
        thisRoomMusic = GetComponent<MusicInRoom>();

        //Overlap box usado para detectar enemigos, spawners y puertas dentro de la habitación
        myCollider = GetComponent<BoxCollider>();
        Vector3 hitColliderPosition = new Vector3(gameObject.transform.position.x + myCollider.center.x, gameObject.transform.position.y + myCollider.center.y, gameObject.transform.position.z+myCollider.center.z);
        Collider[] hitColliders = Physics.OverlapBox(hitColliderPosition, myCollider.size/2, Quaternion.identity, m_enemyLayer);

        //Detectar enemigos al comenzar la partida
        int i = 0;
        while (i < hitColliders.Length)
        {
            EnemyControl_MovementController enemyController = hitColliders[i].GetComponent<EnemyControl_MovementController>();
            Spawner spawnerObject = hitColliders[i].GetComponent<Spawner>();
            Door doorObject = hitColliders[i].GetComponent<Door>();
            DestructibleObject barrelObject = hitColliders[i].GetComponent<DestructibleObject>();

            if (enemyController != null) //Añadir los enemigos a la lista
            {
                AddEnemyAtRoom(enemyController);
            }
            else if (spawnerObject != null) //añadir los spawners a la lista
            {
                AddSpawnerAtRoom(spawnerObject);
            }else if (doorObject != null)
            {
                AddDoorAtRoom(doorObject);
                doorObject.SetActualRoom(this); //Para tener una referencia del active Enemies
            }else if(barrelObject!=null)
            {
                AddBarrelAtRoom(barrelObject);
            }
            i++;
        }

        //Añadir las posiciones iniciales de los enemigos a un diccionario para resetarlas al reentrar a la habitación
        foreach (EnemyControl_MovementController enemy in currentEnemiesInRoom)
        {
            originalEnemiesAtRoomPosition.Add(enemy.transform, enemy.transform.position);
            enemy.gameObject.SetActive(false);
        }
        //Desactivado de los objetos
        DeactivateDoors();
        DeactivateSpawnersInRoom();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController) // Si es el jugador el que entra en la habitación
        {
            m_minimapCamera.gameObject.SetActive(true);

            StartCoroutine(ZoneManager.Instance.ChangeRoom(m_roomCamera, this)); //Cambio de habitación

            if (collision.GetComponent<EnemyControl_MovementController>() != null) 
            {
                AddEnemyAtRoom(collision.GetComponent<EnemyControl_MovementController>()); //Añadir enemigo si está siendo controlado por el jugador y entra a una nueva sala
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
        {
            m_minimapCamera.gameObject.SetActive(false);
        }
    }

    //Functions to add to colection
    public void AddSpawnerAtRoom(Spawner spawner)
    {
        activeSpawnersInRoom.Add(spawner);
    }
    public void AddBulletInRoom(BulletBase bullet)
    {
        activeBulletsInRoom.Add(bullet);
    }
    public void AddEnemyAtRoom(EnemyControl_MovementController newEnemy)
    {
        if (!currentEnemiesInRoom.Contains(newEnemy))
        {
            currentEnemiesInRoom.Add(newEnemy);
        }
    }
    public void AddDummieAtRoom(GameObject dummy)
    {
        activeDummiesInRoom.Enqueue(dummy);
        if (activeDummiesInRoom.Count > maxDummiesInRoom)
        {
            RemoveDummieAtRoom();
        }
    }
    public void AddDoorAtRoom(Door door)
    {
        if (!activeDoorsInRoom.Contains(door))
        {
            activeDoorsInRoom.Add(door);
        }
    }
    public void AddBarrelAtRoom(DestructibleObject barrel)
    {
        if (!barrelsInRoom.Contains(barrel))
        {
            barrelsInRoom.Add(barrel);
        }
    }
    //Pooling de Decals
    public void AddDecalAtZone(GameObject decal)
    {
        activeDecalsInRoom.Enqueue(decal);

        if (activeDecalsInRoom.Count > maxRoomDecals)
        {
            RemoveDecalAtZone();
        }
    }
    public void RemoveDecalAtZone()
    {
        activeDecalsInRoom.Dequeue().TryGetComponent(out EasyDecal decal);
        decal.FadeOut = true;
        decal.Lifetime = 1;
        decal.StartFade();
    }
    public void ClearDecaAndDummiesQueues()
    {
        activeDecalsInRoom.Clear();
        activeDummiesInRoom.Clear();
    }

    //Functions to remove from collection
    public void RemoveBulletInRoom(BulletBase bullet)
    {
        activeBulletsInRoom.Remove(bullet);
    } 
    public void RemoveEnemyAtRoom(EnemyControl_MovementController deleteEnemy)
    {
        if (currentEnemiesInRoom.Contains(deleteEnemy))
        {
            currentEnemiesInRoom.Remove(deleteEnemy);
        }
    }
    public void RemoveDummieAtRoom()
    {
        Destroy(activeDummiesInRoom.Dequeue());
    }
    public void DeleteControlledEnemyFromRoomList()
    {
        EnemyControl_MovementController currentControlledEnemy = null;

        foreach (EnemyControl_MovementController enemy in currentEnemiesInRoom)
        {
            if (enemy == GameManager.Instance.ActualPlayerController)
            {
                currentControlledEnemy = enemy;  //Es el jugador, así que se saca de la lista de enemigos en la sala
                break;
            }
        }
        if (currentControlledEnemy != null)
        {
            RemoveEnemyAtRoom(currentControlledEnemy);
        }
    }
    public void RemoveBarrelAtRoom(DestructibleObject barrel)
    {
        if (barrelsInRoom.Contains(barrel))
        {
            barrelsInRoom.Remove(barrel);
        }
    }
    //Functions to activate things in room
    void ActivateSpawnersInRoom()
    {
        foreach (Spawner spawner in activeSpawnersInRoom)
        {
            spawner.gameObject.SetActive(true);
        }
    }
    void ActivateEnemies()
    {
        foreach (EnemyControl_MovementController enemy in currentEnemiesInRoom)
        {
            enemy.gameObject.SetActive(true);
        }
    }
    void ActivateDoors()
    {
        foreach (Door door in activeDoorsInRoom)
        {
            door.gameObject.SetActive(true);
        }
    }

    //Functions to deactivate things in room
    void DeactivateEnemies()
    {
        foreach (EnemyControl_MovementController enemy in currentEnemiesInRoom)
        {
            if(enemy != GameManager.Instance.ActualPlayerController && enemy.spawnerInstantiatedFrom==null) //Desactivado del enemigo si no ha sido spawneado por un spawner
            {
                enemy.gameObject.SetActive(false);
            }
            else if(enemy != GameManager.Instance.ActualPlayerController)
            {
                enemy.spawnerInstantiatedFrom.RemoveEnemyFromSpawner(enemy.gameObject);
                enemiesToDelete.Add(enemy);
            }
        }
        if (enemiesToDelete.Count != 0) //Eliminado de los enemigos que han sido añadidos a la lista de enemigos a eliminar
        {
            for (int i = 0; i < enemiesToDelete.Count; i++)
            {
                RemoveEnemyAtRoom(enemiesToDelete[i]);
                Destroy(enemiesToDelete[i].gameObject);
            }
            enemiesToDelete.Clear();
        }
    }
    void DeactivateSpawnersInRoom()
    {
        foreach(Spawner spawner in activeSpawnersInRoom)
        {
            spawner.gameObject.SetActive(false);
        }
    }
    void DeactivateBulletsInRoom()
    {
        if (activeBulletsInRoom.Count != 0)
        {
            for (int i = activeBulletsInRoom.Count - 1; i >= 0; i--)
            {
                Destroy(activeBulletsInRoom[i].gameObject);
            }
            activeBulletsInRoom.Clear();
        }
    }
    void DeactivateDoors()
    {
        foreach (Door door in activeDoorsInRoom)
        {
            door.gameObject.SetActive(false);
        }
    }

    void ResetEnemyPositions()
    {
        foreach (PlayerControl_MovementController enemy in currentEnemiesInRoom)
        {
            if (originalEnemiesAtRoomPosition.ContainsKey(enemy.transform))
            {
                enemy.transform.position = originalEnemiesAtRoomPosition[enemy.transform];
            }
        }
    }
    public void DeactivateRoom()
    {
        DeleteControlledEnemyFromRoomList();
        DeactivateEnemies();
        DeactivateSpawnersInRoom();
        ResetEnemyPositions();
        DeactivateBulletsInRoom();
        DeactivateDoors();
        isRoomActive = false;
    }
    public void ActivateRoom()
    {
        ActivateEnemies();
        ActivateSpawnersInRoom();
        ActivateDoors();
        isRoomActive = true;
        thisRoomMusic.StartMusicOnRoomChange();

        UIManager.Instance.mapBehaviour.SetActiveRoomInMap(roomInMap);
    }
    public int ThisRoomNumberInMap() //Sacar en número de la habitacion en el mapa fuera del script
    {
        return roomInMap;
    }
}
