﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using Cinemachine;
using UnityEngine;

public class CutsceneManager : TemporalSingleton<CutsceneManager>
{
    public PlayableDirector director;
    public CinematicUI CutsceneUI;
    public bool cutsceneIsPlaying;
    public GameObject skipText;
    bool m_isSkipeable;
    CinemachineVirtualCamera lastCamera;
    Transform lastPos;
    [SerializeField] Canvas canvas;
    bool m_isBoss2Cutscene = false;
    public void Start()
    {
        skipText.SetActive(false);
    }
    private void Update()
    {
        if (cutsceneIsPlaying && Input.anyKeyDown && skipText.activeSelf && director.playableAsset.name != "Scene1")
        {
            SkipCutscene();           
        }
        else if (m_isSkipeable && Input.anyKeyDown && cutsceneIsPlaying && !skipText.activeSelf)
        {
            StartCoroutine(HideSkipText());
        }
    }   
    public void PlayCutscene(TimelineAsset playable, bool isSkipeable)
    {
        m_isSkipeable = isSkipeable;
        director.playableAsset = playable;
        director.Play();
        cutsceneIsPlaying = true;
    }
    public void PlayCutscene(TimelineAsset playable, bool isSkipeable, bool isBoss2Cutscene)
    {
        m_isSkipeable = isSkipeable;
        director.playableAsset = playable;
        director.Play();
        cutsceneIsPlaying = true;
        m_isBoss2Cutscene = isBoss2Cutscene;
        lastCamera = ZoneManager.Instance.m_activeCamera;
    }
    public void PlayCutscene(TimelineAsset playable, bool isSkipeable, CinemachineVirtualCamera roomCam, Transform camPos)
    {
        m_isSkipeable = isSkipeable;
        director.playableAsset = playable;
        lastCamera = roomCam;
        lastPos = camPos;
        director.Play();
        cutsceneIsPlaying = true;
    }
    public void CutsceneHasEnd ()
    {
        cutsceneIsPlaying = false;
    }

    private IEnumerator HideSkipText()
    {
        skipText.SetActive(true);

        yield return new WaitForSeconds(2);

        skipText.SetActive(false);
    }
    public void SkipCutscene()
    {
        skipText.SetActive(false);
        director.Stop();
        cutsceneIsPlaying = false;
        ReturnCamera();
        canvas.gameObject.SetActive(true);
        if(m_isBoss2Cutscene)
        {
            ActivateBoss2Combat();
        }

        for (int i = 0; i < director.transform.childCount; i++)
        {
            if (director.transform.GetChild(i).gameObject.activeSelf == true)
            {
                director.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    public void ReturnCamera()
    {
        Debug.Log(lastCamera);
        if (lastCamera != null)
        {
            lastCamera.gameObject.SetActive(true);
            StartCoroutine(CenterAim());
        }
    }
    IEnumerator CenterAim()
    {
        yield return new WaitForEndOfFrame();
        FollowPlayer.Instance.ResetPositionToActualController();
        InputManager.Instance.CenterAimTranformInPlayer();
        InputManager.Instance.UnpauseGameInputs();
    }

    public void ActivateBoss2Combat()
    {
        FinalBossZona2Behaviour.Instance.InitialState();
        cutsceneIsPlaying = false;
        canvas.gameObject.SetActive(true);
        ReturnCamera();
    }
}