﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[ExecuteInEditMode]
public class CutsceneDialog : MonoBehaviour
{
    [Range(0, 1)] public float visibleFraction;
    [SerializeField] TextMeshProUGUI myText;
    [SerializeField] float velocity;
    bool canShowCharactersAuto = false;
    int totalCharacters;
    int visibleCharacters;
    float timer;

    private void Start()
    {
        totalCharacters = myText.text.Length;

    }
    // Update is called once per frame
    void Update()
    {
        if (canShowCharactersAuto && visibleCharacters < totalCharacters)
        {
            timer += Time.deltaTime;
            if (timer > velocity)
            {
                visibleCharacters++;
                timer = 0;
            }
        }

        if(!canShowCharactersAuto)
        {
            visibleCharacters = Mathf.RoundToInt(visibleFraction * totalCharacters);
        }

        myText.maxVisibleCharacters = visibleCharacters;
    }

    public void StartShowingCharacters()
    {
        canShowCharactersAuto = true;
    }
}
