﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPossessionBullet : MonoBehaviour     //Este script es para la "bala" con forma de jugador que se instancia para poseer//
{
    private EnemySetControl enemyToMoveTo;
    private Vector3 enemyDirection;
    [SerializeField] private float posessionBulletSpeed;

    public void GivePlayerPossessionBulletATarget(EnemySetControl targetEnemy)  //Pasar referencia del enemigo al por el que lanzarse desde PossessAbility
    {
        enemyToMoveTo = targetEnemy;
    }

    void MoveToEnemyPosition()      //Moverse hacia el enemigo, rota hacia él por si acaso, aunque no deberán moverse al saltar hacia ellos
    {
        if(enemyToMoveTo != null)
        {
            enemyDirection = enemyToMoveTo.transform.position - transform.position;
            enemyDirection.y = 0;
            transform.rotation = Quaternion.LookRotation(enemyDirection);

            transform.position += transform.forward * Time.deltaTime * posessionBulletSpeed;

            if(Vector3.Distance(enemyToMoveTo.transform.position, transform.position) <= 1.5f && !enemyToMoveTo.CanEnemyBePossessed()) //Si el enemigo no puede ser poseído, se activa el player y se destruye esta "bala" de posesión
            {              
                GameManager.Instance.realPlayerGO.transform.position = transform.position;
                GameManager.Instance.realPlayerGO.SetActive(true);

                if (enemyToMoveTo.TryGetComponent(out EnemyAI_Standard enemyToMoveToAI))
                {
                    enemyToMoveToAI.currentAIState = EnemyAI_Standard.AIState.idle;
                }

                if(enemyToMoveTo is ShockerSetControl || enemyToMoveTo is Boss2SetControl)
                { 
                    GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().StartElectricStun(); //Si el enemigo a poseer es un Shocker o Boss 2 con el aura activa, se stunea al jugador
                }

                Destroy(gameObject);
            }

            else if (Vector3.Distance(enemyToMoveTo.transform.position, transform.position) <= 0.5f && enemyToMoveTo.CanEnemyBePossessed())
            {
                enemyToMoveTo.PossessEnemy();
                Destroy(gameObject);
            }
        }

        else
        {
            GameManager.Instance.realPlayerGO.transform.position = transform.position;
            GameManager.Instance.realPlayerGO.SetActive(true);
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        MoveToEnemyPosition();
    }
}
