﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControl_MovementController : MonoBehaviour
{
    float moveHorizontal;
    float moveVertical;

    [Header("Variables de Movimiento")]
    [HideInInspector] public float controlSpeedX;
    [HideInInspector] public float controlSpeedZ;
    public float maxSpeed;
    public float accelerationX;
    public float accelerationZ;
    public float decelerationX;
    public float decelerationZ;
    [HideInInspector] public Rigidbody controlRb;
    [SerializeField] float aimLerpSpeed;

    [Header("El Objeto que Funciona como Brazo o el que Rota")]
    public GameObject armObject;
    [HideInInspector] public Vector3 playerInputDirection;
    [HideInInspector] public float angle;

    [Header("El Padre de todo el modelo")]
    public GameObject modelParent;

    [HideInInspector]
    public Animator m_minimaparrowAnimator;

    [Header("Variables del autoaputado")]
    [SerializeField] float m_autoAimDistanceDistance = 10;

    // Start is called before the first frame update
    protected virtual void Start()
    {      
        controlRb = GetComponent<Rigidbody>();
        m_mySetAnimationVariables = GetComponent<SetAnimatorVariables>();
        m_minimaparrowAnimator = UIManager.Instance.minimapArrow;
    }

    // Update is called once per frame
   protected virtual void Update()
    {
        SetAnimationsVariables(); //Aplicarse antes de modificar la velocidad de rigidbody
        PlayMovementSound();

        moveHorizontal = InputManager.Instance.HorizontalMovement();
        moveVertical = InputManager.Instance.VerticalMovement();

        PlayerControlledMovement(moveHorizontal, moveVertical);

        ControlArmRotation();
        UpdateObjectRotation();
    }
    protected virtual void PlayerControlledMovement(float axisValueX, float axisValueZ)
    {
        controlSpeedX += axisValueX * accelerationX * Time.deltaTime; ////Cálculo de la velocidad con aceleraciones
        controlSpeedZ += axisValueZ * accelerationZ * Time.deltaTime;

        //pruebas para impedir movimiento detrás de objetos con raycast en lugar de colliders.
        if (axisValueX == 0 || axisValueX == 1 && controlSpeedX < 0 || axisValueX == -1 && controlSpeedX > 0) ////Deceleraciones en X
        {
            if (controlSpeedX > 0)  
            {
                controlSpeedX -= decelerationX * Time.deltaTime;
                if (controlSpeedX < 0)
                {
                    controlSpeedX = 0;
                }
            }
            else if (controlSpeedX < 0)
            {
                controlSpeedX += decelerationX * Time.deltaTime;
                if (controlSpeedX > 0)
                {
                    controlSpeedX = 0;
                }
            }
        }

        if (axisValueZ == 0 || axisValueZ == 1 && controlSpeedZ < 0 || axisValueZ == -1 && controlSpeedZ > 0) ////Deceleraciones en Y
        {
            if (controlSpeedZ > 0)  
            {
                controlSpeedZ -= decelerationZ * Time.deltaTime;
                if (controlSpeedZ < 0)
                {
                    controlSpeedZ = 0;
                }
            }
            else if (controlSpeedZ < 0)
            {
                controlSpeedZ += decelerationZ * Time.deltaTime;
                if (controlSpeedZ > 0)
                {
                    controlSpeedZ = 0;
                }
            }
        }

        if (controlSpeedX > maxSpeed)  ////Límite de velocidad
        {
            controlSpeedX = maxSpeed;
        }
        if (controlSpeedX < -maxSpeed)
        {
            controlSpeedX = -maxSpeed;
        }
        if (controlSpeedZ > maxSpeed)
        {
            controlSpeedZ = maxSpeed;
        }
        if (controlSpeedZ < -maxSpeed)
        {
            controlSpeedZ = -maxSpeed;
        }
      
        finalVector = new Vector3(controlSpeedX, controlRb.velocity.y , controlSpeedZ);
        controlRb.velocity = Vector3.ClampMagnitude(finalVector, maxSpeed); ////Aplicación de los cálculos al velocity del rigidbody
    }
    Vector3 finalVector;
    protected void ControlArmRotation()
    {   
        playerInputDirection = InputManager.Instance.RotationInput();

        float playerInputAngle = Mathf.Atan2(playerInputDirection.normalized.z, playerInputDirection.normalized.x) * Mathf.Rad2Deg;
        if (playerInputAngle < 0)
        {
            playerInputAngle += 360;
        }

        Transform transformToAim = null; //Variable del enemigo al que se apuntará automáticamente, si cumple las condiciones
        if (playerInputDirection.sqrMagnitude > 0.3f) ////Si el valor de la dirección es mayor que 0.2, es decir, si el jugador está apuntando
        {
            float previousPriority = 0; //Variable para comparar qué enemigo tiene más prioridad para hacer autoaim

            if (ZoneManager.Instance.m_activeRoom != null && InputManager.Instance.IsControllingWithController)
            {
                for(int i = 0; i< ZoneManager.Instance.m_activeRoom.barrelsInRoom.Count; i++)
                {
                    DestructibleObject barrel = ZoneManager.Instance.m_activeRoom.barrelsInRoom[i];

                    if (barrel != null)
                    {
                        Vector3 vectorToActualEnemy = barrel.transform.position - armObject.transform.position;
                        float distanceToActualEnemy = vectorToActualEnemy.magnitude;
                        float angleToActualEnemy = Mathf.Atan2(vectorToActualEnemy.z, vectorToActualEnemy.x) * Mathf.Rad2Deg;

                        if (angleToActualEnemy < 0) //Corrección para evitar ángulos negativos
                        {
                            angleToActualEnemy += 360;
                        }

                        float angleBetweenEnemyAndPlayerInput = Mathf.Abs(Mathf.DeltaAngle(angleToActualEnemy, playerInputAngle));

                        if (angleBetweenEnemyAndPlayerInput < InputManager.Instance.maxAimAngle / 2 && distanceToActualEnemy < m_autoAimDistanceDistance)
                        {
                            float thisPriority = InputManager.Instance.angleAimPriority * (1 - (angleBetweenEnemyAndPlayerInput / (InputManager.Instance.maxAimAngle / 2))) + InputManager.Instance.distanceAimPriority * (1 - (distanceToActualEnemy / m_autoAimDistanceDistance));
                            if (thisPriority > previousPriority)
                            {
                                transformToAim = barrel.transform;
                                previousPriority = thisPriority;
                            }
                        }
                    }
                }
               
                if (transformToAim == null) //Siempre da prioridad al barril explosivo
                {
                    //Detectar el enemigo más adecuado para hacer auto aim
                    for (int i =0; i< ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom.Count; i++)
                    {
                        EnemyControl_MovementController enemy = ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom[i];

                        if (enemy != GameManager.Instance.ActualPlayerController && enemy != null)
                        {
                            Vector3 vectorToActualEnemy = enemy.transform.position - modelParent.transform.position;
                            float distanceToActualEnemy = vectorToActualEnemy.magnitude;
                            float angleToActualEnemy = Mathf.Atan2(vectorToActualEnemy.z, vectorToActualEnemy.x) * Mathf.Rad2Deg;

                            if (angleToActualEnemy < 0) //Corrección para evitar ángulos negativos
                            {
                                angleToActualEnemy += 360;
                            }

                            float angleBetweenEnemyAndPlayerInput = Mathf.Abs(Mathf.DeltaAngle(angleToActualEnemy, playerInputAngle));
                            if (angleBetweenEnemyAndPlayerInput < InputManager.Instance.maxAimAngle / 2 && distanceToActualEnemy < m_autoAimDistanceDistance)
                            {
                                float thisPriority = InputManager.Instance.angleAimPriority * (1 - (angleBetweenEnemyAndPlayerInput / (InputManager.Instance.maxAimAngle / 2))) + InputManager.Instance.distanceAimPriority * (1 - (distanceToActualEnemy / m_autoAimDistanceDistance));
                                if (thisPriority > previousPriority)
                                {
                                    transformToAim = enemy.transform;
                                    previousPriority = thisPriority;
                                }
                            }
                        }
                    }
                }
            }

            if (transformToAim != null) //Si un enemigo o un barril cumple las condiciones para ser apuntado automáticamente y por tanto, se ha seteado
            {
                Vector3 vectorToAimEnemy = transformToAim.position - modelParent.transform.position;
                if(Physics.Raycast(modelParent.transform.position, vectorToAimEnemy, vectorToAimEnemy.magnitude, InputManager.Instance.autoAimRaycastLayerMask)) //Detección de si hay una pared en medio
                {
                    angle = Mathf.LerpAngle(angle, playerInputAngle, aimLerpSpeed * Time.deltaTime); //Ángulo con la dirección del input
                }
                else
                {
                    angle = Mathf.LerpAngle(angle, Vector3.SignedAngle(vectorToAimEnemy, Vector3.right, Vector3.up), aimLerpSpeed * Time.deltaTime);//Ángulo con la dirección al enemigo

                }
            }

            else //Si no hay enemigo para hacer auto aim
            {
                angle = Mathf.LerpAngle(angle, playerInputAngle, aimLerpSpeed * Time.deltaTime);
            }

            armObject.transform.rotation = Quaternion.AngleAxis(-angle, Vector3.up); ////Se rota el objeto de brazo para igualar la dirección del joystick en el eje Z.
        }

        else if (controlRb.velocity.normalized.magnitude != 0) //Si no se controla la dirección y se está moviendo
        {
            float angleSigned = Vector3.SignedAngle(controlRb.velocity.normalized, Vector3.right, Vector3.up);
            if (angleSigned < 0)
            {
                angleSigned += 360;
            }
            angle = Mathf.LerpAngle(angle, angleSigned, aimLerpSpeed * Time.deltaTime);
            armObject.transform.rotation = Quaternion.AngleAxis(-angle, Vector3.up); ////Se rota el objeto de brazo para igualar la dirección del joystick en el eje Z.
        }
        else //Si NO controla la dirección y no se mueve mantiene el ángulo anterior
        {
            armObject.transform.rotation = Quaternion.AngleAxis(-angle, Vector3.up);           
        }
    }

    public void UpdateObjectRotation()
    {
        modelParent.transform.localEulerAngles = new Vector3(0, -angle, 0);
    }

    ///////////////////////////////////////////////ANIMACIONES Y FX//////////////////////////////////////////////////

    [HideInInspector]
    public SetAnimatorVariables m_mySetAnimationVariables;

    protected Vector3 relativeVector;
    protected virtual void SetAnimationsVariables() //Solo funciona en player
    {
        if (playerInputDirection.normalized.sqrMagnitude < 0.2f)
        {
            m_mySetAnimationVariables.SetIsAimingBool(false); 
        }
       
        float forwardAngleToWorld = Vector3.SignedAngle(armObject.transform.right, Vector3.forward, Vector3.up);

        Vector3 previousRelativeVector = relativeVector;
        relativeVector = (Quaternion.AngleAxis(forwardAngleToWorld, Vector3.up) * controlRb.velocity).normalized;
        relativeVector = Vector3.Lerp(previousRelativeVector, relativeVector, Time.deltaTime * 10);


        m_mySetAnimationVariables.SetMovementX(relativeVector.x);
        m_mySetAnimationVariables.SetMovementZ(relativeVector.z);

        m_mySetAnimationVariables.SetIsMovingBool(controlRb.velocity.magnitude > 0.1f);

        //Minimap animator
        m_minimaparrowAnimator.SetFloat("Angle", forwardAngleToWorld);
    }

    protected float movementSoundTimer;
    protected float movementSoundDelay = 0.3f;
    protected virtual void PlayMovementSound()
    {
        if(controlRb.velocity.magnitude > 0.1f && !GameManager.Instance.IsPlayerRespawning())
        {
            movementSoundTimer += Time.deltaTime;

            if (movementSoundTimer >= movementSoundDelay)
            {
                movementSoundTimer = 0;
                int randomNumber = Random.Range(0, 9);

                //Play a random sound
                switch (randomNumber)
                {
                    case 0:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_1);
                        break;
                    case 1:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_2);
                        break;
                    case 2:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_3);
                        break;
                    case 3:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_4);
                        break;
                    case 4:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_5);
                        break;
                    case 5:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_6);
                        break;
                    case 6:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_7);
                        break;
                    case 7:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_8);
                        break;
                    case 8:
                        MusicManager.Instance.PlaySound(AppSounds.PLAYER_STEP_9);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}