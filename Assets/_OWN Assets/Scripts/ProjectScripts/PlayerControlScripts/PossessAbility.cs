﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossessAbility : MonoBehaviour
{
    [Header("El collider pequeño del alien a ras de suelo (se usa en SetControl)")] //Está aquí y no en Playercontroller para que no lo hereden los enemigos
    public Collider realPlayerWorldCollider;

    [Header("Variables del Raycast")]
    public float raycastDistance;
    private RaycastHit sphereCastHit;
    [SerializeField] LayerMask layersToHitWithPossessionRay;

    [Header("Rayo secundario para chequear otras cosas que no sean enemigos")]
    [SerializeField] LayerMask secondaryLayersToCheck;
    private RaycastHit secondaryRayHit;
    private float distanceToSphereCastHit;
    private float distanceToSecondaryCastHit;

    [Header("El Enemigo en el Raycast")]
    private EnemySetControl enemy_InRaycast;

    [Header("El PlayerControl del mismo objeto Player")]
    PlayerControl_MovementController playerControl_MovementController;

    [Header("El Objeto del Brazo, Sacado del Script de Control en Start")]
    private GameObject armObject;

    [Header("El LineRenderer")]
    private LineRenderer playerLineRenderer;

    [Header("Variables para usar los Triggers de Mando como Botón")]
    private bool leftTrigger_isAxisInUse;

    [Header("Bala de posesión con forma de jugador")]
    [SerializeField] private GameObject possessionBulletGO;
    private Transform modelTransform;

    [Header("Tiempo de stun eléctrico (sin poder poseer)")]
    [SerializeField] private float electricallyStunnedTime;
    [Header("Bool para indicar si está Estuneado por Electricidad")]
    private bool isElectricallyStunned = false;
    private Coroutine electricStunCoroutine = null;

    [Header("Partículas (PlaceHolders)")]
    public ParticleSystem stunPart;
    public ParticleSystem electricalStunPart;

    bool isControllingWithMouse;
    public void StartControllingWithMouse()
    {
        isControllingWithMouse = true;
    }
    public void StopControllingWithMouse()
    {
        isControllingWithMouse = false;
    }
    private void Awake()
    {
        playerControl_MovementController = GetComponent<PlayerControl_MovementController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        armObject = playerControl_MovementController.armObject;
        playerLineRenderer = GetComponent<LineRenderer>();
        modelTransform = playerControl_MovementController.modelParent.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (isControllingWithMouse)
        {
            MouseInput(); //Para ratón
        }
        else
        {
            ControllerInput(); //para mando
        }
    }
    void HideLineRenderer()
    {
        playerLineRenderer.SetPosition(1, Vector3.zero);
        playerLineRenderer.SetPosition(2, Vector3.zero);
    }

    void DrawRaycast()
    {
        Physics.SphereCast(modelTransform.position, 0.3f, armObject.transform.right, out sphereCastHit, raycastDistance, layersToHitWithPossessionRay);
        Physics.Raycast(modelTransform.position + new Vector3(0, 0.3f, 0), armObject.transform.right, out secondaryRayHit, raycastDistance, secondaryLayersToCheck); //Raycast secundario de chequeo para no atravesar paredes
        if (sphereCastHit.collider != null)
        {
            distanceToSphereCastHit = Vector3.Distance(modelTransform.position, new Vector3(sphereCastHit.point.x, 0, sphereCastHit.point.z));
        }
        else
        {
            distanceToSphereCastHit = 0;
        }
        if (secondaryRayHit.collider != null)
        {
            distanceToSecondaryCastHit = Vector3.Distance(modelTransform.position, new Vector3(secondaryRayHit.point.x, 0, secondaryRayHit.point.z));
        }
        else
        {
            distanceToSecondaryCastHit = 0;
        }
        //Debug.DrawRay(modelTransform.position + new Vector3(0, 0.3f, 0), armObject.transform.right * raycastDistance, Color.blue);
        playerLineRenderer.SetPosition(1, (armObject.transform.right * raycastDistance / 2) + new Vector3(0, 0.3f, 0.5f));
        playerLineRenderer.SetPosition(2, (armObject.transform.right * raycastDistance) + new Vector3(0, 0.3f, 0.5f));
    }
    void EmptyRaycastHit()
    {
        if (sphereCastHit.collider != null) ////Chequeos para vaciar la variable del EnemySetControl del enemigo en el Raycast si no hay ninguno en el Raycast o si se salta de uno a otro sin que no haya nada entre medias.
        {
            EnemySetControl enemyCheckScript = sphereCastHit.collider.gameObject.GetComponent<EnemySetControl>();

            if (enemyCheckScript != null && secondaryRayHit.collider == null || enemyCheckScript != null && distanceToSphereCastHit < distanceToSecondaryCastHit) // || es check de distancias por si el secondary Raycast no es nulo e impacta por detras del enemigo
            {
                if (enemy_InRaycast != null)
                {
                    enemy_InRaycast = null;
                }
                enemy_InRaycast = enemyCheckScript;
                playerLineRenderer.material.color = Color.red;
            }
            else if(enemyCheckScript == null)
            {
                enemy_InRaycast = null;
                playerLineRenderer.material.color = Color.white;
            }
            else if (secondaryRayHit.collider != null && distanceToSecondaryCastHit < distanceToSphereCastHit)
            {
                enemy_InRaycast = null;
                playerLineRenderer.material.color = Color.white;
            }
        }
        else
        {
            if (enemy_InRaycast != null)
            {
                enemy_InRaycast = null;
            }
            playerLineRenderer.material.color = Color.white;
        }
    }

    private void ControllerInput()
    {
        if (playerControl_MovementController.playerInputDirection.sqrMagnitude != 0 && !isElectricallyStunned) ////Se lanza Raycast si hay input de rotación registrado en el PlayerControl Y siempre que no esté stuneado por electricidad
        {
            DrawRaycast();
        }

        else ////Si no hay input de rotación, se elimina la visibilidad de LineRenderer y se resetea el RaycastHit;
        {
            PossessAction(); //Antes de resetear el hit, se mira si había un enemigo en el Raycast y se le posee
            HideLineRenderer();
            sphereCastHit = new RaycastHit();
        }

        if (!isElectricallyStunned)
        {
            EmptyRaycastHit();
        }
    }
    private void PossessAction()
    {
        if (enemy_InRaycast != null && !GameManager.Instance.IsPlayerRespawning())
        {
            isElectricallyStunned = false;

            PlayPossessionJumpSound();

            PlayerPossessionBullet possessionBulletLogic = Instantiate(possessionBulletGO, modelTransform.position, modelTransform.rotation).GetComponent<PlayerPossessionBullet>();
            possessionBulletLogic.GivePlayerPossessionBulletATarget(enemy_InRaycast);

            HideLineRenderer();

            enemy_InRaycast.WaitForPossession();
        }
    }

    private void MouseInput()
    {
        if (InputManager.Instance.LeftTrigger() != 0 && !isElectricallyStunned) //Si hay input
        {
            DrawRaycast();
            leftTrigger_isAxisInUse = true;
        }
        else if (InputManager.Instance.LeftTrigger() == 0) //Si no hay input
        {
            if (leftTrigger_isAxisInUse)
            {
                PossessAction();
                leftTrigger_isAxisInUse = false;
                HideLineRenderer();
            }
        }

        if (!isElectricallyStunned)
        {
            EmptyRaycastHit();
        }
    }

    public IEnumerator PlayerStun(float timeStunned) // La funcinalidad del Stun al jugador, se la llama desde SetControl en caso de morir el enemigo poseído.
    {
        if(!stunPart.isPlaying)
        {
            stunPart.Play();
        }

        playerControl_MovementController.playerInputDirection = Vector2.zero;
        playerControl_MovementController.enabled = false;

        yield return new WaitForSeconds(timeStunned);

        playerControl_MovementController.enabled = true;

        if (stunPart.isPlaying)
        {
            stunPart.Stop();
        }
    }

    ///////////////// STUN ELÉCTRICO //////////////////

    public void StartElectricStun()      //Vaciar la corutina de stun eléctrico y comenzarla de nuevo cada vez que se llama a esta función, por el desposeer por causas eléctricas o por el impacto de bala eléctrica
    {
        if(electricStunCoroutine != null)
        {
            StopCoroutine(electricStunCoroutine);
        }

        electricStunCoroutine = StartCoroutine(PlayerElectricalStun());
    }

    private IEnumerator PlayerElectricalStun()     //Corutina de stun eléctrico
    {
        isElectricallyStunned = true;

        enemy_InRaycast = null;
        HideLineRenderer();

        if (!electricalStunPart.isEmitting)
        {
            electricalStunPart.Play();
        }

        yield return new WaitForSeconds(electricallyStunnedTime);

        isElectricallyStunned = false;

        if(electricalStunPart.isEmitting)
        {
            electricalStunPart.Stop();
        }
    }

    void PlayPossessionJumpSound()
    {
        int randomNum = Random.Range(0, 2);
        switch (randomNum)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_POS_JUMP1);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_POS_JUMP2);
                break;
            default:
                break;
        }
    }
}
