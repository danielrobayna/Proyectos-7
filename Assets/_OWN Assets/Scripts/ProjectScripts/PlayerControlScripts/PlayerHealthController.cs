﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class PlayerHealthController : MonoBehaviour
{
    SetAnimatorVariables m_mySetAnimationVariables;
    //variable para el camera shake
    CinemachineImpulseSource impulse;

    //Fire State
    [Header("Variables de estado de fuego")]
    [SerializeField] ParticleSystem fireParticle;
    bool m_isOnFire = false;
    float m_fireTimer = 0;
    int m_damagePerSecond;
    float m_timeInFire;
    float m_timerRepeatDamageInFire;
    float m_timeToDamage;

    //Invulnerability
    [Header("Variables de invencibilidad")]
    [SerializeField] float invulnerabilityTime;
    [SerializeField] SkinnedMeshRenderer rendererToFlash;
    [SerializeField] float flashTime;
    bool m_isInvulnerable = false;
    float m_invulnerableTimer;
    float m_flashTimer;

    private void Start()
    {
        m_mySetAnimationVariables = GetComponent<SetAnimatorVariables>();
        impulse = GetComponent<CinemachineImpulseSource>();
    }
    private void Update()
    {
        States();
    }
    void States()
    {
        //Estado de fuego
        if (m_isOnFire)
        {
            FireState();
        }

        //Estado de invulnerabilidad
        if (m_isInvulnerable)
        {
            InvulnerableState();
        }
    }
    //funciones de estado de fuego
    public void StartFireState(int damagePerSecond, float time, float timeToDamage)
    {
        m_isOnFire = true;
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
        fireParticle.Play();

        m_damagePerSecond = damagePerSecond;
        m_timeInFire = time;
        m_timeToDamage = timeToDamage;
    }
    public void StartFireState(int damagePerSecond, float time)
    {
        m_isOnFire = true;
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
        fireParticle.Play();

        m_damagePerSecond = damagePerSecond;
        m_timeInFire = time;
        m_timeToDamage = 1;
    }
    void StopFireState()
    {
        m_isOnFire = false;
        fireParticle.Stop();
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
    }
    void FireState()
    {
        m_fireTimer += Time.deltaTime;
        m_timerRepeatDamageInFire += Time.deltaTime;

        if (m_timerRepeatDamageInFire > m_timeToDamage)
        {
            m_timerRepeatDamageInFire = 0;
            ReceiveDamageWithFire(m_damagePerSecond);
        }
        if (m_fireTimer > m_timeInFire)
        {
            StopFireState();
        }
    }
    public bool GetIsOnFire()
    {
        return m_isOnFire;
    }

    //Funciones de estado de invulnerabilidad
    public void StartInvulnerabilityState()
    {
        m_isInvulnerable = true;
        m_flashTimer = 0;
        m_invulnerableTimer = 0;
    }
    void StopInvulnerability()
    {
        m_flashTimer = 0;
        m_invulnerableTimer = 0;

        m_isInvulnerable = false;
        rendererToFlash.enabled = true;
    }
    void InvulnerableState()
    {
        m_invulnerableTimer += Time.deltaTime;
        m_flashTimer += Time.deltaTime;
        if (rendererToFlash != null && m_flashTimer > flashTime)
        {
            rendererToFlash.enabled = !rendererToFlash.enabled;
            m_flashTimer = 0;
        }
        if (m_invulnerableTimer > invulnerabilityTime)
        {
            StopInvulnerability();
        }
    }

    public void ResetPlayerStates()
    {
        //FireState
        StopInvulnerability();
        //Invulnerability
        StopFireState();

        StopAllCoroutines();
    }

    //Funciones de daño
    public void DamagePlayer(int damage)
    {
        if ((!m_isInvulnerable))
        {
            m_mySetAnimationVariables.SetHitTrigger();

            /////CAMERA SHAKE///////
            impulse.GenerateImpulse();

            //El orden en importante, para que no mate al jugador y se inicie después la coroutina, haciendo que respawnee con invulnerabilidad
            StartInvulnerabilityState();
            HealthHeartsVisual.healthHeartsSystemStatic.Damage(damage);
            ///////////////////
        }
    }
    public void ReceiveDamageWithFire(int damageReceived) //No genera el estado de invulnerabilidad
    {
        m_mySetAnimationVariables.SetHitTrigger();

        /////CAMERA SHAKE///////
        impulse.GenerateImpulse();

        HealthHeartsVisual.healthHeartsSystemStatic.Damage(damageReceived);
        ///////////////////
    }
}
