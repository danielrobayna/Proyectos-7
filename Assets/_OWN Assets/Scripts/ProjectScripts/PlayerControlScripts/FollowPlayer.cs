﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : TemporalSingleton<FollowPlayer>
{
    [SerializeField] float m_velocity;
    [HideInInspector] public Vector3 desiredPosition;

    public void SetZDesiredPosition()
    {
        float zDesiredPos = 0;
        if(GameManager.Instance.ActualPlayerController != null && !InputManager.Instance.AreGameControlsPaused)
        {
            float playerZPosition = GameManager.Instance.ActualPlayerController.transform.position.z;
            zDesiredPos = (playerZPosition + (InputManager.Instance.aimTranform.position.z - playerZPosition) / InputManager.Instance.aimDistanceInCameraSizeFraction);
            desiredPosition = Vector3.MoveTowards(transform.position, new Vector3(desiredPosition.x, transform.position.y, zDesiredPos), m_velocity * Time.smoothDeltaTime);
        }
    }
    public void SetXDesiredPosition()
    {
        float xDesiredPos = 0;
        if (GameManager.Instance.ActualPlayerController != null && !InputManager.Instance.AreGameControlsPaused)
        {
            float playerXPosition = GameManager.Instance.ActualPlayerController.transform.position.x;
            xDesiredPos = (playerXPosition + (InputManager.Instance.aimTranform.position.x - playerXPosition) / InputManager.Instance.aimDistanceInCameraSizeFraction);
            desiredPosition = Vector3.MoveTowards(transform.position, new Vector3(xDesiredPos, transform.position.y, desiredPosition.z), m_velocity * Time.smoothDeltaTime);
        }
    }
    public Vector3 GetDesiredPosition()
    {
        float xDesiredPos = 0;
        float zDesiredPos = 0;

        float playerZPosition = GameManager.Instance.ActualPlayerController.transform.position.z;
        float playerXPosition = GameManager.Instance.ActualPlayerController.transform.position.x;

        xDesiredPos = (playerXPosition + (InputManager.Instance.aimTranform.position.x - playerXPosition) / InputManager.Instance.aimDistanceInCameraSizeFraction);
        zDesiredPos = (playerZPosition + (InputManager.Instance.aimTranform.position.z - playerZPosition) / InputManager.Instance.aimDistanceInCameraSizeFraction);

        return new Vector3(xDesiredPos, transform.position.y, zDesiredPos); ;
    }
    public void SetDesiredPosition(Vector3 newDesiredPos) //Se llama desde camera script para asegurarse de que se ejecuta antes que la corrección del confiner
    {
        desiredPosition = newDesiredPos;
    }

    public void SetPosition()
    {
        if (!InputManager.Instance.AreGameControlsPaused)
        {
            transform.position = desiredPosition;
        }
    }

    //Resetear la posición al cambiar de escena
    public void ResetPosition()
    {
        transform.position = new Vector3 (GameManager.Instance.realPlayerGO.transform.position.x, transform.position.y, GameManager.Instance.realPlayerGO.transform.position.z);
    }
    //Resetear la posición al cambiar de habitación
    public void ResetPositionToActualController()
    {
        transform.position = new Vector3(GameManager.Instance.ActualPlayerController.transform.position.x, transform.position.y, GameManager.Instance.ActualPlayerController.transform.position.z);
    }
}