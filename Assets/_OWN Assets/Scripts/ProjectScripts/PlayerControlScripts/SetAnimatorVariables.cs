﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimatorVariables : MonoBehaviour
{
    public Animator myAnimator;
    HashSet<string> parametersInAnimator = new HashSet<string>();

    private void Awake()
    {
        GetAnimatorParameters();
    }

    public void SetMovementX(float velocityY)
    {
        if (parametersInAnimator.Contains("MovSpeedX"))
        {
            myAnimator.SetFloat("MovSpeedX", velocityY);

        }
    }
    public void SetMovementZ(float velocityZ)
    {
        if (parametersInAnimator.Contains("MovSpeedZ"))
        {
            myAnimator.SetFloat("MovSpeedZ", velocityZ);
        }
    }
    public void SetIsMovingBool(bool isMoving)
    {
        if (parametersInAnimator.Contains("IsMoving"))
        {
            myAnimator.SetBool("IsMoving", isMoving);
        }
    }
    public void SetFearBool(bool fear)
    {
        if (parametersInAnimator.Contains("Fear"))
        {
            myAnimator.SetBool("Fear", fear);
        }
    }
    public void SetHitTrigger()
    {
        if (parametersInAnimator.Contains("Hit"))
        {
            myAnimator.SetTrigger("Hit");
        }
    }
    public void SetIsPossesedBool(bool value)
    {
        if (parametersInAnimator.Contains("IsPossessed"))
        {
            myAnimator.SetBool("IsPossessed", value);
        }
    }
    public void SetDodgeTrigger()
    {
        if (parametersInAnimator.Contains("Dodge"))
        {
            myAnimator.SetTrigger("Dodge");
        }
    }
    public void SetConsumeBool(bool consume)
    {
        if (parametersInAnimator.Contains("Consume"))
        {
            myAnimator.SetBool("Consume", consume);
        }
    }
    public void SetShootTrigger()
    {
        if (parametersInAnimator.Contains("Shoot"))
        {
            StartCoroutine(ShootTriggerCoroutine());
        }
    }
    IEnumerator ShootTriggerCoroutine()
    {
        myAnimator.SetTrigger("Shoot");
        yield return new WaitForEndOfFrame(); //En algunos casos tardaba demasiado en apagarse, así que lo hago de forma manual tras un frame
        myAnimator.ResetTrigger("Shoot");
    }
    public void ResetShootTrigger()
    {
        if (parametersInAnimator.Contains("Shoot"))
        {
            myAnimator.ResetTrigger("Shoot");
        }
    }

    public void SetAttackBool(bool isActive)
    {
        if (parametersInAnimator.Contains("Attack"))
        {
            myAnimator.SetBool("Attack", isActive);
        }
    }
    public void SetDashMultiplier(float newMultiplier)
    {
        if (parametersInAnimator.Contains("DashMultiplier"))
        {
            myAnimator.SetFloat("DashMultiplier", newMultiplier);
        }
    }
    public void SetShootMultiplier(float newMultiplier)
    {
        if (parametersInAnimator.Contains("ShootMultiplier"))
        {
            myAnimator.SetFloat("ShootMultiplier", newMultiplier);
        }
    }
    public void SetRotationDirectionFloat(float newRotationDirection)
    {
        if (parametersInAnimator.Contains("RotationDirection"))
        {
            myAnimator.SetFloat("RotationDirection", newRotationDirection);
        }
    }
    public void SetIsOnFireBool(bool isOnFire)
    {
        if (parametersInAnimator.Contains("IsOnFire"))
        {
            myAnimator.SetBool("IsOnFire", isOnFire);
        }
    }
    public void SetShieldBool(bool IsShieldUp)
    {
        if (parametersInAnimator.Contains("IsShieldUp"))
        {
            myAnimator.SetBool("IsShieldUp", IsShieldUp);
        }
    }

    //Únicas del player, replantear si ponerlas directamente en sus scripts correspondientes
    public void SetControlTrigger()
    {
        if (parametersInAnimator.Contains("Control"))
        {
            myAnimator.SetTrigger("Control");
        }
    }
    public void SetIsAimingBool(bool isAiming)
    {
        if (parametersInAnimator.Contains("IsAiming"))
        {
            myAnimator.SetBool("IsAiming", isAiming);
        }
    }
    public void SetShockedBool(bool isShocked)
    {
        if(parametersInAnimator.Contains("IsShocked"))
        {
            myAnimator.SetBool("IsShocked", isShocked);
        }
    }
    public void SetSurprisedBool(bool isSurprised)
    {
        if (parametersInAnimator.Contains("IsSurprised"))
        {
            myAnimator.SetBool("IsSurprised", isSurprised);
        }
    }
    public void SetDeadTrigger()
    {
        if (parametersInAnimator.Contains("Dead"))
        {
            myAnimator.SetTrigger("Dead");
        }
    }
    
    public void SetIsPatrollingBool(bool IsPatrolling)
    {
        if (parametersInAnimator.Contains("IsPatrolling"))
        {
            myAnimator.SetBool("IsPatrolling", IsPatrolling);
        }
    }

    void GetAnimatorParameters()
    {
        foreach (AnimatorControllerParameter param in myAnimator.parameters)
        {
            parametersInAnimator.Add(param.name);
        }
    }

    // ANIMACIONES BOSS 2
    public void SetTauntTrigger()
    {
        if (parametersInAnimator.Contains("TauntTrigger"))
        {
            myAnimator.SetTrigger("TauntTrigger");
        }
    }
    public void SetGrenadeTrigger()
    {
        if (parametersInAnimator.Contains("ThrowGrenadeTrigger"))
        {
            myAnimator.SetTrigger("ThrowGrenadeTrigger");
        }
    }
    public void SetWaveAttackTrigger()
    {
        if (parametersInAnimator.Contains("WaveAttackTrigger"))
        {
            myAnimator.SetTrigger("WaveAttackTrigger");
        }
    }
    public void SetChangeToPhase2Trigger()
    {
        if (parametersInAnimator.Contains("ChangeToPhase2Trigger"))
        {
            myAnimator.SetTrigger("ChangeToPhase2Trigger");
        }
    }
    public void SetAreaAttackTrigger()
    {
        if (parametersInAnimator.Contains("AreaAttackTrigger"))
        {
            myAnimator.SetTrigger("AreaAttackTrigger");
        }
    }
    
    public void SetBigCrystalTrigger()
    {
        if (parametersInAnimator.Contains("BigCrystalTrigger"))
        {
            myAnimator.SetTrigger("BigCrystalTrigger");
        }
    }
    public void SetSmallCrystalsTrigger()
    {
        if (parametersInAnimator.Contains("SmallCrystalsTrigger"))
        {
            myAnimator.SetTrigger("SmallCrystalsTrigger");
        }
    }
    public void SetPrepareChargeTrigger()
    {
        if (parametersInAnimator.Contains("PrepareChargeTrigger"))
        {
            myAnimator.SetTrigger("PrepareChargeTrigger");
        }
    }
    public void SetStartChargeTrigger()
    {
        if (parametersInAnimator.Contains("StartChargeTrigger"))
        {
            myAnimator.SetTrigger("StartChargeTrigger");
        }
    }
    public void SetHitPlayerOnChargeBool(bool HitPlayerOnChargeBool)
    {
        if (parametersInAnimator.Contains("HitPlayerOnChargeBool"))
        {
            myAnimator.SetBool("HitPlayerOnChargeBool", HitPlayerOnChargeBool);
        }
    }
    public void SetHitWallOnChargeBool(bool HitWallOnChargeBool)
    {
        if (parametersInAnimator.Contains("HitWallOnChargeBool"))
        {
            myAnimator.SetBool("HitWallOnChargeBool", HitWallOnChargeBool);
        }
    }
    public void SetIsStunnedBool(bool IsStunnedBool)
    {
        if (parametersInAnimator.Contains("IsStunnedBool"))
        {
            myAnimator.SetBool("IsStunnedBool", IsStunnedBool);
        }
    }
    public void SetDeathTrigger()
    {
        if (parametersInAnimator.Contains("DeathTrigger"))
        {
            myAnimator.SetTrigger("DeathTrigger");
        }
    } 
}
