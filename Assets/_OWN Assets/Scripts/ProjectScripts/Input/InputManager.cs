﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputManager : TemporalSingleton<InputManager>
{
    public PlayerInput m_myPlayerInput;
    PlayerInputAsset m_myInputAsset;
    [SerializeField] RectTransform mirilla;
    Image m_mirillaImage;
    [SerializeField] Sprite playerAimSprite;
    [SerializeField] Sprite enemyAimSprite;
    public void SetMirilla(bool setToPlayer)
    {
        if (setToPlayer)
        {
            m_mirillaImage.sprite = playerAimSprite;
        }
        else
        {
            m_mirillaImage.sprite = enemyAimSprite;
        }
    }

    [SerializeField] private LayerMask planeToRaycastAim;
    bool m_gameControlsArePaused = false;
    bool isControllingWithController;

    public bool IsControllingWithController
    {
        get
        {
            return isControllingWithController;
        }
    }
    public bool AreGameControlsPaused
    {
        get
        {
            return m_gameControlsArePaused;
        }
    }
    private void Start()
    {
        m_myInputAsset = new PlayerInputAsset();
        m_myInputAsset.Enable();
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        InputUser.onChange += ControlSchemeChange;
        ChangeInputScheme(m_myPlayerInput.currentControlScheme);
        CenterAimTranformInPlayer();
        m_mirillaImage = mirilla.GetComponent<Image>();
        mainCamera = Camera.main;
        SelectedObjectHandler.Instance.Init(); //Inicia el singleton en caso de empezar directamente en game(para cuando no estás en build)
    }
    public void OnChangeInputDeactivateEvent()
    {
        InputUser.onChange -= ControlSchemeChange;
    }
    void ControlSchemeChange(InputUser user, InputUserChange change, InputDevice device)
    {
        if (change == InputUserChange.ControlSchemeChanged)
        {
            ChangeInputScheme(user.controlScheme.Value.name);
        }
    }
    void ChangeInputScheme(string schemeName)
    {
        if (schemeName.Equals("Keyboard & Mouse"))
        {
            StartControllingWithMouse();
        }
        else
        {
            StartControllingWithGamepad();
        }
    }
    void StartControllingWithMouse()
    {
        isControllingWithController = false;
        mirilla.gameObject.SetActive(true); //activa la mirilla
        GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().StartControllingWithMouse();
        EventSystem.current.SetSelectedGameObject(null); //Desactiva la selección del botón
    }
    void StartControllingWithGamepad()
    {
        isControllingWithController = true;
        mirilla.gameObject.SetActive(false); //Desactiva la mirilla 
        GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().StopControllingWithMouse();
        SelectedObjectHandler.Instance.SetSelectedGameObjectOnControllerReconect();//Selecciona el objeto guardado en selectObjectHandler cuando se comienza a controlar con mando
    }

    //Apuntado
    [Header("Variables del apuntado")]
    public Transform aimTranform;
    public float aimTransformHeight = 1.5f;
    [SerializeField] float mouseVelocity = 0.5f;
    public float MouseVelocity
    {
        get
        {
            return mouseVelocity;
        }
        set
        {
            mouseVelocity = value;
        }
    }
    [SerializeField] float aimVelocityWithController;
    Camera mainCamera;
    [SerializeField] float aimImageWidht;
    [Header("Variables Lookahead")]
    public float aimDistanceWithController;
    public float aimDistanceInCameraSizeFraction;

    [Header("Variables de control del auto aim")]
    public float angleAimPriority;
    public float maxAimAngle;
    public float distanceAimPriority;
    public LayerMask autoAimRaycastLayerMask;

    public void CenterAimTranformInPlayer()
    {
        Vector3 playerPos = GameManager.Instance.ActualPlayerController.transform.position;
        aimTranform.position = new Vector3(playerPos.x, 1.5f, playerPos.z);
    }
    private void Update()
    {
        if (m_myPlayerInput.currentControlScheme.Equals("Keyboard & Mouse") && !AreGameControlsPaused)
        {
            MoveAimPositionWithMouse();
        }
        SetYAimTransformPos();
        //Reposicionar la mirilla en la pantalla
        mirilla.position = GameManager.Instance.mainCamera.WorldToScreenPoint(aimTranform.position);
    }
    void SetYAimTransformPos()
    {
        //La corrección se debe a que si varía la posición en "y" y solo se cambia la z, se proyecta incorrectamente, de forma que hay que calcular la diferencia en altura y ver cuánta distancia en z hay que añadir
        float heightDiference = aimTranform.position.y - aimTransformHeight;
        float correction = Mathf.Tan((90 - mainCamera.transform.eulerAngles.x) * Mathf.Deg2Rad) * heightDiference;

        aimTranform.position = new Vector3(aimTranform.position.x, aimTransformHeight, aimTranform.position.z + correction);
    }
    public Vector3 RotationInput()
    {
        if (!m_gameControlsArePaused)
        {
            if (m_myPlayerInput.currentControlScheme.Equals("Gamepad"))
            {
                Vector2 inputVector = m_myInputAsset.PlayerInputActions.Rotating.ReadValue<Vector2>();
                Vector3 inputVectorToWorld = new Vector3(inputVector.x, 0, inputVector.y);
                SetYAimTransformPos();
                Vector3 aimPosition = GameManager.Instance.ActualPlayerController.transform.position + inputVectorToWorld.normalized * aimDistanceWithController * aimDistanceInCameraSizeFraction * inputVector.magnitude;
                aimTranform.position = Vector3.MoveTowards(aimTranform.position, aimPosition, Time.deltaTime * aimVelocityWithController);//Se utiliza aimDistanceInCameraSizeFraction porque el follow player divide la posición entre esto
                return new Vector3(inputVector.x, 0, inputVector.y);
            }
            if (m_myPlayerInput.currentControlScheme.Equals("Keyboard & Mouse"))
            {
                //Creación del vector de apuntado
                Vector3 directionVector = (aimTranform.position - GameManager.Instance.ActualPlayerController.transform.position);
                return new Vector3(directionVector.x, 0, directionVector.z);
            }
        }
        return new Vector3(0, 0, 0);
    }

    void MoveAimPositionWithMouse()
    {
        if (!m_gameControlsArePaused && ZoneManager.Instance.m_activeCamera != null)
        { 
            //Variables necesarias 
            PlayerControl_MovementController playerController = GameManager.Instance.ActualPlayerController;
            CameraScript actualcamera = ZoneManager.Instance.m_activeCamera.GetComponent<CameraScript>();

            //Cálculo de la posición de la mira
            Vector2 mouseMovement = m_myInputAsset.PlayerInputActions.MouseMovement.ReadValue<Vector2>();
            Vector3 mouseMoventToWorld = new Vector3(mouseMovement.x, 0, mouseMovement.y) * mouseVelocity * Time.smoothDeltaTime;
            aimTranform.position += mouseMoventToWorld;
            //Límite de posición en x de la posición de apuntado
            RaycastHit hit;

            Physics.Raycast(mainCamera.ScreenPointToRay(new Vector3(mainCamera.pixelWidth - aimImageWidht, mainCamera.pixelHeight - aimImageWidht, 0)), out hit, 300f, planeToRaycastAim);
            Vector3 upperRightCorner = hit.point; 

            Physics.Raycast(mainCamera.ScreenPointToRay(new Vector3(mainCamera.pixelWidth - aimImageWidht, aimImageWidht, 0)), out hit, 300f, planeToRaycastAim);
            Vector3 downRightCorner = hit.point;

            Physics.Raycast(mainCamera.ScreenPointToRay(new Vector3(aimImageWidht, mainCamera.pixelHeight - aimImageWidht, 0)), out hit, 300f, planeToRaycastAim);
            Vector3 upperLeftCorner = hit.point;

            Physics.Raycast(mainCamera.ScreenPointToRay(new Vector3(aimImageWidht, aimImageWidht, 0)), out hit, 300f, planeToRaycastAim);
            Vector3 downLeftCorner = hit.point;

            float actualXRightLimitPos = (((aimTranform.position.z - downRightCorner.z) / (upperRightCorner.z - downRightCorner.z)) * (upperRightCorner.x - downRightCorner.x)) + downRightCorner.x;
            float actualXLeftLimitPos = (((aimTranform.position.z - downLeftCorner.z) / (upperLeftCorner.z - downLeftCorner.z)) * (upperLeftCorner.x - downLeftCorner.x)) + downLeftCorner.x;

            if (aimTranform.position.x > actualXRightLimitPos)
            {
                aimTranform.position = new Vector3(actualXRightLimitPos, aimTranform.position.y, aimTranform.position.z);
            }
            else if (aimTranform.position.x < actualXLeftLimitPos)
            {
                aimTranform.position = new Vector3(actualXLeftLimitPos, aimTranform.position.y, aimTranform.position.z);
            }

            //Límite de posición en z de la posición de apuntado
            float zDistanceAngleCorrection = Mathf.Tan((90 - mainCamera.transform.eulerAngles.x) * Mathf.Deg2Rad) * aimTranform.position.y;
            float zMaxPos = upperLeftCorner.z - zDistanceAngleCorrection;
            float zMinPos = downLeftCorner.z - zDistanceAngleCorrection;

            if (aimTranform.position.z > zMaxPos)
            {
                aimTranform.position = new Vector3(aimTranform.position.x, aimTranform.position.y, zMaxPos);
            }
            else if (aimTranform.position.z < zMinPos)
            {
                aimTranform.position = new Vector3(aimTranform.position.x, aimTranform.position.y, zMinPos);
            }
        }
    }
    public float HorizontalMovement()
    {
        if (!m_gameControlsArePaused)
        {
            return m_myInputAsset.PlayerInputActions.HorizontalMovement.ReadValue<float>();
        }
        return 0;
    }
    public float VerticalMovement()
    {
        if (!m_gameControlsArePaused)
        {
            return m_myInputAsset.PlayerInputActions.VerticalMovement.ReadValue<float>();
        }
        return 0;
    }
    public bool DashButtonTriggered()
    {
        if (!m_gameControlsArePaused)
        {
            return m_myInputAsset.PlayerInputActions.DashButton.triggered;
        }
        return false;
        
    }
    public float LeftTrigger()
    {
        if (!m_gameControlsArePaused)
        {
            return m_myInputAsset.PlayerInputActions.LeftTrigger.ReadValue<float>();
        }
        return 0;
    }
    public float RightTrigger()
    {
        if (!m_gameControlsArePaused)
        {
            return m_myInputAsset.PlayerInputActions.RightTrigger.ReadValue<float>();
        }
        return 0;
    }
    public bool ActionButtonTriggered()
    {
        return m_myInputAsset.PlayerInputActions.ActionButton.triggered;
    }
    public bool MapButtonTriggered()
    {
        return m_myInputAsset.PlayerInputActions.MapButton.triggered;
    }
    public bool PauseButtonTriggered()
    {
        return m_myInputAsset.PlayerInputActions.PauseButton.triggered;
    }
    public void PauseGameInputs()
    {
        m_gameControlsArePaused = true;
    }
    public void UnpauseGameInputs()
    {
        m_gameControlsArePaused = false;
    }
    public void OnDeviceLost()
    {
        if (UIManager.Instance != null && GameManager.Instance !=null && !GameManager.Instance.IsGamePaused())
        {
            UIManager.Instance.UsePauseButton();
        }
    }
    public void OnDeviceRegained()
    {
    }
}