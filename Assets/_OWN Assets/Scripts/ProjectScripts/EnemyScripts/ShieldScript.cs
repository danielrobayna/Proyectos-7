﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour
{
    [Header("Bool de control del escudo, poner a TRUE")]
    public bool shieldActive = true;
    private bool shieldBroken = false;

    [Header("Vida del Escudo Antes de Romperse")]
    [SerializeField] private int maxShieldHealth; //De base, balas normales hacen 1 de Daño, sniper 2 y Railgun destruye de 1 toque
    private int currentShieldHealth;
    private MeshRenderer shieldMesh;
    private Color initialShieldColor;

    [Header("Tiempo para recuperar escudo")]
    [SerializeField] private float timeToRecoverShield;

    [Header("Collider del escudo")]
    [SerializeField] private Collider shieldCollider;

    [Header("El modelo escudo")]
    [SerializeField] private GameObject shieldModel;

    [Header("Las partículas de abrir, cerrar y romper escudo")]
    [SerializeField] private ParticleSystem openShieldParticle;
    [SerializeField] private ParticleSystem closeShieldParticle;
    [SerializeField] private ParticleSystem breakShieldParticle;

    [Header("Animator del padre, si tiene")]
    private Animator parentAnimator;

    [Header("Partícula de impacto contra escudo")]
    public GameObject shieldImpactParticle;

    private void Start()
    {
        currentShieldHealth = maxShieldHealth;
        shieldMesh = shieldModel.GetComponent<MeshRenderer>();
        initialShieldColor = shieldMesh.material.GetColor("_MainColor");
        parentAnimator = GetComponentInParent<Animator>();
    }

    public void ShieldChangeLayer(int newLayer)
    {
        gameObject.layer = newLayer;
    }

    public void ActivateShield()
    {
        if (shieldActive == false && !shieldBroken)
        {
            shieldActive = true;

            shieldModel.SetActive(true);
            shieldCollider.enabled = true;

            if (parentAnimator != null) //Animación en el padre
            {
                parentAnimator.SetBool("IsShieldUp", true);
            }

            if (!openShieldParticle.isPlaying)
            {
                openShieldParticle.Play();
            }

            MusicManager.Instance.PlaySound(AppSounds.SHIELD_DEPLOY, this.transform); //Fx
        }
    }

    public void DeactivateShield()
    {
        if (shieldActive == true)
        {
            shieldActive = false;

            shieldModel.SetActive(false);
            shieldCollider.enabled = false;

            if (parentAnimator != null) //Animación en el padre
            {
                parentAnimator.SetBool("IsShieldUp", false);
            }

            if (!closeShieldParticle.isPlaying)
            {
                closeShieldParticle.Play();
            }

            MusicManager.Instance.PlaySound(AppSounds.SHIELD_HIDE, this.transform); //Fx
        }
    }

    public void BreakShield()
    {
        shieldActive = false;
        shieldBroken = true;

        shieldModel.SetActive(false);
        shieldCollider.enabled = false;

        if (!breakShieldParticle.isPlaying)
        {
            breakShieldParticle.Play();
        }

        if (parentAnimator != null) //Animación en el padre
        {
            parentAnimator.SetTrigger("ShieldBroke");
            parentAnimator.SetBool("ShieldBroken", true);
            parentAnimator.SetBool("IsShieldUp", false);
        }

        MusicManager.Instance.PlaySound(AppSounds.SHIELD_BREAK, this.transform); //Fx

        StartCoroutine(RecoverShield());
    }

    IEnumerator RecoverShield()
    {
        yield return new WaitForSeconds(timeToRecoverShield);

        shieldBroken = false;
        currentShieldHealth = maxShieldHealth;
        RefreshShieldColorOnHealth();

        if (parentAnimator != null) //Animación en el padre
        {
            parentAnimator.SetBool("ShieldBroken", false);
        }
    }

    public void RefreshShieldColorOnHealth() //Cambiar color de escudo en función de la vida del mismo
    {        
        shieldMesh.material.SetColor("_MainColor", Color.Lerp(Color.red, initialShieldColor, (float)currentShieldHealth/maxShieldHealth));
    }

    public void ShieldTakeDamage(int damage)
    {
        if (currentShieldHealth > 0)
        {
            currentShieldHealth -= damage;
        }
        if(currentShieldHealth <= 0)
        {
            BreakShield();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        BulletBase bullet = other.GetComponent<BulletBase>();
        Grenade grenade = other.GetComponent<Grenade>();

        if (bullet != null) //Rebote de la bala
        {
            Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
            Vector3 newBulletDirection = Vector3.Reflect(bullet.transform.right, transform.forward) * bulletRb.velocity.magnitude; //Crear vector reflejado de la colisión de la bala con el escudo, multiplicado por la velocidad.

            Instantiate(shieldImpactParticle, bullet.transform.position, transform.rotation);

            bulletRb.velocity = newBulletDirection;     //Aplicar nuevo vector reflejado a la velocidad de la bala
            bullet.transform.rotation = Quaternion.LookRotation(newBulletDirection) * Quaternion.Euler(0, -90, 0);    //Rotar la bala hacia la nueva dirección con una corrección para que sea el transform.right el que mira en la nueva dirección.  

            ShieldTakeDamage(1); //Default de balas normales
            RefreshShieldColorOnHealth();
            MusicManager.Instance.PlaySound(AppSounds.SHIELD_BOUNCE, this.transform); //Fx rebote de la bala
        }

        else if(grenade != null)
        {
            Instantiate(shieldImpactParticle, grenade.transform.position, transform.rotation);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.GetComponent<BulletBase>() != null)
        {
            other.gameObject.layer = 0;
        }
    }
}
