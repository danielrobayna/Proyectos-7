﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTurret_MovementController : EnemyControl_MovementController
{
    AudioSource rotationAudioSource;
    protected override void Update()
    {
        base.Update();
        PlayRotationSound();
    }

    void PlayRotationSound()
    {
        if (previousRotationVelocity != 0 && !isBeingConsumed)
        {
            if (rotationAudioSource == null)
            {
                rotationAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.TURRET_MOVE);
            }
        }
        else if(rotationAudioSource != null)
        {
            rotationAudioSource.Stop();
        }
    }
    private void OnDisable()
    {
        if (rotationAudioSource != null)
        {
            rotationAudioSource.Stop();
        }
    }
}
