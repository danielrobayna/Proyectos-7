﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyControl_MovementController : PlayerControl_MovementController  ///////////HEREDA DE PLAYERCONTROL_MOVEMENTCONTROLLER////////////
{
    protected EnemySetControl thisEnemySetControl;
    ShootingScript thisEnemyShootingScript;
    EnemyHealth thisEnemyHealthScript;

    [Header("Variables para usar los Triggers de Mando como Botón")]
    private bool leftTrigger_isAxisInUse;
    private bool canUseLeftTrigger;
    private float misclickBufferForUnpossess = 0.2f;        //Editar misclick buffer AQUÍ
    private float canUseLT_Timer = 0;  
    [Header("Tiempo que tiene que estar mantenido el LeftTrigger para que cuente como Hold")]
    public float leftTriggerHoldTime;
    private float _leftTriggerTimer = 0;
    public float leftTriggerTimer // Para modificar el valor desde fuera
    {
        get { return _leftTriggerTimer; }
        set { _leftTriggerTimer = value; }
    }

    [Header("Booleano que indica si está siendo consumido")]
    private bool startedConsuming = false;
    private bool _isBeingConsumed = false;
    public bool isBeingConsumed
    {
        get { return _isBeingConsumed; }
        set { _isBeingConsumed = value; }
    }

    [Header("Indicar SI PUEDE O NO hacer Dash (Tick -> SI)")]
    public bool ableToDash;

    [Header("Variables del Dash")]
    public float dashTime;
    public float dashForce;
    private Vector3 lastMovementDirection = new Vector3(1, 0, 0);
    private bool isDashing = false;
    [SerializeField] float dashCooldown;
    float dashCooldownTimer;

    [Header("¿Debe quedarse quieto al disparar? (E.j. Meathead)")]  //No tocar el orden de estos 2 header, parece raro y raro es, pero así es como se ven correctamente en editor
    [Space(-10)]
    [Header("Rellenar SOLO si el enemigo se va a quedar quieto al disparar")]
    [SerializeField] private bool stopOnShoot;

    [Header("¿Expulsar al jugador de la posesión tras x segundos? 0 = NO")] //No tocar el orden de estos 2 header, parece raro y raro es, pero así es como se ven correctamente en editor
    [Space(-10)]
    [Header("Rellenar SOLO si el enemigo expulsa al jugador")]
    [SerializeField] private float timeToUnpossessByForce;
    private float forcedUnpossessTimer = 0;
    private bool _stopUnpossessTimer = false;
    public bool stopUnpossessTimer
    {
        get { return _stopUnpossessTimer; }
        set { _stopUnpossessTimer = value; }
    }
    [Header("Feedback tiempo de desposesión (en el hueso Head)")]
    [Space(-10)]
    [SerializeField] GameObject forcedUnposessCanvas;
    [SerializeField] Image angryIcon;
    [SerializeField] Image unpossessTimeImage;

    [HideInInspector] public Spawner spawnerInstantiatedFrom;

    void OnEnable()
    {
        canUseLeftTrigger = false;  ////Este bool impide que al poseer al enemigo con el left trigger, se inicie inmediatamente el Input de LeftTrigger que desposee al enemigo en este script      
        canUseLT_Timer = 0;
    }

    private void OnDisable() //Desactivar feedback si desposee tras x tiempo
    {
        if (timeToUnpossessByForce != 0)
        {
            forcedUnpossessTimer = 0;
            unpossessTimeImage.fillAmount = 0;
            forcedUnposessCanvas.SetActive(false);
        }

        leftTrigger_isAxisInUse = false; //Resetear variables de leftTrigger y de consumición
        startedConsuming = false;
        isBeingConsumed = false;
        leftTriggerTimer = 0;
        thisEnemySetControl.StopConsumingAction();
    }

    protected override void Start()
    {
        base.Start();
        thisEnemySetControl = GetComponent<EnemySetControl>();
        thisEnemyShootingScript = GetComponent<ShootingScript>();
        thisEnemyHealthScript = GetComponent<EnemyHealth>();
    }

    protected override void Update()
    {
        if(!canUseLeftTrigger && canUseLT_Timer < misclickBufferForUnpossess) //Buffer de tiempo para evitar misclicks al poseer y desposeer inmediatamente.
        {
            canUseLT_Timer += 1 * Time.deltaTime;
        }

        else if(!canUseLeftTrigger && canUseLT_Timer >= misclickBufferForUnpossess)  //Buffer de tiempo para evitar misclicks al poseer y desposeer inmediatamente.
        {
            canUseLeftTrigger = true;
        }

        if(timeToUnpossessByForce != 0)
        {
            forcedUnposessCanvas.SetActive(true);
            if (_stopUnpossessTimer == false)
            {
                forcedUnpossessTimer += 1 * Time.deltaTime;
            }
            unpossessTimeImage.fillAmount = forcedUnpossessTimer / timeToUnpossessByForce;
            angryIcon.color = Color.Lerp(Color.white, Color.red, forcedUnpossessTimer / timeToUnpossessByForce);

            if(forcedUnpossessTimer >= timeToUnpossessByForce)
            {
                forcedUnpossessTimer = 0;
                unpossessTimeImage.fillAmount = 0;
                forcedUnposessCanvas.SetActive(false);
                thisEnemySetControl.UnpossessEnemy(Vector3.zero, EnemySetControl.UnposessTypes.force);
            }
        }
        if (dashCooldownTimer > 0)
        {
            dashCooldownTimer -= Time.deltaTime;
        }

        LeftTriggerInput();

        if (!isBeingConsumed) //Si no hay input de Desposesión/Consumición
        {
            ////Si no hay ShootingScript Y no esta evadiendo, se quita el control. Si hay Shooting Script Y (el enemigo se para al disparar Y no está evadiendo) O (no se para y no está evadiendo), se quita el control.
            if ((thisEnemyShootingScript == null && !isDashing) || (thisEnemyShootingScript != null && ((thisEnemyShootingScript.IsEnemyAbleToShoot() && stopOnShoot) && !isDashing) || (!stopOnShoot && !isDashing)))
            {
                base.Update();

                RightTriggerInput();

                CheckLastMovementDirection();

                if (InputManager.Instance.DashButtonTriggered()) ////Input de Dash
                {
                    if (ableToDash && dashCooldownTimer <= 0)
                    {
                        m_mySetAnimationVariables.SetDodgeTrigger();
                        dashCooldownTimer = dashCooldown;
                        StartCoroutine(DashLogicCoroutine());
                    }
                }
            }

            else if (!isDashing && !thisEnemyShootingScript.IsEnemyAbleToShoot() && stopOnShoot) //Si no está evadiendo y está en el proceso de disparar parado, se quita la velocidad de moviemiento.
            {

                controlRb.velocity = Vector3.zero;
                ControlArmRotation();
                UpdateObjectRotation();
            }

            else if (isDashing)  //Si está evadiendo, solo se rota el jugador en la dirección de moviemiento.
            {
                UpdateObjectRotation();
            }
        }
    }

/////////////////// DASHING /////////////////////////

    private void CheckLastMovementDirection() ////Se guarda la última dirección de movimiento como la dirección de Dash y desposesión si hay Input
    {
        if (InputManager.Instance.HorizontalMovement() != 0 || InputManager.Instance.VerticalMovement() != 0)
        {
            lastMovementDirection = new Vector3(controlSpeedX, 0, controlSpeedZ);
        }
    }

    private IEnumerator DashLogicCoroutine() ////Se aplica el Dash
    {
        isDashing = true;
        controlRb.velocity = lastMovementDirection.normalized * dashForce;
        angle = Vector3.SignedAngle(controlRb.velocity, Vector3.right, Vector3.up);

        if (thisEnemyShootingScript != null && thisEnemyShootingScript.isParticle_Thrower && thisEnemyShootingScript.thisEnemy_Flamethrower!=null)
        {
            thisEnemyShootingScript.DisableThrower(); //desactivado del lanzallamas por si estás disparando
        }

        ////ANIMACIONES////
        m_mySetAnimationVariables.SetDodgeTrigger();
        ///FINAL ANIMACIONES/////

        MusicManager.Instance.PlaySound(AppSounds.HUMAN_DASH);

        yield return new WaitForSeconds(dashTime);

        //yield return new WaitForSeconds(0.1f); //Espera para que no se mueva ni rote hasta pasado un pequeño delay
        isDashing = false;
    }

    public bool IsDashing()
    {
        return isDashing;
    }

    private void UnpossessAction()
    {
        MusicManager.Instance.PlaySound(AppSounds.HUMAN_THROW);
        thisEnemySetControl.UnpossessEnemy(lastMovementDirection, EnemySetControl.UnposessTypes.normal);
    }

    private void ConsumeAction()
    {
        thisEnemySetControl.StartConsuming();
    }
 
    private void AttackAction() 
    {
        if (thisEnemyShootingScript!=null && !gameObject.TryGetComponent(out BomberSetControl bomberSetControl)) //Evitar disparo si es bomber y que no le explote al jugador
        {
            thisEnemyShootingScript.FireInShootingPos(ShootingScript.whoIsShooting.player);            
        }
    }

    private void CallShootingScriptReset() ////Llama al Reset de disparo de ShootingScript. VER el método en Shooting Script       !!!!
    {
         thisEnemyShootingScript.ResetPlayerFiringRateTimer();
    }

    private void LeftTriggerInput()
    {
        if (InputManager.Instance.LeftTrigger() != 0)
        {
            if (canUseLeftTrigger && !isDashing)
            {
                leftTriggerTimer += 1 * Time.deltaTime;
                leftTrigger_isAxisInUse = true;

                if (leftTriggerTimer >= leftTriggerHoldTime && !startedConsuming)
                {
                    startedConsuming = true;
                    ConsumeAction();
                }
            }
        }
        else if (InputManager.Instance.LeftTrigger() == 0)
        {
            if (leftTrigger_isAxisInUse && canUseLeftTrigger)
            {
                if (leftTriggerTimer < leftTriggerHoldTime)
                {
                    UnpossessAction();
                }

                else
                {
                    thisEnemySetControl.StopConsumingAction();
                }
            }

            startedConsuming = false;
            leftTriggerTimer = 0;

            if (canUseLT_Timer >= misclickBufferForUnpossess)
            {
                canUseLeftTrigger = true;
            }
            leftTrigger_isAxisInUse = false;
        }
    }

    private void RightTriggerInput()
    {
        if (InputManager.Instance.RightTrigger() != 0)
        {
            if (thisEnemyShootingScript != null)
            {
                AttackAction();
            }
        }
        else if (InputManager.Instance.RightTrigger() == 0)
        {
            if (thisEnemyShootingScript != null)
            {
                CallShootingScriptReset(); //Se llama al reset al dejar de pulsar Trigger
            }
        }
    }

    /////////////////////////////// ANIMACIONES///////////////////////////////////////////////

    float previousAngle;
    protected float previousRotationVelocity;

    protected override void SetAnimationsVariables()
    {    
        float forwardAngleToWorld = Vector3.SignedAngle(armObject.transform.right,Vector3.forward,Vector3.up);

        //Cálculo del vector de la dirección de movimiento relativa a la rotación actual
        Vector3 previousRelativeVector = relativeVector;
        relativeVector = (Quaternion.AngleAxis(forwardAngleToWorld, Vector3.up) * controlRb.velocity).normalized;
        relativeVector = Vector3.Lerp(previousRelativeVector, relativeVector, Time.deltaTime * 10);

        m_mySetAnimationVariables.SetIsMovingBool(controlRb.velocity.magnitude > 0.1f); //Seteo del booleano para detectar si se mueve

        m_mySetAnimationVariables.SetMovementX(relativeVector.x);
        m_mySetAnimationVariables.SetMovementZ(relativeVector.z);

        //Cálculo del multiplicador de dash
        float multiplier = 1.4f/ dashTime;
        m_mySetAnimationVariables.SetDashMultiplier(multiplier);

        //Cálculo de la rotación
        float rotationVelocity = angle-previousAngle; //En grados por frame

        if (Time.deltaTime != 0) //Comprobación pare evitar el error al dividir por 0
        {
            rotationVelocity /= Time.deltaTime; //En grados por segundo
        }
        else
        {
            rotationVelocity = 0;
        }

        if (rotationVelocity > 20f || rotationVelocity < -20f)
        {
            rotationVelocity = Mathf.Lerp(previousRotationVelocity, rotationVelocity, 5f * Time.deltaTime);
            m_mySetAnimationVariables.SetRotationDirectionFloat(rotationVelocity);
        }
        else
        {
            rotationVelocity = Mathf.Lerp(previousRotationVelocity, 0, 10f * Time.deltaTime);
            m_mySetAnimationVariables.SetRotationDirectionFloat(rotationVelocity);
        }
        previousRotationVelocity = rotationVelocity;
        previousAngle = angle;

        //Minimap animator
        m_minimaparrowAnimator.SetFloat("Angle", forwardAngleToWorld);
    }

    protected override void PlayMovementSound()
    {
          //Debe estar vacío para que no reproduzca sonido por código
    }
}
