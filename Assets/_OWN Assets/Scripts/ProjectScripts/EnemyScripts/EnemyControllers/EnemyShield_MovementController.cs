﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShield_MovementController : EnemyControl_MovementController          ////////Hereda de EnemyControl////////
{
    private EnemyAI_Shield thisEnemyShieldAI;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        thisEnemyShieldAI = (EnemyAI_Shield)GetComponent<EnemyAI_Standard>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (controlRb.velocity == Vector3.zero)
        {
            thisEnemyShieldAI.shieldScript.ActivateShield();
        }

        else
        {
            thisEnemyShieldAI.shieldScript.DeactivateShield();
        }
    }
}
