﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeatBarScript : MonoBehaviour                      //Script de la barra de calor para torretas y el Boss 1//
{
    [Header ("Imagen UI de la Barra de Calor y su Marco")]
    [SerializeField] protected Image heatBarImage;
    [SerializeField] private Animator heatBarFrameAnimator;
    protected int boolAnimatorID;
    [Header("Cantidad de calor a AÑADIR a la barra por segundo")]
    [SerializeField][Range(0,1)] protected float fillAmountPerSecond;
    protected bool beingBurned = false;
    [Header("Tiempo para empezar a vaciar la barra de calor")]
    [SerializeField] private float timeToStartUnfill;
    protected float unfillTimer = 0;
    [Header("Cantidad de calor a VACIAR a la barra por segundo")]
    [SerializeField][Range(0, 1)] private float unfillAmountPerSecond;

    [Header("El SetControl del enemigo para llamar a métodos de muerte")]
    private EnemySetControl thisEnemy_SetControl;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        boolAnimatorID = Animator.StringToHash("hasToBeVisible"); //Convertir el bool de animator en int MUCHO MAS EFICIENTE
        thisEnemy_SetControl = GetComponent<EnemySetControl>();
        heatBarImage.fillAmount = 0;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if(!beingBurned)    //Si no está siendo quemado se llama a vaciar la barra, con un pequeño delay de tiempo si se desea.
        {
            unfillTimer += 1 * Time.deltaTime;

            if(unfillTimer >= timeToStartUnfill)
            {
                UnfillHeatBar();
            }
        }

        if (beingBurned && !heatBarFrameAnimator.GetBool(boolAnimatorID)) //Comprobaciones para idicar al animator que debe MOSTRAR la barra de calor. Se ejecuta solo 1 vez con estos ifs
        {
            ShowHeatBar();
        }

        else if(heatBarImage.fillAmount <= 0 && heatBarFrameAnimator.GetBool(boolAnimatorID)) //Comprobaciones para idicar al animator que debe ESCONDER la barra de calor. Se ejecuta solo 1 vez con estos ifs
        {
            HideHeatBar();
        }
    }

    public virtual void FillHeatBar()       //Se llama desde el script de Flamethrower, TriggerStay. LLena la barra de calor e indica que está siendo quemado.
    {
        unfillTimer = 0;
        heatBarImage.fillAmount += fillAmountPerSecond * Time.deltaTime;
        beingBurned = true;

        if(heatBarImage.fillAmount >= 1 && thisEnemy_SetControl != null)
        {
            thisEnemy_SetControl.CheckEnemyDeath();
        }
    }

    public void StopFillingHeatBar()  //Se llama desde el script de Flamethrower, TriggerExit y FlamethrowerStopped. Pone el bool de estar siendo quemado a false, y entra el acción Update.
    {
        beingBurned = false;
    }

    void UnfillHeatBar()    //Vaciar la barra de calor, se llama desde Update.
    {
        heatBarImage.fillAmount -= unfillAmountPerSecond * Time.deltaTime;
    }

    void ShowHeatBar() //Mostrar barra de calor en animator, llamado desde Update.
    {
        heatBarFrameAnimator.SetBool(boolAnimatorID, true);
    }

    void HideHeatBar()  //Esconder barra de calor en animator, llamado desde Update.
    {
        heatBarFrameAnimator.SetBool(boolAnimatorID, false);
    }
}
