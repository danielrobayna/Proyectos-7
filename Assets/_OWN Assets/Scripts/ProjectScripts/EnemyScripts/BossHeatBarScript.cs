﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BossBattle;

public class BossHeatBarScript : HeatBarScript
{
    private FinalBossZona1Behaviour thisBossBehaviour;

    [Header("Variables de efectos de vida baja")]
    [SerializeField] GameObject lowLifeParticles;
    [SerializeField] float percentageToStartSmoke;
    [SerializeField] MeshRenderer bossRenderer;
    Color rendererColor;

    protected override void Start()
    {
        boolAnimatorID = Animator.StringToHash("hasToBeVisible"); //Convertir el bool de animator en int MUCHO MAS EFICIENTE
        thisBossBehaviour = GetComponent<FinalBossZona1Behaviour>();//En caso de que no tenga enemyset control, ver si es un boss final
        heatBarImage.fillAmount = 0;
        rendererColor = bossRenderer.materials[1].GetColor("_EmissionColor");
    }
    protected override void Update()
    {
        base.Update();

        if (heatBarImage.fillAmount > (percentageToStartSmoke))
        {
            if (!lowLifeParticles.activeSelf)
            {
                lowLifeParticles.SetActive(true);
            }
        }
        else
        {
            if (lowLifeParticles.activeSelf)
            {
                lowLifeParticles.SetActive(false);
            }
        }
        bossRenderer.materials[1].SetColor("_EmissionColor", rendererColor * heatBarImage.fillAmount);
    }
    public override void FillHeatBar()
    {
        if (thisBossBehaviour.isExausted)
        {
            unfillTimer = 0;
            heatBarImage.fillAmount += fillAmountPerSecond * Time.deltaTime;
            beingBurned = true;
        }
        if (heatBarImage.fillAmount >= 1 && thisBossBehaviour != null)
        {
            thisBossBehaviour.KillBoss();
        }
    }
    public void FillOnLaser() //Para rellenar durante el laser
    {
        unfillTimer = 0;
        heatBarImage.fillAmount += 0.01f * Time.deltaTime;
        beingBurned = true;
    }

}
