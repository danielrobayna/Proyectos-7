﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour
{
    int m_randomAnimation;
    // Start is called before the first frame update
    void Start()
    {
        m_randomAnimation = Random.Range(0, 3);
        GetComponent<Animator>().SetInteger("AnimationNumber", m_randomAnimation);
    } 
}
