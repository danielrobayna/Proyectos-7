﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberSetControl : EnemySetControl
{
    [Header ("Variables de control de la explosion")] //Para que no se instancie una segunda explosion en CheckEnemyDeath si ha explotado por el DamagePlayer de la IA
    private bool explode = false; //Comprobación en este script
    public bool hasToExplode //Para modificar exploded desde Bomber_AI y no aparecer en editor.
    {
        get { return explode; }

        set
        {
            explode = value;
        }
    }
    
    public override IEnumerator StunEnemy()
    {
        isStunned = true;

        if (!stunPart.isPlaying) //PLACEHOLDER
        {
            stunPart.Play();
        }

        yield return new WaitForSeconds(timeStunned);

        CheckEnemyDeath();
    }

    public override void PossessEnemy()
    {
        explode = false; //Se pone false el bool que indica a chech enemy que iba a explotar por corutina de disparo

        base.PossessEnemy();
    }

    public override void CheckEnemyDeath()
    {

        if (!isBeingPossessed && !explode) //Muere sin que la IA tenga la orden de explotar
        {
            this_EnemyShootingScript.isChargedShot = false;
            this_EnemyShootingScript.FireInShootingPos(ShootingScript.whoIsShooting.player);
        }

        else if (!isBeingPossessed && explode && !this_EnemyShootingScript.IsEnemyAbleToShoot()) //La IA tiene la orden de explotar, pero aun no ha explotado
        {
            explode = false;
            this_EnemyShootingScript.StopAllCoroutines();
            this_EnemyShootingScript.isChargedShot = false;
            this_EnemyShootingScript.FireInShootingPos(ShootingScript.whoIsShooting.player);
        }

        base.CheckEnemyDeath();
    }

    public override void EnemyDeadWhilePossessed()
    {
        if(!hasBeenConsumed)
        {
            this_EnemyShootingScript.isChargedShot = false;
            this_EnemyShootingScript.FireInShootingPos(ShootingScript.whoIsShooting.player);
        }

        base.EnemyDeadWhilePossessed();       
    }

    [Header("Comprobación de colisión para explotar al lanzar si choca con enemigos o bloqueos de explosión")]
    private EnemySetControl isCollisionEnemy;
    private ExplosionBlockade isCollisionExplosionBlockade;
    private DestructibleObject isCollisionABarrel;

    private void OnCollisionEnter(Collision collision)
    {       
        isCollisionEnemy = collision.gameObject.GetComponent<EnemySetControl>();
        isCollisionExplosionBlockade = collision.gameObject.GetComponent<ExplosionBlockade>();
        isCollisionABarrel = collision.gameObject.GetComponent<DestructibleObject>();

        if(isStunned && (isCollisionEnemy != null || isCollisionExplosionBlockade != null || isCollisionABarrel != null))
        {
            ///// LOGRO ///////
            if (isCollisionEnemy != null)
            {
                SteamManager.Instance.UnlockAchievement("THROW_FIRST_BOMBER");
            }
            /////
            
            ///// LOGRO ///////
            if (isCollisionABarrel != null)
            {
                SteamManager.Instance.UnlockAchievement("DESTROY_BARREL_BOMBER");
            }
            /////

            CheckEnemyDeath();
        }
    }
}
