﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockerSetControl : EnemySetControl
{
    [Header("Partícula Aura Eléctrica")]
    public ParticleSystem electricAuraParticle;

    [Header("Partícula Mochila")]
    public ParticleSystem electricBack;

    [Header("La mochila DEBE activrse antes como anticipación, como 1 segundo")]
    [Header("Tiempo que tarda en volver a activar la mochila y el aura eléctrica")]
    [SerializeField] private float timeToReactivateElectricBack;
    [SerializeField] private float timeToReactivateShockAura;

    AudioSource electricSfxSound;

    protected override void Start()
    {
        base.Start();

        ActivateShockAura();
    }

    private void OnEnable()
    {
        if (!electricAuraParticle.isPlaying) //Para cambio de habitaciones
        {
            electricAuraParticle.Play();
        }
        if (!electricBack.isPlaying)
        {
            electricBack.Play();
        }
    }

    private void ActivateElectricBack() //Activar partículas de la mochila de la espalda
    {    
        if (!electricBack.isPlaying)
        {
            electricBack.Play();
        }
    }

    private void ActivateShockAura() //Activar aura de shock y hacer que el shocker no sea poseíble tras un tiempo de corutina tras ser desactivada
    {
        canBePossessed = false;

        if (!electricAuraParticle.isPlaying)
        {
            electricAuraParticle.Play();
        }

        electricSfxSound = MusicManager.Instance.PlayLoopControlledSound(AppSounds.SHOCKER_LOOP, this.transform);
    }

    public void DeactivateShockAura_ElectricBack() //Desactivar aura de shock y la mochila de la espalda y hacer el shocker poseíble tras el disparo. Se llama en el Shootingscript
    {
        canBePossessed = true;

        if (electricSfxSound != null)
        {
            electricSfxSound.Stop();
        }

        if (electricAuraParticle.isPlaying)
        {
            electricAuraParticle.Stop();
        }
        if(electricBack.isPlaying)
        {
            electricBack.Stop();
        }

        StartCoroutine(ReactivateElectricBack());
        StartCoroutine(ReactivateShockAura());
    }

    private IEnumerator ReactivateElectricBack()
    {
        yield return new WaitForSeconds(timeToReactivateElectricBack);

        ActivateElectricBack();
    }

    private IEnumerator ReactivateShockAura()
    {
        yield return new WaitForSeconds(timeToReactivateShockAura);

        ActivateShockAura();
    }
    private void OnDisable()
    {
        if (electricSfxSound != null){
            electricSfxSound.Stop();
        }
    }
}
