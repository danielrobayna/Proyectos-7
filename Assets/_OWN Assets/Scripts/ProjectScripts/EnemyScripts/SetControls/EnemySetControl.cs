﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using ch.sycoforge.Decal;

public class EnemySetControl : MonoBehaviour
{
    [Header("El PlayerControl de este Enemigo")]
    private EnemyControl_MovementController this_EnemyControl_MovementController;

    [Header("El Script de Disparo/Aatque")]
    protected ShootingScript this_EnemyShootingScript;

    [Header("La IA y el Agente del Enemigo")]
    protected EnemyAI_Standard this_EnemyAI;
    protected NavMeshAgent this_EnemyNavAgent;

    [Header("El PlayerControl del Jugador")]
    protected PlayerControl_MovementController player_MovementController;

    [Header("La Possess Ability del Jugador")]
    private PossessAbility player_PossessAbility;

    [Header("RigidBodies")]
    private Rigidbody thisEnemyRB;

    [Header("Es un enemigo de Tutorial? Solo para enemigos tutorial Zona 1 Hab0 & 1")]
    [SerializeField] private bool isTutorialEnemy; //Para desbloquear logros si no lo son

    [Header("El Transform donde hace TP el Jugador al Desposeer/Consumir")]
    public Transform playerSpawnPos;

    [Header("El Alien de Posesión, está en el hijo con hueso de Cabeza del modelo")]
    public GameObject possessionAlien;

    [Header("Variables de Consumir")]
    [HideInInspector] public bool hasBeenConsumed;
    [SerializeField] float consumeTime;

    [Header("Decal al morir consumido")]
    [SerializeField] private GameObject consumedDecal;

    [Header("El Script de Vida del Enemigo")]
    protected EnemyHealth this_EnemyHealthScript;

    [Header("Fragmentos de Vida Recuperados al ser Consumido")]
    public int healthHealedOnCosuming;

    [Header("Variables de desposesión del Enemigo")]
    public float timeStunned;
    [SerializeField] float m_despossessForceMangitude = 10;

    [Header("Tiempo de Stun de Player si este Enemigo Muere Estando Poseído")]
    public float playerTimeStunned;
    [HideInInspector]
    public bool isStunned = false;
    private Coroutine stunCoroutine;

    [Header("Variables lookahead de este enemigo (DEJAR A 0 SI NO SE SABE QUE SE HACE)")]
    [SerializeField] private float newControllerLookahead = 0;
    [SerializeField] private float newMouseLookahead = 0;
    private float defaultControllerLookahead;
    private float defaultMouseLookahead;

    [Header("Partículas")]
    public GameObject deathPart;
    public GameObject consumedDeathPart;
    public ParticleSystem smokeThrowPart;
    public ParticleSystem stunPart;
    public ParticleSystem consumePart;
    [Header("Partículas de sangre/eléctricas cuando el enemigo está poseído (dentro de Model o del Hueso Head)")]
    public List<ParticleSystem> beingPossessedParticles;

    protected SetAnimatorVariables this_SetAnimationVariables;
    protected EnemySounds this_enemySounds;

    [Header("Prefab que se instancia al morir")]
    [SerializeField] GameObject deathDummy;

    [Header("Variables necesarias para el Boss de la Zona 2")]
    private bool _addDummyToRepairHelperReference = false;
    public bool addDummyToRepairHelperReference   //Se setean desde el script del Boss2
    {
        get { return _addDummyToRepairHelperReference; }
        set { _addDummyToRepairHelperReference = value; }
    }
    private RailgunRepairHelper _repairHelperReference;
    public RailgunRepairHelper repairHelperReference
    {
        get { return _repairHelperReference; }
        set { _repairHelperReference = value; }
    }

    public enum UnposessTypes { normal, electric, force }; //Enum con las diferentes causas de Unposess, para actuar en consecuencia dentro del método.

    // Start is called before the first frame update
    protected virtual void Start()
    {
        StartSaveVariables();
    }

    void StartSaveVariables()
    {
        this_EnemyControl_MovementController = GetComponent<EnemyControl_MovementController>();
        this_EnemyShootingScript = GetComponent<ShootingScript>();
        this_EnemyAI = GetComponent<EnemyAI_Standard>();
        this_EnemyNavAgent = GetComponent<NavMeshAgent>();
        this_EnemyHealthScript = GetComponent<EnemyHealth>();
        player_MovementController = GameManager.Instance.realPlayerGO.GetComponent<PlayerControl_MovementController>();
        player_PossessAbility = GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>();
        thisEnemyRB = GetComponent<Rigidbody>();
        this_SetAnimationVariables = GetComponent<SetAnimatorVariables>();
        this_enemySounds = GetComponent<EnemySounds>();

        defaultControllerLookahead = InputManager.Instance.aimDistanceWithController;
        defaultMouseLookahead = InputManager.Instance.aimDistanceInCameraSizeFraction;
    }

    public virtual void WaitForPossession()    //Con el nuevo sistema de posesión, se llama a esta función para inmovilizar la IA mientras la "bala" de posesión llega hasta el enemigo. Es la "bala" la que llama a PosssessEnemy ahora.
    {
        if (!GameManager.Instance.IsPlayerRespawning())
        {
            if (canBePossessed)
            {
                this_SetAnimationVariables.SetSurprisedBool(true);
                this_EnemyAI.currentAIState = EnemyAI_Standard.AIState.surpriseOnPossess;
            }

            GameManager.Instance.realPlayerGO.GetComponent<PlayerHealthController>().ResetPlayerStates();////////      Elimina los estados alterados del jugador al comenzar a poseerlo
            GameManager.Instance.realPlayerGO.SetActive(false);                                          //// El orden es importante para que se resetee antes de desactivarlo
        }
    }

    ///////////////////////////////////
    bool m_isBeingPossesed = false;
    protected bool isBeingPossessed
    {
        get
        {
            return m_isBeingPossesed;
        }
        set
        {
            if (value)
            {
                InputManager.Instance.SetMirilla(false);
            }
            else
            {
                InputManager.Instance.SetMirilla(true);
            }
            m_isBeingPossesed = value;
        }
    }
    protected bool canBePossessed = true;

    public bool IsEnemyPossessed() //Sacar valor de isBeingPossessed desde otros scripts no hijos
    {
        return isBeingPossessed;
    }

    public bool CanEnemyBePossessed() //Sacar  valor de canBePossessed desde otros scripts no hijos
    {
        return canBePossessed;
    }
    ////////////////////////////////////
    ///

    public virtual void PossessEnemy()  ////Se desactiva la IA y el agente, se activa el control de enemigo, se detiene el movimiento residual del control de jugador, se desactiva el objeto del jugador y se indica a GameManager que este enmigo es ActualPlayer
    {
        if (canBePossessed)
        {
            isBeingPossessed = true;

            if (stunCoroutine != null)
            {
                StopCoroutine(stunCoroutine); //Se evita que se reactive la IA tras acabar el Stun si se esta poseyendo.
            }

            StopParticles();

            if (this_EnemyShootingScript != null)
            {
                this_EnemyShootingScript.StopAllCoroutines(); //Detener corutinas de disparo iniciadas por la IA al ser poseído el enemigo.
                this_EnemyShootingScript.ResetShootingVariables();
            }

            if (this_EnemyAI is EnemyAI_Shield) //Cambiar de layer el escudo si es un enemigo de escudo
            {
                EnemyAI_Shield aiShield = (EnemyAI_Shield)this_EnemyAI;
                aiShield.shieldScript.ShieldChangeLayer(26);
            }

            if (newControllerLookahead != 0 && newMouseLookahead != 0) //Aplicar nuevo lookahead si el enemigo lo tiene
            {
                InputManager.Instance.aimDistanceWithController = newControllerLookahead;
                InputManager.Instance.aimDistanceInCameraSizeFraction = newMouseLookahead;
            }

            PossessEnemySetComponents();

            //Frenar al jugador real, para evitar que se mueva al volver a usarlo
            player_MovementController.controlSpeedX = 0;
            player_MovementController.controlSpeedZ = 0;
            player_MovementController.controlRb.velocity = Vector3.zero;

            gameObject.layer = 8; ////Al ser poseído pasa a tener layer de Player

            PossessSetPlayersInGameManager();

            PossessEnemyEffects();
        }
        this_SetAnimationVariables.SetSurprisedBool(false);
    }

    void PossessEnemySetComponents()
    {
        if(this_EnemyAI.canEnterElectricalStunState && this_EnemyAI.electricStunParticle.IsAlive()) //Apagar estado eléctrico si el enemigo puede entraren él y la partícula de electricidad esta viva
        {
            this_EnemyAI.StopElectricalStun();
        }

        this_EnemyAI.enabled = false;
        this_EnemyNavAgent.enabled = false;
        this_EnemyControl_MovementController.enabled = true;
        possessionAlien.SetActive(true);
        this_SetAnimationVariables.SetIsPossesedBool(true);
        for (int i = 0; i < beingPossessedParticles.Count; i++)
        {
            if (!beingPossessedParticles[i].isPlaying)
            {
                beingPossessedParticles[i].Play();
            }
        }
    }

    void PossessEnemyEffects()
    {
        //Sonido
        this_enemySounds.PlayPossessionSound(); // PLACEHOLDER

        //Seteo de animaciones
        this_SetAnimationVariables.SetIsPossesedBool(true);
    }

    void PossessSetPlayersInGameManager()
    {
        GameManager.Instance.ActualPlayerController = this_EnemyControl_MovementController;
    }

    public virtual void UnpossessEnemy(Vector3 lastMovementDirection, UnposessTypes causeOfUnposess)  //Vector3 para cuando se desposee sin input de rotación
    {
        isBeingPossessed = false;
        ////Se desactiva el control del enemigo, se activa el jugador y se indica que es el ActualPlayer; se le coloca en la posicion del enemigo y se eliminan las colisiones entre ambos
        UnpossessEnemySetComponents();

        if (newControllerLookahead != 0 && newMouseLookahead != 0) //Volver al lookahead normal al desposeer el enemigo si había uno diferente
        {
            InputManager.Instance.aimDistanceWithController = defaultControllerLookahead;
            InputManager.Instance.aimDistanceInCameraSizeFraction = defaultMouseLookahead;
        }

        UnpossessSetPlayersInGameManager();

        if (this_EnemyShootingScript != null)
        {
            this_EnemyShootingScript.StopAllCoroutines(); //Detener corutinas de disparo iniciadas por la player al ser desposeído el enemigo.
            this_EnemyShootingScript.ResetShootingVariables();
        }

        if (!smokeThrowPart.isPlaying) //PLACEHOLDER
        {
            smokeThrowPart.Play();
        }

        gameObject.layer = 9; ////Al ser desposeído vuelve a tener layer de Enemy

        this_EnemyControl_MovementController.controlSpeedX = 0;
        this_EnemyControl_MovementController.controlSpeedZ = 0;
        thisEnemyRB.velocity = Vector3.zero;  ////Se elimina toda velocidad del RB y se aplica una fuerza contraria a la velocidad en la que se estaba moviendo en el último momento

        if (InputManager.Instance.RotationInput() != Vector3.zero && causeOfUnposess == UnposessTypes.normal) //Si hay input de rotación se lanza al enemigoen esa dirección
        {
            thisEnemyRB.AddForce(InputManager.Instance.RotationInput().normalized * m_despossessForceMangitude, ForceMode.Impulse);
        }
        else if (InputManager.Instance.RotationInput() == Vector3.zero && causeOfUnposess == UnposessTypes.normal)   //Si no hay input de rotación se lanza al enemigo en la dirección del vector que hay de entrada en el método, que sale de EnemyController y es la última dirección de movimiento.
        {
            thisEnemyRB.AddForce(lastMovementDirection.normalized * m_despossessForceMangitude, ForceMode.Impulse);
        }

        if (causeOfUnposess == UnposessTypes.normal)
        {
            stunCoroutine = StartCoroutine(StunEnemy()); ////Se inicia el Stun.
        }
        else if (causeOfUnposess == UnposessTypes.electric) //Desposesión por causas eléctricas
        {
            //Estado de Shock a la IA
            this_EnemyNavAgent.enabled = true;
            this_EnemyAI.enabled = true;
            this_EnemyAI.StartElectricalStun();
            Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, false);
            GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().StartElectricStun(); //Stun eléctrico al jugador 
        }
        else if (causeOfUnposess == UnposessTypes.force)
        {
            this_EnemyNavAgent.enabled = true;
            this_EnemyAI.enabled = true;
            Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, false);
        }

        UnpossessEnemyEffects();
    }

    void UnpossessEnemySetComponents()
    {
        UIManager.Instance.SetUnactiveEnemyHealthUI(); //Desactivar barra de vida

        this_EnemyControl_MovementController.enabled = false;
        possessionAlien.SetActive(false);
        for (int i = 0; i < beingPossessedParticles.Count; i++)
        {
            if (beingPossessedParticles[i].isPlaying)
            {
                beingPossessedParticles[i].Stop();
            }
        }
    }

    void UnpossessEnemyEffects()
    {
        //Seteo de animaciones
        this_SetAnimationVariables.SetIsPossesedBool(false);
    }

    void UnpossessSetPlayersInGameManager()
    {
        Vector3 spawnCorrection = GameManager.Instance.realPlayerGO.transform.position - player_MovementController.modelParent.transform.position; //Corregir la posición de spawn del jugador para que se centre en el modelo, que tiene modificado su posicion respecto al objeto
        GameManager.Instance.realPlayerGO.transform.position = playerSpawnPos.position + spawnCorrection;
        GameManager.Instance.realPlayerGO.SetActive(true);
        GameManager.Instance.ActualPlayerController = player_MovementController;
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, true);
    }

    public virtual void CheckEnemyDeath() ////Comprueba si el enemigo ha muerto poseído o no y actúa en consecuencia
    {
        canBePossessed = false;

        if (newControllerLookahead != 0 && newMouseLookahead != 0) //Volver al lookahead normal al morir el enemigo si había uno diferente
        {
            InputManager.Instance.aimDistanceWithController = defaultControllerLookahead;
            InputManager.Instance.aimDistanceInCameraSizeFraction = defaultMouseLookahead;
        }

        //Instanciar dummy
        Quaternion dummyRotation = Quaternion.Euler(0, 90, 0) * this_EnemyControl_MovementController.armObject.transform.rotation;
        GameObject dummy = Instantiate(deathDummy, new Vector3(transform.position.x, 0, transform.position.z), dummyRotation);

        if (isBeingPossessed) ////El enemigo está poseído
        {
            EnemyDeadWhilePossessed();
        }

        else if (!isBeingPossessed)//// El enemigo no está poseído
        {
            Instantiate(deathPart, new Vector3(transform.position.x, 1.3f, transform.position.z), transform.rotation);
            gameObject.SetActive(false);
            this_enemySounds.PlayHitSound(dummy.transform);//Para permitir el sonido de hit cuando muere

            ///// LOGRO ///////
            if (!isTutorialEnemy)
            {
                SteamManager.Instance.UnlockAchievement("KILL_FIRST_ENEMY");
            }
            /////
            
            ///// LOGRO ///////
            SteamManager.Instance.Achievement_KillEnemies100_150_200();
            /////
        }

        if (this_EnemyControl_MovementController.spawnerInstantiatedFrom != null)
        {
            this_EnemyControl_MovementController.spawnerInstantiatedFrom.RemoveEnemyFromSpawner(this.gameObject);
        }

        if (!addDummyToRepairHelperReference) //Si NO se tienen que añadir a la variable de dummy del helper, se lo indica el propio helper
        {
            ZoneManager.Instance.m_activeRoom.AddDummieAtRoom(dummy);
        }
        else //Si SÍ se tienen que añadir a la variable de dummy del helper, se lo indica el propio helper
        {
            repairHelperReference.SetStoredDummy(dummy);
        }

        ZoneManager.Instance.DeleteEnemyFromCurrentRoom(this_EnemyControl_MovementController);

        this_enemySounds.PlayEnemyDeathSound(dummy.transform);
    }

    public virtual void EnemyDeadWhilePossessed() ////Funcionalidad al morir el enemigo mientras estaba poseído
    {
        UIManager.Instance.SetUnactiveEnemyHealthUI(); //Desactivar barra de vida

        StopAllCoroutines();
        Vector3 spawnCorrection = GameManager.Instance.realPlayerGO.transform.position - player_MovementController.modelParent.transform.position; //Corregir la posición de spawn del jugador para que se centre en el modelo, que tiene modificado su posicion respecto al objeto
        GameManager.Instance.realPlayerGO.transform.position = playerSpawnPos.position + spawnCorrection;
        GameManager.Instance.realPlayerGO.SetActive(true);
        GameManager.Instance.ActualPlayerController = player_MovementController;

        this_EnemyControl_MovementController.controlSpeedX = 0; //Eliminar movimiento residual y desactivar control de enemigo
        this_EnemyControl_MovementController.controlSpeedZ = 0;
        this_EnemyControl_MovementController.enabled = false;

        if (!hasBeenConsumed) //Si el enemigo ha muerto poseído pero no por ser consumido, es decir, muerto por ataque, se aplica stun al jugador. Esta funcionalidad está en PossessAbility
        {
            player_PossessAbility.StartCoroutine(player_PossessAbility.PlayerStun(playerTimeStunned));
            Instantiate(deathPart, new Vector3(transform.position.x, 1.3f, transform.position.z), transform.rotation);
        }

        else
        {
            Instantiate(consumedDeathPart, new Vector3(transform.position.x, 1.3f, transform.position.z), transform.rotation);

            //////// DECALS DE SANGRE //////////
            ///
            if (!(this_EnemyAI is EnemyAI_Bomber) && !(this_EnemyAI is EnemyAI_Turret))
            {
                EasyDecal.Project(consumedDecal, gameObject.transform.position, Quaternion.Euler(0, Random.Range(0.0f, 360.0f), 0));
            }

            ///// LOGRO ///////
            if (!isTutorialEnemy)
            {
                SteamManager.Instance.UnlockAchievement("CONSUME_FIRST_ENEMY");
            }
            /////

            ///// LOGRO ///////
            if (!isTutorialEnemy && this_EnemyAI is EnemyAI_Healer)
            {
                SteamManager.Instance.UnlockAchievement("CONSUME_FIRST_HEALER");
            }
            /////
        }

        gameObject.layer = 9; //Layer de enemy
        hasBeenConsumed = false; //Se resetea el bool que indica si el enemigo ha sido consumido.
        gameObject.SetActive(false);
    }

    public virtual IEnumerator ConsumeEnemy()
    {
        thisEnemyRB.velocity = Vector3.zero;
        this_EnemyControl_MovementController.isBeingConsumed = true;
        this_SetAnimationVariables.SetConsumeBool(true);

        ConsumeEffects();

        while (true)
        {
            yield return new WaitForSeconds(consumeTime);
            //Sonido
            this_enemySounds.PlayConsumeSound();

            if (this_EnemyHealthScript.SetEnemyConsumingHealth(2))
            {
                KillEnemyWhileConsuming();
            }
        }
    }
    void KillEnemyWhileConsuming()
    {
        this_enemySounds.PlayEnemyDeathConsumeSound();
        consumePart.Stop(); //Partículas de consumir
        HealthHeartsVisual.healthHeartsSystemStatic.Heal(healthHealedOnCosuming);
        hasBeenConsumed = true;
        CheckEnemyDeath();
    }
    protected void ConsumeEffects()
    {
        consumePart.Play(); //Partículas de consumir
    }

    Coroutine consumingCoroutine;
    public void StartConsuming()
    {
        consumingCoroutine = StartCoroutine(ConsumeEnemy());
    }

    public void StopConsumingAction()
    {
        if (consumingCoroutine != null)
        {
            StopCoroutine(consumingCoroutine);
            consumingCoroutine = null;
            this_EnemyControl_MovementController.isBeingConsumed = false;
            consumePart.Stop();
            this_SetAnimationVariables.SetConsumeBool(false);
        }
    }

    public virtual IEnumerator StunEnemy()  //Se reactiva la IA y el agente al acabar el tiempo de Stun
    {
        isStunned = true;

        if (!stunPart.isPlaying) //PLACEHOLDER
        {
            stunPart.Play();
        }

        yield return new WaitForSeconds(timeStunned);

        Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, false);
        this_EnemyNavAgent.enabled = true;
        this_EnemyAI.enabled = true;
        this_EnemyAI.currentAIState = EnemyAI_Standard.AIState.attacking;
        isStunned = false;

        StopParticles();
    }

    void StopParticles()
    {
        if (stunPart.isPlaying) //PLACEHOLDER
        {
            stunPart.Stop();
        }

        if (smokeThrowPart.isPlaying) //PLACEHOLDER
        {
            smokeThrowPart.Stop();
        }
    }

    private void OnDisable() ////Para cuando se reactiva un enemigo que estaba en Stun;
    {
        if (this_EnemyAI != null && this_EnemyNavAgent != null)
        {
            this_EnemyAI.enabled = true;
            this_EnemyNavAgent.enabled = true;
        }
    }
}
