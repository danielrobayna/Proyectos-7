﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealerSounds : EnemySounds
{
    public override void PlayEnemyDeathSound(Transform dummyTransform)
    {
        MusicManager.Instance.PlaySound(AppSounds.NPC_DEATH, 0.5f, 1, 15, dummyTransform);
    }
}
