﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        //Sonido
        int random = Random.Range(0, 3);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.SHOOTGUN_FIRE_1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.SHOOTGUN_FIRE_2, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.SHOOTGUN_FIRE_3, this.transform);
                break;
            default:
                break;
        }
    }
}
