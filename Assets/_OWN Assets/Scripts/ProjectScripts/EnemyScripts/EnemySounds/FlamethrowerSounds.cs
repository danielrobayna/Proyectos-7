﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamethrowerSounds : EnemySounds
{
    AudioSource fireAudioSource;

    public override void PlayFiringSound()
    {
        if (fireAudioSource == null)
        {
            fireAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.FLAMER_FIRE, this.transform);
        }
    }
    public void StopFireAudio()
    {
        if(fireAudioSource != null)
        {
            fireAudioSource.Stop();
        }
    }
    private void OnDisable()
    {
        if (fireAudioSource != null)
        {
            fireAudioSource.Stop();
        }
    }
}
