﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSound : EnemySounds
{
    public override void PlayEnemyDeathSound(Transform dummyTransform)
    {
        //Sonido
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_1, dummyTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_2, dummyTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_3, dummyTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_4, dummyTransform); 
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_5, dummyTransform);
                break;
            default:
                break;
        }
    }
    public override void PlayConsumeSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.ROBOT_MUERTE, this.transform);
    }
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.TURRET_FIRE, this.transform);
    }
    protected override void ChooseHitSound(Transform dummyTransform)
    {
        //Sonido
        int random = Random.Range(0, 4);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_1, dummyTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_2, dummyTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_3, dummyTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_4, dummyTransform);
                break;
            default:
                break;
        }
    }
}