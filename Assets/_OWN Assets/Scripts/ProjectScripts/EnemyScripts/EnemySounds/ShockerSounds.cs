﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockerSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.SHOCKER_FIRE, this.transform);
    }
}
