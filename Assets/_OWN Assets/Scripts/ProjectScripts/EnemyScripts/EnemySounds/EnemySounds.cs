﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySounds : MonoBehaviour
{
    public virtual void PlayEnemyDeathSound(Transform dummyTransform)
    {
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH1, dummyTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH2, dummyTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH3, dummyTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH4, dummyTransform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH5, dummyTransform);
                break;
            default:
                break;
        }
    }
    public virtual void PlayConsumeSound()
    {
        int randomNum = Random.Range(0, 2);

        switch (randomNum)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_CONSUME_1);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_CONSUME_2);
                break;
            default:
                break;
        }
    }
    public virtual void PlayEnemyDeathConsumeSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.HUMAN_DEATH_CONS);
    }
    public void PlayPossessionSound()
    {
        int randomNum = Random.Range(0, 2);
        switch (randomNum)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_POSSESION_1);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.PLAYER_POSSESION_2);
                break;
            default:
                break;
        }
    }
    public virtual void PlayFiringSound()
    {
        //Sonido
        int random = Random.Range(0, 3);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.PISTOL_FIRE_1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.PISTOL_FIRE_2, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.PISTOL_FIRE_3, this.transform);
                break;
            default:
                break;
        }
    }
    public void PlayHitSound()
    {
        //Sonido
        ChooseHitSound(this.transform);
    }
    public void PlayHitSound(Transform dummyTransform)
    {
        ChooseHitSound(dummyTransform);
    }
    protected virtual void ChooseHitSound(Transform spawnTransform)
    {
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_HIT1, spawnTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_HIT2, spawnTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_HIT3, spawnTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_HIT4, spawnTransform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_HIT5, spawnTransform);
                break;
            default:
                break;
        }
    }
}
