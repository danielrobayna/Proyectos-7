﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.SHIELD_FIRE, this.transform);
    }
}
