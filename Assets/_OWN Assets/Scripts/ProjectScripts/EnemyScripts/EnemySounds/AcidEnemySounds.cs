﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidEnemySounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.ACID_THROW, this.transform);
    }
}
