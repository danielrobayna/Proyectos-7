﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadierSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.GRANADIER_THROW, this.transform);
    }
}