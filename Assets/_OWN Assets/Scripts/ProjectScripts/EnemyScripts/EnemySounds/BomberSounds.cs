﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BomberSounds : EnemySounds
{
    protected override void ChooseHitSound(Transform spawnTransform)
    {
        //Sonido
        int random = Random.Range(0, 4);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_1, spawnTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_2, spawnTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_3, spawnTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_4, spawnTransform);
                break;
            default:
                break;
        }
    }
    public override void PlayConsumeSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.ROBOT_MUERTE, this.transform);
    }
    public override void PlayEnemyDeathSound(Transform dummyTransform)
    {
    }
}
