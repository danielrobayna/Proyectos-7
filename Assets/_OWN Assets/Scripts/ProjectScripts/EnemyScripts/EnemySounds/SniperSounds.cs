﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.SNIPER_FIRE, 30, 100,this.transform);
    }
}
