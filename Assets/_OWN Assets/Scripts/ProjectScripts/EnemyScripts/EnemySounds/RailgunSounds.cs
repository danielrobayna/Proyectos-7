﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunSounds : EnemySounds
{
    public override void PlayEnemyDeathSound(Transform dummyTransform)
    {
        //Sonido
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_1, dummyTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_2, dummyTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_3, dummyTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_4, dummyTransform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_5, dummyTransform);
                break;
            default:
                break;
        }
    }
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.RAILGUN_FIRE, 25, 50,this.transform);
    }
    public override void PlayConsumeSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.ROBOT_MUERTE, this.transform);
    }
    protected override void ChooseHitSound(Transform spawnTransform)
    {
        //Sonido
        int random = Random.Range(0, 4);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_1, spawnTransform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_2, spawnTransform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_3, spawnTransform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.ROBOT_HIT_4, spawnTransform);
                break;
            default:
                break;
        }
    }
}
