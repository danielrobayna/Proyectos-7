﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeatheadSounds : EnemySounds
{
    public override void PlayFiringSound()
    {
        MusicManager.Instance.PlaySound(AppSounds.MEATHEAD_FIRE, this.transform);
    }
}
