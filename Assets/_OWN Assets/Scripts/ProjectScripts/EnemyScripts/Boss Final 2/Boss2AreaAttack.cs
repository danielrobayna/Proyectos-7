﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2AreaAttack : Explosion
{
    protected override void CollisionWithOther(Collider otherObject)  //La explosión chequea en colision with other si es un bloqueo de explosión
    {
        base.CollisionWithOther(otherObject);
    }

    protected override void CollisionWithEnemy(Collider enemy)
    {
        base.CollisionWithEnemy(enemy);
    }

    protected override void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
            base.CollisionWithPlayer(playerHealth);
    }

    protected override void PlayExplosionSound()
    {
        //Sonido
        int random = Random.Range(0, 5);
        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_2, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_3, this.transform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_4, this.transform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.EXPLOSION_5, this.transform);
                break;
            default:
                break;
        }
    }
}
