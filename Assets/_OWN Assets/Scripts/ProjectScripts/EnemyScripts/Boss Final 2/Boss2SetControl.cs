﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2SetControl : EnemySetControl
{
    [Header("Referencia al Comportamiento del Boss 2")]
    FinalBossZona2Behaviour boss2Behaviour;

    [Header("Partícula Aura Eléctrica")]
    public ParticleSystem electricAuraParticle;

    [Header("AudioSource del Aura Eléctrica")]
    private AudioSource electricAuraAudioSource;

    [Header("Tiempo de stun tras carga")]
    [SerializeField] private float timeStunnedAfterCharge;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        boss2Behaviour = GetComponent<FinalBossZona2Behaviour>();
    }

    public IEnumerator StunBossOnCharge() //Stun tras la carga del Boss
    {
        boss2Behaviour.StopAllCoroutines();
        this_SetAnimationVariables.SetIsStunnedBool(true);
        if (!stunPart.isPlaying)
        {
            stunPart.Play();
        }
        boss2Behaviour.enabled = false;

        yield return new WaitForSeconds(timeStunnedAfterCharge);

        if (stunPart.isPlaying)
        {
            stunPart.Stop();
        }
        this_SetAnimationVariables.SetIsStunnedBool(false);
        this_SetAnimationVariables.SetHitWallOnChargeBool(false);
        ActivateShockAura();
        boss2Behaviour.enabled = true;
    }

    public override void WaitForPossession()    //Con el nuevo sistema de posesión, se llama a esta función para inmovilizar la IA mientras la "bala" de posesión llega hasta el enemigo. Es la "bala" la que llama a PosssessEnemy ahora.
    {
        GameManager.Instance.realPlayerGO.GetComponent<PlayerHealthController>().ResetPlayerStates();////////      Elimina los estados alterados del jugador al comenzar a poseerlo
        GameManager.Instance.realPlayerGO.SetActive(false);                                          //// El orden es importante para que se resetee antes de desactivarlo
    }

    public override void PossessEnemy()
    {
        if (canBePossessed)
        {
            if (stunPart.isPlaying)
            {
                stunPart.Stop();
            }

            player_MovementController.controlSpeedX = 0;
            player_MovementController.controlSpeedZ = 0;
            player_MovementController.controlRb.velocity = Vector3.zero;

            this_enemySounds.PlayPossessionSound();

            StopAllCoroutines();
            boss2Behaviour.StopAllCoroutines();
            boss2Behaviour.enabled = false;
            this_EnemyNavAgent.enabled = false;
            possessionAlien.SetActive(true);
            for (int i = 0; i < beingPossessedParticles.Count; i++)
            {
                if (!beingPossessedParticles[i].isPlaying)
                {
                    beingPossessedParticles[i].Play();
                }
            }

            StartCoroutine(ConsumeEnemy());
        }
    }

    public void UnpossessEnemy() //No se hace override, es una sobrecarga sin parámetros de entrada
    {
        possessionAlien.SetActive(false);
        for (int i = 0; i < beingPossessedParticles.Count; i++)
        {
            if (beingPossessedParticles[i].isPlaying)
            {
                beingPossessedParticles[i].Stop();
            }
        }

        Vector3 spawnCorrection = GameManager.Instance.realPlayerGO.transform.position - player_MovementController.modelParent.transform.position; //Corregir la posición de spawn del jugador para que se centre en el modelo, que tiene modificado su posicion respecto al objeto
        GameManager.Instance.realPlayerGO.transform.position = playerSpawnPos.position + spawnCorrection;
        GameManager.Instance.realPlayerGO.SetActive(true);
        Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, true);

        if (boss2Behaviour.CurrentBossPhase2Hits() < 3) //Stun solo si está vivo
        {
            StartCoroutine(StunEnemy());
            ActivateShockAura();
        }
    }

    public override IEnumerator ConsumeEnemy()
    {
        this_SetAnimationVariables.SetConsumeBool(true);
        ConsumeEffects();


        yield return new WaitForSeconds(2f);

        boss2Behaviour.BossReceiveHit();
        HealthHeartsVisual.healthHeartsSystemStatic.Heal(healthHealedOnCosuming);
        consumePart.Stop();
        if(boss2Behaviour.CurrentBossPhase2Hits() < 3) //Solo sale del estado si no va a morir
        {
            this_SetAnimationVariables.SetConsumeBool(false);
        }
        UnpossessEnemy();

    }

    public override IEnumerator StunEnemy() //Stun tras desposesión
    {
        isStunned = true;

        if (!stunPart.isPlaying) 
        {
            stunPart.Play();
        }

        yield return new WaitForSeconds(timeStunned);

        Physics.IgnoreCollision(transform.GetComponent<Collider>(), GameManager.Instance.realPlayerGO.GetComponent<PossessAbility>().realPlayerWorldCollider, false);
        this_EnemyNavAgent.enabled = true;
        boss2Behaviour.enabled = true;
        isStunned = false;

        if (stunPart.isPlaying)
        {
            stunPart.Stop();
        }

        this_SetAnimationVariables.SetIsStunnedBool(false);
        this_SetAnimationVariables.SetHitWallOnChargeBool(false);
    }

    //////////////// AURA ELÉCTRICA ///////////////
    /// 

    public void ActivateShockAura() //Activar aura de shock y que el Boss 2 no sea poseible segun se desposee
    {
        canBePossessed = false;

        if (!electricAuraParticle.isPlaying)
        {
            electricAuraParticle.Play();
        }

        electricAuraAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.SHOCKER_LOOP, this.transform);
    }

    public void DeactivateShockAura() //Desactivar aura de shock y que el Boss 2 sea posible tras un ataque en el Beheaviour
    {
        canBePossessed = true;

        if (electricAuraParticle.isPlaying)
        {
            electricAuraParticle.Stop();
        }

        if (electricAuraAudioSource != null)
        {
            electricAuraAudioSource.Stop();
        }
    }

    public void OnDisable()
    {
        DeactivateShockAura();
    }
}
