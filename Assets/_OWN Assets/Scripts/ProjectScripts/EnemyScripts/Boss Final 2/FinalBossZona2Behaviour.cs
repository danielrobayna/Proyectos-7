﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class FinalBossZona2Behaviour : Boss2Brain
{
    public static FinalBossZona2Behaviour Instance;

    [Header("Elementos básicos")]
    [SerializeField] private float m_aimLerpVelocity;
    [SerializeField] protected float m_distanceAimAheadPlayer;
    private float angle;
    [SerializeField] private Transform bossShootingPos;
    [SerializeField] private Transform waveShootingPos;
    [SerializeField] private GameObject modelParent;
    [SerializeField] private SetAnimatorVariables m_setAnimator;

    [Header("Spawners y enemigos a instanciar")]
    [SerializeField] private List<Spawner> roomSpawners = new List<Spawner>();
    [SerializeField] private GameObject pistolEnemy;
    [SerializeField] private GameObject shieldEnemy;

    [Header("Railguns")]
    [SerializeField] private List<RailgunRepairHelper> railgunRepairSpawner = new List<RailgunRepairHelper>();
    [SerializeField] private GameObject railgunEnemy;
    [SerializeField] private GameObject repairParticle;

    [Header("Suelos eléctricos")]
    [SerializeField] GroundEffect electricGround1;
    [SerializeField] GroundEffect electricGround2;

    [Header("Control")]
    private bool hasFinished = false;
    private bool hasFinishedAttack = false;
    private int electricGroundActivationControl = 1;
    private enum BossPhase { one, two };
    private BossPhase currentPhase;
    private bool isCharging = false;

    [Header("Hit Particle")]
    [SerializeField] private GameObject hitParticle;

    [Header("FASE1")]
    [SerializeField] private GameObject grenade;
    [SerializeField] private GameObject waveAttack;
    private int bossPhase1Hits = 0;
    [Header ("Elementos perdidos en hits")]
    [SerializeField] private List<GameObject> hitLostElements;
    [SerializeField] private GameObject breakArmourParticle;

    [Header("PASO FASE 1 A FASE 2")]
    [SerializeField] private Transform phase2PlayerTpPos;
    [SerializeField] private Image flashImage;
    private Animator flashAnimator;
    [SerializeField] private List<GameObject> objectsToDestroyInPhase2 = new List<GameObject>();
    [SerializeField] private List<GameObject> objectsToActivateInPhase2 = new List<GameObject>();
    [SerializeField] private Material[] phase2Boss2Materials;
    [SerializeField] private SkinnedMeshRenderer boss2MeshRenderer;
    [SerializeField] private List<GameObject> crystalsToActivate;

    [Header("FASE2")]
    [SerializeField] private float timeToFollowPlayer; //Tiempo siguiendo y atacando al jugador en la fase antes de hacer un charge por deafult
    private float timerToFollow = 0;
    [SerializeField] private float timeToAttackPlayer; //Tiempo antes de atacar en la fase 2
    private float timerToAttack;
    private bool aimNotInSight = false;
    private Boss2SetControl boss2SetControl;
    private int bossPhase2Hits = 0;
    [SerializeField] private int numberOfShootAttacks = 2;
    [Header("Carga contra el Jugador")]
    [SerializeField] private float chargeSpeed;
    [SerializeField] private int chargeDamage;
    private bool hitPlayer = false;
    private bool hitWall = false;
    [Header("Disparo Único")]
    [SerializeField] private GameObject bigBossBullet;
    [SerializeField] private GameObject bigShootParticle;
    [Header("Disparo Triple")]
    [SerializeField] private GameObject smallBossBullet;
    [SerializeField] private GameObject smallShootParticle;
    [SerializeField] private int m_bulletsToInstantiateAtOnce;
    [SerializeField] private float m_shootingAngle;
    [Header("Ataque de Área")]
    [SerializeField] private float minDistanceToAreaAttack;
    [SerializeField] private GameObject areaAttackObject;

    protected override void Awake()
    {
        base.Awake();
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        boss2SetControl = GetComponent<Boss2SetControl>();
        boss2SetControl.enabled = false;
        flashAnimator = flashImage.GetComponent<Animator>();

        m_AI_Controller.updateRotation = false;

        SetRotationOnStart();
    }

    private void OnEnable() //Volver a empezar cotrutina de fase 2 tras la carga, cuando se desactiva y se vuelve a reactivar este script por el stun
    {
        if (currentPhase == BossPhase.two)
        {
            ResumePhase2AfterDisable();
        }
    }

    [Header("Inicialización de Boss")]
    private bool bossInitialized = false;

    [Header("Comprobación de logro de pasarselo sin perder vida")]
    private int battleStartPlayerHealth;

    [Header("Comprobación de logro de pasarselo en X tiempo (3m / 180s)")]
    private float timeAchievement = 180;
    private float timeAchievementTimer;

    public void InitialState()
    {
        battleStartPlayerHealth = HealthHeartsVisual.healthHeartsSystemStatic.GetTotalFragments(); //Coger vida del player al empezar la batalla y comprobar si el player ha perdido vida al morir el boss para desbloquear logro
        StartCoroutine(FinalBossPhase1Coroutine());
        StartCoroutine(InitializeTurrets());
        boss2SetControl.ActivateShockAura();
        MusicManager.Instance.PlayBackgroundMusic(AppSounds.BOSS2_MUSIC);

        bossInitialized = true;
    }

    private IEnumerator InitializeTurrets() //Intanciar torretas Railgun iniciales solo al comienzo, sin que haya problemas con el start de ZoneManager
    {
        yield return new WaitForEndOfFrame();

        for (int i = 0; i < railgunRepairSpawner.Count; i++)
        {
            railgunRepairSpawner[i].InstantiateRailguns();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (bossInitialized)
        {
            timeAchievementTimer += 1 * Time.deltaTime;

            UpdateObjectRotation();

            if (currentPhase == BossPhase.two)
            {
                SetAnimationsVariables();
                Aim();
            }
        }
    }

    /// 
    /// //////////////// FASE 1 ///////////////////////////////////////////////////////////
    /// 

    private IEnumerator FinalBossPhase1Coroutine() //Corutina principal de la fase 1, bucle que se vuelve a empezar cuando acaba él mismo
    {
        yield return new WaitForSeconds(1);

        if (bossPhase1Hits == 0)
        {
            m_setAnimator.SetTauntTrigger();

            SelectLaughSound();

            if (!IsThereAliveEnemiesNotTurrets())
            {
                SpawnEnemies(new GameObject[] { shieldEnemy, shieldEnemy });
            }

            RepairTurrets();

            yield return new WaitForSeconds(6);
        }

        else if (bossPhase1Hits >= 1)
        {
            if (!IsThereAliveEnemiesNotTurrets())
            {
                SpawnEnemies(new GameObject[] { shieldEnemy, shieldEnemy });
            }

            StartCoroutine(AttackWithWave(2.5f, 2, 2));
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            yield return new WaitForSeconds(1);

            RepairTurrets();

            StartCoroutine(Activate_Deactivate_ElectricGround(10));
            StartCoroutine(CheckThrowGrenade(10)); // Tienen que estar el mismo tiempo
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            if (!IsThereAliveEnemiesNotTurrets())
            {
                SpawnEnemies(new GameObject[] { shieldEnemy, shieldEnemy });
            }

            yield return new WaitForSeconds(6);
        }

        yield return new WaitForSeconds(2);

        if (!IsThereAliveEnemiesNotTurrets())
        {
            SpawnEnemies(new GameObject[] { shieldEnemy, shieldEnemy });
        }

        StartCoroutine(FinalBossPhase1Coroutine());
    }

    private void SelectLaughSound()
    {
        int random = Random.Range(0, 4);

        switch (random)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.BOSS2_LAUGH_1);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.BOSS2_LAUGH_2);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.BOSS2_LAUGH_3);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.BOSS2_LAUGH_4);
                break;
        }
    }

    IEnumerator ThrowGrenade(float timeTillNextGrenade) //Corutina del  propio lanzamiento de granada
    {
        m_setAnimator.SetGrenadeTrigger(); //Control de Lanzamiento por evento de animación

        yield return new WaitForSeconds(timeTillNextGrenade);

        hasFinishedAttack = true;
    }

    public void InstantiateGrenade()  //Control de Lanzamiento por evento de animación
    {
        GameObject instantiatedGrenade = Instantiate(grenade, bossShootingPos.position, bossShootingPos.rotation);
        instantiatedGrenade.layer = 10;
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_THROW_GRENADE);
    }

    IEnumerator CheckThrowGrenade(float timeInState) //Control de lanzamiento de granadas
    {
        StartCoroutine(ThrowGrenade( 2));

        for (float timeLeft = timeInState; timeLeft > 0; timeLeft -= Time.deltaTime) //Sustituido WaitForSeconds por este bucle for
        {
            if (hasFinishedAttack)
            {
                StartCoroutine(ThrowGrenade(2));
                yield return new WaitUntil(() => hasFinishedAttack);
                hasFinishedAttack = false;
            }
            yield return null;
        }
    }

    IEnumerator AttackWithWave(float timeToStart, float timeToShoot, int numberOfWaves) //Lanzar el ataque Wave
    {
        m_setAnimator.SetTauntTrigger();
        SelectLaughSound();

        for (float timeLeft = timeToStart; timeLeft > 0; timeLeft -= Time.deltaTime) //Sustituido WaitForSeconds por este bucle for
        {
            ResetAim();
            yield return null;
        }

        for (int i = 0; i < numberOfWaves; i++)
        {
            m_setAnimator.SetWaveAttackTrigger();

            yield return new WaitForSeconds(timeToShoot);
        }

        hasFinished = true;
    }

    public void InstantiateWaveAttack()
    {
        Instantiate(waveAttack, waveShootingPos.position, waveShootingPos.rotation);
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_WAVE);
    }

    private IEnumerator Activate_Deactivate_ElectricGround(float timeInState) //Activar y desactivar suelos eléctricos;
    {
        bool doOnce = false;

        if (electricGroundActivationControl % 2 != 0)
        {
            electricGround1.ActivateGround();
        }
        else if (electricGroundActivationControl % 2 == 0)
        {
            electricGround2.ActivateGround();
        }

        for (float timeLeft = timeInState; timeLeft > 0; timeLeft -= Time.deltaTime) //Sustituido WaitForSeconds por este bucle for
        {
            Aim();

            if (timeLeft < 0.5 * timeInState && !doOnce)
            {
                if (electricGroundActivationControl % 2 != 0)
                {
                    electricGround1.DeactivateGround();
                    electricGround2.ActivateGround();
                }
                else if (electricGroundActivationControl % 2 == 0)
                {
                    electricGround1.ActivateGround();
                    electricGround2.DeactivateGround();
                }
                doOnce = true;
            }

            yield return null;
        }

        if (electricGroundActivationControl % 2 != 0)
        {
            electricGround2.DeactivateGround();
        }
        else if (electricGroundActivationControl % 2 == 0)
        {
            electricGround1.DeactivateGround();
        }

        electricGroundActivationControl++;
        hasFinished = true;
    }

    private void RepairTurrets() //Reparar torretas
    {
        for (int i = 0; i < railgunRepairSpawner.Count; i++)
        {
            if (railgunRepairSpawner[i].GetStoredDummy() != null)
            {
                railgunRepairSpawner[i].StartCoroutine(railgunRepairSpawner[i].RepairTurrets());
            }
        }
    }

    ///
    /// //////////////// PASO DE FASE 1 A FASE 2 ///////////////////////////////////////////////////////////
    /// 

    private IEnumerator ChangeFromPhase1To2() //Cambio de la fase 1  a la 2, todos los cambios de escenario van AQUI
    {
        yield return new WaitForSeconds(1);

        m_setAnimator.SetChangeToPhase2Trigger();

        yield return new WaitForSeconds(0.5f);

        MusicManager.Instance.SetLowPassFilter(true);
        flashAnimator.SetTrigger("StartFlash");
        yield return new WaitUntil(() => StartFlash());
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_CHANGE_PHASE);

        GameManager.Instance.realPlayerGO.GetComponent<PlayerHealthController>().StartInvulnerabilityState();
        GameManager.Instance.realPlayerGO.transform.position = phase2PlayerTpPos.position;

        electricGround1.ActivateGround();
        electricGround2.ActivateGround();
        for(int i = objectsToDestroyInPhase2.Count - 1; i >= 0; i--) //Destruir escenario, bucle que se resta, ya que no funciona si se empieza eliminando el primer elemento
        {
            Destroy(objectsToDestroyInPhase2[i]);
            objectsToDestroyInPhase2.Remove(objectsToDestroyInPhase2[i]);
        }
        for(int i = 0; i < objectsToActivateInPhase2.Count; i++)
        {
            objectsToActivateInPhase2[i].SetActive(true);
        }
        for (int i = railgunRepairSpawner.Count - 1; i >= 0; i--) //Eliminar spawner de torretas, bucle que se resta, ya que no funciona si se empieza eliminando el primer elemento
        {
            Destroy(railgunRepairSpawner[i]);
            railgunRepairSpawner.Remove(railgunRepairSpawner[i]);
        }
        for (int i = ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom.Count - 1; i >= 0 ; i--) //Matar enemigos que no sean el Boss 2, bucle que se resta, ya que no funciona si se empieza eliminando el primer elemento
        {
            if (ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom[i].TryGetComponent(out EnemySetControl setControl) && ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom[i].GetComponent<FinalBossZona2Behaviour>() == null)
            {
                setControl.CheckEnemyDeath();
            }
        }
        for(int i = 0; i < crystalsToActivate.Count; i++) //Activar cristales en el modelo del Boss2
        {
            crystalsToActivate[i].SetActive(true);
        }
        boss2MeshRenderer.materials = phase2Boss2Materials; //Cambiar material al Boss2

        flashAnimator.SetTrigger("FinishFlash");
        yield return new WaitUntil(() => FinishFlash());

        MusicManager.Instance.SetLowPassFilter(false);

        NavMeshSurface nm = GameObject.FindObjectOfType<NavMeshSurface>(); ////////////////// CAMBIAR UNA VEZ METIDO EN GAME 2 a UpdateNavmesh ////////////////
        nm.BuildNavMesh();

        yield return new WaitForSeconds(1);

        currentPhase = BossPhase.two;
        boss2SetControl.enabled = true;
        StartCoroutine(FinalBossPhase2Coroutine());
    }

    private bool StartFlash()
    {
        if (flashImage.color.a >= 0.95)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool FinishFlash()
    {
        if (flashImage.color.a <= 0.05)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    ///
    /// //////////////// FASE 2 ///////////////////////////////////////////////////////////
    /// 

    private IEnumerator FinalBossPhase2Coroutine() //Corutina principal de la fase 2, vuelve a empezar en bulce si hace un ataque normal que finaliza la Corutina, o si se vuelve a activar el script tras un stun, o si se vuelve
    {
        yield return new WaitForSeconds(1);

        yield return new WaitUntil(() => MoveToPlayerPhase2());

        yield return new WaitUntil(() => hasFinished);
        hasFinished = false;

        StartCoroutine(ChargeControl(1.5f));
        timerToFollow = 0;
        timerToAttack = 0;

        yield return new WaitUntil(() => hasFinished);
        hasFinished = false;

        StartCoroutine(FinalBossPhase2Coroutine()); //Volver a empezar
    }

    bool isAttacking;

    private bool MoveToPlayerPhase2()
    {
        timerToFollow += 1 * Time.deltaTime;
        timerToAttack += 1 * Time.deltaTime;

        if (timerToFollow < timeToFollowPlayer && !isAttacking)
        {
            m_AI_Controller.SetDestination(GameManager.Instance.ActualPlayerController.modelParent.transform.position);

            if (timerToAttack >= timeToAttackPlayer)
            {
                if (DistanceToPlayer() <= minDistanceToAreaAttack)
                {
                    m_AI_Controller.ResetPath();
                    m_AI_Controller.velocity = Vector3.zero;
                    StartCoroutine(AreaAttack(1));
                    isAttacking = true;
                }

                else if (DistanceToPlayer() > minDistanceToAreaAttack && IsPlayerInSight())
                {
                    StartCoroutine(DecideShootAttack());
                    isAttacking = true;
                }
            }

            return false;
        }

        else if(timerToFollow >= timeToFollowPlayer && !isAttacking)
        {
            return true;
        }

        return false;
    }

    private IEnumerator ChargeControl(float timeToCharge) //Corutina para controlar la carga
    {
        aimNotInSight = true;
        m_AI_Controller.ResetPath();
        m_AI_Controller.velocity = Vector3.zero;
        m_setAnimator.SetHitPlayerOnChargeBool(false);
        m_setAnimator.SetPrepareChargeTrigger();
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_PREPARE_CHARGE);

        yield return new WaitForSeconds(timeToCharge);

        aimNotInSight = false;
        m_setAnimator.SetStartChargeTrigger();

        yield return new WaitUntil(() => Charge());
        hasFinishedAttack = false;
        appliedSpeed = 0;
        isCharging = false;

        yield return new WaitForEndOfFrame();

        if (hitWall)
        {
            hitWall = false;
            boss2SetControl.DeactivateShockAura();
            boss2SetControl.StartCoroutine(boss2SetControl.StunBossOnCharge());  //Stun tras carga
        }

        else if(hitPlayer)
        {         
            hitPlayer = false;
            hasFinished = true;
        }
    }

    private float appliedSpeed = 0;
    private bool Charge() //La carga, se relaciona con OnCollisionEnter
    {
        isCharging = true;

        if (appliedSpeed < chargeSpeed) //Aceleración
        {
            appliedSpeed += 25 * Time.deltaTime;
        }

        transform.Translate(m_armTransform.right * appliedSpeed * Time.deltaTime);

        return hasFinishedAttack;
    }

    private IEnumerator DecideShootAttack() //Decidir si hace disparo triple o único
    {
        aimNotInSight = true;

        for(int i = 0; i < numberOfShootAttacks; i++)
        {
            if (IsPlayerInSight())
            {
                int randomAttack = Random.Range(0, 2);
            
                if (randomAttack == 0)
                {
                    StartCoroutine(ShootThreeBullets(1));
                    yield return new WaitUntil(() => hasFinishedAttack);
                    hasFinishedAttack = false;
                }
                else if (randomAttack == 1)
                {
                    StartCoroutine(ShootOneBullet(1));
                    yield return new WaitUntil(() => hasFinishedAttack);
                    hasFinishedAttack = false;
                }
            }

            else //Romper tanda de ataques si no ve al player
            {
                break;
            }

            yield return new WaitForSeconds(0.5f);
        }

        aimNotInSight = false;
        hasFinished = true;
        timerToAttack = 0;
        isAttacking = false;
    }

    private IEnumerator ShootThreeBullets(float timeToShoot) //Control de lanzamiento de cristales pequeños de 3 en 3
    {
        m_setAnimator.SetSmallCrystalsTrigger();
        yield return new WaitForSeconds(timeToShoot); //Control de Lanzamiento por evento de animación

        hasFinishedAttack = true;
    }

    public void InstantiateSmallBullets() //Control de Lanzamiento por evento de animación
    {
        float angleDiference = m_shootingAngle / m_bulletsToInstantiateAtOnce;
        int subdivisions = Mathf.FloorToInt(m_bulletsToInstantiateAtOnce / 2);
        float initialShootingAngle = -angleDiference * subdivisions;

        Instantiate(smallShootParticle, bossShootingPos.position, bossShootingPos.rotation);
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_SHOOT_SMALL_CRYSTAL);

        for (int i = 0; i < m_bulletsToInstantiateAtOnce; i++)
        {
            GameObject instantiatedBullet = Instantiate(smallBossBullet, bossShootingPos.position, bossShootingPos.rotation * Quaternion.AngleAxis(initialShootingAngle + i * angleDiference, Vector3.up));
            instantiatedBullet.layer = 10;
        }
    }

    private IEnumerator ShootOneBullet(float timeToShoot) //Control de lanzamiento de cristales grandes de 1 en 1
    {
        m_setAnimator.SetBigCrystalTrigger();
        yield return new WaitForSeconds(timeToShoot); //Control de Lanzamiento por evento de animación

        hasFinishedAttack = true;
    }

    public void InstantiateBigBullet() //Control de Lanzamiento por evento de animación
    {
        Instantiate(bigShootParticle, bossShootingPos.position, bossShootingPos.rotation);
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_SHOOT_BIG_CRYSTAL);

        GameObject instantiatedBullet = Instantiate(bigBossBullet, bossShootingPos.position, bossShootingPos.rotation);
        instantiatedBullet.layer = 10;
    }

    private IEnumerator AreaAttack(float timeToShoot) //Ataque en área
    {
        m_setAnimator.SetAreaAttackTrigger();
        yield return new WaitForSeconds(timeToShoot);    //Control de ataque de área por evento de animación 
    }

    public void InstantiateAreaAttack() //Control de ataque de área por evento de animación
    {
        Instantiate(areaAttackObject, waveShootingPos.position, bossShootingPos.rotation);

        hasFinished = true; //Por problemas, se indica que ha acabado el ataque área una vez instanciado el mismo en la animación
        timerToAttack = 0;
        isAttacking = false;
    }

    private void ResumePhase2AfterDisable() //Volver a empezar la corutina principal de fase 2, para volver a empezar el estado de la fase 2 tras el stun, que desactiva este sript. Se llama en OnEnable
    {
        aimNotInSight = false;
        ResetControlVariables();
        StartCoroutine(FinalBossPhase2Coroutine());
    }

    private void OnCollisionEnter(Collision collision)
    {      
        if(isCharging) //Control de colision en la carga
        {
            if (collision.gameObject.TryGetComponent(out PlayerHealthController playerHealth))
            {
                playerHealth.DamagePlayer(chargeDamage);
                hitPlayer = true;
                m_setAnimator.SetHitPlayerOnChargeBool(true);
            }
            else
            {
                m_setAnimator.SetHitWallOnChargeBool(true);
                MusicManager.Instance.PlaySound(AppSounds.BOSS2_STRUCK_WALL);
                hitWall = true;
            }

            hasFinishedAttack = true;
        }
    }

    private void KillBoss()
    {
        boss2SetControl.StopAllCoroutines();
        Destroy(boss2SetControl); //Destruir componente SetControl.
        StopAllCoroutines();
        m_setAnimator.SetDeathTrigger();
        electricGround1.DeactivateGround();
        electricGround2.DeactivateGround();

        //Sonidos
        MusicManager.Instance.PlayBackgroundMusic(AppSounds.ZONA2_B_MUSIC);

        ///// LOGRO ///////
        SteamManager.Instance.UnlockAchievement("COMPLETE_ZONE_2");
        /////

        ///// LOGRO ///////
        if (HealthHeartsVisual.healthHeartsSystemStatic.GetTotalFragments() >= battleStartPlayerHealth)
        {
            SteamManager.Instance.UnlockAchievement("NO_DAMAGE_SECOND_BOSS");
        }
        /////
        
        ///// LOGRO ///////
        if (timeAchievementTimer <= timeAchievement)
        {
            SteamManager.Instance.UnlockAchievement("BOSS_RUSH_2");
        }
        /////

        StartCoroutine(LoadEndCutscene());  //ELIMINAR SI SE HACE UNA CINEMÁTICA FINAL
    }

    private IEnumerator LoadEndCutscene() //ELIMINAR SI SE HACE UNA CINEMÁTICA FINAL
    {
        yield return new WaitForSeconds(1);
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_DEATH);

        yield return new WaitForSeconds(8);

        UIManager.Instance.Fade();

        yield return new WaitUntil(() => UIManager.Instance.IsScreenOnBlack());

        GameManager.Instance.EraseSavings(); //Borrar todos los datos guardados al completar el juego
        Time.timeScale = 1;
        MusicManager.Instance.StopBackgroundMusic();
        GetComponent<SimpleSceneLoading>().LoadScene();
        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnChangeInputDeactivateEvent();
        }
    }

    ///
    /// ///////////////// FUCIONES BÁSICAS //////////////////////////
    ///
    /// 

    public void BossReceiveHit() //Método de daños al Boss 2
    {
        if (currentPhase == BossPhase.one)
        {
            bossPhase1Hits++;
            m_setAnimator.SetHitTrigger();

            if (bossPhase1Hits == 1)
            {
                StopAllCoroutines();
                ResetControlVariables();
                hitLostElements[6].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[6].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                StartCoroutine(FinalBossPhase1Coroutine());
            }

            else if(bossPhase1Hits == 2)
            {
                StopAllCoroutines();
                ResetControlVariables();
                hitLostElements[5].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[5].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                hitLostElements[4].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[4].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                StartCoroutine(FinalBossPhase1Coroutine());                
            }

            else if(bossPhase1Hits == 3)
            {
                StopAllCoroutines();
                ResetControlVariables();
                hitLostElements[3].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[3].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                hitLostElements[2].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[2].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                hitLostElements[1].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[1].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                hitLostElements[0].SetActive(false);
                Instantiate(breakArmourParticle, hitLostElements[0].GetComponent<MeshRenderer>().bounds.center, breakArmourParticle.transform.rotation);
                StartCoroutine(ChangeFromPhase1To2());
            }
        }

        else if(currentPhase == BossPhase.two)
        {
            bossPhase2Hits++;
            
            if (bossPhase2Hits == 3)
            {
                KillBoss();
            }
        }

        Instantiate(hitParticle, new Vector3(transform.position.x, 1.3f, transform.position.z), transform.rotation);
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_HIT);
    }

    private void ResetControlVariables()
    {
        hasFinished = false;
        hasFinishedAttack = false;
    }

    public int CurrentBossPhase2Hits() //Sacar fuera del script cuantos hits tiene en la fase 2
    {
        return bossPhase2Hits;
    }

    private void Aim()
    {
        if (currentPhase == BossPhase.one || ((IsPlayerInSight() && currentPhase == BossPhase.two) || aimNotInSight) && !isCharging) //Apuntar sin más en fase 1, apuntar si ve al juador en fase 2 o si se activa el bool de apuntar de todas formas y no está cargando
        {
            Vector3 vector = VectorToPlayerFixedAim() + GameManager.Instance.ActualPlayerController.gameObject.GetComponent<Rigidbody>().velocity.normalized * m_distanceAimAheadPlayer * DistanceToPlayer();
            float angleSigned = Vector3.SignedAngle(vector, Vector3.right, Vector3.up);
            if (angleSigned < 0)
            {
                angleSigned += 360;
            }
            angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angleSigned, m_aimLerpVelocity * Time.deltaTime);

            m_armTransform.localEulerAngles = new Vector3(0, -angle, 0);
        }
        else if (currentPhase == BossPhase.two && m_AI_Controller.velocity.normalized.magnitude != 0) //Si no ve al jugador y se está moviendo en fase 2
        {
            angle = Vector3.SignedAngle(m_AI_Controller.velocity.normalized, Vector3.right, Vector3.up);
            angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angle, m_aimLerpVelocity * Time.deltaTime);
            m_armTransform.localEulerAngles = new Vector3(0, -angle, 0);
        }
        else if(currentPhase == BossPhase.two) //Si NO vee al jugador y no se mueve mantiene el ángulo anterior en fase 2
        {
            m_armTransform.rotation = Quaternion.AngleAxis(-angle, Vector3.up);
        }
    }

    private void ResetAim()
    {
        float angleSigned = Vector3.SignedAngle(transform.right, Vector3.forward, Vector3.up);
        if (angleSigned < 0)
        {
            angleSigned += 360;
        }
        angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angleSigned, m_aimLerpVelocity * Time.deltaTime);

        m_armTransform.localEulerAngles = new Vector3(0, -angle, 0);
    }

    private bool IsThereAliveEnemiesNotTurrets() //Si hay vivo algun enemigo además de las torretas y el boss
    {
        bool isPlayerControllingAnEnemyNotTurret = (GameManager.Instance.ActualPlayerController.GetComponent<EnemyControl_MovementController>() != null) && (GameManager.Instance.ActualPlayerController.GetComponent<HeatBarScript>() == null);
        int enemiesInRoomNotTurrets = 0;

        for (int i = 0; i < ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom.Count; i++)
        {
            if(ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom[i].GetComponent<HeatBarScript>() == null && ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom[i].GetComponent<FinalBossZona2Behaviour>() == null)
            {
                enemiesInRoomNotTurrets++; //Se suman al número de enemigos vivos solo los que no son torretas(no tienen HeatBarScript) ni boss
            }
        }

        if (enemiesInRoomNotTurrets == 0 /*|| (isPlayerControllingAnEnemyNotTurret && enemiesInRoomNotTurrets == 1)*/)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void SpawnEnemies(GameObject[] enemiesToSpawn)
    {
        for (int i = 0; i < enemiesToSpawn.Length; i++)
        {
            roomSpawners[i].SpawnEnemy(enemiesToSpawn[i]);
        }
    }

    private void SetRotationOnStart()
    {
        angle = -transform.rotation.eulerAngles.y;
        transform.rotation = Quaternion.identity;
    }

    private void UpdateObjectRotation()
    {
        modelParent.transform.localEulerAngles = new Vector3(0, -angle, 0);
    }


    ///////////////// ACTUALIZACIÓN DE ANIMACIONES////////////////////

    protected Vector3 relativeVector;
    protected void SetAnimationsVariables()
    {
        if (m_setAnimator != null)
        {
            float forwardAngleToWorld = Vector3.SignedAngle(m_armTransform.transform.right, Vector3.forward, Vector3.up);

            Vector3 previousRelativeVector = relativeVector;
            relativeVector = (Quaternion.AngleAxis(forwardAngleToWorld, Vector3.up) * m_AI_Controller.velocity).normalized;
            relativeVector = Vector3.Lerp(previousRelativeVector, relativeVector, Time.deltaTime * 10);

            m_setAnimator.SetIsMovingBool(m_AI_Controller.velocity.magnitude > 0.1f);

            m_setAnimator.SetMovementX(relativeVector.x);
            m_setAnimator.SetMovementZ(relativeVector.z);
        }
    }
}
