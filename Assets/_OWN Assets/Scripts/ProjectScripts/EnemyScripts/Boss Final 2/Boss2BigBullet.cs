﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2BigBullet : BulletBase
{
    [Header("Tiempo de Stun al jugador")]
    [SerializeField] private float playerStunTime;

    protected override void CollisionWithPlayer(PlayerHealthController playerHealth)
    {
        base.CollisionWithPlayer(playerHealth);

        playerHealth.TryGetComponent(out PossessAbility playerPossessAbility);
        playerHealth.TryGetComponent(out PlayerControl_MovementController playerController);
        playerPossessAbility.StartCoroutine(playerPossessAbility.PlayerStun(playerStunTime));    //Stun al jugador 
        playerController.controlRb.velocity = Vector3.zero;
    }
}
