﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveAttack : MonoBehaviour
{
    [SerializeField] private float waveAttackSpeed;
    [SerializeField] private int waveDamage;
    private ParticleSystem thisSystem;
    private List<ParticleSystem.Particle> playerEnterParticles = new List<ParticleSystem.Particle>();
    private int playerEnterNum;

    private void Start()
    {
        thisSystem = GetComponent<ParticleSystem>();
        thisSystem.trigger.SetCollider(0, GameManager.Instance.realPlayerGO.GetComponent<Collider>());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.forward * waveAttackSpeed * Time.deltaTime);
    }

    private void OnParticleCollision(GameObject other) //Gestionar colisiones con enemigos y enemigos poseidos
    {
        if(other.gameObject.TryGetComponent(out EnemyHealth enemyHealth))
        {
            enemyHealth.ReceiveDamage(waveDamage);

            enemyHealth.TryGetComponent(out EnemySetControl enemySetControl);
            enemySetControl.StopConsumingAction();

            if(enemySetControl.IsEnemyPossessed())
            {
                thisSystem.trigger.SetCollider(0, null); //Si el enemigo que mata la wave está poseido, se elimina el trigger del player para no hacerle daño al desposeerse
            }
        }
    }

    private void OnParticleTrigger() //Gestionar colision con el Trigger de player, que hay que encontrar y asignar en Start. Hay que setear correctamente el módulo de Triggers en el editor de la partícula.
    {
        playerEnterNum = thisSystem.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, playerEnterParticles);

        if (playerEnterNum >= 1)
        {           
            if(thisSystem.trigger.GetCollider(0).TryGetComponent(out PlayerHealthController playerHealth))
            {
                playerHealth.DamagePlayer(waveDamage);
            }
        }
    }
}
