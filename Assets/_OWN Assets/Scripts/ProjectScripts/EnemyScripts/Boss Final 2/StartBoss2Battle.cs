﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Timeline;
using UnityEngine;

public class StartBoss2Battle : MonoBehaviour
{
    [SerializeField] private FinalBossZona2Behaviour boss2Behaviour;
    [SerializeField] private Door battleEnterDoor;
    [SerializeField] private Transform respawnPosition;
    [SerializeField] private GameObject doorCollision;
    [SerializeField] private TimelineAsset playable;
    [SerializeField] bool skipCutscene;
    CutsceneManager cutManager;
    private void Start()
    {
        cutManager = FindObjectOfType<CutsceneManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PossessAbility>() != null && !GameManager.Instance.IsPlayerRespawning())
        {
            StartCutsceneBoss2OnTrigger();
        }
    }

    private void StartCutsceneBoss2OnTrigger()
    {
        cutManager.PlayCutscene(playable, skipCutscene, true);
        doorCollision.SetActive(true);
        GameManager.Instance.SaveGame(respawnPosition.position); ////Guardar el juego al usar el Trigger de inicio de batalla
        battleEnterDoor.CloseDoor();
        Destroy(gameObject);
    }
}
