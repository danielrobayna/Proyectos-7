﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Boss2Brain : MonoBehaviour
{
    [Header("Variables de sentidos")]
    [Tooltip("El angulo total está calculado tomando de origen la dirección en la que mira el enemigo y hacia hacia derecha e izquierda, por lo que un valor de 90º equivale a 90º hacia derecha e izquierda, osea 180º de visión en la dirección en que mira")]
    [SerializeField] protected LayerMask m_sightCollisionMask;

    [Header("Objeto Brazo que rota")]
    [SerializeField] protected Transform m_armTransform;

    [Header("Componentes")]
    protected NavMeshAgent m_AI_Controller;

    protected float m_originalStoppingDistance;

    protected virtual void Awake()
    {
        m_AI_Controller = GetComponent<NavMeshAgent>();
        //m_originalStoppingDistance = m_AI_Controller.stoppingDistance;
    }

    ////////////////////Funciones de referencia del jugador////////////////////////

    protected Vector3 VectorToPlayer()  ///El Vector a jugador desde la base del enemigo, para cuestiones de NAVEGACIÓN
    {
        Vector3 vectorToPlayer = GameManager.Instance.ActualPlayerController.transform.position - transform.position;
        return new Vector3(vectorToPlayer.x, 0, vectorToPlayer.z);
    }
    protected Vector3 VectorToPlayerFixedAim()  ///El Vector a jugador desde la posición del brazo, para cuestiones de APUNTADO
    {
        Vector3 vectorToPlayer = GameManager.Instance.ActualPlayerController.transform.position - m_armTransform.position;
        return new Vector3(vectorToPlayer.x, 0, vectorToPlayer.z);
    }
    protected float DistanceToPlayer()
    {
        return VectorToPlayer().magnitude;
    }
    protected bool IsPlayerInSight()
    {
        if (!Physics.SphereCast(m_armTransform.position, 0.2f, VectorToPlayerFixedAim(), out RaycastHit hit, DistanceToPlayer(), m_sightCollisionMask)) //Si el raycast no tiene nada de por medio, ve al jugador
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    protected bool isPlayerFurtherThanStoppingDistance()
    {
        if (m_AI_Controller.stoppingDistance < DistanceToPlayer())
        {
            return true;
        }
        else return false;
    }
}
