﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI_Standard : AIBrain
{
    [Header("Variables de disparo al jugador")]
    [SerializeField] protected float m_distanceAimAheadPlayer;
    [SerializeField] protected float m_aimLerpVelocity=1;
    [SerializeField] float m_timeTillFistShoot;
    float firstShootTimer;
    [Header("La distancia mínima para disparar al jugador")]
    [SerializeField] protected float distanceToShootPlayer; ///Una distancia mínima para disparar
    [Header("Reloj de búsqueda al jugador")]
    [SerializeField] protected float m_clockDelay;
    protected float m_clockTimer;

    [Header("Variables para checkear posiciones de Movimiento con Calculate Path")]
    private bool canPositionBeReached = false;

    [Header("Variables para checkeo del RunAway")]
    protected bool hasCheckedRunAwayPos = false;
    protected bool isRunAwayPosDecided = false;
    private Vector3 runAwayDestination;

    [Header("Variables para checkeo del BurningPanic")]
    [SerializeField] private bool canEnterFirePanicState;
    private bool hasCheckedPanicPos = false;
    private Vector3 panicDestination;
    private float timeToSearchNextPanicPos = 1;
    private float searchPanicTimer = 0;

    [Header("Variables del Shock/Stun Eléctrico")]
    public bool canEnterElectricalStunState;
    [SerializeField] private float timeElectricallyStuned;
    private float electricalStunTimer;
    public ParticleSystem electricStunParticle;

    protected SetAnimatorVariables m_mySetAnimationVariables;

    [Header("Componentes")]
    protected ShootingScript m_shootingScript;
    Rigidbody m_playerRigidbody;
    protected Rigidbody m_myRigidbody;

    [Header("Modelo del enemigo")]
    [SerializeField] GameObject modelParent;

    [Header("Tick SOLO si el enemigo patrulla")]
    [Space(10)]
    public bool hasPatrolBehaviour;
    public List<Transform> patrolPointsList = new List<Transform>();
    private Transform currentListPoint;
    private bool patrolPointDecided = false;

    public float timeToWaitOnPatrolPoint;
    private float timerWaitBetweenPatrolPoint = 0;

    public enum AIState
    {
        idle,
        attacking,
        runningAway,
        surpriseOnPossess,
        burningPanic,
        electricallyShocked,
        surrender   //Para el Healer
    }
    public AIState currentAIState = AIState.idle;
    protected float angle;

    protected virtual void OnEnable()
    {
        currentAIState = AIState.idle;
        electricalStunTimer = 0;
    }

    protected virtual void Start()
    {
        m_shootingScript = GetComponent<ShootingScript>();
        m_myRigidbody = GetComponent<Rigidbody>();
        m_mySetAnimationVariables = GetComponent<SetAnimatorVariables>();

        m_AI_Controller.updateRotation = false;

        SetRotationOnStart();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        UpdateStateMachine();
        Aim();
        UpdateObjectRotation();

        //Seteo de animaciones
        SetAnimationsVariables();
    }
    protected virtual void UpdateStateMachine()
    {
        //Idle
        if (currentAIState == AIState.idle)
        {
            IdleState();          
        }
        //Sorprendido al saltar el jugador hacia él
        else if (currentAIState == AIState.surpriseOnPossess) //Se llama desde el SetControl
        {
            hasCheckedRunAwayPos = false;
            isRunAwayPosDecided = false;
            //Animación     
        }
        //Atacando
        else if (currentAIState == AIState.attacking)
        {
            AttackingState();
        }
        //Huyendo
        else if (currentAIState == AIState.runningAway)
        {
            RunningAwayState();
        }
        //Stun eléctrico
        else if (currentAIState == AIState.electricallyShocked)
        {
            ElectricalStunState();
        }
        //Pánico al quemarse
        else if(currentAIState == AIState.burningPanic)
        {
            BurningPanicState();
        }      
    }

    ////////////////////ESTADOS////////////////////////
    protected virtual void IdleState()
    {
        //Movimiento
        if (hasPatrolBehaviour)
        {
            Patrol();
        }

        //Pasar a atacar
        if (DetectPlayerInInitialState())
        {
            currentAIState = AIState.attacking;
        }
    }

    protected virtual void AttackingState()
    {
        //Movimiento
        m_clockTimer += Time.deltaTime;
        if (m_clockTimer > m_clockDelay)
        {
            AttackingFindNewDestination();
            m_clockTimer = 0;
        }

        //Disparo
        if (IsPlayerInSight() && DistanceToPlayer() <= distanceToShootPlayer) //Si puede disparar
        {
            firstShootTimer += Time.deltaTime;
            if (firstShootTimer > m_timeTillFistShoot)
            {
                 DamagePlayer();
            }
        }
        else //Si no puede disparar
        {
            firstShootTimer = 0;
            m_shootingScript.ResetEnemyFiringRateTimer();
        }

        //Pasar a huir
        if (IsPlayerInSight() && IsPlayerTooNear() && m_shootingScript.IsEnemyAbleToShoot()) //Comportamiento de huir si ve al jugador y esta muy cerca, no solo si siente que esta cerca aunque no lo vea. Si se usa el bool IsAbleToShoot solo huye si no esta disparando
        {
            firstShootTimer = 0;
            currentAIState = AIState.runningAway;
        }
    }
    protected virtual void RunningAwayState()
    {
        if(!hasCheckedRunAwayPos)
        {
            CheckRunAwayPos();
        }

        else if(hasCheckedRunAwayPos && isRunAwayPosDecided)
        {
            CheckDistanceToRunAwayPos();
        }

        else if(hasCheckedRunAwayPos && !isRunAwayPosDecided)
        {
            DamagePlayer();
        }

        //Pasar a atacar
        if (!IsPlayerTooNear())
        {
            currentAIState = AIState.attacking;
            m_AI_Controller.autoBraking = true; //Se vuelve a activar el autoBraking, desactivado al darse como válida la posición de huida
            hasCheckedRunAwayPos = false;
        }
    
    }

    protected virtual void BurningPanicState()  //El estado se comienza desde los métodos Start y Stop, más abajo.
    {
        if(!hasCheckedPanicPos)
        {
            CheckBurningPanicPos();
            searchPanicTimer = 0;
        }
        else if(hasCheckedPanicPos)
        {
            searchPanicTimer += 1 * Time.deltaTime;

            if(searchPanicTimer >= timeToSearchNextPanicPos)
            {
                hasCheckedPanicPos = false;
            }
        }
    }

    protected virtual void ElectricalStunState()
    {
        m_mySetAnimationVariables.SetShockedBool(true);
        electricalStunTimer += 1 * Time.deltaTime;

        if (electricalStunTimer >= timeElectricallyStuned)
        {
            StopElectricalStun();
            currentAIState = AIState.attacking;
        }
    }

    ////////////////////ACCIONES////////////////////////
    protected virtual void Aim()
    {
        if (IsPlayerInSight() && currentAIState != AIState.idle && currentAIState != AIState.electricallyShocked && currentAIState != AIState.burningPanic && currentAIState != AIState.electricallyShocked)
        {
            Vector3 vector = VectorToPlayerFixedAim() + GameManager.Instance.ActualPlayerController.gameObject.GetComponent<Rigidbody>().velocity.normalized * m_distanceAimAheadPlayer * DistanceToPlayer();
            float angleSigned = Vector3.SignedAngle(vector, Vector3.right, Vector3.up);
            if (angleSigned < 0)
            {
                angleSigned += 360;
            }
            angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angleSigned, m_aimLerpVelocity * Time.deltaTime);
         
            m_armTransform.localEulerAngles = new Vector3(0, -angle, 0);
        }
        else if (m_AI_Controller.velocity.normalized.magnitude != 0) //Si no ve al jugador y se está moviendo
        {
            angle = Vector3.SignedAngle(m_AI_Controller.velocity.normalized, Vector3.right, Vector3.up);
            angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angle, m_aimLerpVelocity * Time.deltaTime);
            m_armTransform.localEulerAngles = new Vector3(0, -angle, 0); 
        }
        else //Si NO vee al jugador y no se mueve mantiene el ángulo anterior
        {
            m_armTransform.rotation = Quaternion.AngleAxis(-angle, Vector3.up);
        }
    }
    protected virtual void Patrol()
    {
        m_AI_Controller.stoppingDistance = 0.1f;

        if (!patrolPointDecided)
        {
            CheckPatrolPoint();
        }

        else if (patrolPointDecided)
        {
            CheckDistanceToPatrolPoint();
        }

        if (DetectPlayerInInitialState())
        {
            currentAIState = AIState.attacking;
            patrolPointDecided = false;
        }
    }
    protected virtual void AttackingFindNewDestination()
    {
        if (IsPlayerInSight() && m_shootingScript.IsEnemyAbleToShoot()) //Si se usa el bool IsAbleToShoot, se mueve si no esta cargando disparo o disparando ráfaga
        {
            m_AI_Controller.stoppingDistance = m_originalStoppingDistance;
            if (isPlayerFurtherThanStoppingDistance())
            {
                FindQuickNewDestination(GameManager.Instance.ActualPlayerController.transform.position);
            }
        }
        else if (!IsPlayerInSight() && m_shootingScript.IsEnemyAbleToShoot() && !m_shootingScript.IsEnemyASniper()) //Si se usa el bool IsAbleToShoot, se mueve si no esta cargando disparo o disparando ráfaga
        {                                                                                                           //Si es un sniper no persigue al jugador
            m_AI_Controller.stoppingDistance = 0.5f;
            FindQuickNewDestination(GameManager.Instance.ActualPlayerController.transform.position);
        }
    }
    public virtual void DamagePlayer()
    {
        m_shootingScript.FireInShootingPos(ShootingScript.whoIsShooting.enemy);       
    }

    ////////////////////CONDICIONES////////////////////////
    ///
    protected bool IsPlayerTooNear()
    {
        if (DistanceToPlayer()< m_runAwayDistance)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected bool DetectPlayerInInitialState()
    {
        float sightAngle = Mathf.Abs(Vector3.SignedAngle(VectorToPlayerModelFixedAim(), m_armTransform.right, Vector3.up));
        ///Debug detección del enemigo///

        /*Debug.DrawRay(m_armTransform.position, VectorToPlayerFixedAim(),Color.yellow);
        Debug.DrawRay(m_armTransform.position, Quaternion.Euler(0, m_visionAngle/2, 0)* m_armTransform.right*100, Color.red);
        Debug.DrawRay(m_armTransform.position, Quaternion.Euler(0, -m_visionAngle/2, 0) * m_armTransform.right*100, Color.red);
        Debug.Log(sightAngle);*/

        if (DistanceToPlayer() < m_playerHearingDetectionDistance || (IsPlayerInSight() && sightAngle < m_visionAngle / 2 && DistanceToPlayer() < m_visionDistance)) //Si oye al jugador o si le ve y el player está en un angulo de menos del doble de m_visionAngle, ve al jugador
        {
            return true;
        }
        else { return false; }
    }
    public virtual void ExternalDetectPlayer()
    {
        currentAIState = AIState.attacking;
    }

    ////////////////////FUNCIONALIDAD////////////////////////
    ///
    public virtual void FindQuickNewDestination(Vector3 newDestinationPosition)
    {
        m_AI_Controller.SetDestination(newDestinationPosition);
    }

    NavMeshPath path;
    protected virtual void FindCheckDestination(Vector3 newDestination) //Siempre que se use este método, ejecutarlo primero antes de hacer comprobaciones de canPositionbeReached
    {
        path = new NavMeshPath();
        m_AI_Controller.CalculatePath(newDestination, path);
        if (path.status == NavMeshPathStatus.PathComplete)
        {
            m_AI_Controller.path = path;
            canPositionBeReached = true;
        }
        else
        {
            canPositionBeReached = false;
        }
    }

    protected void CheckRunAwayPos()    //Se ejecuta desde el Update en el RunningAwayState para localizar una posición válida de huida.
    {
        m_AI_Controller.stoppingDistance = 0.1f;
        Vector3 oppositeDirectionVector = -VectorToPlayerModel().normalized;
        runAwayDestination = new Vector3(oppositeDirectionVector.x + transform.position.x, 0, oppositeDirectionVector.z + transform.position.z);

        FindCheckDestination(runAwayDestination); //Ejecutar este método antes de comprobar canPositionBeReached

        if (canPositionBeReached)
        {
            isRunAwayPosDecided = true;
            m_AI_Controller.autoBraking = false; //Para que cuando huye no parezca que va parándose.
        }

        else
        {
            isRunAwayPosDecided = false;
        }

        hasCheckedRunAwayPos = true;
    }
    protected void CheckDistanceToRunAwayPos()   ///Chequeo de la distancia hasta la posición de huida
    {
        if (m_AI_Controller.remainingDistance <= 0.1f) ///El stopping distance +0.1f es para checkear la posicion en la que se para
        {
            hasCheckedRunAwayPos = false;
            isRunAwayPosDecided = false;
        }
    }

    public virtual void StartElectricalStun()
    {
        if(canEnterElectricalStunState)
        {
            electricalStunTimer = 0;

            m_AI_Controller.autoBraking = false;
            if (gameObject.activeSelf)
            {
                m_AI_Controller.ResetPath();
            }
            m_AI_Controller.velocity = Vector3.zero;

            hasCheckedRunAwayPos = false;
            isRunAwayPosDecided = false;
            currentAIState = AIState.electricallyShocked;

            if (m_shootingScript != null) //Comprobación para healers
            {
                m_shootingScript.StopAllCoroutines();   //Detener corutinas de disparo iniciadas por la IA al ser electrificado.
                m_shootingScript.ResetShootingVariables();
            }

            if (!electricStunParticle.isEmitting)
            {
                electricStunParticle.Play();
            }
        }

        else
        {
            currentAIState = AIState.attacking;
        }
    }

    public virtual void StopElectricalStun()
    {
        if (electricStunParticle.isEmitting)
        {
            electricStunParticle.Stop();
        }
        electricalStunTimer = 0;
        m_AI_Controller.autoBraking = true;
        m_mySetAnimationVariables.SetShockedBool(false);
    }

    public virtual void StartBurningPanic() //Se llama para empezar estado de pánico al quemarse desde el script del lanzallamas
    {
        if(canEnterFirePanicState && currentAIState != AIState.electricallyShocked) //Tiene prioridad el stun eléctrico
        {
            currentAIState = AIState.burningPanic;

            hasCheckedRunAwayPos = false;   ///Se vacian variables del RunAway, ya que se inicia este estado desde cualquier momento y pueden quedarse guardados valores oincorrectos del RunAway
            isRunAwayPosDecided = false;

            //Animación
            m_mySetAnimationVariables.SetIsOnFireBool(true);
        }
    }
    public void StopBurningPanic()  //Se llama para parar estado de pánico desde enemyHealth cuando deja de quemarse en la corutina
    {
        if(canEnterFirePanicState)
        {
            currentAIState = AIState.attacking;
            hasCheckedPanicPos = false;
            m_AI_Controller.autoBraking = true;

            //Animación
            m_mySetAnimationVariables.SetIsOnFireBool(false);
        }
    }
    private void CheckBurningPanicPos()
    {
        m_AI_Controller.stoppingDistance = 0.1f;
        Quaternion rotation = Quaternion.AngleAxis(Random.Range(-160, 160), Vector3.up); //Para sacar la posición random de movimiento al quemarse le sumo al Vector de dirección un ángulo random de -160.º a 160º...                                                                                         
        Vector3 panicDirectionVector = rotation * -VectorToPlayerModel().normalized * 2;          //...desde el vector opuesto al jugador, de manera que solo NO pueda moverse en un ángulo de 40º hacia el jugador 
        panicDestination = new Vector3(panicDirectionVector.x + transform.position.x, 0, panicDirectionVector.z + transform.position.z);

        FindCheckDestination(panicDestination); //Ejecutar este método antes de comprobar canPositionBeReached

        if (canPositionBeReached)
        {
            m_AI_Controller.autoBraking = false;
        }

        else
        {
            CheckBurningPanicPos(); //Se vuelve a ejecutar
        }

        hasCheckedPanicPos = true;
    }

    int currentPatrolPoint = 0;
    private void CheckPatrolPoint() ///Chequeo de a que punto de patrulla ir, se llama en estado Patrol
    {
        if (currentPatrolPoint >= patrolPointsList.Count)
        {
            currentPatrolPoint = 0;
        }
        currentListPoint = patrolPointsList[currentPatrolPoint];
        FindQuickNewDestination(currentListPoint.position);
        currentPatrolPoint++;
        patrolPointDecided = true;
    }
    private void CheckDistanceToPatrolPoint()   ///Chequeo de la distancia hasta el punto de patrulla
    {
        if (Vector3.Distance(transform.position, currentListPoint.position) <= m_AI_Controller.stoppingDistance + 0.1f) ///El stopping distance +0.1f es para checkear la posicion en la que se para
        {
            timerWaitBetweenPatrolPoint += 1 * Time.deltaTime;

            if (timerWaitBetweenPatrolPoint >= timeToWaitOnPatrolPoint) ///Una vez pasado cierto tiempo en el punto, se chequea el siguiente punto
            {
                patrolPointDecided = false;
                timerWaitBetweenPatrolPoint = 0;
            }
        }
    }
    void SetRotationOnStart()
    {
        angle = -transform.rotation.eulerAngles.y;
        transform.rotation = Quaternion.identity;
    }

    ///////////////////////////////////Animaciones//////////////////////////////
    public void UpdateObjectRotation()
    {
        modelParent.transform.localEulerAngles = new Vector3(0, -angle, 0);
    }

    protected Vector3 relativeVector;

    protected void SetAnimationsVariables()
    {
        if (m_mySetAnimationVariables != null)
        {
            float forwardAngleToWorld = Vector3.SignedAngle(m_armTransform.transform.right, Vector3.forward, Vector3.up);

            Vector3 previousRelativeVector = relativeVector;
            relativeVector = (Quaternion.AngleAxis(forwardAngleToWorld, Vector3.up) * m_AI_Controller.velocity).normalized;
            relativeVector = Vector3.Lerp(previousRelativeVector, relativeVector, Time.deltaTime * 10);

            m_mySetAnimationVariables.SetIsMovingBool(m_AI_Controller.velocity.magnitude > 0.1f);

            m_mySetAnimationVariables.SetMovementX(relativeVector.x);
            m_mySetAnimationVariables.SetMovementZ(relativeVector.z);
        }      
    }
}
