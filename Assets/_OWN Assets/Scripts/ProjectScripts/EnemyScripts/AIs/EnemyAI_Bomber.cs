﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class EnemyAI_Bomber : EnemyAI_Standard
{
    BomberSetControl m_myBomberSetControl;

    protected override void Start()
    {
        base.Start();
        m_myBomberSetControl = GetComponent<BomberSetControl>();
    }
    protected override void AttackingFindNewDestination()
    {
        if (m_shootingScript.IsEnemyAbleToShoot())
        {
            FindQuickNewDestination(GameManager.Instance.ActualPlayerController.transform.position);
        }
        m_mySetAnimationVariables.SetAttackBool(true);
    }
    public override void DamagePlayer()
    {
        m_shootingScript.FireInShootingPos(ShootingScript.whoIsShooting.player);

        m_myBomberSetControl.hasToExplode = true; //Se pone este bool a true para no instanciar la explosion en la muerte del bomber
    }
}
