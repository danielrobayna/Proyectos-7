﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI_Turret : EnemyAI_Standard
{
    protected override void Update()
    {
        base.Update();
        PlayRotationSound();
    }

    //Sonidos
    AudioSource rotationAudioSource;
    void PlayRotationSound()
    {
        if (IsPlayerInSight())
        {
            if(rotationAudioSource == null)
            {
                rotationAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.TURRET_MOVE, this.transform);
            }
        }
        else if(rotationAudioSource != null)
        {
            rotationAudioSource.Stop();
        }
    }
    private void OnDisable()
    {
        if(rotationAudioSource != null)
        {
            rotationAudioSource.Stop();
        }
    }
}
