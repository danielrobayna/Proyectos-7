﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI_Shield : EnemyAI_Standard          //Hereda de Standard//
{
    [Header("Control de OnEnable")]
    private bool initialized = false;

    [Header ("Velocidad de apuntado del enemigo con el escudo sacado")]
    [SerializeField] private float shieldActiveAimSpeed;
    private float initialEnemyAimSpeed;

    [Header("El Escudo del enemigo Shield")]
    public ShieldScript shieldScript;

    protected override void OnEnable()
    {
        base.OnEnable();

        if (initialized)
        {
            if (!shieldScript.shieldActive)
            {
                shieldScript.ActivateShield();
                m_aimLerpVelocity = shieldActiveAimSpeed;
                m_mySetAnimationVariables.SetShieldBool(true);
            }
        }

        shieldScript.ShieldChangeLayer(25);
    }

    protected override void Start()
    {
       
        base.Start();
        initialized = true;
        initialEnemyAimSpeed = m_aimLerpVelocity;
        m_aimLerpVelocity = shieldActiveAimSpeed;
        m_mySetAnimationVariables.SetShieldBool(true);
        m_mySetAnimationVariables.SetIsPatrollingBool(false);

    }

    protected override void Patrol()
    {
        m_mySetAnimationVariables.SetIsPatrollingBool(true);
        base.Patrol();
       
        shieldScript.DeactivateShield();
        
    }
    public override void StartBurningPanic()
    {
        base.StartBurningPanic();

        if (shieldScript.shieldActive)
        {
            shieldScript.DeactivateShield();
            m_aimLerpVelocity = initialEnemyAimSpeed;
        }
    }

    protected override void ElectricalStunState()
    {
        base.ElectricalStunState();

        if (shieldScript.shieldActive)
        {
            shieldScript.DeactivateShield();
            m_aimLerpVelocity = initialEnemyAimSpeed;
        }
    }

    protected override void AttackingFindNewDestination()
    {
        m_mySetAnimationVariables.SetIsPatrollingBool(false);
        if (IsPlayerInSight())
        {
            if (!shieldScript.shieldActive && m_AI_Controller.velocity == Vector3.zero)
            {
                shieldScript.ActivateShield();
                m_aimLerpVelocity = shieldActiveAimSpeed;
            }

            m_AI_Controller.stoppingDistance = m_originalStoppingDistance;
            if (DistanceToPlayer() > distanceToShootPlayer)                 //Shield solo persigue si está el player más lejos de lo que puede disparar.
            {
                if (shieldScript.shieldActive)
                {
                    shieldScript.DeactivateShield();
                    m_aimLerpVelocity = initialEnemyAimSpeed;
                }
                FindQuickNewDestination(GameManager.Instance.ActualPlayerController.transform.position);
            }
        }
        else if (!IsPlayerInSight())
        {
            if (shieldScript.shieldActive)
            {
                shieldScript.DeactivateShield();
                m_aimLerpVelocity = initialEnemyAimSpeed;
            }
            m_AI_Controller.stoppingDistance = 0.5f;
            FindQuickNewDestination(GameManager.Instance.ActualPlayerController.transform.position);
        }
    }
}
