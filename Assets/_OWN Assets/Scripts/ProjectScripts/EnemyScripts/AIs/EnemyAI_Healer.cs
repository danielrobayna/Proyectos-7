﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI_Healer : EnemyAI_Standard
{
    NavMeshPath path;

    // Update is called once per frame
    protected override void UpdateStateMachine()
    {
        if (currentAIState == AIState.idle)
        {
            IdleState();
        }
        //Sorprendido al saltar el jugador hacia él
        else if (currentAIState == AIState.surpriseOnPossess) //Se llama desde el SetControl
        {
            hasCheckedRunAwayPos = false;
            isRunAwayPosDecided = false;
            //Animación
        }
        else if(currentAIState == AIState.runningAway)
        {
            RunningAwayState();
        }
        else if(currentAIState == AIState.surrender)
        {
            m_mySetAnimationVariables.SetFearBool(true);
        }
        else if(currentAIState == AIState.attacking) //Si por cualquier razón el healer entra en attacking, se redirige automáticamente a Idle.
        {
            currentAIState = AIState.idle;
        }
        //Stun eléctrico
        else if (currentAIState == AIState.electricallyShocked)
        {
            ElectricalStunState();
        }
        //Pánico al quemarse
        else if (currentAIState == AIState.burningPanic)
        {
            BurningPanicState();
        }
    }
    protected override void IdleState()
    {
        if (hasPatrolBehaviour)
        {
            Patrol();
        }
        if (IsPlayerTooNear())
        {
            m_mySetAnimationVariables.SetFearBool(true);
            currentAIState = AIState.runningAway;
            PlayScreamSound();
        }
    }
    protected override void RunningAwayState()
    {
        if (!hasCheckedRunAwayPos)
        {
            CheckRunAwayPos();
        }

        else if (hasCheckedRunAwayPos && isRunAwayPosDecided)
        {
            CheckDistanceToRunAwayPos();
        }

        else if (hasCheckedRunAwayPos && !isRunAwayPosDecided)
        {
            currentAIState = AIState.surrender;
            m_AI_Controller.autoBraking = true;
        }

        if (!IsPlayerTooNear())
        {
            currentAIState = AIState.idle;
            m_AI_Controller.autoBraking = true; //Se vuelve a activar el autoBraking, desactivado al darse como válida la posición de huida
            hasCheckedRunAwayPos = false;
        }
    }
    public override void ExternalDetectPlayer()
    {
        currentAIState = AIState.runningAway;
    }
    protected override void Aim()
    {
        if (m_AI_Controller.velocity.normalized.magnitude != 0) //Si no se controla la dirección y se está moviendo
        {
            angle = Vector3.SignedAngle(m_AI_Controller.velocity.normalized, Vector3.right, Vector3.up);
            angle = Mathf.LerpAngle(-m_armTransform.localEulerAngles.y, angle, m_aimLerpVelocity * Time.deltaTime);
            m_armTransform.localEulerAngles = new Vector3(0, -angle, 0);
        }
    }
    void PlayScreamSound()
    {
        int randomNumber = Random.Range(0, 3);

        //Play a random sound
        switch (randomNumber)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.NPC_SCREAM1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.NPC_SCREAM2, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.NPC_SCREAM3, this.transform);
                break;
            default:
                break;
        }
    }
}
