﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBrain : MonoBehaviour        //Script con los Sentidos de la IA//
{
    [Header("Variables de sentidos")]
    [Tooltip("El angulo total está calculado tomando de origen la dirección en la que mira el enemigo y hacia hacia derecha e izquierda, por lo que un valor de 90º equivale a 90º hacia derecha e izquierda, osea 180º de visión en la dirección en que mira")]
    [SerializeField] protected float m_visionAngle; //El ángulo de visión
    [SerializeField] protected float m_visionDistance; //Distancia a la que debe estar el player para que se detectecte por visión
    [SerializeField] protected float m_playerHearingDetectionDistance; //Para salir de idle
    [SerializeField] protected float m_runAwayDistance; //Cuánto se tiene que acercar el jugador para que comience a huir
    [SerializeField] protected LayerMask m_sightCollisionMask;

    [Header("Objeto Brazo que rota")]
    [SerializeField] protected Transform m_armTransform;

    [Header("Componentes")]
    protected NavMeshAgent m_AI_Controller;

    protected float m_originalStoppingDistance;

    private void Awake()
    {
        m_AI_Controller = GetComponent<NavMeshAgent>();
        m_originalStoppingDistance = m_AI_Controller.stoppingDistance;
    }

    ////////////////////Funciones de referencia del jugador////////////////////////

    protected Vector3 VectorToPlayerFixedAim()  ///El Vector a OBJETO jugador desde la posición del brazo, para cuestiones de APUNTADO
    {
        Vector3 vectorToPlayer = GameManager.Instance.ActualPlayerController.transform.position - m_armTransform.position;
        return new Vector3(vectorToPlayer.x, 0, vectorToPlayer.z);
    }
    protected Vector3 VectorToPlayerModel()  ///El Vector al modelo del jugador desde la base del enemigo, para cuestiones de NAVEGACIÓN
    {
        Vector3 vectorToPlayer = GameManager.Instance.ActualPlayerController.modelParent.transform.position - transform.position;
        return new Vector3(vectorToPlayer.x, 0, vectorToPlayer.z);
    }
    protected Vector3 VectorToPlayerModelFixedAim() ///El Vector a jugador desde el brazo a MODELO, para que la VISIÓN vaya hacia el modelo real, como en el caso del player que esta separado del objeto.
    {
        Vector3 vectorToPlayer = GameManager.Instance.ActualPlayerController.modelParent.transform.position - m_armTransform.position;
        return new Vector3(vectorToPlayer.x, 0, vectorToPlayer.z);
    }
    protected float DistanceToPlayer() ///Calculado al modelo
    {
        return VectorToPlayerModel().magnitude;
    }
    protected bool IsPlayerInSight()
    {
        if (!Physics.SphereCast(m_armTransform.position, 0.2f, VectorToPlayerFixedAim(), out RaycastHit hit, DistanceToPlayer(), m_sightCollisionMask)) //Si el raycast no tiene nada de por medio, ve al jugador
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    protected bool isPlayerFurtherThanStoppingDistance()
    {
        if (m_AI_Controller.stoppingDistance < DistanceToPlayer())
        {
            return true;
        }
        else return false;
    }
}
