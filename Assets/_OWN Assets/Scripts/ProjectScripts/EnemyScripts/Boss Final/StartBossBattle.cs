﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BossBattle
{
    public class StartBossBattle : MonoBehaviour
    {
        [SerializeField] FinalBossCommon finalBossBehaviour;
        [SerializeField] Door bossDoor;
        [SerializeField] Transform respawnPosition;

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
            {
                StartBattleOnTrigger();
            }
        }
        void StartBattleOnTrigger()
        {
            finalBossBehaviour.InitialState();

            GameManager.Instance.SaveGame(respawnPosition.position); ////Guardar el juego al usar el Checkpoint

            bossDoor.CloseDoor();
            Destroy(this.gameObject);
        }
    }
}

