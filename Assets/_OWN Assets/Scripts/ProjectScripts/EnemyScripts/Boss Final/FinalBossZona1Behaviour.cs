﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace BossBattle
{
    public class FinalBossZona1Behaviour : FinalBossCommon
    {
        bool hasFinished;
        bool hasFinishedAttack;

        [Header("Respawn Variables")]
        [SerializeField] List<Spawner> humanBossSpawners = new List<Spawner>();
        [SerializeField] List<Spawner> bomberBossSpawners = new List<Spawner>();
        [SerializeField] GameObject pistolPrefab;
        [SerializeField] GameObject bomberPrefab;
        [SerializeField] GameObject flamePrefab;

        [Header("FX Variables")]
        [SerializeField] List<FinalBoss1Perforadoras> drills;
        [SerializeField] Animator bossAnimator;
    
        public bool isExausted = false;
        CinemachineImpulseSource cinemachineImpulse; //camera shake

        BossHeatBarScript bossHeatBar;

        [Header("Inicialización de Boss")]
        private bool bossInitialized = false;

        [Header("Comprobación de logro de pasarselo sin perder vida")]
        private int battleStartPlayerHealth;

        [Header("Comprobación de logro de pasarselo en X tiempo (2m 40s / 160s)")]
        private float timeAchievement = 160;
        private float timeAchievementTimer;

        //AudioSources
        AudioSource bossLoopAudioSource;
        AudioSource bossLaserAudioSource;
        AudioSource bossExhaustAudioSource;
        AudioSource bossDrillsMovAudioSource;
        AudioSource bossAimAudioSource;


        private void Start()
        {
            cinemachineImpulse = GetComponent<CinemachineImpulseSource>();
            bossHeatBar = GetComponent<BossHeatBarScript>();
        }
        private void Update()
        {
            //Debug de coroutinas, se puede borrar
            
            /*
            if (Input.GetKeyDown(KeyCode.U))
            {
                StartCoroutine(ThrustAttack());
            }*/

            if(bossInitialized)
            {
                timeAchievementTimer += 1 * Time.deltaTime;
            }
        }
        public override void InitialState()
        {
            battleStartPlayerHealth = HealthHeartsVisual.healthHeartsSystemStatic.GetTotalFragments(); //Coger vida del player al empezar la batalla y comprobar si el player ha perdido vida al morir el boss para desbloquear logro
            timeAchievementTimer = 0;
            StartCoroutine(InitialCoroutine());
        }

        [Header("Variables de inicio del boss")]
        [SerializeField] List<GameObject> lightsInRoom = new List<GameObject>();
        [SerializeField] float timeBetweenStartLights;
        IEnumerator InitialCoroutine()
        {
            MusicManager.Instance.PlaySound(AppSounds.BOSS1_START, 10f , 40f, this.transform);
            MusicManager.Instance.PlayBackgroundMusic(AppSounds.BOSS1_MUSIC);
            bossLoopAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_LOOP, 10f, 40f, this.transform);

            bossInitialized = true;

            foreach(GameObject actualLight in lightsInRoom)
            {
                MusicManager.Instance.PlaySound(AppSounds.BOSS1_LIGHTS, 0.2f);
                actualLight.SetActive(true);
                for (int i =0; i< actualLight.transform.childCount; i++)
                {
                    actualLight.transform.GetChild(i).gameObject.SetActive(!actualLight.transform.GetChild(i).gameObject.activeSelf);
                }
                yield return new WaitForSeconds(timeBetweenStartLights);
            }

            StartCoroutine(FinalBossBehaviourCoroutine());
        }
        protected override IEnumerator FinalBossBehaviourCoroutine()
        {
            yield return new WaitForSeconds(2);

            StartCoroutine(DecideAttack());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            yield return new WaitForSeconds(0); //Esto es para dejar tiempo antes de empezar a spawnear, pero los ataques ya se han terminado

            SpawnEnemies(new InstrantiatedEnemy[] { new InstrantiatedEnemy(flamePrefab, 1)});

            yield return new WaitForSeconds(4);

            StartCoroutine(LaserAttack());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            StartCoroutine(Exhaust());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            StartCoroutine(DecideAttack());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            yield return new WaitForSeconds(4);

            SpawnEnemies(new InstrantiatedEnemy[] { new InstrantiatedEnemy(pistolPrefab, 2)});

            yield return new WaitForSeconds(4);

            SpawnEnemies(new InstrantiatedEnemy[] {new InstrantiatedEnemy(bomberPrefab, 2)});

            yield return new WaitForSeconds(10);

            SpawnEnemies(new InstrantiatedEnemy[] { new InstrantiatedEnemy(flamePrefab, 1)});

            yield return new WaitForSeconds(4);

            StartCoroutine(LaserAttack());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            StartCoroutine(Exhaust());
            yield return new WaitUntil(() => hasFinished);
            hasFinished = false;

            StartCoroutine(FinalBossBehaviourCoroutine());
        }

        [Header("Atributos para decidir ataques")]
        [SerializeField] int numberOfAttacks = 2;
        IEnumerator DecideAttack()
        {
            int attacksDone = 0;
            while (attacksDone < numberOfAttacks)
            {
                attacksDone++;
                int randomAttack = Random.Range(0, 2);
                if (randomAttack == 0)
                {
                    StartCoroutine(ThrustAttack());
                    yield return new WaitUntil(() => hasFinishedAttack);
                    hasFinishedAttack = false;
                }
                else if (randomAttack == 1)
                {
                    StartCoroutine(SweepAttack());
                    yield return new WaitUntil(() => hasFinishedAttack);
                    hasFinishedAttack = false;
                }
                yield return new WaitForSeconds(0.5f); //Tiempo entre ataques
            }
            hasFinished = true;
        }

        [Header("Atributos Ataque de Thrust")]
        [SerializeField] float thrustAcceleration;
        [SerializeField] float recoverVelocity;
        [SerializeField] float anticipationTimeAimingToPlayer;
        [SerializeField] float rotationVelocity;
        [SerializeField] float timeInImpactPlace;
        [SerializeField] LayerMask distanceDetectionLayerMask;

        IEnumerator ThrustAttack()
        {
            FinalBoss1Perforadoras randomFinalBossPerf = null;
            float timeInThrust;
            int counter = 0;

            while(counter<drills.Count)
            {
                counter++;
                randomFinalBossPerf = SelectDrill(randomFinalBossPerf);
                //Preparation of the attack
                bossAnimator.enabled = true;
                if (randomFinalBossPerf.isLeft)
                {
                    bossAnimator.SetBool("LeftThustAnticipation", true);
                }
                else
                {
                    bossAnimator.SetBool("RightThustAnticipation", true);
                }

                bossDrillsMovAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_DRILL_PREP, 5f, 50f, this.transform);

                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(bossAnimator.GetCurrentAnimatorStateInfo(0).length);
                bossAnimator.enabled = false;

                bossDrillsMovAudioSource.Stop();

                //Vector to player
                Vector3 vectorToPlayer = VectorToPlayerNoY(randomFinalBossPerf.transform.position);
              
                //RotationToPlayer
                randomFinalBossPerf.finalBossDrillRb.velocity = Vector3.zero;
                bossAimAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_AIM, 10f, 40f, this.transform);

                while (true)
                {
                    randomFinalBossPerf.transform.localRotation = Quaternion.RotateTowards(randomFinalBossPerf.transform.localRotation, Quaternion.LookRotation(-vectorToPlayer, Vector3.up), rotationVelocity * Time.deltaTime); //realiza la rotación hacia el jugador
                    if (randomFinalBossPerf.transform.localRotation == Quaternion.LookRotation(-vectorToPlayer, Vector3.up))
                    {
                        randomFinalBossPerf.transform.localRotation = Quaternion.LookRotation(-vectorToPlayer, Vector3.up);
                        break;
                    }
                    yield return new WaitForEndOfFrame();
                }

                yield return new WaitForSeconds(anticipationTimeAimingToPlayer);

                bossAimAudioSource.Stop();
                //Attack
                MusicManager.Instance.PlaySound(AppSounds.BOSS1_THROW_DRILL);
                RaycastHit hit;
                Physics.Raycast(randomFinalBossPerf.raycastPoint.position, vectorToPlayer.normalized, out hit, 100f, distanceDetectionLayerMask, QueryTriggerInteraction.Ignore);
                randomFinalBossPerf.ActivateThrustParticles();

                timeInThrust = Mathf.Sqrt(hit.distance * 2 / thrustAcceleration);
                float attackTimer = 0;
                while (attackTimer < timeInThrust)
                {
                    attackTimer += Time.deltaTime;
                    randomFinalBossPerf.finalBossDrillRb.velocity = vectorToPlayer.normalized * (randomFinalBossPerf.finalBossDrillRb.velocity.magnitude + thrustAcceleration * Time.deltaTime); 
                    yield return new WaitForEndOfFrame();
                }

                //Time in place before recover
                MusicManager.Instance.PlaySound(AppSounds.BOSS1_IMPACT_DRILL);
                randomFinalBossPerf.StopThrustParticles();
                cinemachineImpulse.GenerateImpulse();
                randomFinalBossPerf.ActivateImpactParticles();
                randomFinalBossPerf.finalBossDrillRb.velocity = Vector3.zero;
                yield return new WaitForSeconds(timeInImpactPlace);

                //Recover
                randomFinalBossPerf.StopImpactParticles();
                timeInThrust = hit.distance / recoverVelocity;
                randomFinalBossPerf.finalBossDrillRb.velocity = -vectorToPlayer.normalized * recoverVelocity;
                yield return new WaitForSeconds(timeInThrust);

                //Rotation Recovery
                randomFinalBossPerf.finalBossDrillRb.velocity = Vector3.zero;
                bossAimAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_AIM, 10f, 40f, this.transform);

                while (true)
                {
                    randomFinalBossPerf.transform.localRotation = Quaternion.RotateTowards(randomFinalBossPerf.transform.localRotation, Quaternion.identity, rotationVelocity * Time.deltaTime); //realiza la rotación hacia el jugador
                    if (randomFinalBossPerf.transform.localRotation == Quaternion.identity)
                    {
                        randomFinalBossPerf.transform.localRotation = Quaternion.identity;
                        break;
                    }
                    yield return new WaitForEndOfFrame();
                }

                bossAimAudioSource.Stop();

                //Final recovery
                bossDrillsMovAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_DRILL_PREP, 5f, 50f, this.transform);
                bossAnimator.enabled = true;
                if (randomFinalBossPerf.isLeft)
                {
                    bossAnimator.SetBool("LeftThustAnticipation", false);
                }
                else
                {
                    bossAnimator.SetBool("RightThustAnticipation", false);
                }

                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds(bossAnimator.GetCurrentAnimatorStateInfo(0).length);
                bossAnimator.enabled = false;
                bossDrillsMovAudioSource.Stop();
            }
            hasFinishedAttack = true;
        }
        //Funciones para ThrustAttack
        FinalBoss1Perforadoras SelectDrill(FinalBoss1Perforadoras lastPerf)
        {
            if (lastPerf == null)
            {
                int randomPos = Random.Range(0, drills.Count);

                return drills[randomPos];
            }
            else
            {
                foreach(FinalBoss1Perforadoras actualDrill in drills)
                {
                    if (actualDrill!= lastPerf)
                    {
                        return actualDrill;
                    }
                }

            }
            return lastPerf;
        }
        Vector3 VectorToPlayerNoY(Vector3 actualPosition)
        {
            Vector3 vectorToPlayerWithY = (GameManager.Instance.ActualPlayerController.transform.position - actualPosition).normalized;
            return new Vector3(vectorToPlayerWithY.x, 0, vectorToPlayerWithY.z);
        }

        IEnumerator SweepAttack()
        {
            bossAnimator.enabled = true; //Activa el animator del drill para que funcione
            yield return new WaitForEndOfFrame();

            bossAnimator.SetBool("IsAttacking", true); //Colocar drills en posición adecuada para el ataque
            bossDrillsMovAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_DRILL_PREP, 5f, 50f, this.transform);

            yield return new WaitForEndOfFrame();
            yield return new WaitUntil(() => bossAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"));

            //EndAttack
            bossDrillsMovAudioSource.Stop();
            hasFinishedAttack = true;
            bossAnimator.SetBool("IsAttacking", false);
            bossAnimator.enabled = false; ////Desactiva el animator del drill para evitar que interfiera en el movimiento
        }

        [Header("Atributos ataque laser")]
        [SerializeField] GameObject laserParticles;
        [SerializeField] GameObject anticipationParticles;
        [SerializeField] GameObject anticipationLaser;

        [SerializeField] float laserChargingTime = 0.5f;
        [SerializeField] float laserAnticipationTime = 1;
        [SerializeField] float timeShootingLaser = 5;
        [SerializeField] int laserDamageToPlayer = 1;

        [SerializeField] LayerMask laserLayerMask;
        IEnumerator LaserAttack()
        {
            bossAnimator.enabled = true; //Activa el animator
            bossAnimator.SetBool("IsShovelDown", true);
            MusicManager.Instance.PlaySound(AppSounds.BOSS1_SHOVEL);

            //Anticipación
            anticipationParticles.SetActive(true);

            yield return new WaitForSeconds(laserChargingTime); //tiempo de anticipación hasta que aparece el laser falso

            Vector3 playerInitialPos = GameManager.Instance.ActualPlayerController.transform.position;
            Vector3 laserInitialPos = new Vector3(anticipationLaser.transform.position.x, playerInitialPos.y, anticipationLaser.transform.position.z);
            Vector3 vectorToPlayer = new Vector3(playerInitialPos.x - laserInitialPos.x, playerInitialPos.y, playerInitialPos.z - laserInitialPos.z);

            anticipationLaser.transform.rotation = Quaternion.LookRotation(vectorToPlayer, Vector3.up); //realiza la rotación hacia el jugador
            laserParticles.transform.rotation = Quaternion.LookRotation(vectorToPlayer, Vector3.up); //realiza la rotación hacia el jugador

            yield return new WaitForEndOfFrame(); //Evita que se vea durante 1 frame el laser mal colocado
            anticipationLaser.SetActive(true);

            yield return new WaitForSeconds(laserAnticipationTime); //Tiempo de anticipación

            //Ataque
            anticipationLaser.SetActive(false);
            laserParticles.SetActive(true);
            cinemachineImpulse.GenerateImpulse();
            bossLaserAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_LASER, 5, 30, this.transform);

            float laserTimer = 0;
            while (laserTimer < timeShootingLaser)
            {
                laserTimer += Time.deltaTime;

                RaycastHit capsuleHit;
                Physics.SphereCast(laserInitialPos, 1f, vectorToPlayer, out capsuleHit, 1000f, laserLayerMask);

                PlayerHealthController playerHealth = capsuleHit.transform.gameObject.GetComponent<PlayerHealthController>();
                EnemyHealth enemyHealth = capsuleHit.transform.gameObject.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.ReceiveDamage(laserDamageToPlayer);
                }
                else if (playerHealth != null)
                {
                    playerHealth.DamagePlayer(laserDamageToPlayer);
                }
                bossHeatBar.FillOnLaser();

                yield return new WaitForEndOfFrame();
            }

            //Recuperación
            laserParticles.SetActive(false);
            anticipationParticles.SetActive(false);
            bossLaserAudioSource.Stop();

            hasFinished = true;
        }

        [Header("Atributos de exhausted")]
        [SerializeField] float exhaustedTime;
        [SerializeField] List<ParticleSystem> exhaustedParticles = new List<ParticleSystem>();
        IEnumerator Exhaust()
        {
            isExausted = true;
            bossExhaustAudioSource = MusicManager.Instance.PlayLoopControlledSound(AppSounds.BOSS1_EXHAUST,5,30,this.transform);

            //Iniciar partículas y animación
            foreach (ParticleSystem ps in exhaustedParticles)
            {
                var psMain = ps.main;
                psMain.duration = exhaustedTime;
                ps.Play();
            }

            yield return new WaitForSeconds(exhaustedTime);
            bossExhaustAudioSource.Stop();

            bossAnimator.SetBool("IsShovelDown", false);
            MusicManager.Instance.PlaySound(AppSounds.BOSS1_SHOVEL);

            yield return new WaitForEndOfFrame();
            yield return new WaitWhile(() => bossAnimator.GetCurrentAnimatorStateInfo(0).IsName("ShovelUp"));

            bossAnimator.enabled = false; //Desactiva el animator
            isExausted = false;
            hasFinished = true;
        }

        void SpawnEnemies(InstrantiatedEnemy[] enemiesToSpawn)
        {
            for (int i = 0; i < enemiesToSpawn.Length; i++)
            {
                for (int j = 0; j < enemiesToSpawn[i].quantity; j++)
                {
                    int actualInt;
                    if (enemiesToSpawn[i].quantity == 1)
                    {
                        actualInt = Random.Range(0, 2);
                    }
                    else
                    {
                        actualInt = j % 2;
                    }

                    if (enemiesToSpawn[i].enemyType == bomberPrefab)
                    {
                        bomberBossSpawners[actualInt].SpawnEnemy(enemiesToSpawn[i].enemyType);
                    }
                    else
                    {
                        humanBossSpawners[actualInt].SpawnEnemy(enemiesToSpawn[i].enemyType);

                    }

                }
            }
        }

        [Header("Death State variable")]
        [SerializeField] List<GameObject> objetsToChangeSetActive = new List<GameObject>();
        [SerializeField] List<Animator> drillsAnimators = new List<Animator>();
        [SerializeField] GameObject lifeBarCanvas;
        bool isDead;
        public override void KillBoss()
        {
            if (!isDead)
            {
                isDead = true;
                StopAllCoroutines(); //Parar comportamiento

                //Activar y desactivar objetos
                foreach (GameObject objectToChange in objetsToChangeSetActive)
                {
                    objectToChange.SetActive(!objectToChange.activeSelf);
                }
                foreach(Animator anim in drillsAnimators)
                {
                    anim.enabled = false;
                }

                bossHeatBar.enabled = false;
                lifeBarCanvas.SetActive(false);

                bossAnimator.enabled = true;
                bossAnimator.SetTrigger("hasDead");

                //Sonidos
                MusicManager.Instance.PlaySound(AppSounds.BOSS1_DEATH, 10f, 40f, this.transform);
                if (bossLoopAudioSource != null)
                {
                    bossLoopAudioSource.Stop();
                }
                if (bossExhaustAudioSource != null)
                {
                    bossExhaustAudioSource.Stop();
                }
                if (bossDrillsMovAudioSource != null)
                {
                    bossDrillsMovAudioSource.Stop();
                }

                MusicManager.Instance.StopBackgroundMusic();


                ///// LOGRO ///////
                SteamManager.Instance.UnlockAchievement("COMPLETE_ZONE_1");
                ///// LOGRO ///////
                
                if (HealthHeartsVisual.healthHeartsSystemStatic.GetTotalFragments() >= battleStartPlayerHealth)
                {
                    SteamManager.Instance.UnlockAchievement("NO_DAMAGE_FIRST_BOSS");
                }
                /////

                ///// LOGRO ///////
                if (timeAchievementTimer <= timeAchievement)
                {
                    SteamManager.Instance.UnlockAchievement("BOSS_RUSH_1");
                }
                /////

                //Cargar nueva zona con cinemática
                GetComponent<LoadSceneDuringCutscene>().LoadScenes();
                GameManager.Instance.EraseSavedMapData(); //Borrar mapa para empezar de nuevo en zona 2
            }          
        }

        //Utility
        bool IsThereAliveEnemies()
        {
            bool isPlayerControllingAnEnemy = GameManager.Instance.ActualPlayerController.GetComponent<EnemyControl_MovementController>() != null;
            int enemiesInRoom = ZoneManager.Instance.m_activeRoom.currentEnemiesInRoom.Count;

            if (enemiesInRoom == 0 || (isPlayerControllingAnEnemy && enemiesInRoom == 1))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        float DistanceToPlayer()
        {
            return (this.transform.position - GameManager.Instance.ActualPlayerController.transform.position).magnitude;
        }
        class InstrantiatedEnemy
        {
            public GameObject enemyType;
            public float quantity;

            public InstrantiatedEnemy(GameObject newEnemy, float newCount)
            {
                enemyType = newEnemy;
                quantity = newCount;
            }
        }
    }
}