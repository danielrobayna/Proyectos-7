﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BossBattle
{
    public class FinalBoss1Perforadoras : DamageObject
    {
        [HideInInspector] public Rigidbody finalBossDrillRb;
        [Header("Final Boss Effects")]
        [SerializeField] GameObject thrustParticles;
        public Transform raycastPoint;
        [SerializeField] GameObject impactParticles;
        public bool isLeft;

        private void Start()
        {
            finalBossDrillRb = GetComponent<Rigidbody>();
        }
        public void ActivateThrustParticles()
        {
            thrustParticles.SetActive(true);
        }
        public void StopThrustParticles()
        {
            thrustParticles.SetActive(false);
        }
        public void ActivateImpactParticles()
        {
            impactParticles.SetActive(true);
        }
        public void StopImpactParticles()
        {
            impactParticles.SetActive(false);
        }
    }
}
