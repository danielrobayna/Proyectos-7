﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BossBattle
{
    public class FinalBossCommon : MonoBehaviour
    {
        public virtual void InitialState()
        {
            StartCoroutine(FinalBossBehaviourCoroutine());
        }
        protected virtual IEnumerator FinalBossBehaviourCoroutine() { yield return null; }
        public virtual void KillBoss()
        {
            StopAllCoroutines();
        }
    }
}