﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BossBattle
{
    public class BossFinalReflectBullet : MonoBehaviour
    {
        public void BulletImpact (BulletBase bullet)
        {
            ReflectBullet(bullet);
        }
      
        private void ReflectBullet (BulletBase bullet)
        {
            if (bullet != null) //Rebote de la bala
            {
                Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();
                Vector3 newBulletDirection = Vector3.Reflect(bullet.transform.right, transform.forward) * bulletRb.velocity.magnitude; //Crear vector reflejado de la colisión de la bala con el escudo, multiplicado por la velocidad.
                bullet.ResetDestroyVariables();

                bulletRb.velocity = newBulletDirection;     //Aplicar nuevo vector reflejado a la velocidad de la bala
                bullet.transform.rotation = Quaternion.LookRotation(newBulletDirection) * Quaternion.Euler(0, -90, 0);    //Rotar la bala hacia la nueva dirección con una corrección para que sea el transform.right el que mira en la nueva dirección.  
                bullet.gameObject.layer = 0;

                MusicManager.Instance.PlaySound(AppSounds.BOSS1_HIT);
            }
        }
    }
}