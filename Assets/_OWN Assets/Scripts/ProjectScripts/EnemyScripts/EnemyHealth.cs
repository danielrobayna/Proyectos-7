﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class EnemyHealth : MonoBehaviour
{
    [Header("La Vida del Enemigo")]
    public float enemyHealth;
    [HideInInspector]public float defaultEnemyHealth;

    [Header("El SetControl del Enemigo")]
    private EnemySetControl thisEnemySetControl;

    [Header("La IA del Enemigo")]
    private EnemyAI_Standard thisEnemy_AI;

    protected SetAnimatorVariables m_mySetAnimationVariables;

    //variable para el camera shake
    CinemachineImpulseSource impulse;
    //variable para los sonidos
    public EnemySounds this_enemySounds;

    [Header("Variables estado de fuego")]
    [SerializeField] ParticleSystem fireParticle;
    bool m_isOnFire = false;
    float m_fireTimer = 0;
    float m_damagePerSecond;
    float m_timeInFire;
    float m_timerRepeatDamageInFire;
    float m_timeToDamage;

    [Header("Variables de invencibilidad")]
    [SerializeField] float invulnerabilityTime;
    [SerializeField] float flashTime;
    [SerializeField] SkinnedMeshRenderer rendererToFlash;
    bool m_isInvulnerable = false;
    float m_invulnerableTimer;
    float m_flashTimer;
   
    void Awake()
    {
        defaultEnemyHealth = enemyHealth;   ////Se guarda la vida inicial del enemigo.
    }

    // Start is called before the first frame update
    void Start()
    {
        thisEnemySetControl = GetComponent<EnemySetControl>();
        thisEnemy_AI = GetComponent<EnemyAI_Standard>();
        m_mySetAnimationVariables = GetComponent<SetAnimatorVariables>();
        impulse = GetComponent<CinemachineImpulseSource>();
        this_enemySounds = GetComponent<EnemySounds>();
    }

    private void OnEnable()
    {
        enemyHealth = defaultEnemyHealth; ////Al desactivarse el enemigo, recupera la vida perdida
    }
    private void Update()
    {
        //Importante el orden, para no llamar al hud después de morir por fuego
        if (thisEnemySetControl.IsEnemyPossessed())
        {
            UIManager.Instance.SetEnemyHealthUI(enemyHealth, defaultEnemyHealth); // Actualización de la barra de vida. Desactivación en SetControl.
        }
        States();
    }
    void States()
    {
        //Estado de fuego
        if (m_isOnFire)
        {
            FireState();
        }

        //Estado de invulnerabilidad
        if (m_isInvulnerable)
        {
            InvulnerableState();
        }

        //Estado de 
    }

    //Funciones de estado de fuego
    public void StartFireState(float damagePerSecond, float time, float timeToDamage)
    {
        m_isOnFire = true;
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
        fireParticle.Play();

        m_damagePerSecond = damagePerSecond;
        m_timeInFire = time;
        m_timeToDamage = timeToDamage;

        //IA
        if (thisEnemy_AI.enabled)
        {
            thisEnemy_AI.StartBurningPanic();                                 //Para aplicar estado de pánico por quemarse a la IA si esta activa la IA
        }
    }
    public void StartFireState(float damagePerSecond, float time)
    {
        m_isOnFire = true;
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
        fireParticle.Play();

        m_damagePerSecond = damagePerSecond;
        m_timeInFire = time;
        m_timeToDamage = 1;

        //IA
        if (thisEnemy_AI.enabled)
        {
            thisEnemy_AI.StartBurningPanic();                                 //Para aplicar estado de pánico por quemarse a la IA si esta activa la IA
        }
    }
    public void StartFireState(float damagePerSecond, float time, bool CanPanic)
    {
        m_isOnFire = true;
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;
        fireParticle.Play();

        m_damagePerSecond = damagePerSecond;
        m_timeInFire = time;
        m_timeToDamage = 1;
     
    }
    void StopFireState()
    {
        m_isOnFire = false;
        fireParticle.Stop();
        m_fireTimer = 0;
        m_timerRepeatDamageInFire = 0;

        //IA
        thisEnemy_AI.StopBurningPanic(); //Desactivar el estado de pánico de la IA al dejar de quemarse
    }
    void FireState()
    {
        m_fireTimer += Time.deltaTime;
        m_timerRepeatDamageInFire += Time.deltaTime;

        if (m_timerRepeatDamageInFire > m_timeToDamage)
        {
            m_timerRepeatDamageInFire = 0;
            ReceiveDamageWithFire(m_damagePerSecond);
        }
        if (m_fireTimer > m_timeInFire)
        {
            StopFireState();
        }
    }
    public bool GetIsOnFire()
    {
        return m_isOnFire;
    }

    //Funciones de estado de invulnerabilidad
    public void StartInvulnerabilityState()
    {
        m_isInvulnerable = true;
        m_flashTimer = 0;
        m_invulnerableTimer = 0;
    }
    public void StopInvulnerability()
    {
        m_flashTimer = 0;
        m_invulnerableTimer = 0;

        m_isInvulnerable = false;
        rendererToFlash.enabled = true;
    }
    void InvulnerableState()
    {
        m_invulnerableTimer += Time.deltaTime;
        m_flashTimer += Time.deltaTime;
        if (rendererToFlash != null && m_flashTimer > flashTime)
        {
            rendererToFlash.enabled = !rendererToFlash.enabled;
            m_flashTimer = 0;
        }
        if (m_invulnerableTimer > invulnerabilityTime)
        {
            StopInvulnerability();
        }
    }

    //Funciones de daño
    public void ReceiveDamage(float damageReceived) ////Recibir daño
    {
        m_mySetAnimationVariables.SetHitTrigger(); //Animación de daño

        if (thisEnemySetControl.IsEnemyPossessed())
        {
            if (!m_isInvulnerable)
            {
                enemyHealth -= damageReceived;

                /////CAMERA SHAKE///////
                impulse.GenerateImpulse();

                if (enemyHealth <= 0)
                {
                    thisEnemySetControl.CheckEnemyDeath(); ////Al tener vida 0 se manda al SetControl chequear la muerte
                }
                else
                {
                    StartInvulnerabilityState(); //Se vuelve invulnerable
                }
            }
        }
        else //Recibir daño cuando no está poseído
        {
            ReceiveDamageWhenUnpossesed(damageReceived);
        }
    }
    public void ReceiveDamageWithFire(float damageReceived) //No genera el estado de invulnerabilidad
    {
        m_mySetAnimationVariables.SetHitTrigger(); //Animación de daño

        if (thisEnemySetControl.IsEnemyPossessed())
        {
            enemyHealth -= damageReceived;

            /////CAMERA SHAKE///////
            impulse.GenerateImpulse();

            if (enemyHealth <= 0)
            {
                thisEnemySetControl.CheckEnemyDeath(); //Al tener vida 0 se manda al SetControl chequear la muerte
            }
        }
        else //Recibir daño cuando no está poseído
        {
            ReceiveDamageWhenUnpossesed(damageReceived);
        }
    }
    void ReceiveDamageWhenUnpossesed(float damage)
    {
        enemyHealth -= damage;
        if (enemyHealth <= 0)
        {
            thisEnemySetControl.CheckEnemyDeath(); ////Al tener vida 0 se manda al SetControl chequear la muerte
        }
    }
    public bool SetEnemyConsumingHealth(int damageReceived) //Gestionado de la muerte por set control ya que es donde se realiza la acción
    {
        enemyHealth -= damageReceived;
        if (enemyHealth <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
