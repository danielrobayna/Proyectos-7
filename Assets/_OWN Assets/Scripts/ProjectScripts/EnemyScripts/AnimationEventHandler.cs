﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventHandler : MonoBehaviour  ////ESTE SCRIPT CONTROLA EVENTOS DE ANIMACION EN ENEMIGOS, ESTA METIDO EN EL OBJETO DEL ANIMATOR/////
{
    [Header("Variables para eventos en los pies")]
    [SerializeField] Transform rightFoot;
    [SerializeField] Transform leftFoot;
    private float speedX;
    private float speedZ;
    private Animator enemyAnimator;

    [Header("Variables para eventos del Boss 2")]
    private FinalBossZona2Behaviour boss2Behaviour;

    [Header("Indentificar si es un Meathead")]
    private MeatheadSounds enemyIsMeathead;

    private void Start()
    {
        enemyAnimator = GetComponent<Animator>();
        boss2Behaviour = GetComponentInParent<FinalBossZona2Behaviour>();
        enemyIsMeathead = transform.parent.parent.GetComponent<MeatheadSounds>();
    }

    /////// EVENTOS DE CAMINAR //////

    public void RightFrontFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) > Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX"))) //Se hace esta comparación para no ejecutar el evento más de una vez por el blend tree.
        {
            Instantiate(footstepParticle, rightFoot.position, transform.rotation);
            if(enemyIsMeathead)
            {
                PlayHeavyMovementSound();
            }
            else
            {
                PlayMovementSound();
            }
        }
    }

    public void LeftFrontFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) > Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX")))
        {
            Instantiate(footstepParticle, leftFoot.position, transform.rotation);
            if (enemyIsMeathead)
            {
                PlayHeavyMovementSound();
            }
            else
            {
                PlayMovementSound();
            }
        }
    }

    public void RightSideFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) < Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX")))
        {
            Instantiate(footstepParticle, rightFoot.position, transform.rotation);
            if (enemyIsMeathead)
            {
                PlayHeavyMovementSound();
            }
            else
            {
                PlayMovementSound();
            }
        }
    }

    public void LeftSideFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) < Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX")))
        {
            Instantiate(footstepParticle, leftFoot.position, transform.rotation);
            if (enemyIsMeathead)
            {
                PlayHeavyMovementSound();
            }
            else
            {
                PlayMovementSound();
            }
        }
    }
    void PlayMovementSound()
    {
        int randomNumber = Random.Range(0, 5);

        //Play a random sound
        switch (randomNumber)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_STEP1, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_STEP2, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_STEP3, this.transform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_STEP4, this.transform);
                break;
            case 4:
                MusicManager.Instance.PlaySound(AppSounds.HUMAN_STEP5, this.transform);
                break;
            default:
                break;
        }
    }

    //////////////////// EVENTOS DE ANIMACION DEL BOSS 2 ///////////////////////////
    ///

    public void Boss2InstantiateWaveAttack()
    {
        boss2Behaviour.InstantiateWaveAttack();
    }

    public void Boss2ThrowGrenade()
    {
        boss2Behaviour.InstantiateGrenade();
    }

    public void Boss2ShootSmallCristals()
    {
        boss2Behaviour.InstantiateSmallBullets();
    }

    public void Boss2ShootBigCrystal()
    {
        boss2Behaviour.InstantiateBigBullet();
    }

    public void Boss2InstantiateAreaAttack()
    {
        boss2Behaviour.InstantiateAreaAttack();
    }

    public void RightHeavyFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) > Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX"))) //Se hace esta comparación para no ejecutar el evento más de una vez por el blend tree.
        {
            Instantiate(footstepParticle, rightFoot.position, transform.rotation);
            PlayHeavyMovementSound();
        }
    }

    public void LeftHeavyFootstepEvent(Object footstepParticle)
    {
        if (Mathf.Abs(enemyAnimator.GetFloat("MovSpeedZ")) > Mathf.Abs(enemyAnimator.GetFloat("MovSpeedX")))
        {
            Instantiate(footstepParticle, leftFoot.position, transform.rotation);
            PlayHeavyMovementSound();
        }
    }

    public void PlayBossDeathFallSound1()
    {
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_DEATH_FALL_1);
    }

    public void PlayBossDeathFallSound2()
    {
        MusicManager.Instance.PlaySound(AppSounds.BOSS2_DEATH_FALL_2);
    }

    void PlayHeavyMovementSound()
    {
        int randomNumber = Random.Range(0, 4);

        //Play a random sound
        switch (randomNumber)
        {
            case 0:
                MusicManager.Instance.PlaySound(AppSounds.HEAVY_STEP_1, 0.2f, 4f, 25f, this.transform);
                break;
            case 1:
                MusicManager.Instance.PlaySound(AppSounds.HEAVY_STEP_2, 0.2f, 4f, 25f, this.transform);
                break;
            case 2:
                MusicManager.Instance.PlaySound(AppSounds.HEAVY_STEP_3, 0.2f, 4f, 25f, this.transform);
                break;
            case 3:
                MusicManager.Instance.PlaySound(AppSounds.HEAVY_STEP_4, 0.2f, 4f, 25f, this.transform);
                break;
            default:
                break;
        }
    }
}
