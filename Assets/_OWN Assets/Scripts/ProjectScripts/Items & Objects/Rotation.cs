﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField] private float rotationSpeedX, rotationSpeedY, rotationSpeedZ;

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(rotationSpeedX, rotationSpeedY, rotationSpeedZ);
    }
}
