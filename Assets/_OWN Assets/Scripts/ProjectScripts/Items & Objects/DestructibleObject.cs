﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour
{
    public ParticleSystem breakParticles;
    CapsuleCollider m_collider;
    [SerializeField]
    bool isExplosive;
    [SerializeField]
    GameObject m_explosion;
    [SerializeField]
    GameObject m_debris;
    // Start is called before the first frame update
    void Start()
    {
        m_collider = GetComponent<CapsuleCollider>();
        if(breakParticles != null)
        {
            StartCoroutine(PausedParticles());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BulletBase>() != null)
        {
            Destroy();
        }
    }
    private IEnumerator PausedParticles()
    {
        yield return new WaitForSeconds(0.75f);
        breakParticles.Pause();
    }
    public void Destroy()
    {
        if (breakParticles != null)
        {
            breakParticles.gameObject.SetActive(true);
        }
        ZoneManager.Instance.m_activeRoom.RemoveBarrelAtRoom(this);
        StartCoroutine(PausedParticles());
        gameObject.layer = 13;
        if(isExplosive)
        {
          GameObject exp =  Instantiate(m_explosion, transform.position, transform.rotation);
            Instantiate(m_debris, transform.position, transform.rotation);
            Destroy(gameObject);
            if (breakParticles != null)
            {
                exp.GetComponentInChildren<ParticleSystem>().gameObject.SetActive(false);
            }
        }
    }
}
