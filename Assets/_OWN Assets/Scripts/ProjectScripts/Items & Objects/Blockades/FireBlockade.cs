﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBlockade : BlockadeBase
{
    [SerializeField] private float burningTime;
    [SerializeField] private GameObject lightsObject;
    [SerializeField] private Collider blockadeColl;

    [Header("Todos los ParticleSystem en el prefab")]
    private ParticleSystem[] blockadeParticles;

    [Header("Todos los MeshRenderer en el prefab")]
    private MeshRenderer[] blockadeMeshes;

    [Header("Material con shader de dissolve")]
    [SerializeField] private Material dissolveMaterial;
    private float dissolveAmount;
    private bool dissolve = false;
    [Header("Tiempo en disolverse")]
    [SerializeField] private float speedToDissolve;

    private void Start()
    {
        blockadeColl = GetComponent<Collider>();
        blockadeParticles = GetComponentsInChildren<ParticleSystem>();
        blockadeMeshes = GetComponentsInChildren<MeshRenderer>();
        dissolveAmount = 0;
    }

    public void Update()
    {
        if (dissolve)
        {
            dissolveAmount += speedToDissolve * Time.deltaTime;
            for (int i = 0; i < blockadeMeshes.Length; i++)
            {
                blockadeMeshes[i].material.SetFloat("_DissolveAmount", dissolveAmount);               
            }
        }
    }

    //Bloqueo que se rompe con fuego.
    //Añadir funcionalidad extra si es necesario.
    public override void BreakBlockade()
    {
        hasBeenDestroyed = true;
        StartCoroutine(BurnBlockade());
        GameManager.Instance.SaveGameObjectStatusOnly();
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
    }

    IEnumerator BurnBlockade()
    {
        lightsObject.SetActive(true);
        MusicManager.Instance.PlaySound(AppSounds.BLOCKADE_FIRE_DESTROY, 5, 50, this.transform);

        for (int i = 0; i < blockadeParticles.Length; i++) //Activar partículas
        {
            if (!blockadeParticles[i].isPlaying)
            {
                blockadeParticles[i].Play();
            }
        }

        yield return new WaitForSeconds(burningTime);

        for(int i = 0; i < blockadeMeshes.Length; i++) //Aplicar material con shader dissolve al todas las mallas
        {
            blockadeMeshes[i].material = dissolveMaterial;
            blockadeMeshes[i].material.SetFloat("_DissolveAmount", 0);
        }

        dissolve = true;    //Deshacer bloqueo

        for (int i = 0; i < blockadeParticles.Length; i++) //Desactivar partículas
        {
            if (blockadeParticles[i].isPlaying)
            {
                blockadeParticles[i].Stop();
            }
        }

        yield return new WaitForSeconds(1);

        blockadeColl.enabled = false; //Desactivar el collider
        lightsObject.SetActive(false); //Desactivar luces

        yield return new WaitForSeconds(2);

        gameObject.SetActive(false);    //Desactivar objeto
    }
}
