﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockadeBase : MonoBehaviour
{
    [HideInInspector] public bool hasBeenDestroyed;
    [SerializeField] protected int blackadeMapId;
    //Método base para romper bloqueo, heredado por resto de scripts.
    public virtual void BreakBlockade()
    {
        gameObject.SetActive(false);    
        hasBeenDestroyed = true;
        GameManager.Instance.SaveGameObjectStatusOnly();  //Guardar partida al romper bloqueos
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
    }
    public virtual void BreakBlockadeOnLoad() //Para la carga del nivel, solo se desactiva, sin ejecutar animaciones ni efectos
    {
        gameObject.SetActive(false);    
        hasBeenDestroyed = true;
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
    }
}
