﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class ElectricBlockade : BlockadeBase
{
    [SerializeField] Animator OpenDoorAnimator;
    [SerializeField] NavMeshObstacle obstacle;
    [SerializeField] GameObject openedDoor;
    [SerializeField] GameObject closedDoor;

    [Header("Solo para la puerta de bloqueo que se cierra en Game2")]
    [SerializeField] BoxCollider closeTrigger;
    [SerializeField] BoxCollider disableTrigger;
    [SerializeField] List<GameObject> electricgrounds;
    [SerializeField] GameObject electricParticles;

    private bool isOpen = true;

    private void Start()
    {       
        if (closeTrigger != null)
        {
            OpenDoorAnimator.SetTrigger("openDoor");
        }
        if(disableTrigger != null)
        {
            disableTrigger.enabled = false;
        }
    }
    public override void BreakBlockade()
    {
        if (!hasBeenDestroyed)
        {
            hasBeenDestroyed = true;
            GameManager.Instance.SaveGameObjectStatusOnly();

            OpenDoorAnimator.SetTrigger("openDoor");
            obstacle.enabled = false;
            ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
            electricParticles.SetActive(false);
            if (disableTrigger != null)
            {
                disableTrigger.enabled = true;
            }

            MusicManager.Instance.PlaySound(AppSounds.BLOCKADE_ELECTRIC_DESTROY, 5, 50, this.transform);
        }
    }
    public override void BreakBlockadeOnLoad()
    {
        hasBeenDestroyed = true;
        openedDoor.SetActive(true);
        closedDoor.SetActive(false);
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
        electricParticles.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(closeTrigger != null) //Cerrar puerta solo so tiene volumen de trigger en la variable
        {
            if(other.gameObject.GetComponent<PossessAbility>() != null || (other.TryGetComponent(out EnemySetControl enemySetControl)  && enemySetControl.IsEnemyPossessed()))
            {
                if(!openedDoor.activeSelf && isOpen) //cerrar puerta solo si el objeto que se pone como puerta cerrada al cargar partida esta desactivado y si no se ha abierto la puerta aún
                {
                    isOpen = false;
                    OpenDoorAnimator.SetTrigger("closeDoor");
                    closeTrigger.enabled = false;
                    if (electricgrounds.Count > 0)
                    {
                        for (int i = 0; i < electricgrounds.Count; i++)
                        {
                            electricgrounds[i].GetComponent<GroundEffect>().ActivateGround();
                            electricgrounds[i].GetComponent<BoxCollider>().enabled = true;
                        }
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(disableTrigger != null && disableTrigger.enabled && other != GameManager.Instance.realPlayerGO.GetComponent<Collider>())
        { //Llamar a disable solo si hay trigger de disable, esta activo, y el que entra no es el trigger del player, para que no se pueda llamar en el collider del generador
            disableTrigger.enabled = false;
            for (int i = 0; i < electricgrounds.Count; i++)
            {
                electricgrounds[i].GetComponent<GroundEffect>().DeactivateGround();
                electricgrounds[i].GetComponent<BoxCollider>().enabled = false;
            }
        }
    }
}
