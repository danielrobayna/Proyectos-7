﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunBlockade : BlockadeBase
{
    [SerializeField] private Collider blockadeCollider;
    [SerializeField] private GameObject shieldBlockadeObject;

    public override void BreakBlockade()
    {
        blockadeCollider.enabled = false;
        shieldBlockadeObject.SetActive(false);
        hasBeenDestroyed = true;
        GameManager.Instance.SaveGameObjectStatusOnly();  //Guardar partida al romper bloqueos
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
        MusicManager.Instance.PlaySound(AppSounds.BLOCKADE_SHIELD_DESTROY, 5, 50, this.transform);
    }

    public override void BreakBlockadeOnLoad() //Para la carga del nivel, solo se desactiva, sin ejecutar animaciones ni efectos
    {
        blockadeCollider.enabled = false;
        shieldBlockadeObject.SetActive(false);
        hasBeenDestroyed = true;
        ZoneManager.Instance.mapManager.DeleteBlockadeInMap(blackadeMapId);
    }
}
