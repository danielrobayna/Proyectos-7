﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBlockade : BlockadeBase
{
    [SerializeField] private GameObject explotionGO;
    [SerializeField] private GameObject rubbleGO;

    [Header("Es este el bloqueo de la cinemática? Para no desbloquear el logro")]
    [SerializeField] private bool isThisCinematicDestroyedBlockade = false;

    //Bloqueo que se rompe con una explosión.
    //Añadir funcionalidad extra si es necesario.
    public override void BreakBlockade()
    {
        base.BreakBlockade();

        ///// LOGRO ///////
        if (!isThisCinematicDestroyedBlockade)
        {
            SteamManager.Instance.UnlockAchievement("BREAK_FIRST_BLOCKADE"); //Solo se desbloquea en este tipo de bloqueos porque estos son los primeros y no se puede proseguir sin romperlos
        }
        /////

        Instantiate(explotionGO, transform.position, transform.rotation);
        Instantiate(rubbleGO, transform.position + new Vector3(0, 0.5f, 0), transform.rotation);
    }
    public override void BreakBlockadeOnLoad() //Para la carga del nivel, solo se desactiva, sin ejecutar animaciones ni efectos
    {
        base.BreakBlockadeOnLoad();
        Instantiate(rubbleGO, transform.position + new Vector3(0, 0.5f, 0), transform.rotation);
    }
}
