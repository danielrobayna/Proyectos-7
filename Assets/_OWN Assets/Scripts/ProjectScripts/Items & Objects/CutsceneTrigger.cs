﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Timeline;
using Cinemachine;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour
{
    CinemachineVirtualCamera roomCam;
    [Header("Playable a reproducir")]
    public TimelineAsset playable;
    public bool isSkipeable;

     public bool hasBeenTriggered;

    [Header("El Trigger de la cinemática")]
    Collider triggerCollider;

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.layer == 8 && playable != null)
        {         
            PlayPlayable();
        }
    }

    CutsceneManager cutManager;
    void Start()
    {
        cutManager = FindObjectOfType<CutsceneManager>();
        triggerCollider = GetComponent<Collider>();
        triggerCollider.enabled = false;
        StartCoroutine(ActivateTrigger());
    }
    IEnumerator ActivateTrigger()
    {
        yield return new WaitForEndOfFrame();

        triggerCollider.enabled = true;
    }
    public void PlayPlayable()
    {
        hasBeenTriggered = true;
        roomCam = transform.parent.GetComponentInChildren<RoomManager>().roomCamera;       
        cutManager.PlayCutscene(playable, isSkipeable, roomCam, roomCam.transform);
        GameManager.Instance.SaveGameObjectStatusOnly();
        this.gameObject.SetActive(false);
    }
    public void DeactivateTriggerOnLoad() //Desde LoadGame
    {
        hasBeenTriggered = true;
        gameObject.SetActive(false);
    }
}
