﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.XInput;

public class TutorialSlowmo : MonoBehaviour
{
    [Header("Enemigo y bala que detiene el tiempo")]
    [SerializeField] private EnemyAI_Standard enemy;
    [SerializeField] private Transform enemyShootPos;
    [SerializeField] private GameObject bullet;

    [Header("El objeto del tutorial de disparo")]
    [SerializeField] private GameObject tutorialDisparoObject;

    [Header("El controlador de personaje del enemigo que posee el jugador")]
    private EnemyControl_MovementController tutorialEnemyToPossess;

    [Header("Objetos de texto y Renderer de prompts")]
    [SerializeField] private GameObject[] textObjects;
    [SerializeField] private SpriteRenderer promptRenderer;
    [Header("Prompts")]
    [SerializeField] Sprite xboxPrompt, playStationPrompt, keyboardPrompt;

    bool coroutineStarted = false;
    bool tutorialBulletShot = false;
    bool slowmoTutorialFinished = false;

    private void Update()
    {
        ShowButtonPrompts();

       if(enemy.currentAIState == EnemyAI_Standard.AIState.attacking && !coroutineStarted && !slowmoTutorialFinished)
       {
            StartCoroutine(SlowmoCoroutine());
       }

       else if(coroutineStarted && tutorialBulletShot && !slowmoTutorialFinished)
        {
            if (InputManager.Instance.DashButtonTriggered())
            {
                Time.timeScale = 1;

                for (int i = 0; i < textObjects.Length; i++)
                {
                    textObjects[i].SetActive(false);
                }
                tutorialDisparoObject.SetActive(true);
                slowmoTutorialFinished = true;
            }
        }
    }

    private IEnumerator SlowmoCoroutine()
    {
        coroutineStarted = true;
        tutorialDisparoObject.SetActive(false);

        yield return new WaitForSeconds(1f);

        GameObject toInstantiate = Instantiate(bullet, enemyShootPos.transform.position, enemyShootPos.transform.rotation);
        toInstantiate.GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
        toInstantiate.layer = 10;
        tutorialBulletShot = true;

        yield return new WaitForSeconds(0.01f);

        Time.timeScale = 0.05f;

        for (int i = 0; i < textObjects.Length; i++)
        {
            textObjects[i].SetActive(true);
        }

        if (!(GameManager.Instance.ActualPlayerController is PlayerControl_MovementController))
        {
            tutorialEnemyToPossess = (EnemyControl_MovementController)GameManager.Instance.ActualPlayerController;
        }

        if (tutorialEnemyToPossess != null && !tutorialEnemyToPossess.ableToDash)
        {
            tutorialEnemyToPossess.ableToDash = true;
        }
    }

    void ShowButtonPrompts()
    {
        if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Keyboard & Mouse")
        {
            promptRenderer.sprite = keyboardPrompt;
        }

        else if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Gamepad")
        {
            if (Gamepad.current == DualShockGamepad.current && Gamepad.current != null)
            {
                promptRenderer.sprite = playStationPrompt;
            }
            else if (Gamepad.current == XInputControllerWindows.current && Gamepad.current != null)
            {
                promptRenderer.sprite = xboxPrompt;
            }
        }
    }
}
