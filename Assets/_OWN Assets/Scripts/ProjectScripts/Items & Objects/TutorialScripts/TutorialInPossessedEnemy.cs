﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.XInput;

public class TutorialInPossessedEnemy : MonoBehaviour
{
    public EnemySetControl enemySetControl;
    [SerializeField] private GameObject textObject;
    [SerializeField] private GameObject promptObject;
    [SerializeField] private SpriteRenderer promptRenderer;
    [Header("Prompts")]
    [SerializeField] Sprite xboxPrompt, playStationPrompt, keyboardPrompt;
    [SerializeField] float texto;
    [SerializeField] float promp;

    private bool doOnlyIfHasBeenPossesed = false;

    // Update is called once per frame
    void Update()
    {
        if(enemySetControl.gameObject.activeSelf && enemySetControl.IsEnemyPossessed())
        {
            doOnlyIfHasBeenPossesed = true;
            ShowButtonPrompts();

            textObject.SetActive(true);
            promptObject.SetActive(true);   //Mover los objetos a la posición del enemigo si está poseído.
            textObject.transform.position = new Vector3(GameManager.Instance.ActualPlayerController.transform.position.x, textObject.transform.position.y, GameManager.Instance.ActualPlayerController.transform.position.z - texto);
            promptObject.transform.position = new Vector3(GameManager.Instance.ActualPlayerController.transform.position.x, promptObject.transform.position.y, GameManager.Instance.ActualPlayerController.transform.position.z - promp);
        }

        else if(doOnlyIfHasBeenPossesed && (!enemySetControl.gameObject.activeSelf || !enemySetControl.IsEnemyPossessed()))
        {
            textObject.SetActive(false);
            promptObject.SetActive(false);

            doOnlyIfHasBeenPossesed = false;
        }
    }

    void ShowButtonPrompts()
    {
        if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Keyboard & Mouse")
        {
            promptRenderer.sprite = keyboardPrompt;
        }

        else if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Gamepad")
        {
            if (Gamepad.current == DualShockGamepad.current && Gamepad.current != null)
            {
                promptRenderer.sprite = playStationPrompt;
            }
            else if (Gamepad.current == XInputControllerWindows.current && Gamepad.current != null)
            {
                promptRenderer.sprite = xboxPrompt;
            }
        }
    }
}
