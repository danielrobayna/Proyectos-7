﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.DualShock;
using UnityEngine.InputSystem.XInput;

public class TutorialObject : MonoBehaviour
{
    [SerializeField] private GameObject[] textObjects;
    [SerializeField] private SpriteRenderer promptRenderer;
    [Header("Prompts")]
    [SerializeField] Sprite xboxPrompt, playStationPrompt, keyboardPrompt;

    private GameObject playerInTrigger;
    private bool doOnce = false;

    CutsceneManager cutManager;
    void Start()
    {
        cutManager = FindObjectOfType<CutsceneManager>();
    }
    private void Update()
    {
        if (cutManager.director.state != UnityEngine.Playables.PlayState.Paused)
        {
            GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            GetComponent<BoxCollider>().enabled = true;
        } 
        if (playerInTrigger != null && !playerInTrigger.activeSelf && doOnce)
        {
            for (int i = 0; i < textObjects.Length; i++)
            {
                textObjects[i].SetActive(false);
            }
            doOnce = false;
        }
     

    }

    private void OnTriggerEnter(Collider other)
    {
        doOnce = true;

        if (other.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
        {
            playerInTrigger = other.gameObject;

            for (int i = 0; i < textObjects.Length; i++)
            {
                textObjects[i].SetActive(true);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {       
        ShowButtonPrompts();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerControl_MovementController>() == GameManager.Instance.ActualPlayerController)
        {
            for (int i = 0; i < textObjects.Length; i++)
            {
                textObjects[i].SetActive(false);
            }
        }
    }

    void ShowButtonPrompts()
    {
        if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Keyboard & Mouse")
        {
            promptRenderer.sprite = keyboardPrompt;
        }

        else if (InputManager.Instance.m_myPlayerInput.currentControlScheme == "Gamepad")
        {
            if (Gamepad.current == DualShockGamepad.current && Gamepad.current != null)
            {
                promptRenderer.sprite = playStationPrompt;
            }
            else if (Gamepad.current == XInputControllerWindows.current && Gamepad.current != null)
            {
                promptRenderer.sprite = xboxPrompt;
            }
        }
    }
}
