﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionSpawn : MonoBehaviour
{
    public GameObject explotion;
    void Start()
    {
        Instantiate(explotion, transform.position, transform.rotation);
    }
}
