﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextObject : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.mainCamera != null)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - GameManager.Instance.mainCamera.transform.position);
        }
    }
}
