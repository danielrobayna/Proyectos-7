﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    [SerializeField] float secondsTillDestroy;
    private void OnEnable()
    {
        StartCoroutine(DestroyAfterSeconds());
    }
    IEnumerator DestroyAfterSeconds()
    {
        yield return new WaitForSeconds(secondsTillDestroy);
        Destroy(this.gameObject);
    }
}
