﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Door : MonoBehaviour
{
    Animator myAnimator;
    RoomManager actualRoom; //Se setea en el awake de room manager
    [SerializeField] bool isManual;
    bool isClosed;

    [Header("Elementos de UI para indicar enemigos, solo si isManual es False")]
    [SerializeField] GameObject enemiesRemainingCanvas;
    [SerializeField] TextMeshProUGUI totalEnemiesCount;
    [SerializeField] TextMeshProUGUI enemiesRemainingCount;

    void Start()
    {
        myAnimator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if(!isManual)
        {
            enemiesRemainingCanvas.SetActive(true);
            if (actualRoom != null)
            {
                totalEnemiesCount.text = actualRoom.currentEnemiesInRoom.Count.ToString();
            }
        }
    }

    private void OnDisable()
    {
        if (!isManual)
        {
            enemiesRemainingCanvas.SetActive(false);
        }
    }

    private void Update()
    {
        if (actualRoom != null && !isManual)
        {
            bool isPlayerControllingAnEnemy = GameManager.Instance.ActualPlayerController.GetComponent<EnemyControl_MovementController>() != null;
            int enemiesInRoom = actualRoom.currentEnemiesInRoom.Count;

            if(enemiesInRoom > 0)
            {
                enemiesRemainingCount.text = enemiesInRoom.ToString();
            }

            if ((enemiesInRoom == 0 || (enemiesInRoom == 1 && isPlayerControllingAnEnemy)))
            {
                if (isClosed)
                {
                    OpenDoor();
                    enemiesRemainingCanvas.SetActive(false);
                }
             
            }
            else
            {
                if (!isClosed)
                {
                    CloseDoor();
                    enemiesRemainingCanvas.SetActive(true);
                }
            }
        }    
    }
    public void OpenDoor()
    {
        isClosed = false;
        myAnimator.SetBool("isClose", false);
        MusicManager.Instance.PlaySound(AppSounds.DOOR_SFX, 5, 50, this.transform);
    }
    public void CloseDoor()
    {
        isClosed = true;
        myAnimator.SetBool("isClose", true);
        MusicManager.Instance.PlaySound(AppSounds.DOOR_SFX, 5, 50, this.transform);
    }
    public void SetActualRoom(RoomManager newRoom)
    {
        actualRoom = newRoom;
    }
}
