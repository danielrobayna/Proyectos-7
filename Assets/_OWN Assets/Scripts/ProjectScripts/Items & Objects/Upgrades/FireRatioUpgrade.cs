﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRatioUpgrade : BaseUpgrade
{
    [Header("Valor a SUMAR al multiplicador actual de ratio de disparo que se resta", order = 0)]
    [Header("(El multiplicador va de 0 a 1, con cada vez menor valor de ratio)", order = 1)]
    [Range(0, 1)]
    public float amountToAdd;

    protected override void UpgradeEffects()
    {
        GameManager.Instance.playerShootRatioMultiplier += amountToAdd;
    }
}
