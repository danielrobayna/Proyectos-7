﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUpgrade : MonoBehaviour
{
    private PlayerControl_MovementController isPlayerControlledCharacterInTrigger;
    [HideInInspector] public bool hasBeenTaken;

    [SerializeField] private GameObject upgradeTakenParticle;

    private void OnTriggerEnter(Collider other)
    {
        isPlayerControlledCharacterInTrigger = other.GetComponent<PlayerControl_MovementController>();

        if(isPlayerControlledCharacterInTrigger != null && isPlayerControlledCharacterInTrigger == GameManager.Instance.ActualPlayerController)
        {
            UpgradeEffects();
            DeactivateUpgradeObject();
        }
    }

    public void DeactivateUpgradeObject()
    {
        hasBeenTaken = true;

        Instantiate(upgradeTakenParticle, transform.position, upgradeTakenParticle.transform.rotation);
        MusicManager.Instance.PlaySound(AppSounds.UPGRADE_PICK_UP);

        ///// LOGRO ///////
        SteamManager.Instance.UnlockAchievement("GET_FIRST_UPGRADE");
        /////

        ///// LOGRO ///////
        SteamManager.Instance.Achievement_GetAllUpgrades();
        /////

        gameObject.SetActive(false);
        GameManager.Instance.SaveGameObjectStatusOnly(); //Guardar partida al cogerlos
    }

    public void DeactivateUpgradeObjectOnLoad()  //Desde LoadGame
    {
        hasBeenTaken = true;
        gameObject.SetActive(false);
    }

    protected virtual void UpgradeEffects()
    {
        //Rellenar en Hijos con funcionalidad ingame.
    }
}
