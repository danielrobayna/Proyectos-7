﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpgrade : BaseUpgrade
{
    [Header("Valor a SUMAR al multiplicador actual de Daño", order = 0)]
    [Header("(El multiplicador va de 1 a 2, con cada vez mayor valor de Daño)", order = 1)]
    [Range(0, 1)]
    public float amountToAdd;

    protected override void UpgradeEffects()
    {
        GameManager.Instance.playerDamageMultiplier += amountToAdd;
    }
}
