﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeContainerUpgrade : BaseUpgrade
{
    protected override void UpgradeEffects()
    {
        HealthHeartsVisual.healthHeartsSystemStatic.AddNewHeartsLogic(1);
    }
}
