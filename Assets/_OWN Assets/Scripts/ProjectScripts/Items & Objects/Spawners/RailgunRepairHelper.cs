﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunRepairHelper : MonoBehaviour
{
    [SerializeField] private GameObject repairParticle;
    [SerializeField] private GameObject railgunEnemy;
    private GameObject storedDummy;
    private Spawner thisSpawner;

    // Start is called before the first frame update
    void Start()
    {
        thisSpawner = GetComponent<Spawner>();   
    }

    public IEnumerator RepairTurrets() //Reparar torretas
    {
        if (storedDummy != null)
        {
            Instantiate(repairParticle, storedDummy.transform.position, repairParticle.transform.rotation);

            yield return new WaitForSeconds(0.5f);

            Destroy(storedDummy);
        }

        yield return new WaitForSeconds(0.1f);

        InstantiateRailguns();
    }

    public void InstantiateRailguns() //Intanciar Torretas Railgun e indicarles que sus dummies se tienen que añadir a la lista del Boss y no a la de la habitación
    {
        thisSpawner.SpawnEnemy(railgunEnemy).TryGetComponent(out EnemySetControl spawnedSetControl);
        spawnedSetControl.addDummyToRepairHelperReference = true;
        spawnedSetControl.repairHelperReference = this;
    }

    public void SetStoredDummy(GameObject dummy)
    {
        storedDummy = dummy;
    }

    public GameObject GetStoredDummy()
    {
        return storedDummy;
    }

    private void OnDisable()
    {
        Destroy(storedDummy);
    }
}
