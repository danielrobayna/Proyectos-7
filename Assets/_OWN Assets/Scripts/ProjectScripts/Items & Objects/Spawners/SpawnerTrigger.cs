﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTrigger : MonoBehaviour
{
    [SerializeField] Spawner mySpawner;

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent(out EnemyControl_MovementController enemy) && other.gameObject.Equals(mySpawner.actualEnemy))
        {
            mySpawner.CloseDoor();
        }
    }
}
