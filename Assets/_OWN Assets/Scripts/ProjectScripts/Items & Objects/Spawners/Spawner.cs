﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    [Header("Variables de Spawneo de enemigos")]
    [SerializeField] float timeBetweenSpawns;
    float originalTimeBetweenSpawns;
    float timer;
    [SerializeField] float randomDeviationBetweenSpawns;
    [SerializeField] GameObject enemyToSpawn;
    [SerializeField] bool ignoreMaxEnemies;
    [SerializeField] float maxEnemiesInstantiated;
    List<GameObject> actualEnemiesInstantiated = new List<GameObject>();
    [SerializeField] float playerDistanceToStartSpawning;
    [SerializeField] Transform enemySpawnPoint;
    [SerializeField] Transform enemyWalkPos;
    Animator m_anime;

    [Header("Enemigo spawneado que se añade a listas")]
    private GameObject m_actualEnemy;
    public GameObject actualEnemy
    {
        get { return m_actualEnemy; }
    }

    [Header("Si tiene un helper para reparar railguns")]
    RailgunRepairHelper repairHelper;

    // Start is called before the first frame update
    void Start()
    {
        repairHelper = GetComponent<RailgunRepairHelper>();

        originalTimeBetweenSpawns = timeBetweenSpawns;
        m_anime = GetComponent<Animator>();

        timer = 0;
    }

    private void OnEnable()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if ((GameManager.Instance.ActualPlayerController.transform.position - this.transform.position).magnitude < playerDistanceToStartSpawning) //Spawneo de enemigos
        {
            if (actualEnemiesInstantiated.Count == 0)
            {
                timer -= Time.deltaTime;
            }
            if (timer <= 0 && (maxEnemiesInstantiated > actualEnemiesInstantiated.Count || ignoreMaxEnemies))
            {
                if (repairHelper != null)
                {
                    timer = timeBetweenSpawns;
                    timeBetweenSpawns = Random.Range(originalTimeBetweenSpawns - randomDeviationBetweenSpawns, originalTimeBetweenSpawns + randomDeviationBetweenSpawns);
                    repairHelper.StartCoroutine(repairHelper.RepairTurrets());
                }
                else
                {
                    timer = timeBetweenSpawns;
                    timeBetweenSpawns = Random.Range(originalTimeBetweenSpawns - randomDeviationBetweenSpawns, originalTimeBetweenSpawns + randomDeviationBetweenSpawns);
                    SpawnEnemy();
                }
            }
        }
    }

    public List<GameObject> SpawnerIntantiatedEnemies()
    {
        return actualEnemiesInstantiated;
    }

    public GameObject SpawnEnemy(GameObject newEnemyToSpawn) //Método llamado de manera externa
    {
        m_actualEnemy = Instantiate(newEnemyToSpawn, enemySpawnPoint.position, this.transform.rotation);
        actualEnemiesInstantiated.Add(m_actualEnemy);
        if (m_anime != null)
        {
            OpenDoor();
        }

        if (m_actualEnemy.GetComponent<EnemyControl_MovementController>() != null)
        {
            ZoneManager.Instance.m_activeRoom.AddEnemyAtRoom(m_actualEnemy.GetComponent<EnemyControl_MovementController>());
            m_actualEnemy.GetComponent<EnemyControl_MovementController>().spawnerInstantiatedFrom = this;
            if (enemyWalkPos != null)
            {
                m_actualEnemy.GetComponent<NavMeshAgent>().SetDestination(enemyWalkPos.position);
                m_actualEnemy.GetComponent<NavMeshAgent>().stoppingDistance = 0.1f;
            }
        }

        return m_actualEnemy;
    }
    private void SpawnEnemy()
    {
        m_actualEnemy = Instantiate(enemyToSpawn, enemySpawnPoint.position, this.transform.rotation);
        actualEnemiesInstantiated.Add(m_actualEnemy);
        if (m_anime != null)
        {
            OpenDoor();
        }

        if (m_actualEnemy.GetComponent<EnemyControl_MovementController>() != null)
        {
            ZoneManager.Instance.m_activeRoom.AddEnemyAtRoom(m_actualEnemy.GetComponent<EnemyControl_MovementController>());
            m_actualEnemy.GetComponent<EnemyControl_MovementController>().spawnerInstantiatedFrom = this;
            if (enemyWalkPos != null)
            {
                m_actualEnemy.GetComponent<NavMeshAgent>().SetDestination(enemyWalkPos.position);
                m_actualEnemy.GetComponent<NavMeshAgent>().stoppingDistance = 0.1f;
            }
        }
    }
    public void RemoveEnemyFromSpawner(GameObject enemy)
    {
        Destroy(enemy);
        actualEnemiesInstantiated.Remove(enemy);
    }
    public void OpenDoor()
    {
        m_anime.SetBool("isOpen", true);
    }
    public void CloseDoor()
    {
        m_anime.SetBool("isOpen", false);

    }
}