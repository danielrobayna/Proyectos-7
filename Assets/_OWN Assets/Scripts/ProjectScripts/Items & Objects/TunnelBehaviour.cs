﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode] //Para el Update
public class TunnelBehaviour : MonoBehaviour
{
    [SerializeField]
    Collider collisionBox;

    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z); //Que no se pueda modificar el transform Y en editor, siempre debería estar en 0
    }

    private void OnCollisionEnter(Collision collision)  //Se ignoran colisiones con bombers y player, que pueden entrar al tunel.
    {
        BomberSetControl bomber = collision.gameObject.GetComponent<BomberSetControl>();
        PossessAbility player = collision.gameObject.GetComponent<PossessAbility>();

        if (bomber != null)
        {
            Physics.IgnoreCollision(bomber.gameObject.GetComponent<Collider>(), collisionBox, true);
        }

        if (player != null)
        {
            Physics.IgnoreCollision(player.gameObject.GetComponent<Collider>(), collisionBox, true);
        }
    }
}
