﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleMoveToPoint : MonoBehaviour
{
    [Header("Delay Para moverse al target")]
    public float delay;
    private float delayTimer;

    private ParticleSystem system;

    private static ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1000];

    int count;

    void Start()
    {
        if (system == null)
            system = GetComponent<ParticleSystem>();
    }
    void Update()
    {
        if (delayTimer < delay)
        {
            delayTimer += 1 * Time.deltaTime;
        }

        if (delayTimer >= delay)
        {
            ParticleSystem.ExternalForcesModule external = system.externalForces;
            external.enabled = true;
        }
    }
}
