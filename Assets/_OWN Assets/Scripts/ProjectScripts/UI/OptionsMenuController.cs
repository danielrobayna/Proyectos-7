﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.IO;
using System.Xml.Serialization;

public class OptionsMenuController : MonoBehaviour
{
    [SerializeField] GameObject optionsPanel;
    [SerializeField] GameObject mainPanel;
    [SerializeField] Slider m_musicSlider;
    [SerializeField] Slider m_sfxSlider;
    [SerializeField] Slider m_sensitivitySlider;
    [SerializeField] TMP_Dropdown m_resolutionDropdawn;
    [SerializeField] Toggle m_fullScreenToggle;
    [SerializeField] Button m_applyButton;
    [SerializeField] Button m_backButton;

    //Resolution settings
    public Resolution[] resolutions;

    float m_previousMusicSetting;
    float m_previousSfxSetting;
    float m_previousSensitivity;
    bool m_previousFullscreenSetting;
    int m_previousResolution;

    GameObject m_previousFirstSelectedObject;

    GameSettings m_gameSettings;
    static string SAVE_FOLDER;

    // Start is called before the first frame update
    void Start()
    {
        SAVE_FOLDER = SaveSystem.SAVE_FOLDER;

        m_musicSlider.value = MusicManager.Instance.MusicVolumeSave;
        m_sfxSlider.value = MusicManager.Instance.SfxVolumeSave;

        resolutions = Screen.resolutions;
      
        for(int i =0; i < resolutions.Length; i++)
        {
            m_resolutionDropdawn.options.Add(new TMP_Dropdown.OptionData(resolutions[i].ToString())); //Añadir al dropdown las resoluciones

            //Marca como activa en el dropdown la resolución actual
            //Lo separo debido a que en build el refresh rate de current se pone a 59, 75, etc (una menos de lo que debería, luego no lo pilla bien)
            bool sameWidth = resolutions[i].width == Screen.currentResolution.width;
            bool sameHeight = resolutions[i].height == Screen.currentResolution.height;
            bool nearTheSameRefreshRate = resolutions[i].refreshRate - Screen.currentResolution.refreshRate <=1;

            if (sameWidth && sameHeight && nearTheSameRefreshRate)
            {
                m_resolutionDropdawn.value = i;
            }
        }

        LoadSettings();

        m_applyButton.interactable = false; //Debe estar después de setear los valores de los sliders
        SaveInitialSettings();
    }

    public void OnButtonOptions()
    {
        optionsPanel.SetActive(true);
        mainPanel.SetActive(false);

        m_previousFirstSelectedObject = EventSystem.current.currentSelectedGameObject;
        SelectedObjectHandler.Instance.DetectIfSelectObject(m_musicSlider.gameObject);

        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
    }
    public void OnButtonBack()
    {
        DeactivateOptionsPanel();
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
    }
    public void DeactivateOptionsPanel()
    {
        optionsPanel.SetActive(false);
        mainPanel.SetActive(true);

        ResetOptionsVariables(); //Cuidado que llama al OnValueChanged de los slider
        m_applyButton.interactable = false; //Importante hacerlo después de resetear, ya que se llama a onValueChanged al cambiar el valor del slider

        SelectedObjectHandler.Instance.DetectIfSelectObject(m_previousFirstSelectedObject);
    }

    public void OnButtonExit()
    {
        //SteamManager.Instance.ExitSteamClient(); //Hacer que SteamManager apague el cliente de Steam

        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void OnExitToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0, LoadSceneMode.Single);
        if (InputManager.Instance != null)
        {
            InputManager.Instance.OnChangeInputDeactivateEvent();
        }
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
        MusicManager.Instance.SetLowPassFilter(false);
    }

    public void OnApplyButton()
    {
        SaveSettings();

        m_applyButton.interactable = false;
        EventSystem.current.SetSelectedGameObject(m_backButton.gameObject);
        SaveInitialSettings();
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
    }

    public void OnMusicValueChanged() //Cuidado que se llama en el OnButtonback al llamar a resetvariables
    {
        MusicManager.Instance.MusicVolume = m_musicSlider.value;
        m_applyButton.interactable = true;
    }
    public void OnSfxValueChanged() //Cuidado que se llama en el OnButtonback al llamar a resetvariables
    {
        MusicManager.Instance.SfxVolume = m_sfxSlider.value;
        m_applyButton.interactable = true;
    }
    public void OnResolutionChanged()
    {
        Screen.SetResolution(resolutions[m_resolutionDropdawn.value].width, resolutions[m_resolutionDropdawn.value].height, Screen.fullScreen);
        m_applyButton.interactable = true;
    }
    public void OnMouseSensitivityChanged()
    {
        if (InputManager.Instance != null)
        {
            InputManager.Instance.MouseVelocity = m_sensitivitySlider.value;

        }
        m_applyButton.interactable = true;
    }
    public void OnFullScreenToggle()
    {
        Screen.fullScreen = m_fullScreenToggle.isOn;
        m_applyButton.interactable = true;
    }
    void SaveInitialSettings() //Permite guardar los ajustes en caso de cambiarlos pero no darle a apply
    {
        m_previousMusicSetting = m_musicSlider.value;
        m_previousSfxSetting = m_sfxSlider.value;
        m_previousFullscreenSetting = m_fullScreenToggle.isOn;
        m_previousResolution = m_resolutionDropdawn.value;
        m_previousSensitivity = m_sensitivitySlider.value;
    }
    void ResetOptionsVariables() //Resetea los valores en caso de salir sin dar a apply
    {
        m_musicSlider.value = m_previousMusicSetting;
        m_sfxSlider.value = m_previousSfxSetting;
        m_fullScreenToggle.isOn = m_previousFullscreenSetting;
        m_resolutionDropdawn.value = m_previousResolution;
        m_sensitivitySlider.value = m_previousSensitivity;
    }

    //Save Settings
    void SaveSettings()
    {
        MusicManager.Instance.MusicVolumeSave = m_musicSlider.value;
        MusicManager.Instance.SfxVolumeSave = m_sfxSlider.value;
        m_gameSettings.fullscreenSetting = m_fullScreenToggle.isOn;
        m_gameSettings.resolution = m_resolutionDropdawn.value;
        m_gameSettings.mouseSensitivity = m_sensitivitySlider.value;

        SaveSystem.Serialize(m_gameSettings, "saveSettings");
    }
    void LoadSettings()
    {
        m_gameSettings = Deserialize<GameSettings>();
        if (m_gameSettings != null)
        {
            m_fullScreenToggle.isOn = m_gameSettings.fullscreenSetting;
            m_resolutionDropdawn.value = m_gameSettings.resolution;
            m_sensitivitySlider.value = m_gameSettings.mouseSensitivity;
        }
        else
        {
            m_gameSettings = new GameSettings();
        }
        m_resolutionDropdawn.RefreshShownValue();
    }

    public static T Deserialize<T>() where T : GameSettings
    {
        if (File.Exists(SAVE_FOLDER + "saveSettings.xml"))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StreamReader reader = new StreamReader(SAVE_FOLDER + "saveSettings.xml");
            T deserialized = (T)serializer.Deserialize(reader.BaseStream);
            reader.Close();
            return deserialized;
        }
        else
            return null;
    }

    public class GameSettings
    {
        public bool fullscreenSetting;
        public int resolution;
        public float mouseSensitivity;
    }
}
