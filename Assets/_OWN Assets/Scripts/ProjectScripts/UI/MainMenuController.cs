﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using UnityEngine.EventSystems;

public class MainMenuController : TemporalSingleton<MainMenuController>
{
    [SerializeField] GameObject continueButton;
    [SerializeField] LoadSceneDuringCutscene loadSceneOnMenu;
    LoadSceneOnContinue loadSceneOnContinue;
    [SerializeField] PlayerInput m_myPlayerInput;
    bool m_isControllingWithController;
    public bool IsControllingWithController
    {
        get
        {
            return m_isControllingWithController;
        }
    }
    public override void Awake()
    {
        base.Awake();
        SaveSystem.Init();
        //SteamManager.Instance.Init(); //Hacer que SteamManager se intancie en escena y se inicie.
    }
    void Start()
    {
        loadSceneOnContinue = GetComponent<LoadSceneOnContinue>();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;

        SaveObject loadedObject = SaveSystem.Deserialize<SaveObject>(); ////LLamada de carga al SaveSystem

        if (loadedObject != null) ////Si hay datos
        {
            continueButton.SetActive(true);
        }

        //Creación del evento para permitir activar y desactivar el cursor y setear cuál el último objeto seleccionado al comenzar a controlar el mando
        InputUser.onChange += ControlSchemeChange;
        ChangeInputScheme(m_myPlayerInput.currentControlScheme);

        SelectedObjectHandler.Instance.Init(); //Llamada al init, una función vacía pero que activa el singleton
    }

    //Detección de cambio de control de mando a rotón y viceversa
    public void OnChangeInputDeactivateEvent()
    {
        InputUser.onChange -= ControlSchemeChange;
    }
    void ControlSchemeChange(InputUser user, InputUserChange change, InputDevice device)
    {
        if (change == InputUserChange.ControlSchemeChanged)
        {
            ChangeInputScheme(user.controlScheme.Value.name);
        }
    }
    void ChangeInputScheme(string schemeName)
    {
        if (schemeName.Equals("Keyboard & Mouse"))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.lockState = CursorLockMode.Confined;
            EventSystem.current.SetSelectedGameObject(null);
            m_isControllingWithController = false;
        }
        else
        {
            SelectedObjectHandler.Instance.SetSelectedGameObjectOnControllerReconect();
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            m_isControllingWithController = true;
        }
    }

    //Funciones de botones
    public void OnNewGameResetSave()
    {
        SaveSystem.EraseSaveFile();
        Debug.Log("Savings Erased!");
        loadSceneOnMenu.LoadScenes();
        OnChangeInputDeactivateEvent();
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
        SteamManager.Instance.ResetUpgradesCountStat(); //Reset de stat de logro de upgrades.
    }
    public void OnContinueLoadScenes()
    { 
        loadSceneOnContinue.LoadScenes();
        OnChangeInputDeactivateEvent();
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
    }
    public void OnCreditsButton()
    {
        OnChangeInputDeactivateEvent();
        MusicManager.Instance.PlaySound(AppSounds.BUTTON);
    }
}
