﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MoveScrollDropdown : ScrollRect, IMoveHandler, IPointerClickHandler
{
    private const float speedMultiplier = 0.02f;
    public float xInput = 0;
    public float yInput = 0;

    float xSpeed = 0;
    float ySpeed = 0;

    float maxSpeed = 0.1f;
    private float hPos, vPos;

    void IMoveHandler.OnMove(AxisEventData e)
    {
        if (e.moveVector.x > 0)
        {
            xInput = 1;
        }
        else if (e.moveVector.x < 0)
        {
            xInput = -1;
        }

        if (e.moveVector.y > 0)
        {
            yInput = 1;
        }
        else if (e.moveVector.y < 0)
        {
            yInput = -1;
        }
    }

    void Update()
    {
        if (m_myInputAsset.PlayerInputActions.VerticalMovement.ReadValue<float>() != 0 || m_myInputAsset.PlayerInputActions.VerticalUIControllerDropdownMovement.ReadValue<float>() != 0)
        {
            SnapToChildSelectedElement();
        }

        xSpeed += xInput * speedMultiplier;
        ySpeed += yInput * speedMultiplier;

        //Fricción
        xSpeed = Mathf.Lerp(xSpeed, 0, 0.2f);
        ySpeed = Mathf.Lerp(ySpeed, 0, 0.2f);

        xSpeed = Mathf.Clamp(xSpeed, -maxSpeed, maxSpeed);
        ySpeed = Mathf.Clamp(ySpeed, -maxSpeed, maxSpeed);

        hPos = horizontalNormalizedPosition + xSpeed;
        vPos = verticalNormalizedPosition + ySpeed;

        if (movementType == MovementType.Clamped)
        {
            hPos = Mathf.Clamp01(hPos);
            vPos = Mathf.Clamp01(vPos);
        }

        //Aplicar velocidad final
        normalizedPosition = new Vector2(hPos, vPos);

        xInput = 0;
        yInput = 0;
    }

    public void OnPointerClick(PointerEventData e)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        base.OnBeginDrag(eventData);
    }







    PlayerInputAsset m_myInputAsset;

    protected override void Start()
    {
        base.Start();
        SnapToChildSelectedElement();
        m_myInputAsset = new PlayerInputAsset();
        m_myInputAsset.Enable();
    }


    public void SnapToChildSelectedElement()
    {
        RectTransform child = EventSystem.current.currentSelectedGameObject.GetComponent<RectTransform>();
        Canvas.ForceUpdateCanvases();
        var contentPos = (Vector2)this.transform.InverseTransformPoint(this.content.position);
        var childPos = (Vector2)this.transform.InverseTransformPoint(child.position + new Vector3(0, child.rect.height / 2, 0));
        var endPos = contentPos - childPos;

        // If no horizontal scroll, then don't change contentPos.x
        if (!this.horizontal)
        {
            endPos.x = contentPos.x;
        }
        // If no vertical scroll, then don't change contentPos.y
        if (!this.vertical)
        {
            endPos.y = contentPos.y;
        }
        this.content.anchoredPosition = endPos;
    }
}
