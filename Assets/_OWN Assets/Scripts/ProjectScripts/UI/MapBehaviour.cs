﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MapBehaviour : MonoBehaviour
{
    public List<Image> mapRooms;
    [HideInInspector] public List<int> activatedRooms = new List<int>();
    public List<Image> blockades;
    public Color highlightedColor;

    public Image activeRoomImage;

    public void DiscoverRoom(int currentRoom)
    {
        if (!activatedRooms.Contains(currentRoom))
        {
            activatedRooms.Add(currentRoom);
            mapRooms[currentRoom].gameObject.SetActive(true);
            GameManager.Instance.SaveGameObjectStatusOnly();
        }
    }

    public void SetActiveRoomInMap(int currentRoom)
    {
        if (activeRoomImage != null)
        {
            activeRoomImage.color = Color.white;
        }
        activeRoomImage = mapRooms[currentRoom];
        activeRoomImage.color = highlightedColor;
    }

    //SaveSystem Functions
    public void SetActiveRoomsOnLoad(int[] loadActivatedRooms)
    {
        activatedRooms.Clear();
        for(int i =0; i < loadActivatedRooms.Length; i++)
        {
            activatedRooms.Add(loadActivatedRooms[i]);
            mapRooms[loadActivatedRooms[i]].gameObject.SetActive(true);
        }
    }
    public int[] GetActiveRoomsOnSave()
    {
        return activatedRooms.ToArray();
    }

    //Blockade
    public void DeleteBlockadeInMap(int currentBlockade)
    {
        blockades[currentBlockade].gameObject.SetActive(false);
    }
}
