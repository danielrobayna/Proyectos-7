﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectedObjectHandler : PersistentSingleton<SelectedObjectHandler>
{
    GameObject m_previousSelectedGameObject;

    private void Update()
    {
        if (EventSystem.current != null)
        {
            var currentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
            if (currentSelectedGameObject != null && currentSelectedGameObject != m_previousSelectedGameObject)
            {
                ChangePreviousSelectedObject(currentSelectedGameObject);
            }
        }
    }
    public void ChangePreviousSelectedObject(GameObject newSelectedObject)
    {
        m_previousSelectedGameObject = newSelectedObject;
    }
    public void SetSelectedGameObjectOnControllerReconect()
    {
        if (m_previousSelectedGameObject != null)
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(m_previousSelectedGameObject);
        }
    }
    //Función que detecta qué tipo de control se usa y selecciona en el eventsystem dependiendo de si es necesario o no
    public void DetectIfSelectObject(GameObject objectToSelect)
    {
        ChangePreviousSelectedObject(objectToSelect);

        //Detectar playerinput actual
        if (InputManager.Instance != null)
        {
            if (InputManager.Instance.IsControllingWithController)
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(objectToSelect);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
            }

        }
        else if (MainMenuController.Instance != null)
        {
            if (MainMenuController.Instance.IsControllingWithController)
            {
                EventSystem.current.SetSelectedGameObject(null);
                EventSystem.current.SetSelectedGameObject(objectToSelect);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }
    }
    public void Init() //Para iniciar el singleton
    {
        
    }
}
