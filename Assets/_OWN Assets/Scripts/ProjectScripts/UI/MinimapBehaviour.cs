﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MinimapBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject m_playerArrow;
    Transform m_playerT;

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.ActualPlayerController != null)
        {
            m_playerT = GameManager.Instance.ActualPlayerController.gameObject.transform;
            m_playerArrow.transform.position = new Vector3(m_playerT.position.x, 2, m_playerT.position.z);
            m_playerArrow.transform.rotation = m_playerT.GetComponent<PlayerControl_MovementController>().armObject.transform.rotation;
        }
    }
}