﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Gravit_SlopesController : MonoBehaviour
{
    [SerializeField] private LayerMask whatIsGround;
    private Rigidbody rb;
    private NavMeshAgent this_ObjectAIAgent;

    private Vector3 fixedRaycastPos;
    [Header("Posición del origen del raycast en Y")]
    [SerializeField] private float fixedRaycastYOrigin;
    private bool grounded;
    [Header("Ditancia del Raycast")]
    public float raycastDistance;
    [Header("Velocidad de caída")]
    public float fallSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        this_ObjectAIAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckGround();
        ApplyGravity();
    }

    private void CheckGround()  //Se tira un rayo en la zona inferior del objeto para mirar si hay suelo
    {
        fixedRaycastPos = new Vector3(transform.position.x, transform.position.y + fixedRaycastYOrigin, transform.position.z); //La fixed position está porque es bueno que el raycaste sté un poco subido dentro del collider...
                                                                                                                               //...con solo un poco de rayo saliendo por debajo para una mayor precisión.
        grounded = Physics.Raycast(fixedRaycastPos, -transform.up, out RaycastHit hitInfo, raycastDistance, whatIsGround, QueryTriggerInteraction.Ignore);
        //Debug.DrawRay(fixedRaycastPos, - transform.up * raycastDistance, Color.yellow);
    }

    private void ApplyGravity()
    {
        if (this_ObjectAIAgent == null) //Ejecutar directamente la gravedad en objetos sin Agente de IA, como el player.
        {
            if (!grounded)
            {
                rb.constraints &= ~RigidbodyConstraints.FreezePositionY;    //Se habilita movimiento en y para caer
                rb.velocity = new Vector3(rb.velocity.x, -fallSpeed, rb.velocity.z);
            }
            else
            {
                rb.constraints |= RigidbodyConstraints.FreezePositionY; //Se bloquea el movimiento en y si hay suelo
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            }
        }

        else
        {
            if (!this_ObjectAIAgent.enabled)    //Si tiene IA, como los enemigos, se ejecuta gravedad si no está activa la IA.
            {
                if (!grounded)
                {
                    rb.constraints &= ~RigidbodyConstraints.FreezePositionY;
                    rb.velocity = new Vector3(rb.velocity.x, -fallSpeed, rb.velocity.z);
                }
                else
                {
                    rb.constraints |= RigidbodyConstraints.FreezePositionY;
                    rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                }
            }
        }
    }
}
