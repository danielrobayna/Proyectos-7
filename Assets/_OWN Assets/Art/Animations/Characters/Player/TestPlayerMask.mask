%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: TestPlayerMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Polygonal Alien Telekinis
    m_Weight: 1
  - m_Path: Rig
    m_Weight: 1
  - m_Path: RigCrownGizmo
    m_Weight: 0
  - m_Path: RigLArm1Gizmo
    m_Weight: 1
  - m_Path: RigLArm2Gizmo
    m_Weight: 1
  - m_Path: RigLArmPalmGizmo
    m_Weight: 1
  - m_Path: RigMain
    m_Weight: 0
  - m_Path: RigMain/RigCrown
    m_Weight: 0
  - m_Path: RigMain/RigCrown/Crown
    m_Weight: 1
  - m_Path: RigMain/RigLArm1
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmIndex1
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmIndex1/RigLArmIndex2
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmIndex1/RigLArmIndex2/RigLArmIndex3
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmPinky1
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmPinky1/RigLArmPinky2
    m_Weight: 1
  - m_Path: RigMain/RigLArm1/RigLArm2/RigLArmPalm/RigLArmPinky1/RigLArmPinky2/RigLArmPinky3
    m_Weight: 1
  - m_Path: RigMain/RigRArm1
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmIndex1
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmIndex1/RigRArmIndex2
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmIndex1/RigRArmIndex2/RigRArmIndex3
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmPinky1
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmPinky1/RigRArmPinky2
    m_Weight: 1
  - m_Path: RigMain/RigRArm1/RigRArm2/RigRArmPalm/RigRArmPinky1/RigRArmPinky2/RigRArmPinky3
    m_Weight: 1
  - m_Path: RigMain/RigTailL1
    m_Weight: 0
  - m_Path: RigMain/RigTailL1/RigTailL2
    m_Weight: 0
  - m_Path: RigMain/RigTailL1/RigTailL2/RigTailL3
    m_Weight: 0
  - m_Path: RigMain/RigTailL1/RigTailL2/RigTailL3/RigTailL4
    m_Weight: 0
  - m_Path: RigMain/RigTailM1
    m_Weight: 0
  - m_Path: RigMain/RigTailM1/RigTailM2
    m_Weight: 0
  - m_Path: RigMain/RigTailM1/RigTailM2/RigTailM3
    m_Weight: 0
  - m_Path: RigMain/RigTailM1/RigTailM2/RigTailM3/RigTailM4
    m_Weight: 0
  - m_Path: RigMain/RigTaiR1
    m_Weight: 0
  - m_Path: RigMain/RigTaiR1/RigTaiR2
    m_Weight: 0
  - m_Path: RigMain/RigTaiR1/RigTaiR2/RigTaiR3
    m_Weight: 0
  - m_Path: RigMain/RigTaiR1/RigTaiR2/RigTaiR3/RigTaiR4
    m_Weight: 0
  - m_Path: RigMainGizmo
    m_Weight: 0
  - m_Path: RigRArm1Gizmo
    m_Weight: 1
  - m_Path: RigRArm2Gizmo
    m_Weight: 1
  - m_Path: RigRArmPalmGizmo
    m_Weight: 1
  - m_Path: RigTailL1Gizmo
    m_Weight: 0
  - m_Path: RigTailL2Gizmo
    m_Weight: 0
  - m_Path: RigTailL3Gizmo
    m_Weight: 0
  - m_Path: RigTailM1Gizmo
    m_Weight: 0
  - m_Path: RigTailM2Gizmo
    m_Weight: 0
  - m_Path: RigTailM3Gizmo
    m_Weight: 0
  - m_Path: RigTailM4Gizmo
    m_Weight: 0
  - m_Path: RigTaiR1Gizmo
    m_Weight: 0
  - m_Path: RigTaiR2Gizmo
    m_Weight: 0
  - m_Path: RigTaiR3Gizmo
    m_Weight: 0
  - m_Path: RigTaiR4Gizmo
    m_Weight: 0
